---
title: Welcome
subtitle: MATH 202 Linear Algebra
...

Welcome to MATH 202 Linear Algebra!

Please see the `syllabus.pdf` under Resource in Sakai for everything about the course.

Questions about the course or mathematics should be posted on [Ed-Discussion](https://edstem.org). 
Emails will not get replied, unless it is about private matter.
