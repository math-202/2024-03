---
title: Presentations Topics
subtitle: MATH 202 Linear Algebra
colorlinks: true
urlcolor: Maroon
geometry: margin=1in
...

Here's collection of possible presentation topics ---

* Linear Programming and Its Applications
* Applications of Linear Algebra in Cryptography
* [The GraphBLAS](https://graphblas.org/)
* [Linear Network Coding](https://en.wikipedia.org/wiki/Linear_network_coding)
* [SVD and Its Applications](https://en.wikipedia.org/wiki/Singular_value_decomposition)
* [In Mysterious Pattern, Math and Nature Converge](https://www.quantamagazine.org/in-mysterious-pattern-math-and-nature-converge-20130205/)
* [AI Reveals New Possibilities in Matrix Multiplication](https://www.quantamagazine.org/ai-reveals-new-possibilities-in-matrix-multiplication-20221123/)
* [New Algorithm Breaks Speed Limit for Solving Linear Equations](https://www.quantamagazine.org/new-algorithm-breaks-speed-limit-for-solving-linear-equations-20210308/)
* [Matrix Multiplication Inches Closer to Mythic Goal](https://www.quantamagazine.org/mathematicians-inch-closer-to-matrix-multiplication-goal-20210323/)
* [Symmetric Polynomails](https://en.wikipedia.org/wiki/Symmetric_polynomial)
* [Applications of Linear Algebra in Computer Graphics](https://www.jstor.org/stable/10.4169/j.ctt19b9kd8?turn_away=true)
* [Applications of Linear Algebra in Bracketology](https://www.jstor.org/stable/10.4169/j.ctt19b9kd8?turn_away=true)
* [D-Optimal Designs](https://www.itl.nist.gov/div898/handbook/pri/section5/pri521.htm)
* [Symmetric Polynomails](https://en.wikipedia.org/wiki/Symmetric_polynomial)
* Fourier Transform: Linear Algebra in Signal and Image Analysis
* Principal Component Analysis: Dimensionality Reduction and Beyond
* Markov Chains: Linear Algebra in Stochastic Processes
* Google's PageRank Algorithm: Linear Algebra Behind the Scenes
* Linear Algebra in Machine Learning: Support Vector Machines
* QR Factorization: Technique and Applications
* Control Theory: The Role of Linear Algebra
* Quantum Mechanics: Linear Algebra in the Quantum Realm
* Compressed Sensing: Linear Algebra in Signal Processing
* Social Network Analysis: Linear Algebra at the Core
