# MATH202 -- Linear Algebra (March-May 2024)

This repository contains materials for the course MATH202 (Linear Algebra)
taught at [Duke Kunshan University](https://dukekunshan.edu.cn/) 
by [me (Xing Shi Cai)](https://newptcai.gitlab.io)
starting from March 2024.

See the [`pdf`](./pdf) folder for the syllabus, slides, quizzes etc.

You are free to use any material here.
