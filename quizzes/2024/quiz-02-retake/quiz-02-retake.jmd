---
title: MATH 202 Linear Algebra
subtitle: Quiz 02 Retake
author: "Instructor: Xing Shi Cai"
date: 2024-04-17
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
#using Plots
using Random
using SymPy
using RowEchelon
using WeaveHelper

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your name and \textbf{SIS} on Canvas in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Name: $`j print_empty_box(5.7, 1.5)`$
    \hfil
    SIS: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

\pagebreak{}

# Reference {-}

:bomb: 
When you use the Invertible Matrix Theorem in a proof,
be clear which of items (a.\ to l.) you are using.

\begin{tcolorbox}[title={The Invertible Matrix Theorem}]
    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
    \item[a.] $A$ is an invertible matrix.
    \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
    \item[c.] $A$ has $n$ pivot positions.
    \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
    \item[e.] The columns of $A$ form a linearly independent set.
    \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
    \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
    \item[h.] The columns of $A$ spans $\dsR^n$.
    \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
    \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
    \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
    \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
\end{tcolorbox}

# Linear Transformations (1pt)

:cross_mark_button: the box in front of each true statement.

:bomb: Three or more incorrect choices will lead to a score of 0.

* `j print_checked()` The transformation $T(x_1, x_2) = (2 x_1 - 3 x_2,  4 x_1, 5 x_2)$ is a linear transformation.
* `j print_checked()` Every matrix transformation is a linear transformation.
* `j print_checked()` If $A$ is a $3 \times 5$ matrix and $T$ is a transformation defined by $T(\bfx) = A \bfx$, then the domain of $T$ is $\mathbb{R}^5$.
* `j print_checked()` The range of the transformation $\bfx \mapsto A \bfx$ is the set of all linear combinations of the columns of $A$.
* `j print_checked()` If $A$ is an $n \times n$ invertible matrix, then the range of the transformation $\bfx \mapsto A \bfx$ is $\mathbb{R}^n$.

# Invertible Matrices (1pt)

Not included.

# Invertible Matrices (1pt)


Let 
\begin{equation*} 
    A = \begin{bmatrix}
        1 & 0 & 0 & \cdots & 0 \\
        1 & 2 & 0 & \cdots & 0 \\
        1 & 2 & 3 & \cdots & 0 \\
        \vdots & \vdots & \vdots & \ddots & \vdots \\
        1 & 2 & 3 & \cdots & n
    \end{bmatrix}
    ,
    \qquad
    B 
    =
    \begin{bmatrix}
    1 & 0 & 0 & \cdots & 0 \\
    -\frac{1}{2} & \frac{1}{2} & 0 & & 0 \\
    0 & -\frac{1}{3} & \frac{1}{3} & & 0 \\
    \vdots & & \ddots & \ddots & \vdots \\
    0 & 0 & \cdots & -\frac{1}{n} & \frac{1}{n}
    \end{bmatrix}
\end{equation*}

For $j = 1, \ldots, n$, let $\bfa_j, \bfb_j,$ and $\bfe_j$ denote the $j$th columns of $A, B,$ and
$I$, respectively. 

## Step 1

Show that $A \bfe_i = \bfa_i$ for all $i = 1, \ldots, n$.

```julia

answer = L"""
We have
\begin{equation*}
    A \bfe_i = 0 \bfa_1 + 0 \bfa_2 + \cdots + 0 \bfa_{i-1} + 1 \bfa_i + 0 \bfa_{i+1} + \cdots + 0 \bfa_n
    =
    \bfa_i
\end{equation*}
"""

long_answer(answer)

```

`j quiz_pagebreak()`


## Step 2

Prove that $A$ is an invertible matrix by first proving $A B = I_n$ and then applying the Invertible Matrix Theorem.


:bulb: 
Note that $\bfa_j = j\left(\frac{1}{j+1}\bfa_{j+1} + \bfe_j\right)$ 
and $\bfb_j = \frac{\bfe_j}{j} - \frac{\bfe_{j+1}}{j+1}$ for
$j = 1, \ldots, n-1$, $\bfa_n = n \bfe_n$, and $\bfb_n = \frac{1}{n} \bfe_n$.

```julia

answer = L"""
We have
\begin{equation*}
    \begin{aligned}
        A 
        B
        &
        =
        \begin{bmatrix} A \bfb_1 & A \bfb_2 & \cdots & A \bfb_n \end{bmatrix} 
        \\
        &
        =
        \begin{bmatrix} A (\bfe_1-\frac{\bfe_2}{2}) & A (\frac{\bfe_2}{2-} \frac{\bfe_3}{3} ) & \cdots & A (\frac{\bfe_{n-1}}{n-1}-\frac{\bfe_n}{n} ) & A \frac{\bfe_n}{n} \end{bmatrix}
        \\
        &
        =
        \begin{bmatrix} \bfa_1 - \frac{1}{2} \bfa_2 & \frac{\bfa_2}{2} - \frac{1}{3} \bfa_3 & \cdots & \frac{\bfa_{n-1}}{n-1} - \frac{1}{n} \bfa_n & \frac{1}{n} \bfa_n \end{bmatrix}
        \\
        &
        =
        \begin{bmatrix} \bfe_1 & \bfe_2 & \cdots & \bfe_n \end{bmatrix} = I_n
    \end{aligned}
\end{equation*}
By items a and k of the Invertible Matrix Theorem, $A$ is invertible.
"""

long_answer(answer, height=10)

```

`j quiz_pagebreak()`

# Partitioned Matrices (1pt)

Let $A = \begin{bmatrix} B & 0 \\ 0 & C \end{bmatrix}$,
where $B$ and $C$ are square.

## 

Show that if $A$ is invertible, then $B$ and $C$ are invertible.

```julia

answer = L"""
Let $A^{-1} = \begin{bmatrix} D & E \\ F & G \end{bmatrix}$
where $D$ and $G$ are squares of the same size as $B$ and $C$ respectively.

Then
\begin{equation*}
AA^{-1} = 
\begin{bmatrix} B & 0 \\ 0 & C \end{bmatrix}
\begin{bmatrix} D & E \\ F & G \end{bmatrix} 
= \begin{bmatrix} BD & BE \\ CF & CG \end{bmatrix}
= I
\end{equation*}

For $AA^{-1}$ to be the identity matrix $I$, we need $BD$ and $CG$ both to be identity matrices,
which directly implies that $B$ and $C$ must be invertible
by the items a and k of the Invertible Matrix Theorem.
"""

long_answer(answer, vfill=true)
```

`j quiz_pagebreak()`

## 

Show that if $B$ and $C$ are invertible, then $A$ is invertible.

:bulb: To show $A$ is invertible, guess what $A^{-1}$ should be and check if it works.

```julia

answer = L"""
If $B$ and $C$ are invertible, let's guess $A^{-1}$ to be of the form $\begin{bmatrix} B^{-1} & 0 \\ 0 & C^{-1} \end{bmatrix}$.

We verify this by computing:

\begin{equation*}
\begin{bmatrix} B & 0 \\ 0 & C \end{bmatrix}
\begin{bmatrix} B^{-1} & 0 \\ 0 & C^{-1} \end{bmatrix}
= \begin{bmatrix} BB^{-1} & 0 \\ 0 & CC^{-1} \end{bmatrix}
= \begin{bmatrix} I & 0 \\ 0 & I \end{bmatrix}
= I,
\end{equation*}

So by a and item k of the Invertible Matrix Theorem, $A$ is invertible.
"""

long_answer(answer, vfill=true)
```
