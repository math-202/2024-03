---
title: MATH 202 Linear Algebra
subtitle: Final Exam
author: "Instructor: Xing Shi Cai"
date: 2024-05-06
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using Random
using SymPy
using RowEchelon
using WeaveHelper
using LinearAlgebra
set_default(env = :raw)

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your name and \textbf{SIS} on Canvas in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Name: $`j print_empty_box(5.7, 1.5)`$
    \hfil
    SIS: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

\pagebreak{}

# True or False (1pt)

:cross_mark_button: Mark the box in front of each true statement.

:bomb: Three or more incorrect choices will result in a score of 0.

* [x] Elementary row operations on an augmented matrix never change the solution set of
      the associated linear system.
* [ ] Two matrices are row equivalent if they have the same number of rows.
* [ ] The solution set of a linear system involving variables $x_1, \ldots, x_n$ is a list
  of numbers $(s_1, \ldots, s_n)$ that makes each equation in the system a true statement
  when the values $s_1, \ldots, s_n$ are substituted for $x_1, \ldots, x_n$, respectively.
* [ ] A consistent linear system has a unique solution.
* [x] Two linear systems are equivalent if they have the same solution set.

```julia; results="tex"

answer = L"""
\emoji{bomb} Correction:

Note that the 3rd one is false, because we are talking about a solution set, not a solution.
A solution set can be empyt, containing one solution, or containing infinitely many solutions.

Initially this one was judged as true when the final is exam is graded.
Thus you will not get points deduction regardless of your choice of this item.
And these who got it correctly get a +0.3 bouns points.
"""

print_in_solution(answer)
```

# Linear Independence (1pt)

##

```julia; results="hidden"

Random.seed!(32562)
while true
    global A = rand(-7:10, (3, 3))
    if rank(A) == 2
        break
    end
end
A_sym = Sym.(A)
@syms h
A_sym[3,3] = h
A_sym
c = A_sym[1:2, 1:2] \ A_sym[1:2, 3]
@assert dot(A_sym[3, 1:2], c) == A[3,3]

```

Find the value of $h$ which makes the columns of the following matrix linearly *dependent*.

\begin{equation*}
    `j print(latexify(A_sym))`
\end{equation*}

`j short_answer(L"h", A[3,3])`

##

```julia; results="hidden"

Random.seed!(7076)
while true
    global A = rand(-5:3, (3, 3))
    if det(A) == 0
        break
    end
end
A_sym = Sym.(A)
A_rref = rationalize.(rref(A))
A_str = latexify.(eachcol(A_sym))

```

Are the columns of the following matrix $A$ linearly *independent*?
\begin{equation*}
    A = `j print(latexify(A_sym))`
\end{equation*}
Justify your answer.

```julia; results="tex"

answer=L"""
The columns of the matrix are linearly dependent.

There are many way to see this.
The easist way is to note that the second column is the zero vector.
A set containing the zero vector is always linearly dependent.

Or you can give a linear combination of the columns of $A$ such as:
\begin{equation*}
    0 %$(A_str[1])
    +
    1 %$(A_str[2])
    +
    0 %$(A_str[3])
    =
    \begin{bmatrix} 0 \\ 0 \\ 0 \end{bmatrix} 
\end{equation*}

Another way is to show that
\begin{equation*}
    A \sim %$(latexify(A_rref))
\end{equation*}
Thus the linear system $A \bfx = \bfzero$ has a free variable and it has infinitly many solutions.
By the Invertible Matrix Theorem, the columns of $A$ are linearly dependent.

You may also note that $\det(A) = 0$. Thus it is not inverbile.
By the Invertible Matrix Theorem, the columns of $A$ are linearly dependent.
"""

long_answer(answer, height=5)
```

# Matrix Operations (1pt)

Let $C$ and $A$ be two matrices.
Suppose $C A = I_n$, where $I_n$ is the $n \times n$ identity matrix. 

##

Show that the equation $A \bfx = 0$ has only the trivial solution *without* using the Invertible Matrix Theorem.

```julia; results="tex"

C = hcat(I(2), zeros(Int, 2, 1))
A = vcat(I(2), zeros(Int, 1, 2))
@assert C*A == I(2)

answer=L"""
Assume that $A \bfx = \bfzero$.
Multiply both sides of $C A = I_n$ by $\bfx$, we have
$$
CA\mathbf{x} = I_n \bfx
\quad \iff \quad
C\mathbf{0} = \bfx 
\quad \iff \quad
\mathbf{0} = \bfx
$$
Thus, $A \bfx = 0$ only has the trivial solution $\mathbf{x} = \mathbf{0}$.

\emoji{bomb} Note that you cannot use the Invertible Matrix Theorem here because $A$ and
$C$ are not necessarily square matrices.
For example, we may have
\begin{equation*}
    A = %$(latexify(A)),
    \qquad
    C = %$(latexify(C))
\end{equation*}
and $C A = I_2$.
"""

long_answer(answer, vfill=true)
```

##

Explain why $A$ cannot have more columns than rows *without* using the Invertible Matrix Theorem.

```julia; results="tex"

answer=L"""
If $A$ has more columns than rows,
then in the linear system $A \bfx = \bfzero$,
there is at least one free variable.
Thus, $A \bfx = \bfzero$ has infinitely many solution,
which contradicts the fact that $A \bfx = \bfzero$ has only the trivial solution.

\emoji{bomb} Note again that you cannot claim that $A$ or $C$ have inverse because they
may not be square matrices.
"""

long_answer(answer, vfill=true)
```

`j quiz_pagebreak()`

# Characteristics of Invertible Matrices (1pt)

\begin{tcolorbox}[title={The Invertible Matrix Theorem (IMT)}]
    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
    \item[a.] $A$ is an invertible matrix.
    \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
    \item[c.] $A$ has $n$ pivot positions.
    \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
    \item[e.] The columns of $A$ form a linearly independent set.
    \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
    \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
    \item[h.] The columns of $A$ spans $\dsR^n$.
    \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
    \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
    \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
    \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
\end{tcolorbox}

Use the Invertible Matrix Theorem in your answers below
and make it clear which of items among (a) to (l) you are using.

##

Let $A$ and $B$ be $n \times n$ matrices.
Use IMT to show that if $AB$ is invertible, then so is $A$.


```julia; results="tex"

answer=L"""
Since $AB$ is invertible, there exists a $n \times n$ matrix $W$ such that
\begin{equation*}
    (AB) W = I_n
\end{equation*}
where $I_n$ is the $n \times n$ identity matrix.
This implies that
\begin{equation*}
    A(B W) = I_n
\end{equation*}
Thus $A$ satisify item (k) of IMT, which is equivalent to item (a) of IMT, i.e., the matrix $A$ is invertible.
"""

long_answer(answer, vfill=true)
```

## 

Let $A$ be an invertible $n \times n$ matrix.
Use IMT to show that the columns of $A^{-1}$ are linearly independent.

`j quiz_pagebreak()`

```julia; results="tex"

answer=L"""
Since $A$ is invertible, there exists a unique matrix $A^{-1}$ such that
\begin{equation*}
    A A^{-1} = I_n
\end{equation*}
Thus $A^{-1}$ satisify item (j) of IMT, which is equivalent to item (h) of IMT, i.e., the columns of $A^{-1}$ are linearly independent.
"""

long_answer(answer, height=10)
```

# Applications to Computer Graphics (1pt)

A three-dimensional object is represented on the two-dimensional computer screen by
projecting the object onto a \textit{viewing plane}. For simplicity, let the $xy$-plane
represent the computer screen, and imagine that the eye of a viewer is along the positive
$z$-axis, at a point $(0, 0, d)$. A \textit{perspective projection} maps each point $(x,
y, z)$ onto an image point $(x^\ast{}, y^\ast{}, 0)$ so that the two points and the eye position,
called the \textit{center of projection}, are on a line. See the figure below.

![Perspective Projection of $(x, y, z)$ to $(x^\ast{}, y^\ast{}, z^\ast{})$](./perspective.png){width=400px}

Using homogeneous coordinates, we can represent the perspective projection by a matrix
$$
P = \begin{bmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & -1/d & 1 \end{bmatrix}
$$

##

Find a formula for computing $(x^\ast{}, y^\ast{}, 0)$, the perspective projection of the point $(x, y, z)$.

`j short_answer(L"$(x^\ast{}, y^\ast{}, 0)$", L"\left(\frac{x}{1-z/d}, \frac{y}{1-z/d}, 0\right)")`

##

```julia; results="hidden"

cols = 3
d = 8
P = [1 0 0 0; 0 1 0 0; 0 0 0 0; 0 0 -1//d 1]
S = [4 1 5; 3 5 4; 4 4 1]
S_homo = vcat(S, ones(Int, (1, cols)))
S_proj_homo = P * S_homo
S_proj = hcat([S_proj_homo[1:3, i] ./ S_proj_homo[4, i] for i in 1:cols]...)
S_proj_float = float.(S_proj)
S_str = join(["(" .* join(latexify.(S[:, i]), ", ") .* ")" for i in 1:cols], ", ")
S, S_proj

```

Let $S$ be the triangle with vertices $`j print(S_str)`$.
Draw the image of $S$ on the viewing plane under the perspective projection with the center of
projection at $(0, 0, `j d`)$.

Answer:

\begin{figure}[htpb]
    \centering
    \colorbox{white}{%
        \begin{tikzpicture}[
            scale=1.0,
            every node/.style={black},
            declare function={
                xmin=0.0; xmax=10.0;
                ymin=0.0; ymax=10.0;
            },
            ]
            % Grid
            \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

            % Axes
            \draw[black, thick] (xmin, 0) -- (xmax, 0) node[right] {$x$};
            \draw[black, thick] (0, ymin) -- (0, ymax) node[above] {$y$};

            % Adding ticks on the x-axis
            \foreach \x in {1,2,...,10} {
                \draw (\x, 0.1) -- (\x, -0.1) node[below] {$\x$};
            }

            % Adding ticks on the y-axis
            \foreach \y in {1,2,...,10} {
                \draw (0.1, \y) -- (-0.1, \y) node[left] {$\y$};
            }

```julia; results="tex"

answer = L"""
\draw [fill=blue, opacity=0.5] 
    (%$(S_proj_float[1, 1]), %$(S_proj_float[2, 1])) -- (%$(S_proj_float[1, 2]), %$(S_proj_float[2, 2])) -- (%$(S_proj_float[1, 3]), %$(S_proj_float[2, 3])) -- cycle;
"""
print_in_solution(answer)

for i in 1:cols
    node = L"\node at (%$(S_proj_float[1, i]), %$(S_proj_float[2, i])) {$(%$(latexify(S_proj[1, i])), %$(latexify(S_proj[2, i])))$};"
    print_in_solution(node)
end

```

        \end{tikzpicture}
    }%
    \caption{The answer of Problem 5}
\end{figure}

# Volume and Linear Transformations (1pt)

\begin{tcolorbox}[title={Theorem 10 (3.3)}]
If $T$ is determined by a $3 \times 3$ matrix $A$, and if $S$ is a region of finite volume
in $\mathbb{R}^3$, then
$$
\text{volume of } T(S) = \left| \det A \right| \cdot \text{volume of } S
$$
\end{tcolorbox}

Let $T: \mathbb{R}^3 \rightarrow \mathbb{R}^3$ be the linear transformation determined by the matrix
$$
A = 
\begin{bmatrix}
a & 0 & 0 \\
0 & b & 0 \\
0 & 0 & c
\end{bmatrix}
$$
where $a, b,$ and $c$ are positive numbers. 
Let $S$ be the unit ball, whose bounding surface has the equation $x_1^2 + x_2^2 + x_3^2 = 1$.

##

Show that $T(S)$ is bounded by the ellipsoid with the equation 
$$
\frac{x_1^2}{a^2} + \frac{x_2^2}{b^2} + \frac{x_3^2}{c^2} = 1.
$$

```julia; results="tex"

answer = L"""
Let $(y_1, y_2, y_3)$ be the cooridinates of a point on the boundary of the unit ball $S$. 
This means that
$$
y_1^2 + y_2^2 + y_3^2 = 1.
$$

Applying the transformation $T$ to the point $(y_1, y_2, y_3)$, we obtain its new coordinates
\begin{equation*}
    \begin{bmatrix}
        x_1 \\ x_2 \\ x_3
    \end{bmatrix}
    =
    T\left( 
    \begin{bmatrix}
        y_1 \\ y_2 \\ y_3
    \end{bmatrix}
    \right)
    =
    A
    \begin{bmatrix}
        y_1 \\ y_2 \\ y_3
    \end{bmatrix}
    =
    \begin{bmatrix}
        ay_1 \\ by_2 \\ cy_3
    \end{bmatrix}
\end{equation*}
Thus, we have
\begin{equation*}
    y_1 = \frac{x_1}{a},
    \quad
    y_2 = \frac{x_2}{b},
    \quad
    y_3 = \frac{x_3}{c}.
\end{equation*}

Substituting $y_1, y_2, y_3$ for $x_1/a, x_2/b, x_3/c$ respectively into the unit sphere equation,
we obtain
$$
\frac{x_1^2}{a^2} + \frac{x_2^2}{b^2} + \frac{x_3^2}{c^2} = 1.
$$
Thus, $T(S)$ is indeed the ellipsoid as defined.
"""

long_answer(answer, height=8)

```

##

Use the fact that the volume of the unit ball is $\frac{4}{3}\pi$ to determine the volume
of $T(S)$

`j short_answer(L"\text{volume of } T(S)", L"\frac{4}{3} a b c\pi")`

# Vector Spaces and Subspaces (1pt)

:cross_mark_button: Mark the box in front of each true statement.

:bomb: Three or more incorrect choices will result in a score of 0.

* [ ] A vector space is not necessarily a subspace.
* [ ] A subspace is not necessarily a vector space.
* [x] $\mathbb{R}^2$ is not a subspace of $\mathbb{R}^3$.
* [x] The set of all polynomials of degree two or less forms a subspace of the vector space of all polynomials of degree three or less.
* [x] A subset $H$ of a vector space $V$ qualifies as a subspace of $V$ if it satisfies the following conditions:
  (i) The zero vector of $V$ is included in $H$,
  (ii) For any vectors $\bfu, \bfv \in H$, the vector $\bfu + \bfv$ is also in $H$,
  (iii) For any vector $\bfu \in H$ and any scalar $c$, the scalar multiple $c \bfu$ is in $H$.

# Coordinate Systems (1pt)

```julia; results="hidden"

Random.seed!(8568)
while true
    global B = rand(-1:4, (3, 3))
    if rank(B) == 3 && det(B) == 1
        break
    end
end
bvects = [latexify(B[:, i]) for i in 1:3]

x_coords = rand(-1:4, 3)
x = B*x_coords

while true
    global y_coords = rand(-1:4, 3)
    global y = B*y_coords
    if max(abs.(y)...) < 5
        break
    end
end
y, y_coords


```

Consider the following basis of $\mathbb{R}^3$:
\begin{equation*}
    \scB
    =
    \left\{
    `j print(bvects[1])`, `j print(bvects[2])`, `j print(bvects[3])`
    \right\}
\end{equation*}

##

Find the vector $\bfx$ with $\scB$-coordinates
\begin{equation*}
    [\bfx]_\scB = `j print(latexify(x_coords))`
\end{equation*}

`j short_answer(L"\bfx", latexify(x))`

##

Find the $\scB$-coordinates of the vector
\begin{equation*}
    \bfy = `j print(latexify(y))`
\end{equation*}

`j short_answer(L"[\bfy]_\scB", latexify(y_coords))`

# The Dimension of a Vector Space (1pt)

In the following statements, $V$ is a vector space, $p$ is a positive integer and
$\mathbf{v}_1, \mathbf{v}_2, \ldots$ are vectors in $V$.

:cross_mark_button: Mark the box in front of each true statement.

:bomb: Three or more incorrect choices will result in a score of 0.

* `j print_checked()` If there exists a set $\{\mathbf{v}_1, \ldots, \mathbf{v}_p\}$ that spans $V$, then $\dim V \leq p$.
* [ ] If there exists a linearly independent set $\{\mathbf{v}_1, \ldots, \mathbf{v}_p\}$ in $V$, then $\dim V \leq p$.
* [ ] If there exists a linearly dependent set $\{\mathbf{v}_1, \ldots, \mathbf{v}_p\}$ in $V$, then $\dim V \geq p$.
* [ ] If $\dim V = p > 0$, there may not exist a set of $p+1$ vectors $\{\mathbf{v}_1, \ldots, \mathbf{v}_{p+1}\}$
  such that $\mathrm{Span}\left\{\mathbf{v}_1, \ldots, \mathbf{v}_{p+1}\right\} = V$.
* [ ] If for every set of $p$ vectors $\{\mathbf{v}_1, \ldots, \mathbf{v}_{p}\}$ in $V$,
  $\mathrm{Span}\left\{\mathbf{v}_1, \ldots, \mathbf{v}_{p}\right\} \ne V$, then $\dim V \ge p+2$.

# Linear Difference Equations (1pt)

\begin{tcolorbox}[title={Casorati Matrix}]
Let $\{u_{k}\}$, $\{v_{k}\}$, and $\{w_{k}\}$ be three signals in $\mathbb{S}$.
The matrix 
\begin{equation*}
    \begin{bmatrix}
        u_{k} & v_{k} & w_{k} \\
        u_{k+1} & v_{k+1} & w_{k+1} \\
        u_{k+2} & v_{k+2} & w_{k+2} \\
    \end{bmatrix}
\end{equation*}
is call \emph{Casorati matrix} of signals, denoted by $C(k)$.
If the $C(k)$ is invertible for some $k$, then $\{u_k\}$, $\{v_k\}$ and $\{w_k\}$ are linearly independent.

\end{tcolorbox}

##

Are the signals $\{k\}$, $\{k^2\}$ and $\{k^3\}$ linearly independent?
Prove your answer using the Casorati matrix.

```julia; results="tex"

@syms k
u(k)=k
v(k)=k^2
w(k)=k^3
casorat(k) = [u(k) v(k) w(k); u(k+1) v(k+1) w(k+1); u(k+2) v(k+2) w(k+2)]
ck = latexify(casorat(k))
c1 = latexify(casorat(1))
c1_rref = rationalize.(rref(casorat(1))) |> latexify
d1_det = latexify(rationalize.(det(casorat(1))))
ck_det = latexify(factor(det(casorat(k))))
rts = join(keys(sympy.roots(det(casorat(k)))), ", ")

answer = L"""
Let $C(k)$ be the Casorati matrix of the three signals.
In other words
\begin{equation*}
    C(k)
    =
    %$(ck)
\end{equation*}
Then
\begin{equation*}
    C(1)
    =
    %$(c1)
    \sim
    %$(c1_rref)
\end{equation*}
is an invertible matrix by IMT, since it is row equivalent to the identity matrix.
Thus the three signals are linearly independent.

\emoji{bulb} You can also show that $C(1)$ is invertible by showing that $\det(C(1)) = %$(d1_det)$.

\emoji{bulb} Since $\det(C(k)) = %$(ck_det)$,
any choice of $k \notin \{%$(rts)\}$ will work.
"""

long_answer(answer, vfill=true)
```

##

Are the signals $\{k^2\}$ and $\{ 3 k |k| \}$ linearly independent?
Prove your answer.

:bulb: Hint: When are *two* vectors linearly dependent?

`j quiz_pagebreak()`

```julia; results="tex"

answer = L"""
Assume that there exists some constant $c \in \mathbb{R}$ such that $k^2 = c 3k|k|$ for all integers $k$. We analyse the equation under different conditions for $k$.

1. **Case $k > 0$:**
   \begin{equation*}
       k^2 = c \cdot 3k^2
   \end{equation*}
   Simplifying, we find:
   \begin{equation*}
       1 = 3c \implies c = \frac{1}{3}
   \end{equation*}

2. **Case $k < 0$:**
   Here, $|k| = -k$ because $k$ is negative. Substituting $|k| = -k$, we have:
   \begin{equation*}
       k^2 = c \cdot 3k(-k) = -c \cdot 3k^2
   \end{equation*}
   Simplifying, we find:
   \begin{equation*}
       1 = -3c \implies c = -\frac{1}{3}
   \end{equation*}

We arrive at a contradiction: $c = \frac{1}{3}$ and $c = -\frac{1}{3}$ cannot simultaneously hold for the same $c$. Therefore, no such $c$ exists, and $\{k^2\}$ is not a scalar multiple of $\{3k|k|\}$.

By similar reasoning, $\{3k|k|\}$ is not a scalar multiple of $\{k^2\}$.

**Conclusion:** The sequences $\{k^2\}$ and $\{3k|k|\}$ are linearly independent because neither sequence is a scalar multiple of the other.

\emoji{bomb} Note that you cannot use Casorati matrices to prove that $\{k^2\}$ and $\{ 3
k |k| \}$ are linearly independent since $C(k)$ is not invertible for all $k$.
"""

long_answer(answer, vfill=true)
```

# Eigenvectors and Linear Transformations (1pt)

```julia; results="hidden"

Random.seed!(7078)
while true
    global A = rand(-3:7, (2, 2))
    global ls, vs = eigen(A)
    global ls = unique(ls)
    if !(0.0 in ls) && length(ls) == 2 && all(isreal.(ls)) && all(isinteger.(ls))
        break
    end
end
ls, vs = eigen(Sym.(A))
@assert vs * diagm(ls) * inv(vs) == A
T_str = latexify(diagm(ls))
T_str_r = latexify(diagm(reverse(ls)))
B_str = join(latexify.(eachcol(vs)), ", ")
B_str_r = join(latexify.(reverse(eachcol(vs))), ", ")

```

Define $T: \mathbb{R}^2 \rightarrow \mathbb{R}^2$ by $T(\bfx) = A \bfx$
where
\begin{equation*}
    A = `j print(latexify(A))`
\end{equation*}
Let $\scB$ be a basis for $\mathbb{R}^2$.
Then $[T]_\scB$ is the unique matrix which satisfies
\begin{equation*}
    [T(\bfx)]_\scB = [T]_\scB [\bfx]_\scB, \qquad \text{for all } \bfx \in \dsR^2.
\end{equation*}

##

Find a basis $\scB$ for which $[T]_{\scB}$ is diagonal.

```julia; results="tex"

short_answer(L"\scB", L"\left\{ %$B_str \right\} \text{ or } \left\{ %$B_str_r \right\}", height=3)

note = L"""
Note: Scaling these two eigenvectors by non-zero scalars also works.
"""

print_in_solution(note)

```

##

What is $[T]_\scB$ given your choice of $\scB$ above?

`j short_answer(L"[T]_\scB", L"%$T_str \text{ or } %$T_str_r", height=3)`

`j quiz_pagebreak()`

# Complex Eigenvalues (1pt)

\begin{tcolorbox}[title={Complex Conjugate}]
    The complex conjugate of a complex number $\lambda = a+b i$, denoted by
    $\overline{\lambda}$, is the complex number $a-b i$.
\end{tcolorbox}

##

Let $A$ be a $n \times n$ matrix whose entries are real numbers.
Show that if $\lambda$ is a complex eigenvalue of $A$,
then $\overline{\lambda}$ is also an eigenvalue of $A$.

```julia; results="tex"

answer = L"""
Let $\bfx \in \dsC^n$ be an eigenvector corresponding to $\lambda$.
Since $A$ contains only real numbers,
we have $\overline{A} = A$. Thus
\begin{equation*}
    A \overline{\bfx}
    =
    \overline{A} \overline{\bfx}
    =
    \overline{A \bfx}
    =
    \overline{\lambda \bfx}
    =
    \overline{\lambda} \overline{\bfx}
\end{equation*}
Therefore, $\overline{\lambda}$ is an eigenvalue of $A$ with corresponding eigenvector $\overline{\bfx}$.
"""

long_answer(answer, vfill=true)
```

##

Find the eigenvalues of the following matrix

```julia; results="tex"

Random.seed!(4394)
while true
    global mat = rand(-1:4, 2,2)
    global vals = eigvals(Sym.(mat))
    if imag(vals[1]) != 0
        break
    end
end
mat, vals

print(process_latex(L"""
\begin{equation*}
%$(latexify(mat))
\end{equation*}
"""))

short_answer(L"\left\{ %$(latexify(vals[1])), %$(latexify(vals[2])) \right\}")
```
