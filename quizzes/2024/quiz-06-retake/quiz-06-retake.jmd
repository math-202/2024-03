---
title: MATH 202 Linear Algebra
subtitle: Quiz 06 Retake
author: "Instructor: Xing Shi Cai"
date: 2024-05-06
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
#using Plots
using Random
using SymPy
using RowEchelon
using WeaveHelper
using LinearAlgebra
set_default(env = :raw)

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your name and \textbf{SIS} on Canvas in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Name: $`j print_empty_box(5.7, 1.5)`$
    \hfil
    SIS: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

\pagebreak{}

# True of False (1pt)

:cross_mark_button: the box in front of each true statement.

:bomb: Three or more incorrect choices will lead to a score of 0.

* [ ] If a $2 \times 2$ matrix with real entries has two eigenvalues, they must be distinct.
* [x] If a $2 \times 2$ matrix with real entries has complex eigenvalues, then it has two linearly independent eigenvectors.
* [ ] The eigenvalues of an upper triangular matrix are located on its secondary diagonal (top-right to bottom-left).
* [ ] If $\bfv$ is an eigenvector with eigenvalue 3 of a matrix $A$, then $3 \bfv$ is an eigenvector with eigenvalue $9$ of $A$.
* [ ] All eigenspaces of a matrix $A$ are null spaces of $A$.

# Characteristic Polynomial (1pt)

```julia; results="hidden"

Random.seed!(6981)
while true
    global A = rand(-1:4, (2,2))
    global vals, vects = eigen(Sym.(A))
    if imag(vals[1]) == 0
        break
    end
end
@syms λ
B = A - λ*I
char = det(B)

```
##

Let
\begin{equation*}
    A = `j print(latexify(A))`
\end{equation*}

Find the characteristic polynomial of matrix $A$. Please provide your answer in expanded form, where all terms are fully simplified and multiplied out, as shown below:
\begin{equation*}
c_1 \lambda^2 + c_2 \lambda + c_3
\end{equation*}

`j short_answer(latexify(char))`

##

List the eigenvalues of $A$, repeated according to their multiplicities.

`j short_answer(join(vals, ", "))`

# Eigenvectors and Linear Transformations (1pt)

```julia; results="hidden"

Random.seed!(2304)
while true
    global A = rand(-1:4, (2,2))
    global vals, vects = eigen(Sym.(A))
    println(size(vects))
    if rank(A) == 2 && size(vects) == (2, 2) && vals[1] != vals[2] && imag(vals[1]) == 0
        break
    end
end
A

while true
    global B = hcat(vects)
    if rank(B) == 2
        break
    end
end
B
C = A-vals[1]*I
D = rationalize.(rref(Int.(C)))
TB = sympy.simplify.(inv(B)*A*B)

```

Let $A = `j print(latexify(A))`$ and $B = \{\bfb_1, \bfb_2\}$, 
for $\bfb_1 = `j print(latexify(B[:, 1]))`$, 
$\bfb_2 = `j print(latexify(B[:, 2]))`$.
Define $T: \mathbb{R}^2 \rightarrow \mathbb{R}^2$ by $T(\bfx) = A \bfx$.

##

List the eigenvalues of $A$, repeated according to their multiplicities.

`j short_answer(join(latexify.(vals), ", "))`

##

List the dimension of the eigenspace of for each eigenvalue of $A$, 
in the order of the eigenvalues listed above.

`j short_answer(L"1, 1")`

##

Explain why $A$ is or is not diagonalizable.

```julia; results="tex"

answer = L"""
The matrix $A$ can be diagonalized because the sum of the dimensions of its eigenspaces
corresponding to each *distinct* eigenvalue is $2$,
and $A$ is a $2 \times 2$ matrix.
"""

long_answer(answer, vfill=true)

```
##

Find $[T]_B$, the $B$ matrix for $T$. In other words, $[T(\bfx)]_B = [T]_B [\bfx]_B$ for all
$\bfx \in \dsR^2$.

`j short_answer(L"[T]_B", latexify(TB), height=2)`

#

\begin{tcolorbox}[title={Theorem 9}]
Let $A$ be a real $2 \times 2$ matrix with a complex eigenvalue $\lambda = a - bi$ (where $b \neq 0$) and an associated eigenvector $\bfv$ in $\mathbb{C}^2$. Then
$$
A = PCP^{-1},
$$
where 
$$
P = \begin{bmatrix} \operatorname{Re} \bfv & \operatorname{Im} \bfv \end{bmatrix}
$$
and 
$$
C = \begin{bmatrix} a & -b \\ b & a \end{bmatrix}.
$$
\end{tcolorbox}

`j quiz_pagebreak()`

```julia; results="hidden"

Random.seed!(6449)
while true
    global A = rand(-2:3, (2,2))
    global vals, vects = eigen(Sym.(A))
    if imag(vals[1]) != 0
        global as = real.(vals)
        global bs = -1 .* imag.(vals)
        global r = sqrt(as[1]^2 + bs[1]^2)
        global ϕs = log.(as./r .+ sympy.I .* bs ./ r) ./ sympy.I
        if bs[1]  == 1
            break
        end
    end
end
Ps = [[real.(vects[:, i]) imag.(vects[:, i])] for i in 1:2]
Cs = [[as[i] -bs[i]; bs[i] as[i]] for i in 1:2]
@assert A == Ps[1]*Cs[1]*inv(Ps[1])
@assert A == Ps[2]*Cs[2]*inv(Ps[2])

```

Let $A = `j print(latexify(A))`$.
Use Theorem 9 to find an invertible matrix $P$ and 
a matrix $C$ in the form $\begin{bmatrix} a & -b \\ b & a \end{bmatrix}$
such that
\begin{equation*}
    A = PCP^{-1}.
\end{equation*}

##

`j short_answer(L"P", L"%$(latexify(Ps[1])) \text{ or } %$(latexify(Ps[2]))", height=2.5)`

##

`j short_answer(L"C", L"%$(latexify(Cs[1])) \text{ or } %$(latexify(Cs[2]))", height=2.5)`

##

What does the linear transformation $\bfx \mapsto C \bfx$ do to a vector?

```julia; results="tex"

answer = L"""

The transformation $\bfx \mapsto C \bfx$ rotates a vector counter-clockwise by
$%$(latexify(ϕs[1]))$ or $%$(latexify(ϕs[2]))$ (depending your answer to the previous two questions) 
radians and scale it by $%$(latexify(r))$.
"""

long_answer(answer, vfill=true)

```

##

Compute $A^{12} \begin{bmatrix} 1 \\ 0 \end{bmatrix}$

`j short_answer(L"A^{12} \begin{bmatrix} 1 \\ 0 \end{bmatrix}", latexify(A^12*[1;0]))`

```julia; results="tex"

answer = L"""
Explaination:

We have
\begin{equation*}
    A^{12} \begin{bmatrix} 1 \\ 0 \end{bmatrix} 
    = P C^{12} P^{-1} \begin{bmatrix} 1 \\ 0 \end{bmatrix} 
    = P \left(64 \left(P^{-1} \begin{bmatrix} 1 \\ 0 \end{bmatrix}\right)\right)
    = \begin{bmatrix} -64 \\ 0 \end{bmatrix}
\end{equation*}
"""

print_in_solution(answer)
```
