module Lecture4_7

using Plots
using Statistics
using Random
using LaTeXStrings

export generate_moving_average_plot

# Custom function to calculate the moving average
function movingmean(data::Vector{Float64}, window_width::Int)
    n = length(data)
    avg = Vector{Float64}(undef, n - window_width + 1)
    for i in 1:(n - window_width + 1)
        avg[i] = sum(data[i:(i + window_width - 1)]) / window_width
    end
    return avg
end

# Function to generate the moving average plot
function generate_moving_average_plot(window_width::Int)
    # Set random seed for reproducibility
    Random.seed!(127)

    data_num = 102

    # Generate random data
    data = rand(data_num)

    # Compute moving average with the given window size
    avg = movingmean(data, window_width)

    # Adjust the data and moving average to match lengths
    data_adj = data[1:end-window_width+1]

    # Plot the original data and the moving average
    plt = plot([data_adj avg], label=["Original" "Moving Average"], 
        linewidth=2, legend=:bottomright, 
        xlabel="Index", ylabel="Value",
        )

    # Define the path for saving the plot
    save_path = "moving_average_plot_widow_$(window_width).pdf"

    # Save the plot to PDF
    savefig(plt, save_path)

    # Return the path where the plot is saved
    return save_path
end

# Change plot fonts
plot_font = "Computer Modern"
default(fontfamily=plot_font,
        linewidth=2, framestyle=:box, label=nothing, grid=false)
scalefontsizes(2.5)

end # module
