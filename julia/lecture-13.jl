# # Lecture 13
using LinearAlgebra, RowEchelon, SymPy

# ## Null spaces

## Example
A = [1 -3 -2; -5 9 1]
u = [5; 3; -2]
A * u
 
## Find the null space
@syms x[1:5];
A =  [-3 6 -1 1 -7;
    1 -2 2 3 -1;
    2 -4 5 8 -4];
xs = sympy.Matrix([x[i] for i in 1:5]);
sol = sympy.solve(A*xs, xs)

## Example
A =  [2 4 -2 1; 
    -2 -5 7 3;
    3 7 -8 6]
@syms x[1:4];
xs = sympy.Matrix([x[i] for i in 1:4]);
sol = sympy.solve(A*xs, xs)