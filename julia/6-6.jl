# # 6.6 Machine Learning and Linear Models
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings


## Example 1 -- Find the least-squares line
data = [2 1;
    5 2;
    7 3;
    8 3;]
xs = data[:, 1]
ys = data[:, 2]
X = hcat(ones(Integer, length(xs)), xs)
β = X \ ys
line(x) = dot(β, [1 x])

## draw the picture
pic = scatter(xs, ys, label="data", size=(600, 400), thickness_scaling = 2)
plot!(line, xlims=(0, 12), label = latexify("y = β_0 + β_1 * x"))
savefig(pic, "/tmp/6.6-example-1.png")
pic

## Exercise -- Find the curve
data = [1 7.9;
    2 5.4;
    3 -0.9;]
xs = data[:, 1]
ys = data[:, 2]
X = hcat(cos.(xs), sin.(xs))
β = X \ ys
curve(x) = dot(β, [cos(x) sin(x)])

## draw the picture
pic = scatter(xs, ys, label="data", size=(800, 600), thickness_scaling = 2)
plot!(curve, xlims=(0, 5), label = latexify("y = β_0 * cos(x) + β_1 * sin(x)"))
savefig(pic, "/tmp/6.6-exercise.png")
pic
