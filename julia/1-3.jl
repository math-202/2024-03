# # Section 1.3
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

## Example 5 --- Linear equations
A = [1 2 7;
     -2 5 4;
     -5 6 -3]
rref(A)

## Example 6
A = [1    5 -3;
     -2 -13  8;
     3   -3  1]
rref(A)
