# Lecture 25
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)

## 6.4 

### Example 4

A = [1 0 0;
    1 1 0;
    1 1 1;
    1 1 1]
F = qr(A)
Q = Matrix(F.Q)
R = Matrix(F.R)
A1 = round.(Q*R)
A == A1
