# # Lecture 08
using SymPy

# ## Partitions of Matrices

## Compute A*B

@syms a::real b::real c::real d::real e::real f::real
A = [-3 1 2; 1 -4 5];
B = [a b; c d; e f];
A * B

## Compute A*B with col and row
C = sum([A[:, i]*B[i, :]' for i in 1:3])

## Check the two methods gets the same answer
A * B == C
