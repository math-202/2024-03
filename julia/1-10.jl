# # Section 1.10
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

## Example 9
B = [1 2;
     3 4]
C = [1 2;
     4 3]
A = [1 0;
     0 0]
@assert A*B == A*C
