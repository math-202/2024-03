# # Section 2.1
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

## Example 9
v = [1 
     0 
     0 
     1]
c = v' * v
M = c*I(4) - v * v'
@assert v' * M * v == 0

ws = [symbols("w$i", real=true) for i in 1:4]
ex = simplify(expand(ws' * M * ws))
