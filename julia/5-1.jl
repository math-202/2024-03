# # Section 5.1 Eigenvalues
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)

## The Owl
A = [0 0 .33; 
     .18 0 0; 
     0 .71 .94]
λs = eigvals(A)
vs = eigvecs(A)

## Lower trigangular array
A = LowerTriangular(rand(-1:5, (5,5)))
eigvals(A)

## Upper trigangular array
A = UpperTriangular(rand(-1:5, (5,5)))
eigvals(A)
