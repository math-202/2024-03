# ## 5.2 Characteristic equations
using LinearAlgebra, RowEchelon, SymPy, Random, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)
set_default(env=:raw)

## Example 1
@syms λ
A = [0 -1;
1 0]
evals = eigvals(Sym.(A))
evecs = eigvecs(Sym.(A))
B = A - λ*I
charEq = det(B)
@syms xs[1:2]
B.subs(λ, evals[1]) * xs
B.subs(λ, evals[2]) * xs

## Example 7
A0 = [1//2 -3//5; 
      3//4 11//10]
A = Sym.(A0)
evals = eigvals(A)
evecs = eigvecs(A)
v1 = 5 .* evecs[:, 1]
P = hcat(real.(v1), imag.(v1))
x0 = [2.0; 0.0]
x0B = inv(P) * x0
C = inv(P) * A * P
x1B = C * x0B
x1 = P * x1B
xs = [x0]
for i in 1:10
    push!(xs, round.(A0 * xs[i], digits=4))
    println("$(i-1)/$(xs[i][1])/$(xs[i][2]),")
end
