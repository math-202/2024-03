# # 6.1 Inner Product, Length, and Orthogonality
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Motivation example

# ### First example

# The data
xs = Sym.([70; 80; 90])
ys = [2; 1; 0]
X = hcat(ones(Int, 3), xs)
βs = X \ ys
# Scatter plot
scatter(xs, 
        ys,
        xlabel="Average grade", 
        label="Delays of presentation")
# Define a function for the straight line
line(x) = βs[1] + βs[2] * x
# Generate points for the line
line_xs = range(minimum(xs), maximum(xs), length=100)
line_ys = line.(line_xs)
# Overlay the line on the existing scatter plot
plt = plot!(line_xs, line_ys, label="Fitted Line")
# Save the plot
savefig(plt, "/tmp/grade-procrastinateion-regression-1.pdf")

# The data
xs = Sym.([75; 82; 88])
ys = [2; 1; 0]
X = hcat(ones(Int, 3), xs)
βs = X \ ys
# Scatter plot
scatter(xs, 
        ys,
        xlabel="Average grade", 
        label="Delays of presentation")
# Define a function for the straight line
line(x) = βs[1] + βs[2] * x
# Generate points for the line
line_xs = range(minimum(xs), maximum(xs), length=100)
line_ys = line.(line_xs)
# Overlay the line on the existing scatter plot
plt = plot!(line_xs, line_ys, label="Fitted Line")
# Save the plot
savefig(plt, "/tmp/grade-procrastinateion-regression-2.pdf")

# # Projection
plot([0,3],[0, 4],arrow=true,linewidth=4,label="",aspect_ratio=1, size=(600,800))
plot!([0,3],[0, 0],arrow=true,linewidth=4,label="",aspect_ratio=1)
plot!([3,3],[0, 4],arrow=true,linewidth=4,label="",aspect_ratio=1)
plot!([0,1],[0, 0],arrow=true,linewidth=4,label="",aspect_ratio=1)
annotate!(1.2,2.2,text(L"\mathbf{y} = \mathbf{z} + \mathbf{\hat{y}}",20))
annotate!(1.7,0.2,text(L"\mathbf{\hat{y}}",20))
annotate!(0.5,0.2,text(L"\mathbf{u}",20))
annotate!(2.8,2,text(L"\mathbf{z}",20))
