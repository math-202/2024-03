# # Lecture 15
using LinearAlgebra, RowEchelon, SymPy

## Change of coordinates
PB = [2 -1; 1 1]
x = [4; 5]
invPB = inv(PB)
xB = invPB * x

## # Example in R^3

## In the span
v1 = [3; 6; 2]
v2 = [-1; 0; 1]
x = [3; 12; 7]
A = hcat(v1, v2, x)
A1 = rref(A)

## Coordinates
A1[:, 1:2] \ A1[:, 3]