# ## 5.4 Eigenvectors and Linear Transformations, Discrete Dynamical System
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)
set_default(env=:raw)

## TPS 1
A = Sym.([5 -3
    -7 1])
D = diagm(eigvals(A))
P = eigvecs(Sym.(A))
A1 = P*D*inv(P)
A == A1
