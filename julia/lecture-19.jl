# # Lecture 19
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Markov chain

## Example: City and suburb
A = [.95 .03; .05 .97]
x0 = [.6; .4]
x1 = A * x0
x2 = A * x1

## Distant future
xs = zeros(2,100)
xs[:, 1] = x0
for i in 2:100
    xs[:, i] = A *xs[:, i-1]
end

## convergence
A^100 * [0.6; 0.4]
A^100 * [0.9; 0.1]
A^100 * [0.75; 0.25]
A^100 * [0.99; 0.01]

B = [0.96 0.06;
    0.04 0.94]

B^100 * [0.6; 0.4]
B^100 * [0.9; 0.1]
B^100 * [0.75; 0.25]
B^100 * [0.99; 0.01]

## Stable distribution
B = rref(rationalize.(A) - I)
lhs = vcat(B[1:end-1, :], [1 1])
rhs = [0; 1]
sol = lhs \ rhs

## More than one steady-state vectors
I(2)^100 * [0.6; 0.4]
I(2)^100 * [0.9; 0.1]
I(2)^100 * [0.75; 0.25]

## One or more than one steady-state vectors
B = [0 0.5;
    1 0.5]
B^100 * [0.6; 0.4]
B^100 * [0.9; 0.1]
B^100 * [0.75; 0.25]
B^100 * [0.45; 0.55]

## Why
B^2
B^3
B^4

## Regular matrix
P = [.5 .5 .3; .3 .8 .3; .2 0 .4]
P^2

## Exercise 18
@syms α, β, q
A = [1-α β;
    α 1-β]
xs = [q; 1-q]
B = (A - I(2)) * xs