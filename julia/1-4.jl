# # Section 1.4
using LinearAlgebra, RowEchelon, SymPy, Latexify

## For making slides
using Latexify
copy_to_clipboard(true)

## Matrix multiplication
A = [1 2 -1
     0 -5 3]
x = [4 
     3 
     7]
A * x

## Matrix multiplication
A = [2 -3
     8 0
     -5 2]
x = [4
    7]
A * x

## When is the system consistent?
@syms b[1:3]
A = [1 3 4 b[1]
    -4 2 -6 b[2]
    -3 -2 -7 b[3]]
A.echelon_form()
