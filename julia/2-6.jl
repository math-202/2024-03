# # 2.6 Input-Output Model
using LinearAlgebra, RowEchelon, Random, Latexify

# Setup Latexify
copy_to_clipboard(true)
set_default(env = :raw)

# help function
include("./helper.jl")


## The example
C = rationalize.([.5 .4 .2; .2 .3 .1; .1 .1 .3])
d = [50; 30; 20]
C1 = inv(I - C)
x = C1 * d
xfloat = round.(float.(x), digits=2)

## Approximate solution
C = convert.(Float64, C)
C2 = sum([C^i for i in 0:32])
x1 = C2 * d
xerror = round.(x1 - x, digits = 3)
