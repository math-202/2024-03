# # Section 1.5
using LinearAlgebra, RowEchelon, SymPy, Latexify

include("./helper.jl")

## For making slides
using Latexify
copy_to_clipboard(true)

## Homogeneous system
A = [3 5 -4
    -3 -2 4
    6 1 -8]
rref(A)

## Nonhonogeneous system
A = [3 5 -4
    -3 -2 4
    6 1 -8]
b = [7
    -1
    -4]
rationalorint.(rref(hcat(A,b)))

## Exercise
A = [1 3 1 1
    -4 -9 2 -1
    0 -3 -6 -3]
rationalorint.(rref(A))
