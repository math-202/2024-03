\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{3d}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Matrix Equations, Solution Sets}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.3}{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 33, 35}
            \sectionhomework{1.4}{3, 5, 9, 11, 13, 15, 17, 19, 21, 35, 37, 39}
        \end{column}
    \end{columns}
\end{frame}

\section{1.3 Vector Equations}

\subsection{Vectors}

\begin{frame}
    \frametitle{Raster Graphs and Vector Graphs}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{raster-vector.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Vectors in $\dsR^{2}$}

    A matrix with only one column is a \alert{column vector/vector}.
    Examples of vectors ---
    \begin{equation*}
        \bfu
        =
        \begin{bmatrix}
        3 \\
        -1
        \end{bmatrix}
        ,
        \qquad
        \bfv
        =
        \begin{bmatrix}
        0.2 \\
        0.3
        \end{bmatrix}
        ,
        \qquad
        \bfw
        =
        \begin{bmatrix}
        w_{1} \\
        w_{2}
        \end{bmatrix}
    \end{equation*}
    where $w_1, w_2 \in \dsR$.

    We let $\dsR^{2}$ denote the set of vectors with two entries.

    \pause{}

    Two vectors are \alert{equal} if only if their corresponding entries are equal.
    For example,
    \begin{equation*}
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        \ne
        \begin{bmatrix}
        -1 \\
        3 \\
        \end{bmatrix}
        ,
        \qquad
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
        3 \\
        -1 \\
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Vector Arithmetic}
    \cake{} How should we define the following operations on vectors?

    Addition
    \begin{equation*}
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        +
        \begin{bmatrix}
        2 \\
        5 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \blankshort{}\\
            \blankshort{}\\
        \end{bmatrix}
        .
    \end{equation*}
    Subtraction
    \begin{equation*}
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        -
        \begin{bmatrix}
        2 \\
        5 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \blankshort{}\\
            \blankshort{}\\
        \end{bmatrix}
        .
    \end{equation*}
    Scalar multiple
    \begin{equation*}
        c
        \begin{bmatrix}
        1 \\
        -2 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \blankshort{}\\
            \blankshort{}\\
        \end{bmatrix}
    \end{equation*}
    where $c \in \dsR$.
\end{frame}

\subsection{Geometric Descriptions}

\begin{frame}
    \frametitle{Vectors as Points}

    We can think of $\dsR^{2}$ as the set of points (or arrows) on a plane.

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.45, every node/.style={black}]
                % Set seed for random number generator
                \pgfmathsetseed{48}
                % Draw x and y axes
                \draw[thin] (-6,0) -- (6,0) node[anchor=west] {$x_1$};
                \draw[thin] (0,-6) -- (0,6) node[anchor=south] {$x_2$};
                % Define random integers for arrows
                \pgfmathsetmacro{\vx}{int(rand*5)}
                \pgfmathsetmacro{\vy}{int(rand*5)}
                \pgfmathsetmacro{\wx}{int(rand*5)}
                \pgfmathsetmacro{\wy}{int(rand*5)}
                \pgfmathsetmacro{\xx}{int(rand*5)}
                \pgfmathsetmacro{\xy}{int(rand*5)}
                % Draw arrows
                \drawvector{\vx,\vy}{$(\vx,\vy)$};
                \drawvector{\wx,\wy}{$(\wx,\wy)$};
                \drawvector{\xx,\xy}{$(\xx,\xy)$};
            \end{tikzpicture}
        \end{center}
        \caption{Pictures of some vectors}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Parallelogram Rule}

    Adding two vectors can be visualized as a parallelogram.

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=1, every node/.style={black}]
                % Draw x and y axes
                \draw[thin] (0,0) -- (8,0) node[anchor=west] {$x_1$};
                \draw[thin] (0,0) -- (0,5) node[anchor=south] {$x_2$};
                % Define locations for arrows
                \coordinate (u) at (2,3);
                \coordinate (v) at (5,1);
                % Draw parallelogram
                \fill[MintCream] (0,0) -- (u) -- ($(u)+(v)$) -- (v) -- cycle;
                % Draw arrows
                \drawvector{u}{$\symbf{u}$};
                \drawvector{v}{$\symbf{v}$};
                \drawvector{$(u)+(v)$}{$\symbf{u}+\symbf{v}$};
            \end{tikzpicture}
        \end{center}
        \caption{The parallelogram rule of vector addition}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Scalar Multiplication}

    Multiplying a vector can be visualized as extending or shrinking it.

    \begin{figure}[htpb]
        \centering
        \begin{center}
            \begin{tikzpicture}[scale=0.8, every node/.style={black}]
                % Draw x and y axes
                \draw[thin] (-5,0) -- (7,0) node[anchor=west] {$x_1$};
                \draw[thin] (0,-3) -- (0,3) node[anchor=south] {$x_2$};
                % Define locations for arrows
                \coordinate (u) at (3,-1);
                % Draw arrows
                \drawvector{$2*(u)$}{$2 \bfu$};
                \drawvector{u}{$\bfu$};
                \drawvector{$0.5*(u)$}{$\frac{1}{2}\bfu$};
                \drawvector{$-3/2*(u)$}{$-\frac{3}{2} \bfu$};
                \drawvector{$-1*(u)$}{$- \bfu$};
            \end{tikzpicture}
        \end{center}
        \caption{Scalar multiplications in $\dsR^{2}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Vectors in $\dsR^{3}$}

    $\dsR^{3}$ is set of vectors with $3$ entries.
    We can think of it as the set of points in the $3$-dimensional space.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=1.5,
            x  = {(-0.25cm,-0.15cm)},
            y  = {(0.7cm,-0.15cm)},
            z  = {(0cm,0.95cm)}]
            % Define style for the axes and cubes
            \tikzstyle{axis} = [->,black,thin];

            % Draw the axes
            \draw[axis] (0,0,0) -- (3.5,0,0) node[anchor=north east]{$x_1$};
            \draw[axis] (0,0,0) -- (0,3.5,0) node[anchor=north west]{$x_2$};
            \draw[axis] (0,0,0) -- (0,0,2.5) node[anchor=south]{$x_3$};

            \begin{scope}[canvas is yz plane at x=1]
                \draw (0,0) rectangle (1,1);
            \end{scope}
            \begin{scope}[canvas is xz plane at y=1]
                \draw (0,0) rectangle (1,1);
            \end{scope}
            \begin{scope}[canvas is yx plane at z=1]
                \draw (0,0) rectangle (1,1);
            \end{scope}
            \begin{scope}[canvas is yz plane at x=2]
                \draw (0,0) rectangle (2,2);
            \end{scope}
            \begin{scope}[canvas is xz plane at y=2]
                \draw (0,0) rectangle (2,2);
            \end{scope}
            \begin{scope}[canvas is yx plane at z=2]
                \draw (0,0) rectangle (2,2);
            \end{scope}
            % Define locations for arrows
            \coordinate (a) at (1,1,1);
            % Draw arrows
            \drawvectorIII{$2*(a)$}{$2 \bfa$};
            \drawvectorIII{$(a)$}{$\bfa$};
        \end{tikzpicture}
        \caption{Scalar multiplications in $\dsR^{3}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Vectors in $\dsR^{n}$}

    $\dsR^{n}$ is set of vectors with $n$ entries, e.g.,
    \begin{equation*}
        \bfu
        =
        \begin{bmatrix}
            u_{1} \\
            u_{2} \\
            \vdots \\
            u_{n} \\
        \end{bmatrix}
    \end{equation*}
    where $u_{1}, u_{2}, \dots, u_{n}$ are real numbers.

    The zero vector $\bfzero$ is the vector with all entries being $0$.

    \hint{} When you write a vector $\bfu$ with $\temoji{pencil}$, 
    you can use $\vec{u}$ to improve the readability.
\end{frame}

\begin{frame}
    \frametitle{Algebraic Properties of $\boldsymbol{\dsR^{n}}$}

    For all $\bfu, \bfv, \bfw \in \dsR^{n}$ and all $c, d \in \dsR$,
    we can prove

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item[(i)] $\bfu + \bfv = \bfv + \bfu$
                \item[(ii)] $(\bfu + \bfv) + \bfw = \bfv + (\bfu+\bfw)$
                \item[(iii)] $\bfu + \bfzero= \bfzero + \bfu = \bfu$
                \item[(iv)] $\bfu + (-\bfu)= (-\bfu) + \bfu = \bfzero$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item[(v)] $c(\bfu + \bfv) = c \bfu + c \bfv$
                \item[(vi)] $(c+d) \bfu = c \bfu+ d \bfu$
                \item[(vii)] $c (d \bfu) = (c d) \bfu$
                \item[(viii)] $1 \bfu = \bfu$
            \end{itemize}
        \end{column}
    \end{columns}

    \vspace{1em}

    \cake{} How should we define the vector $-\bfu$?
\end{frame}

\subsection{Linear Combinations}%

\begin{frame}
    \frametitle{Linear Combinations}

    The \alert{linear combination} of vectors $\bfv_{1}, \dots, \bfv_{p}$
    with weight $c_1, \dots, c_p \in \dsR$ is the vector
    \begin{equation*}
        \bfy =
        c_1 \bfv_1
        +
        c_2 \bfv_2
        +
        \dots
        +
        c_p \bfv_p
        .
    \end{equation*}
    \only<1>{%
        \cake{} 
        We have only defined adding two vectors together.
        So, why do not we need to put in some $($ and $)$ to indicate
        the order of addition?
    }%

    \pause{}

    For example, some \emph{linear combinations} of $\bfv_1, \bfv_2$ are
    \begin{equation*}
        \sqrt{3} \bfv_1+ \bfv_2
        ,
        \qquad
        \frac{1}{2} \bfv_1
        ,
        \qquad
        \bfzero
    \end{equation*}

    \cake{} What are the weights in them?
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \cake{}
    Can you write $\bfu$ and $\bfw$ as linear combinations of $\bfv_{1}, \bfv_{2}$?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{1.3-fig-8.jpg}
        \caption{Linear combinations of $\bfv_{1}, \bfv_{2}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Let
    \begin{equation*}
        \bfa_1
        =
        \begin{bmatrix}
            1, \\ -2 \\ -5
        \end{bmatrix}
        ,
        \qquad
        \bfa_2
        =
        \begin{bmatrix}
            2, \\ 5 \\ 6
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            7, \\ 4 \\ -3
        \end{bmatrix}
    \end{equation*}

    Can $\bfb$ be an \emph{linear combination} of $\bfa_{1}, \bfa_{2}$?

    In other words, can we find $x_1, x_2 \in \dsR$ such that
    \begin{equation*}
        x_1 \bfa_1 + x_2 \bfa_2 = \bfb
    \end{equation*}

    \pause{}

    This is the same as solving the linear system with the \emph{augmented matrix}
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2  & \bfb
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[label=current]
    \frametitle{Vector Equations and Linear Systems}

    A \emph{vector equation} 
    \[
        x_1\mathbf{a}_1 + x_2\mathbf{a}_2 + \dots + x_n\mathbf{a}_n = \mathbf{b}
    \]
    has the same solution set as the \emph{linear system} whose augmented matrix is
    \[
        \begin{bmatrix}
            \mathbf{a}_1 & \mathbf{a}_2 & \cdots & \mathbf{a}_n & \mathbf{b}
        \end{bmatrix}
    \]
    In particular, \(\mathbf{b}\) can be generated by a \emph{linear combination} of
    \(\mathbf{a}_1, \dots, \mathbf{a}_n\) if and only if the above
    linear system is consistent.
\end{frame}

%\begin{frame}
%    \frametitle{The many ways to write linear systems}
%    A linear system can be \emoji{pencil} as ---
%    \begin{itemize}
%        \item a list of \emph{linear equations};
%        \item an \emph{augmented matrix};
%        \item a \emph{vector equation}.
%    \end{itemize}
%\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Can $\bfb$ be a linear combination of $\bfv_1, \bfv_2, \bfv_3$?
%    Is the solution unique?
%
%    \begin{center}
%        \begin{tikzpicture}[
%                scale=1.5,
%                every node/.style={black},
%                ]
%            % Set seed for random number generator
%            \pgfmathsetseed{43}
%            % Define random angles for arrows
%            \pgfmathsetmacro{\a}{rand*360}
%            \pgfmathsetmacro{\b}{rand*360}
%            \pgfmathsetmacro{\c}{rand*360}
%            \pgfmathsetmacro{\d}{rand*360}
%            % Draw arrows
%            \drawvector{\a:1}{$v_1$};
%            \drawvector{\b:1.5}{$v_2$};
%            \drawvector{\c:2}{$v_3$};
%            \draw[->, ultra thick] (0, 0) -- (\d:3) node[anchor=south] {$\bfb$};
%        \end{tikzpicture}
%    \end{center}
%\end{frame}

\subsection{Spans}

\begin{frame}[c]
    \frametitle{\zany{} Attention span}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{attention-span.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spans}
    Let $\veclist{}$ be vectors in $\dsR^{n}$.

    The set of \emph{all} linear combinations of $\veclist{}$, denoted by
    \begin{equation*}
        \Span{\veclist{}},
    \end{equation*}
    is called the subset of $\dsR^{n}$ \alert{spanned} by $\veclist{}$.

    \cake{} What are the \emph{spans} of $\bfzero$?
\end{frame}

\begin{frame}
    \frametitle{Spans in $\dsR^{2}$}
    
    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=1.3, 
                every node/.style={black}
                ]
                % Draw the grid
                \draw[step=1.0,thin,lightgray] (-1,-1) grid (3,2);
                % Draw x and y axes
                \draw[thin] (-1.3,0) -- (3.3,0) node[anchor=west] {$x_1$};
                \draw[thin] (0,-1.3) -- (0,2.3) node[anchor=south] {$x_2$};
                % Draw arrows
                \drawvector{2,1}{$\bfu$};
                \only<2>{%
                    \drawvector{-1,-0.5}{$\bfv$};
                }%
                \only<3>{%
                    \drawvector{1,0.5}{$\bfv$};
                }%
                \only<4>{%
                    \drawvector{1,-1}{$\bfv$};
                }%
            \end{tikzpicture}
        }%
        \caption{\cake{} What is the span of $\{\bfu\only<2->{, \bfv}\}$?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spans in $\dsR^{3}$}
    
    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=1.5,
                x  = {(-0.25cm,-0.15cm)},
                y  = {(0.7cm,-0.15cm)},
                z  = {(0cm,0.95cm)}]
                % Define style for the axes and cubes
                \tikzstyle{axis} = [->,black,thin];

                % Draw the axes
                \draw[axis] (-2.5,0,0) -- (3.5,0,0) node[anchor=north east]{$x_1$};
                \draw[axis] (0,-1.0,0) -- (0,3.5,0) node[anchor=north west]{$x_2$};
                \draw[axis] (0,0,-1.5) -- (0,0,2.5) node[anchor=south]{$x_3$};

                % Define locations for arrows
                \only<1-2>{%
                    \draw[thin, SeaGreen] ($-1*(u)$) -- ($3*(u)$);
                }%
                \coordinate (u) at (1,1,1);
                \coordinate (v) at (4,3,0.5);
                \coordinate (uv) at ($(u)+(v)$);
                \coordinate (w1) at ($2*(u)+2*(v)$);
                \coordinate (w2) at ($2*(u)-0.5*(v)$);
                \coordinate (w3) at ($-0.5*(u)+2*(v)$);
                \coordinate (w4) at ($-0.5*(u)-0.5*(v)$);
                % Draw arrows
                \only<2>{%
                    \drawvectorIII{$2*(u)$}{$\bfv$};
                }%
                \only<3>{%
                    \draw[white, fill=MintCream, fill opacity=0.9] (0,0,0) -- (u) --
                        ($(u) + (v)$) -- (v) -- cycle;
                    \draw[white, fill=MintCream, fill opacity=0.6] (w1) -- (w2) 
                        -- (w4) -- (w3) -- cycle;
                    \drawvectorIII{$(v)$}{$\bfv$};
                    \drawvectorIII{$(uv)$}{$\bfu+\bfv$};
                }%
                \drawvectorIII{$(u)$}{$\bfu$};
            \end{tikzpicture}
        }%
        \caption{\cake{} What is the span of $\{\bfu\only<2->{, \bfv}\}$?}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Example 6}
%
%    Let
%    \begin{equation*}
%        \bfa_{1}
%        =
%        \begin{bmatrix}
%            1 \\ -2 \\ 3
%        \end{bmatrix}
%        ,
%        \quad
%        \bfa_{2}
%        =
%        \begin{bmatrix}
%            5 \\ -13 \\ -3
%        \end{bmatrix}
%        ,
%        \quad
%        \bfb
%        =
%        \begin{bmatrix}
%            -3 \\ 8 \\ 1
%        \end{bmatrix}
%        .
%    \end{equation*}
%    Is $\bfb$ in $\Span{\bfa_1, \bfa_2}$?
%\end{frame}

\section{1.4 The Matrix Equations $A \bfx = \bfb$}

\subsection{Matrix Equations}

\begin{frame}{Product of a Matrix and a Vector}
    If $A$ is a $m \times n$ matrix, with columns
    $\veclist[n]{a}$, and $\bfx \in \dsR^n$, then
    the \alert{product of $A$ and $\bfx$} is defined by
    \begin{equation*}
        A \bfx =
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ \vdots \\ x_n
        \end{bmatrix}
        =
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        .
    \end{equation*}

    \cake{} In other words, $A \bfx$ is the \blankshort{}
    of $\veclist[n]{a}$ with $x_1, \dots, x_n$ being the \blankshort{}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            4 \\ 3 \\ 7
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 \\ 6
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A Simple Example}

    \cake{} Can you compute
    \begin{equation*}
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1
        \end{bmatrix}
        \begin{bmatrix}
            r \\ s \\ t
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Identity matrix}

    A matrix of the following form is called the \alert{identity matrix}
    \begin{equation*}
        I_n = \begin{bmatrix}
            1 & 0 & \cdots & 0 \\
            0 & 1 & \cdots & 0 \\
            \vdots & \vdots & \ddots & \vdots \\
            0 & 0 & \cdots & 1 \\
        \end{bmatrix}
    \end{equation*}
    \puzzle{} Exercise: Can you show that $I_n \bfx = \bfx$ for any $\bfx \in \dsR^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Linear system as matrix equations}

    Consider the matrix equation
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            4 \\ 1
        \end{bmatrix}
    \end{equation*}
    What is the equivalent linear system?
\end{frame}

\begin{frame}
    \frametitle{Theorem 3 --- Three Ways to Write Linear Systems}

    If $A = [\bfa_1 \, \dots \, \bfa_n]$ is a $m \times n$ matrix, 
    and $\bfb \in \dsR^m$, 
    then
    the \emph{matrix equation}
    \begin{equation*}
        A  
        \begin{bmatrix} 
            x_1 \\
            x_2 \\
            \vdots \\
            x_n
        \end{bmatrix} 
        = \bfb
    \end{equation*}
    has the same solution set as the \emph{vector equation}
    \begin{equation*}
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        =
        \bfb
    \end{equation*}
    and the \emph{linear system} with the \emph{augmented matrix}
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n & \bfb
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Existence of Solutions}

\begin{frame}
    \frametitle{Example 3}

    Is the following equation \emph{consistent} for \emph{all} $b_1, b_2, b_3$?
    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 \\
            -4 & 2 & -6 \\
            -3 & -2 & -7 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            b_1 \\ b_2 \\ b_3
        \end{bmatrix}
    \end{equation*}

    \hint{}
    Does reducing the matrix to echelon form give the answer?

    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            -4 & 2 & -6 & b_2 \\
            -3 & -2 & -7 & b_3 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            0 & 14 & 10 & 4 b_{1} + b_{2} \\
            0 & 0 & 0 & 14 \cdot b_1 - 7 \cdot b_2 + 14 \cdot b_3 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- When Is a Linear System Consistent?}

    If $A = \begin{bmatrix} \bfa_1 & \dots & \bfa_n \end{bmatrix}$ 
    is a $m \times n$ matrix
    and $\bfx \in \dsR^n$, 
    then the followings are equivalent
    \begin{enumerate}[<+->]
        \item[a.] For each $\bfb \in \dsR^{m}$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
            \emph{consistent}.
        \item[b.] For each $\bfb \in \dsR^{m}$, $\bfb$ is a \emph{linear combination} of $\bfa_1, \dots, \bfa_n$.
        \item[c.] For each $\bfb \in \dsR^{m}$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
        \item[d.] The \emph{echelon} form of $A$ has a pivot position in every \emph{row}.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Is it possible to find $\bfa_1, \bfa_2, \bfa_3 \in \dsR^{4}$
    such that every
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \bfa_3
        \end{bmatrix}
        \bfx 
        = 
        \bfb
    \end{equation*}
    always have a solution for any $\bfb \in \dsR^{4}$?

    \hint{} 
    Can you have a pivot position in each row of
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \bfa_3
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5 --- Properties of Matrix-Vector Product}

    If $A$ is $m \times n$ matrix, $\bfu, \bfv \in \dsR^n$, $c \in \dsR$, then
    \begin{enumerate}[a.]
        \item $A (\bfu + \bfv) = A \bfu + A \bfv$
        \item $A (c \bfu) = c (A \bfu$)
    \end{enumerate}

    \bomb{} You may be asked to prove such statements.
\end{frame}

\end{document}
