#!/usr/bin/env julia

# Your Julia script code goes here# A script to build all tex file

function compile(dir, file, logfile)
    cd(dir)
    try
        println("Compiling $file")
        run(`lualatex --interaction=batchmode $file \>/dev/null 2\>\&1`)
        println("Compiling $file done")
    catch
        open(logfile, "w+") do io
            msg = "Failed to compile $file"
            write(io, msg)
            println(msg)
        end;
    end
end

function main()
    log = tempname() .* ".log"
    println("See log in $log")
    rootdir = abspath(".")
    for (root, _, files) in walkdir(rootdir)
        for f in files
            basename, ext = splitext(f)
            if ext == ".tex" && root != rootdir && startswith(basename, r"welcome|lecture")
                dir = abspath(root)
                compile(dir, f, log)
            end
        end
    end
    cd(rootdir)
end

main()
