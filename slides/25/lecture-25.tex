\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{angles}

\title{Lecture \lecturenum{} --- Inner Product, Length, and Orthogonality}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{%
        Assignments%
        \footnote{\homeworknote{}}%
        \footnote{%
            \smiling{}
            Skip ``Angels in $\dsR^{2}$ and $\dsR^{3}$`` in 6.1.
        }%
    }%

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.1}{1, 5, 9, 13, 15, 29, 33, 35, 37}
        \end{column}
    \end{columns}
\end{frame}

\section{6 Orthogonality and Least Squares}

\begin{frame}[c]
    \frametitle{\zany{} Models}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{model} (noun)  --- a simplified description, especially a mathematical
            one, of a system or process to assist calculations and predictions.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

            \alert{Deep Learning} means using data to determine how a model behaves.

            \begin{flushright}
                --- 3Blue1Brown
            \end{flushright}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-fashion-model.jpg}
                \caption{%
                    When a model meets another model.
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A Simple Model}

    \think{} Can we build a model to predicate your grades based the number of classes
    you have missed?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=0, xmax=30,
                    ymin=0, ymax=120,
                    xtick={0,5,...,30},
                    ytick={0,20,...,100},
                    xlabel={Missed Classes},
                    ylabel={Grades},
                    x label style={at={(axis description cs:0.5,-0.1)},anchor=north, font=\footnotesize},
                    y label style={at={(axis description cs:-0.1,.5)},rotate=90,anchor=south, font=\footnotesize},
                    samples=40, % increase samples because of potential duplicate x values
                    clip=false,
                    scale only axis=true,
                    width=0.7\textwidth,
                    height=0.4\textwidth,
                    legend style={%
                        font=\footnotesize,
                        line width=0.4pt
                    },
                    ]
                    % Simulate the data with random noise around the line y = 95 - 2.7x
                    \addplot[
                        only marks,
                        scatter, % enable scatter to simulate the 'randomness' of data points
                        draw=BrightRed,
                        mark=*,
                        mark options={BrightRed, fill=BrightRed, scale=0.6},
                        ]
                        coordinates {
                            (0,95 + rand*10) (1,92.3 + rand*10) (2,89.6 + rand*10) (2,89.6 + rand*10) % Duplicate x at 2
                            (3,86.9 + rand*10) (4,84.2 + rand*10) (5,81.5 + rand*10) (6,78.8 + rand*10)
                            (7,76.1 + rand*10) (8,73.4 + rand*10) (9,70.7 + rand*10) (10,68 + rand*10)
                            (11,65.3 + rand*10) (12,62.6 + rand*10) (13,59.9 + rand*10) (14,57.2 + rand*10)
                            (15,54.5 + rand*10) (16,51.8 + rand*10) (17,49.1 + rand*10) (18,46.4 + rand*10)
                            (19,43.7 + rand*10) (20,41 + rand*10) (21,38.3 + rand*10) (22,35.6 + rand*10)
                            (23,32.9 + rand*10) (24,30.2 + rand*10) (25,27.5 + rand*10) (26,24.8 + rand*10)
                            (27,22.1 + rand*10) (28,19.4 + rand*10) (28,19.4 + rand*10) % Duplicate x at 28
                        };
                    \addlegendentry{Data};

                    % Add the fitted line without noise
                    \addplot[
                        smooth,
                        thick,
                        draw=DaylightBlue,
                        domain=-1:29,
                        ] {95-2.7*x};
                    \addlegendentry{Model};
                \end{axis}
            \end{tikzpicture}
        }%

        \caption{%
            A model to predict your grades
        }%
    \end{figure}
\end{frame}

\section{6.1 Inner Product, Length, and Orthogonality}

\subsection{Inner Product}

\begin{frame}
    \frametitle{The Inner Product}

    If $\bfu, \bfv \in \dsR^{n}$, then the \alert{inner/dot product} of them is
    the only entry in the $1 \times 1$ matrix $\bfu^{T} \bfv$.

    In other words, since
    \begin{equation*}
        \bfu^{T} \bfv
        =
        \begin{bmatrix}
            u_1 & u_{2} & \dots & u_n
        \end{bmatrix}
        \begin{bmatrix}
            v_1 \\ v_2 \\ \vdots \\ v_n
        \end{bmatrix}
        =
        [
        u_1 v_1 + u_2 v_2 + \cdots + u_n v_n
        ]
    \end{equation*}
    we have
    \begin{equation*}
        \bfu \cdot \bfv = u_1 v_1 + u_2 v_2 + \cdots + u_n v_n
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let
    \begin{equation*}
        \bfu
        =
        \begin{bmatrix}
            2 \\ -5 \\ -1
        \end{bmatrix}
        ,
        \quad
        \bfv
        =
        \begin{bmatrix}
            3 \\ 2 \\ -3
        \end{bmatrix}
        .
    \end{equation*}
    \cake{} What are $\bfu \cdot \bfv$ and $\bfv \cdot \bfu$?
\end{frame}


\begin{frame}
    \frametitle{Theorem 1: Properties of Inner Products}

    Let $\bfu, \bfv, \bfw \in \dsR^{n}$ and $c \in \dsR$.
    Then
    \vspace{-.5em}
    \begin{enumerate}[a.]
        \item $\bfu \cdot \bfv = \bfv \cdot \bfu$.
        \item $(\bfu+\bfv) \cdot \bfw = \bfu \cdot \bfw + \bfv \cdot \bfw$.
        \item $(c \bfu) \cdot \bfv = c (\bfu \cdot \bfv) = \bfu \cdot (c \bfv)$.
        \item $\bfu \cdot \bfu \ge 0$, and $\bfu \cdot \bfu = 0$ if and only if
            $\bfu = \bfzero$.
    \end{enumerate}

    \only<1>{%
        \puzzle{} See Exercises 29 and 30 for the proof.
    }%

    \only<2>{%
        Combining (b) and (c), we have
        \begin{equation*}
            (c_1 \bfu_{1} + \dots + c_p \bfu_{p}) \cdot \bfw
            =
            c_1 (\bfu_{1} \cdot \bfw) + \cdots + c_p (\bfu_{p} \cdot \bfw)
        \end{equation*}
    }%
\end{frame}

\subsection{The Length of a Vector}

\begin{frame}
    \frametitle{Length of Vectors in $\dsR^{2}$}

    \cake{} If $\bfv = \begin{bmatrix} a \\ b \end{bmatrix}$, how should we define
    its length?

    \begin{figure}[htpb]
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=0.7,
                every node/.style={black},
                declare function={
                    xmin=-3.0; xmax=3.0;
                    ymin=-0.0; ymax=4.0;
                },
                ]
                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                \draw[MintGreen, ultra thick] (xmin,ymax) -- (xmin,0) node[midway, label=right:$|b|$] {};
                \draw[MintGreen, ultra thick] (xmin,0) -- (0,0) node[midway, label=below:$|a|$] {};
                \drawvector{xmin,ymax}{$\bfv$};
            \end{tikzpicture}
        }%
        \caption{A visual representation of a complex number.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Length of vectors in $\dsR^{n}$}

    The \alert{length/norm} of a vector
    \begin{equation*}
        \bfv = \begin{bmatrix} v_1 \\ \vdots \\ v_n
        \end{bmatrix} \in \dsR^{n}
    \end{equation*}
    is defined by
    \begin{equation*}
        \norm{\bfv}
        =
        \sqrt{\bfv \cdot \bfv}
        =
        \sqrt{v_1^2 + \cdots + v_n^2}
    \end{equation*}

    \only<1>{%
        \cake{} What is $\norm{c \bfv}$?
    }%

    \only<2>{%
        \cake{} What about $\norm{\norm{\bfv}^{-1}\bfv}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Unit Vector}

    A vector whose length is $1$ is called a \alert{unit vector}.

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                % Define grid and axes limits as variables
                declare function={
                    xmin=-2.0; xmax=2.0;
                    ymin=-2.0; ymax=2.0;
                },
                % Scale of the entire picture
                scale=1.3,
                ]

                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Unit circle
                \draw[circle style] (0, 0) circle (1);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                % Random unit vectors
                \foreach \angle/\label in {40/$\bfu$, 160/$\bfv$, 280/$\bfw$}{
                    \draw[vector arrow] (0,0) -- (\angle:1cm) node[vector point, at end,
                        label={[label distance=0.0cm, black]\angle:\label}] {};
                }

                % Vectors (0, 1) and (1, 0)
                \drawvector{1, 0}{below right:$\bfe_1$};
                \drawvector{0, 1}{above left:$\bfe_2$};

            \end{tikzpicture}
        }%
        \caption{Unit vectors in $\dsR^{2}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Normalizing}

    For a nonzero vector $\bfv$, $\bfu = \norm{\bfv}^{-1} \bfv$ is
    the \emph{unit vector} in the same direction as $\bfv$.

    Computing $\bfu$ is called \alert{normalizing} $\bfv$.

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                % Define grid and axes limits as variables
                declare function={
                    xmin=-2.0; xmax=2.0;
                    ymin=-2.0; ymax=2.0;
                },
                % Scale of the entire picture
                scale=1.0,
                ]

                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Unit circle
                \draw[circle style] (0, 0) circle (1);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                % Random unit vectors
                \drawvector{3/2,4/2}{$\bfv$};
                \drawvector{3/5,4/5}{$\bfu$};
            \end{tikzpicture}
        }%
        \caption{Normalization of $\bfv$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} Let
    \begin{equation*}
        \bfv =
        \begin{bmatrix}
            1 \\ -2 \\ 2 \\ 0
        \end{bmatrix}
    \end{equation*}
    Find a unit vector $\bfu$ in the direction of $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let $W$ be the subspace of $\dsR^2$ spanned by $\bfx = \begin{bmatrix} \frac{2}{3} \\
    1 \end{bmatrix}$.

    \only<1>{%
        \cake{}
        Can you think of a basis of $W$?
    }%

    \only<2>{%
        \think{}
        How to find a unit vector $\bfz$ that is a basis for $W$.
    }%

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                % Define grid and axes limits as variables
                declare function={
                    xmin=-3.0; xmax=3.0;
                    ymin=-1.0; ymax=3.0;
                },
                % Scale of the entire picture
                scale=1.0,
                ]

                % Define the coordinates of the vector x
                \coordinate (x) at (2/3,1);

                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Unit circle
                \draw[circle style] (0, 0) circle (1);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                \draw[BrightRed, thick] ($3*(x)$) -- ($-1*(x)$)
                    node[near start, black, below right] {$W$};
                \only<1>{%
                    \drawvector{x}{$\bfx$};
                }%
                \only<2>{%
                    \drawvector{$3*(x)$}{$3 \bfx$};
                }%
            \end{tikzpicture}
        }%
        \caption{What is a basis of unit length for $W$?}
    \end{figure}
\end{frame}

\subsection{Distance in \texorpdfstring{$\dsR^{n}$}{R^n}}

\begin{frame}
    \frametitle{Distance in $\dsR^{1}$}

    For $a$ and $b$ in $\dsR$, what is the distance between them?

    \only<3>{%
        Answer: The distance between $a$ and $b$ is $\abs{a-b}$.\footnote{%
            This sentence is written by \emoji{robot} \url{https://codeium.com/}
        }%

        \think{} Can we define this using vectors?
    }%

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}
                \only<1>{%
                    \coordinate (a) at (2,0);
                    \coordinate (b) at (8,0);
                }%
                \only<2>{%
                    \coordinate (a) at (6,0);
                    \coordinate (b) at (3,0);
                }%
                % Draw the number line
                \draw[grid style] (-0.5,0) -- (9.5,0);
                \foreach \x in {0,1,2,...,9}
                \draw[shift={(\x,0)},color=black] (0pt,3pt) -- (0pt,-3pt);
                \foreach \x in {0,1,2,...,9}
                \draw[shift={(\x,0)},color=black] (0pt,0pt) -- (0pt,-3pt) node[below] {$\x$};

                \only<1-2>{%
                    % Draw the points a and b
                    \node at ($(a)+(0,0.3)$) {$a$};
                    \node at ($(b)+(0,0.3)$) {$b$};
                }%

                % Draw the braces and annotate the distance
                \only<1>{%
                    \draw[decorate,decoration={brace,amplitude=10pt,mirror},yshift=-4pt]
                        ($(a)+(0,-1.0)$) -- ($(b)+(0,-1.0)$)
                        node [black,midway,yshift=-20pt] {\footnotesize $6$ units apart};
                }%
                \only<2->{%
                    \draw[decorate,decoration={brace,amplitude=10pt,mirror},yshift=-4pt]
                        ($(b)+(0,-1.0)$) -- ($(a)+(0,-1.0)$)
                        node [black,midway,yshift=-20pt] {\footnotesize $3$ units apart};
                }%
                \draw[dashed, thin] ($(a)+(0,-0.7)$) -- ($(a)+(0,-1.0)$);
                \draw[dashed, thin] ($(b)+(0,-0.7)$) -- ($(b)+(0,-1.0)$);
                \only<3>{%
                    \drawvector{a}{$\bfa=[a]$};
                    \drawvector{b}{$\bfb=[b]$};
                }%
            \end{tikzpicture}
        }%
        \caption{Distances in $\dsR^{1}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distance in $\dsR^{2}$}
    For $\bfu, \bfv \in \dsR^{2}$, how should we define \alert{distance between
    $\bfu$ and $\bfv$}?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                % Define grid and axes limits as variables
                declare function={
                    xmin=-2.0; xmax=3.0;
                    ymin=-1.0; ymax=4.0;
                },
                % Scale of the entire picture
                scale=1.0,
                ]

                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                \draw[vector line, SeaGreen] (2,3) -- (-1,2) node[midway, above, black] {$\dist(\bfu, \bfv)$};
                \drawvector{2,3}{$\bfv$};
                \drawvector{-1,2}{$\bfu$};
            \end{tikzpicture}
        }%
        \caption{What is the distance between $\bfu$ and $\bfv$?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distance in $\dsR^{n}$}
    For $\bfu, \bfv \in \dsR^{n}$, the \alert{distance between $\bfu$ and $\bfv$} is
    defined by
    \begin{equation*}
        \dist(\bfu, \bfv) = \norm{\bfu - \bfv}.
    \end{equation*}

    \cake{} If
    $\bfu = \begin{bmatrix} u_1 \\ \vdots \\ u_n \end{bmatrix}$,
    and
    $\bfv = \begin{bmatrix} v_1 \\ \vdots \\ v_n \end{bmatrix}$
    then what is $\dist(\bfu, \bfv) $?
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    What is the distance between $\bfu = (7, 1)$ and $\bfv = (3,2)$?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                % Define grid and axes limits as variables
                declare function={
                    xmin=-7.0; xmax=7.0;
                    ymin=-2.0; ymax=2.0;
                },
                % Scale of the entire picture
                scale=0.7,
                ]

                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                \coordinate (u) at (7,1);
                \coordinate (v) at (3,2);

                \draw[thin, dashed] ($(u)-(v)$) -- (u);
                \draw[thin, dashed] ($(u)-(v)$) -- ($-1*(v)$);
                \draw[vector line, SeaGreen] (u) -- (v) node[midway, above] {$\norm{\bfu - \bfv}$};
                \drawvector{v}{$\bfv$};
                \drawvector{u}{$\bfu$};
                \drawvector[JuliaOrange]{$-1*(v)$}{$-\bfv$};
                \drawvector[JuliaOrange]{$(u)-(v)$}{$\bfu-\bfv$};
            \end{tikzpicture}
        }%
        \caption{What is the distance between $\bfu$ and $\bfv$?}
    \end{figure}
\end{frame}

\subsection{Orthogonal Vectors}

\begin{frame}
    \frametitle{Orthogonal Vectors in $\dsR^{2}$}

    In $\dsR^{2}$, the lines determined by $\bfu$ and $\bfv$ are
    called \alert{orthogonal/perpendicular}
    if
    \begin{equation*}
        \dist(\bfu, \bfv) = \dist(\bfu, -\bfv)
    \end{equation*}
    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{%
                \begin{tikzpicture}[
                    % Define grid and axes limits as variables
                    declare function={
                        xmin=-7.0; xmax=7.0;
                        ymin=-2.0; ymax=2.0;
                    },
                    % Scale of the entire picture
                    scale=0.7,
                    every node/.style={
                        black
                    }
                    ]

                    \begin{scope}[rotate=-45]
                        \coordinate (u) at (0,4);
                        \coordinate (v) at (2,0);
                        \coordinate (o) at (0,0);

                        \draw[thin, SeaGreen] (v) -- (u) node[midway, below right] {$\norm{\bfu - \bfv}$};
                        \draw[thin, SeaGreen] ($-1*(v)$) -- (u) node[midway, above left] {$\norm{\bfu - (-\bfv)}$};
                        \draw[thin, BrightRed] ($-1.5*(v)$) -- ($1.5*(v)$);
                        \draw[thin, BrightRed] ($-0.5*(u)$) -- ($1.5*(u)$);
                        \drawvector{v}{below left:$\bfv$};
                        \drawvector{u}{above:$\bfu$};
                        \drawvector{$-1*(v)$}{below left:$-\bfv$};

                        % Draw the right angle
                        \pic [draw,red,thick,angle radius=0.3cm] 
                                {right angle = v--o--u};
                    \end{scope}
                \end{tikzpicture}
            }%
            \caption{Two orthogonal vectors}
        \end{figure}
    }%
    \only<2>{%
        This happens if and only if $\bfu \cdot \bfv = 0$.

        \hint{} In other words $\bfu \cdot \bfv$ measures the angle between $\bfu$ and $\bfv$.
    }%
\end{frame}

\begin{frame}
    \frametitle{Orthogonal Vectors in $\dsR^{n}$}

    In $\dsR^{n}$, we say two vectors $\bfu$ and $\bfv$ are \alert{orthogonal}
    if $\bfu \cdot \bfv = 0$.

    \cake{} Which vector is orthogonal to all vectors?

    \cake{} Which vector is orthogonal to itself?
\end{frame}

\begin{frame}
    \frametitle{Theorem 2 --- The Pythagorean Theorem}

    Two vectors $\bfu$ and $\bfv$ are orthogonal if and only if
    \begin{equation*}
        \norm{\bfu+\bfv}^{2}
        =
        \norm{\bfu}^{2}
        +
        \norm{\bfv}^{2}
    \end{equation*}

    \pause{}

    \begin{figure}
        \begin{center}
            \includegraphics[width=0.5\linewidth]{pythagoras.jpg}
        \end{center}
    \end{figure}
\end{frame}

\subsection{Orthogonal Complements}

\begin{frame}
    \frametitle{Orthogonal to a Subspace}

    If $\bfz$ is orthogonal to every vector in a subspace $W$ of $\dsR^{n}$,
    then we say that $\bfz$ is \alert{orthogonal} to $W$.

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=1.5,
                x  = {(-0.7cm,-0.7cm)},
                y  = {(1.0cm,-0.2cm)},
                z  = {(0cm,1.0cm)}]

                % Original vectors
                \coordinate (o) at (0, 0, 0);
                \coordinate (v1) at (1.5, 1.5, 0.0);
                \coordinate (v2) at (-1.5, 1.5, 0.0);
                \coordinate (v3) at (-1.5, -1.5, 0.0);
                \coordinate (v4) at (1.5, -1.5, 0.0);

                % Adding the surface
                \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) -- cycle;
                \node at (0.8, -0.5, 0.0)  {$W$};

                % Set up the vectors
                \coordinate (w1) at (-0.5, 0.5, 0.0);
                \coordinate (w2) at (-0.2, -0.5, 0.0);
                \coordinate (w3) at (0.9, 0.9, 0.0);
                \coordinate (z) at (0.0, 0.0, 1.0);

                % Draw the axes
                \draw[axis 3d] (0,0,0) -- (1.5,0,0) node[anchor=north east]{$x_1$};
                \draw[axis 3d] (0,0,0) -- (0,1.5,0) node[anchor=north west]{$x_2$};
                \draw[axis 3d] (0,0,0) -- (0,0,1.5) node[anchor=south]{$x_3$};
                \drawvectorIII{z}{above left:$\bfz$};
                \tikzset{
                    vector arrow/.style={vector line, ->, JuliaOrange},
                    red right angle/.style={draw,red,thick,angle radius=0.3cm}
                }

                \drawvectorIII{w1}{above right:$\bfw_1$};
                \drawvectorIII{w2}{above left:$\bfw_2$};
                \drawvectorIII{w3}{below left:$\bfw_3$};
                % Draw the right angle
                \pic [red right angle] {right angle = w1--o--z};
            \end{tikzpicture}
            \caption{The vector $\bfz$ is orthogonal to $W$}
        \end{figure}
    }%

    \only<2>{%
        For example, if
        \begin{equation*}
            \bfz =
            \begin{bmatrix}
                0 \\
                0 \\
                1
            \end{bmatrix}
            ,
            \qquad
            W
            =
            \left\{
            \begin{bmatrix}
                x_1 \\
                x_2 \\
                0
            \end{bmatrix}
            :
            x_1, x_2 \in \dsR
            \right\}
        \end{equation*}
        Then $\bfz$ is orthogonal to $W$.
    }%
\end{frame}

\begin{frame}
    \frametitle{Orthogonal Complements}

    The sets of all vectors which are orthogonal to $W$ is called its
    \alert{orthogonal complement},
    denoted by $W^{\perp}$ ($W$ perpendicular).

    \only<2->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=1.5,
                x  = {(-0.7cm,-0.7cm)},
                y  = {(1.0cm,-0.2cm)},
                z  = {(0cm,1.0cm)}]

                % Original vectors
                \coordinate (o) at (0, 0, 0);
                \coordinate (v1) at (1.5, 1.5, 0.0);
                \coordinate (v2) at (-1.5, 1.5, 0.0);
                \coordinate (v3) at (-1.5, -1.5, 0.0);
                \coordinate (v4) at (1.5, -1.5, 0.0);

                \only<3>{%
                    \draw[vector line] (0,0,0) -- (0,0,-0.7);
                }%

                \only<2-3>{%
                    % Adding the surface
                    \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) -- cycle;
                }%

                \only<2>{%
                    \node at (0.8, -0.5, 0.0)  {$W$};
                }%

                \only<3>{%
                    \draw[vector line] (0,0,0) -- (0,0,2.1) node[midway, right, black]{$W$};
                }%

                % Draw the axes
                \draw[axis 3d] (0,0,0) -- (1.5,0,0) node[anchor=north east]{$x_1$};
                \draw[axis 3d] (0,0,0) -- (0,1.5,0) node[anchor=north west]{$x_2$};
                \draw[axis 3d] (0,0,0) -- (0,0,1.5) node[anchor=west]{$x_3$};

                \only<4>{%
                    \filldraw[surface yellow] (1, 0, 0) -- (1, 1, 0) -- (1, 1, 1) -- (1, 0, 1) -- cycle;
                    \filldraw[surface yellow] (0, 0, 1) -- (0, 1, 1) -- (1, 1, 1) -- (1, 0, 1) -- cycle;
                    \filldraw[surface yellow] (0, 1, 0) -- (0, 1, 1) -- (1, 1, 1) -- (1, 1, 0) -- cycle;
                    \node at (0.0, 1.2, 1.2)  {$W = \dsR^{3}$};
                }%
            \end{tikzpicture}
            \caption{\cake{} %
                If $W$ is the %
                \only<2>{%
                    $x_1x_2$ plane
                }%
                \only<3>{%
                    $x_3$ axis
                }%
                \only<4>{%
                    $\dsR^{3}$
                }%
                , what is $W^{\perp}$?
            }
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you think of a vector $\bfx$ and a subspace $W$ such that $\bfx$ is in both $W$
    and $W^{\perp}$?
\end{frame}

\begin{frame}
    \frametitle{Properties of orthogonal complements}

    \begin{enumerate}
        \item A vector $\bfx$ is in $W^{\perp}$ if and only if $\bfx$ is orthogonal to every vector in a set that spans $W$.
        \item $W^{\perp}$ is a subspace of $\dsR^n$.
    \end{enumerate}
    \puzzle{} The proofs are in Exercises 37 and 38.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=1.2,
            x  = {(-0.7cm,-0.7cm)},
            y  = {(1.0cm,-0.2cm)},
            z  = {(0cm,1.0cm)}]

            % Original vectors
            \coordinate (o) at (0, 0, 0);
            \coordinate (v1) at (1.5, 1.5, 0.0);
            \coordinate (v2) at (-1.5, 1.5, 0.0);
            \coordinate (v3) at (-1.5, -1.5, 0.0);
            \coordinate (v4) at (1.5, -1.5, 0.0);

            % Adding the surface
            \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) -- cycle;
            \node at (0.8, -0.5, 0.0)  {$W$};

            % Draw the axes
            \draw[axis 3d] (0,0,0) -- (1.5,0,0) node[anchor=north east]{$x_1$};
            \draw[axis 3d] (0,0,0) -- (0,1.5,0) node[anchor=north west]{$x_2$};
            \draw[axis 3d] (0,0,0) -- (0,0,1.5) node[anchor=west]{$x_3$};
            \drawvectorIII{0, 0, 0.6}{above left:$\bfz$};
            \drawvectorIII{1, 0, 0}{below right:$\bfe_1$};
            \drawvectorIII{0, 1, 0}{below right:$\bfe_2$};
        \end{tikzpicture}
        \caption{If $\bfz$ is orthogonal to $\bfe_{1}$ and $\bfe_{2}$, then it is in $W^{\perp}$.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 7 (4.5) --- The big picture of linear algebra}

    Let
    $A =
    \begin{bmatrix}
        3 & 0 & -1 \\
        3 & 0 & -1 \\
        4 & 0 & 5
    \end{bmatrix}
    $
    .
    Then
    $(\rowspace A)^{\perp} = \nullspace A$,
    $(\colspace A)^{\perp} = \nullspace A^{T}$.

    \think{} Is this true in general?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{4.5-fig-2.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 3 --- $\nullspace A$, $\rowspace A$, $\colspace A$}

    Let $A$ be an $m \times n$ matrix.
    Then
    \begin{equation*}
        (\rowspace A)^{\perp} = \nullspace A
        ,
        \qquad
        (\colspace A)^{\perp} = \nullspace A^{T}
        .
    \end{equation*}

    \only<2-3>{%
        \begin{figure}[htpb]
            \centering
            \begin{center}
                \begin{tikzpicture}[scale=1.5]
                    \only<2>{%
                        % Define the outer area A
                        \fill[MintCream] (0,0) .. controls (1,1) and (2,1) .. (3,0) .. controls (3.5,-1) and (2,-2) .. (1,-1) .. controls (0.5,-0.5) and (-0.5,-0.5) .. cycle;

                        % Define the inner area B
                        \fill[PastelYellow] (0.5,-0.5) .. controls (0.5,0.5) and (2,0.5) .. (2.4,0) .. controls (2,-0.5) and (1.5,-0.5) .. cycle;

                        % Labels for A and B
                        \node at (2.0, -0.9) {\large $(\rowspace A)^{\perp}$};
                        \node at (1.5, 0) {\large $\nullspace A$};
                    }%
                    \only<3>{%
                        % Define the outer area A
                        \fill[PastelYellow] (0,0) .. controls (1,1) and (2,1) .. (3,0) .. controls (3.5,-1) and (2,-2) .. (1,-1) .. controls (0.5,-0.5) and (-0.5,-0.5) .. cycle;

                        % Define the inner area B
                        \fill[MintCream] (0.5,-0.5) .. controls (0.5,0.5) and (2,0.5) .. (2.4,0) .. controls (2,-0.5) and (1.5,-0.5) .. cycle;

                        % Labels for A and B
                        \node at (2.0, -0.9) {\large $\nullspace A$};
                        \node at (1.5, 0) {\large $(\rowspace A)^{\perp}$};
                    }%
                \end{tikzpicture}
            \end{center}
            \caption{%
                \only<2>{%
                    Step 1: Show that $\nullspace A \subseteq (\rowspace A)^{\perp}$.
                }%
                \only<3>{%
                    Step 2: Show that $(\rowspace A)^{\perp} \subseteq \nullspace A$.
                }%
            }%
        \end{figure}
    }%
    \only<4>{%
        Step 1: Show that $\nullspace A \subseteq (\rowspace A)^{\perp}$.
    }
    \only<5>{%
        Step 2: Show that $(\rowspace A)^{\perp} \subseteq \nullspace A$.
    }%
    \only<4-5>{%
        If
        \begin{equation*}
            A = 
            \begin{bmatrix}
                a_{11} & a_{12} & \dots & a_{1n} \\
                a_{21} & a_{22} & \dots & a_{2n} \\
                \vdots & \vdots & \ddots & \vdots \\
                a_{m1} & a_{m2} & \dots & a_{mn}
            \end{bmatrix}
            ,
            \qquad
            \bfx =
            \begin{bmatrix}
                x_1 \\
                x_2 \\
                \vdots \\
                x_n
            \end{bmatrix}
        \end{equation*}
    }%
    \only<4>{%
        \cake{}
        Then what does $\bfx \in \nullspace A$ mean?
    }%
    \only<5>{%
        \cake{}
        Then what does $\bfx \in (\rowspace A)^{\perp}$ mean?
    }%
    \only<6>{%
        Step 3: \cake{} How can get the second equation from the first?
    }%
\end{frame}

%\section{Angles in \texorpdfstring{$\dsR^2$}{R^2} and \texorpdfstring{$\dsR^3$}{R^3}}
%
%\begin{frame}
%    \frametitle{Angles}
%
%    If $\bfu$ and $\bfv$ are in either $\dsR^2$ or $\dsR^3$, then
%    \begin{equation*}
%        \bfu \cdot \bfv
%        =
%        \norm{\bfu} \norm{\bfv} \cos \theta
%        ,
%    \end{equation*}
%    where $\theta$ is the angle between the two.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{6.1-09.jpg}
%    \end{figure}
%
%    Proof --- By \emph{the law of cosine}
%    \begin{equation*}
%        \norm{\bfu-\bfv}^2
%        =
%        \norm{\bfu}^2
%        +
%        \norm{\bfv}^2
%        -
%        2
%        \norm{\bfu}
%        \norm{\bfv}
%        \cos(\theta)
%    \end{equation*}
%    Thus, \dots
%\end{frame}

\end{document}
