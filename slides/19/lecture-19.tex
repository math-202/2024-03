\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Digital Signal Processing, Application to Difference Equations}

\begin{document}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.7}{1, 5, 7, 9, 11, 23, 25, 27, 29, 31}
        \end{column}
    \end{columns}
\end{frame}

\section{4.7 Digital Signal Processing}

\subsection{Discrete-Time Signals}

\begin{frame}
    \frametitle{Music signal}

    The sounds produced from a $CD$ are produced from music that has been sampled
    $44,100$ times per second.
    \begin{figure}[htpb]
        \centering
            \includegraphics<1>[width=0.8\linewidth]{cd.jpg}%
            \only<2>{%
                \begin{tikzpicture}
                    \begin{axis}[
                        axis lines=middle,
                        xmin=-6, xmax=26,
                        ymin=-2.1, ymax=2.1,
                        xtick=\empty,
                        ytick=\empty,
                        xlabel=$t$,
                        ylabel=$y$,
                        clip=false,
                        scale only axis=true,
                        % set `width' and `height' to the desired values
                        width=0.9\textwidth,
                        height=0.3\textwidth,
                        ]

                        \addplot[
                            smooth,
                            DaylightBlue,
                            thick,
                            domain=-5:25,
                            samples=200,
                        ] {0.8*sin(deg(x)*0.5) + 0.6*cos(deg(x)*0.7) - 0.4*sin(deg(x)*1.1) + 0.2*cos(deg(x)*1.5) - 0.1*sin(deg(x)*2)};

                        \addplot[
                            ycomb,
                            draw=BrightRed,
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-5:25,
                            samples=61,
                        ] {0.8*sin(deg(x)*0.5) + 0.6*cos(deg(x)*0.7) - 0.4*sin(deg(x)*1.1) + 0.2*cos(deg(x)*1.5) - 0.1*sin(deg(x)*2)};
                    \end{axis}
                \end{tikzpicture}
            }%
        \caption{\only<1>{CD (Compact Disc)}\only<2>{Sampled data from a music signal}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \alert{Digital Signal Processing} (DSP) involves manipulating and analyzing digital
    signals.

    Discrete-time signals (doubly infinite sequences), sampled versions of continuous signals, are central
    to DSP.

    Vector spaces of discrete-time signals provide a framework for DSP concepts.

    Linear transformations, like \alert{Linear Time Invariant}  
    and Fourier analysis, operate within this vector space.
\end{frame}

\begin{frame}
    \frametitle{Discrete-Time Signals}

    The vector space of discrete-time signals, denoted by$\dsS$, contains 
    all doubly infinite sequences in the form of
    \begin{equation*}
        \{y_{k}\}
        =
        \dots, y_{-2}, y_{-1}, y_{0}, y_{1}, y_{2}, \dots
    \end{equation*}

    \only<2->{%
        For example,
    }%
    \only<2>{$\{y_k\} = \{0.9^{k}\} = (\dots, 0.9^{-2}, 0.9^{-1}, 0.9^{0}, 0.9^{1}, 0.9^{2}, \dots)$ }%
    \only<3>{$\{y_k\} =  (\dots, 0, 0, 1, 0, 0, \dots)$ }%
    \only<4>{$\{y_k\} =  (\dots, 1, 1, 1, 1, 1, \dots)$ }%
    \only<5>{$\{y_k\} =  \{\cos(1/6 \pi k + 1/4 \pi)\}$ }%
    \only<2->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-16, xmax=16,
                    ymin=-7.0, ymax=7.5,
                    xtick={-15,-10,-5,...,15},
                    ytick=\empty,
                    xlabel=$k$,
                    ylabel=$y$,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.9\textwidth,
                    height=0.35\textwidth,
                    axis background/.style={fill=white}
                    ]

                    \only<2>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-15:15,
                            samples=31,
                            ] {0.9^x};
                    }%
                    \only<3>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-15:15,
                            samples=31,
                            ] {ifthenelse(x == 0, 6, 0)};
                    }%
                    \only<4>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-15:15,
                            samples=31,
                            ] {6};
                    }%
                    \only<5>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-15:15,
                            samples=31,
                            ] {6*sin(deg(x*pi/6 + pi/4))};
                    }%
                \end{axis}
            \end{tikzpicture}
            \caption{%
                \only<2>{%
                    An exponential signal
                }%
                \only<3>{%
                    The delta signal $\delta$
                }%
                \only<4>{%
                    A constant signal
                }%
                \only<5>{%
                    A periodic signal
                }%
            }%
        \end{figure}
    }%
\end{frame}

\subsection{Linear Time Invariant}

\begin{frame}
    \frametitle{Example 1}

    Let $S$ be the transformation that shifts a signal one unit to the right.

    \only<1>{%
        In other words, 
        \begin{equation*}
            \begin{aligned}
                \{y_{k}\} = (\dots, y_{-2}, y_{-1}, & \textcolor{BrightRed}{y_{0}}, y_{1}, y_{2}, \dots) \\
                S(\{y_{k}\}) = (\dots, y_{-1}, y_{0}, & \textcolor{BrightRed}{y_{1}}, y_{2}, y_{3}, \dots) \\
            \end{aligned}
        \end{equation*}
    }%

    \pause{}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=-6, xmax=6,
                ymin=-0.0, ymax=1.3,
                xtick={-5,...,5},
                ytick={0, 1},
                xlabel=$k$,
                ylabel=$y$,
                clip=false,
                scale only axis=true,
                % set `width' and `height' to the desired values
                width=0.9\textwidth,
                height=0.4\textwidth,
                axis background/.style={fill=white},
                legend style={line width=0.5pt},
                line width=2pt,
                axis line style={thin},
                ]

                \only<2->{%
                    \addplot[
                        ycomb,
                        draw=DaylightBlue,
                        mark=square*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=1.1},
                        domain=-5:5,
                        samples=11,
                        ] {ifthenelse(x == 0, 1, 0)};
                    \addlegendentry{$\delta$};
                }%
                \only<3>{%
                    \addplot[
                        ycomb,
                        draw=JuliaOrange,
                        mark=*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=1.0},
                        domain=-5:5,
                        samples=11,
                        ] {ifthenelse(x == 1, 1, 0)};
                        \addlegendentry{$S(\delta)$};
                }%
                \only<4>{%
                    \addplot[
                        ycomb,
                        draw=JuliaOrange,
                        mark=*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=1.0},
                        domain=-5:5,
                        samples=11,
                        ] {ifthenelse(x == 2, 1, 0)};
                        \addlegendentry{$S^{2}(\delta)$};
                }%
                \only<5>{%
                    \addplot[
                        ycomb,
                        draw=JuliaOrange,
                        mark=*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=1.0},
                        domain=-5:5,
                        samples=11,
                        ] {ifthenelse(x == -1, 1, 0)};
                        \addlegendentry{$S^{-1}(\delta)$};
                }%
                \only<6>{%
                    \addplot[
                        ycomb,
                        draw=JuliaOrange,
                        mark=*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=1.0},
                        domain=-5:5,
                        samples=11,
                        ] {ifthenelse(x == -2, 1, 0)};
                        \addlegendentry{$S^{-2}(\delta)$};
                }%
            \end{axis}
        \end{tikzpicture}
        \caption{%
            The delta signal $\delta$\only<3->{ shifted}
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\acf{lti}}

    \only<1->{%
        A transformation $T: \dsS \mapsto \dsS$ is an \ac{lti} 
        if for all signals $\{x_{k}\}, \{y_{k}\} \in \dsS$ and scalars $c \in \dsR$:
        \begin{itemize}
            \only<1-4>{%
                \item[(i)] $T(\{x_{k} + y_{k}\}) = T(\{x_{k}\}) + T(\{y_{k}\})$ 
            }%
            \only<2-4>{%
                \item[(ii)] $T(c \{x_{k}\}) = c T(\{x_{k}\})$
            }%
            \only<4->{%
                \item[(iii)] 
                    if $T(\{x_{k}\}) = \{y_{k}\}$, 
                    then $T(\{x_{k + q}\}) = \{y_{k + q}\}$ for all $q \in \dsZ$
            }%
        \end{itemize}
    }%
    
    \only<2>{%
        \cake{} What do we transformations that satisfy (i) and (ii)?
    }%

    \only<3>{%
        \begin{block}{Theorem 16}
            A \acf{lti} is a special type of linear
            transformation.
        \end{block}
    }%
    \only<5->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=6,
                    ymin=-1.0, ymax=1.3,
                    xtick={-5,...,5},
                    ytick={0, 1},
                    xlabel=$k$,
                    ylabel=$y$,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.9\textwidth,
                    height=0.4\textwidth,
                    axis background/.style={fill=white},
                    legend style={font=\footnotesize, line width=0.5pt},
                    line width=2pt,
                    axis line style={thin},
                    ]

                    \only<5>{%
                        \addplot[
                            ycomb,
                            draw=DaylightBlue,
                            mark=square*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.1},
                            domain=-5:5,
                            samples=11,
                            ] {ifthenelse(x == 0, 1, 0)};
                        \addlegendentry{$\{x_{k}\}$};
                        \addplot[
                            ycomb,
                            draw=JuliaOrange,
                            mark=*,
                            mark options={JuliaOrange, fill=JuliaOrange, scale=0.9},
                            domain=-5:5,
                            samples=11,
                            ] {ifthenelse(x == 1, 1, 0)};
                        \addlegendentry{$\{y_k\} = S(\{x_{k}\})$};
                    }%
                    \only<6>{%
                        \addplot[
                            ycomb,
                            draw=DaylightBlue,
                            mark=square*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.1},
                            domain=-5:5,
                            samples=11,
                            ] {ifthenelse(x == -3, 1, 0)};
                        \addlegendentry{$\{x_{k+3}\}$};
                        \addplot[
                            ycomb,
                            draw=JuliaOrange,
                            mark=*,
                            mark options={JuliaOrange, fill=JuliaOrange, scale=0.9},
                            domain=-5:5,
                            samples=11,
                            ] {ifthenelse(x == -2, 1, 0)};
                        \addlegendentry{$\{y_{k+3}\} = S(\{x_{k+3}\})$};
                    }%
                \end{axis}
            \end{tikzpicture}
            \caption{%
                The transformation $S$ in Example 1 is an \ac{lti}
            }%
        \end{figure}
    }%
\end{frame}

\subsection{Digital Signal Processing}

\begin{frame}
    \frametitle{Example 2 --- Smoothing}
    The \alert{moving average \ac{lti}} transformation
    with period $m$ is defined by
    \begin{equation*}
        M_{m}(\{x_{k}\})
        =
        \{y_{k}\}
        \quad
        \text{where}
        \quad
        y_{k}
        =
        \frac{1}{m}
        \sum_{j=k-m+1}^{k} x_{j}
    \end{equation*}
    \only<1>{%
        Taking the moving average makes it easier to spot trends.
    }%

    \only<2->{%
        \begin{figure}[htpb]
            \centering
            \only<2>{%
                \includegraphics[width=0.5\textwidth]{moving_average_plot_widow_2.pdf}
            }%
            \only<3>{%
                \includegraphics[width=0.5\textwidth]{moving_average_plot_widow_4.pdf}
            }%
            \only<4>{%
                \includegraphics[width=0.5\textwidth]{moving_average_plot_widow_8.pdf}
            }%
            \caption{%
                Moving average with period 
                \only<2>{%
                    $m = 2$
                }%
                \only<3>{%
                    $m = 4$
                }%
                \only<4>{%
                    $m = 8$
                }%
            }%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Auralization}

    \alert{Auralization}  in DSP refers to the process of 
    improving acoustic properties of a generated sounds.

    Let $T = 44,100$. The following picture shows how combining sounds
    enhances $\{\cos(440 \pi k /T)\}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=\textwidth]{4.7-fig-4-1.pdf}
        \includegraphics<2>[width=\textwidth]{4.7-fig-4-2.pdf}
        \includegraphics<3>[width=\textwidth]{4.7-fig-4-3.pdf}
        \includegraphics<4>[width=\textwidth]{4.7-fig-4-4.pdf}
    \end{figure}
\end{frame}


\subsection{Generating the Bases for Subspaces of \texorpdfstring{$\dsS$}{S}}

\begin{frame}
    \frametitle{Signals of length $n$}
    Let $\dsS_{n}$ be the set of signals of length $n$.

    In other words, $\{y_{k}\} \in \dsS_{n}$ if and only if
    $y_{k} = 0$ when $k < 0$ or $k > n$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=-6, xmax=16,
                ymin=-1.1, ymax=1.6,
                xtick={-5,0,5,10,15},
                ytick=\empty,
                xlabel=$k$,
                ylabel=$y$,
                clip=false,
                scale only axis=true,
                % set `width' and `height' to the desired values
                width=0.9\textwidth,
                height=0.3\textwidth,
                axis background/.style={fill=white}
                ]

                \addplot[
                    ycomb,
                    draw={BrightRed},
                    mark=*,
                    mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                    domain=-5:15,
                    samples=21,
                    ] {ifthenelse(x >= 0 && x <= 10, cos(deg(x)*pi/6), 0)};
            \end{axis}
        \end{tikzpicture}
        \caption{A signal in $\dsS_{10}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 17}

    \only<1-3>{%
        The set $\dsS_{n}$ is a subspace of $\dsS$.

        \begin{figure}
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=16,
                    ymin=-1.8, ymax=1.8,
                    xtick={-1,0,1,2},
                    ytick=\empty,
                    xlabel=$k$,
                    ylabel=$y$,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.9\textwidth,
                    height=0.5\textwidth,
                    legend style={font=\footnotesize,line width=0.5pt},
                    axis background/.style={fill=white},
                    line width=2pt,
                    axis line style={thin},
                    ]

                    \only<1>{%
                        \addplot[
                            ycomb,
                            draw={JuliaOrange},
                            mark=square*,
                            mark options={JuliaOrange, fill=JuliaOrange, scale=1.2},
                            domain=-5:15,
                            samples=21,
                            ] {ifthenelse(x >= 0 && x <= 10, sin(deg(x)*pi/6)+cos(deg(x)*pi/6), 0)};
                        \addlegendentry{$\{x_k\}+\{y_k\}$};
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=triangle*,
                            mark options={BrightRed, fill=BrightRed, scale=1.0},
                            domain=-5:15,
                            samples=21,
                        ] {ifthenelse(x >= 0 && x <= 10, cos(deg(x)*pi/6), 0)};
                        \addlegendentry{$\{x_k\}$};
                    }%
                    \only<2>{%
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=square*,
                            mark options={BrightRed, fill=BrightRed, scale=1.0},
                            domain=-5:15,
                            samples=21,
                            ] {ifthenelse(x >= 0 && x <= 10, 1.2*sin(deg(x)*pi/6), 0)};
                        \addlegendentry{$c \{y_k\}$};
                    }%
                    \only<1-2>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-5:15,
                            samples=21,
                            ] {ifthenelse(x >= 0 && x <= 10, sin(deg(x)*pi/6), 0)};
                        \addlegendentry{$\{y_k\}$};
                    }%
                    \only<3>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=square*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=0.8},
                            domain=-5:15,
                            samples=21,
                            ] {0};
                        \addlegendentry{$\{0\}$};
                    }%
                \end{axis}
            \end{tikzpicture}
            \caption{%
                \only<1-2>{%
                    $\dsS_{10}$ is closed under
                }%
                \only<1>{%
                    addition
                }%
                \only<2>{%
                    scalar multiplication
                }%
                \only<3>{%
                    $\dsS_{10}$ contains \blankshort{}.
                }%
            }%
        \end{figure}
    }%
    \only<4->{%
        The subspace $\dsS_{n}$ has a basis
        \begin{equation*}
            \scB_{n} = 
            \{\delta, S(\delta), \dots, S^{n}(\delta)\}
        \end{equation*}
    }%
    \only<4>{%
        Step 1: Show that any $\{y_n\} \in \dsS_{n}$ is a linear combination of $\scB_{n}$.
    }%
    \only<5>{%
        Step 2: Show $\scB_{n}$ is linearly independent.
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    Consider the following signal in $\dsS_{2}$ ---
    \begin{figure}
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=-1, xmax=6,
                ymin=-2.1, ymax=4.1,
                xtick={-1,...,6},
                ytick={-1,...,3},
                xlabel=$k$,
                ylabel=$y$,
                clip=false,
                scale only axis=true,
                % set `width' and `height' to the desired values
                width=0.9\textwidth,
                height=0.35\textwidth,
                legend style={font=\footnotesize, line width=0.5pt},
                axis background/.style={fill=white},
                grid=both,
                line width=2pt,
                axis line style={thin},
                ]

                \addplot[
                    ycomb,
                    draw={DaylightBlue},
                    mark=square*,
                    mark options={DaylightBlue, fill=DaylightBlue, scale=1.2},
                    domain=0:5,
                    samples=6,
                    ] {ifthenelse(x==0, 2, ifthenelse(x==1, 3, ifthenelse(x==2, -1, 0)))};
                \addlegendentry{$\{y_k\}$};
                \addplot[
                    ycomb,
                    draw={BrightRed},
                    mark=*,
                    mark options={BrightRed, fill=BrightRed, scale=1.1},
                    domain=-0:5,
                    samples=6,
                    ] {ifthenelse(x == 0, 1, 0)};
                \addlegendentry{$\{\delta\}$};
                \addplot[
                    ycomb,
                    draw={SeaGreen},
                    mark=*,
                    mark options={SeaGreen, fill=SeaGreen, scale=1.0},
                    domain=-0:5,
                    samples=6,
                    ] {ifthenelse(x == 1, 1, 0)};
                \addlegendentry{$\{S(\delta)\}$};
                \addplot[
                    ycomb,
                    draw={JuliaOrange},
                    mark=*,
                    mark options={JuliaOrange, fill=JuliaOrange, scale=0.9},
                    domain=-0:5,
                    samples=6,
                    ] {ifthenelse(x == 2, 1, 0)};
                \addlegendentry{$\{S^{2}(\delta)\}$};
            \end{axis}
        \end{tikzpicture}
    \end{figure}
    What is $\{y_{k}\}$'s coordinates with relative to 
    $\scB_{2} = \{\delta, S(\delta), S^{2}(\delta)\}$?
\end{frame}

\begin{frame}
    \frametitle{Finitely Supported Signals}
    If a signal $\{y_{k}\}$ contains only finitely many non-zero entries,
    then it is called a \alert{finitely supported} signal.

    Let $\dsS_{f}$ be the set of \alert{finitely supported} signals.

    \cake{} Is $\{0.9^{-k}\} \in \dsS_{f}$? What about $\delta$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 18}

    The set $\dsS_{f}$ is a subspace of $\dsS$. (\emoji{eyes} Section 4.1)

    It has a basis
    \begin{equation*}
        \scB_{f} = 
        \{S^{j}(\delta) : j \in \dsZ\}
    \end{equation*}

    \only<1>{%
        \cake{} What is the dimension of $\dsS_{f}$?
    }%
    \only<2>{%
        Proof is very similar to Theorem 17.
    }%
\end{frame}

\begin{frame}[label=current]
    \frametitle{Practice Problem 2}

    Show that
    \begin{equation*}
        T(\{x_{k}\}) = \{3 x_{k} - 2 x_{k - 1}\}
    \end{equation*}
    is \acf{lti}.
\end{frame}

\begin{frame}
    \frametitle{Practice Problem 3}

    Let
    \begin{equation*}
        T(\{x_{k}\}) = \{3 x_{k} - 2 x_{k - 1}\}.
    \end{equation*}
    Find a non-zero signal $\{x_{k}\}$ such that 
    \begin{equation*}
        T(\{x_{k}\}) = \{0\}.
    \end{equation*}
    \only<1>{%
        \cake{} In other words, we are asked to find a non-zero signal in \blankshort{} of
        $T$.
    }%
    \only<2>{%
        \hint{} Note that
        \begin{equation*}
            3 x_{k} - 2 x_{k-1} = 0
        \end{equation*}
        for all $k \in \dsZ$.
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{} Exercise 26}
    Let
    \begin{equation*}
        W = \left\{ \{x_k\} \mid x_k = 
            \begin{cases} 
                0 & \text{if } k < 0 \\
                r & \text{if } k \geq 0 
            \end{cases} \right\}
        \end{equation*}
     where  $r$  can be any real number.
     \begin{figure}
         \begin{tikzpicture}
             \begin{axis}[
                 axis lines=middle,
                 xmin=-3, xmax=6,
                 ymin=-0.1, ymax=2.5,
                 xtick={-2,...,6},
                 ytick={-1,...,2},
                 xlabel=$k$,
                 ylabel=$y$,
                 clip=false,
                 scale only axis=true,
                 % set `width' and `height' to the desired values
                 width=0.9\textwidth,
                 height=0.25\textwidth,
                 legend style={font=\footnotesize, line width=0.5pt},
                 axis background/.style={fill=white},
                 grid=both,
                 line width=2pt,
                 axis line style={thin},
                 ]

                 \addplot[
                     ycomb,
                     draw={DaylightBlue},
                     mark=square*,
                     mark options={DaylightBlue, fill=DaylightBlue, scale=1.2},
                     domain=-2:5,
                     samples=8,
                     ] {ifthenelse(x>=0, 1.5, 0)};
                 \addlegendentry{A signal in $W$ with $r=1.5$}
                 \end{axis}
         \end{tikzpicture}
     \end{figure}
     \cake{}
     What are the three things that we need to check to
     show that \( W \) is a subspace of \( S \).
\end{frame}

\end{document}
