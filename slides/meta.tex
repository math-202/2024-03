%! TEX program = lualatex
%&luaheader

\input{../colour.tex}
\input{../emoji.tex}

%-----------------------------------------------------------------------------------------
% Theme
%-----------------------------------------------------------------------------------------
\usetheme[progressbar=frametitle, block=fill, titleformat=smallcaps]{metropolis}
\metroset{subsectionpage=progressbar}

%-----------------------------------------------------------------------------------------
% Standard packages
%-----------------------------------------------------------------------------------------
% This can be omit -- https://tex.stackexchange.com/a/370279/8297
%\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}

\usepackage{amsmath,amsfonts,latexsym, xspace,amsthm,graphicx, amssymb}

% set path to images
\graphicspath{{../images/application}{../images/linear-algebra}{../images/people}{../images/misc}{../images/exercises}}

\usepackage{enumerate}

\usepackage{mathtools}

% This does not work with Beamer: https://tex.stackexchange.com/q/481022/8297
% \usepackage{cleveref}

%-----------------------------------------------------------------------------------------
% Links and references
%-----------------------------------------------------------------------------------------
% No need to load hyperref because beamer loads it.
% https://tex.stackexchange.com/a/264001/8297

%-----------------------------------------------------------------------------------------
% Captions
%-----------------------------------------------------------------------------------------
\usepackage[font={scriptsize},justification=centering]{caption}
\usepackage{subcaption}

% reduce space after caption
\captionsetup{belowskip=0pt}

%-----------------------------------------------------------------------------------------
% Matrix
%-----------------------------------------------------------------------------------------
% Increase matrix vertical spacing
% \renewcommand*{\arraystretch}{1.3}

%-----------------------------------------------------------------------------------------
% Figure
%-----------------------------------------------------------------------------------------
\newcommand{\figchar}[1]{%
  \begingroup\normalfont
  \includegraphics[height=\fontcharht\font`\B]{#1}%
  \endgroup
}
\usepackage[export]{adjustbox} %https://tex.stackexchange.com/a/91580/8297 

%-----------------------------------------------------------------------------------------
% For typesetting numbers
%-----------------------------------------------------------------------------------------
\usepackage{siunitx}

%-----------------------------------------------------------------------------------------
% Beamer
%-----------------------------------------------------------------------------------------
% For easier writing
\setbeamertemplate{background}[grid]

\usepackage[normalem]{ulem}

% Fixes the frame numbering
\usepackage{appendixnumberbeamer}

% Fix a warning with \appendix
% https://tex.stackexchange.com/a/567706/8297
\pdfstringdefDisableCommands{%
    \def\translate#1{#1}%
}

% Colour emph
\setbeamercolor{emph}{fg=Purple}
\renewcommand<>{\emph}[1]{%
    {\usebeamercolor[fg]{emph}\only#2{\itshape}#1}%
}
%\setbeamercolor{alerted text}{fg=Maroon}

% For shrinking vertical space (Better to have space)
% https://tex.stackexchange.com/a/38407/8297
% \usepackage{etoolbox}% http://ctan.org/pkg/etoolbox
%\AtBeginEnvironment{figure}{\vspace{-1em}}\AtEndEnvironment{figure}{\vspace{-1em}}

% For svg picture
\usepackage{svg}

% Not to create duplicate slides for using pause after item
\newcommand{\pauseafteritem}{\pause[\thebeamerpauses]}

% Change paragraph spacing
\newcommand{\colbigskip}{\setlength{\parskip}{\bigskipamount}}
\newcommand{\colmedskip}{\setlength{\parskip}{\medskipamount}}
\newcommand{\colsmallskip}{\setlength{\parskip}{\smallskipamount}}

%-----------------------------------------------------------------------------------------
% Table
%-----------------------------------------------------------------------------------------
\usepackage{array} % For defining column types
\usepackage{booktabs} % For better-looking tables
%\usepackage{siunitx} % For aligning numerical values
\usepackage{multirow}
\usepackage{booktabs}
\subtitle{\coursecode{} --- \coursename{}}

\author{Xing Shi Cai \texorpdfstring{\url{https://newptcai.gitlab.io}}{}}

\institute{Duke Kunshan University, Kunshan, China}

%-----------------------------------------------------------------------------------------
% Brackets
%-----------------------------------------------------------------------------------------
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

%-----------------------------------------------------------------------------------------
% Logic
%-----------------------------------------------------------------------------------------
\newcommand{\imp}{\rightarrow}
\renewcommand{\iff}{\Leftrightarrow}

%-----------------------------------------------------------------------------------------
% Linear Algebra
%-----------------------------------------------------------------------------------------
\usepackage{systeme}

\usepackage{etoolbox}
\newcommand\veclist[2][]{%
    \ifstrempty{#1}{%
        \ifstrempty{#2}{%
                \symbf{v}_{1}, \ldots, \symbf{v}_{p}% #1 are #2 are both empty
            }{%
                \symbf{#2}_{1}, \ldots, \symbf{#2}_{p}% #2 empty
            }%
    }{%
        \symbf{#2}_{1}, \ldots, \symbf{#2}_{#1}% #1 and #2 are not empty
    }%
}

\newcommand\Span[1]{\mathrm{Span}\left\{#1\right\}}

\newcommand{\bb}[1]{\cellcolor{DaylightBlue}{#1}}
\newcommand{\bbs}{\bb{\blacksquare{}}}
\newcommand{\bast}{\bb{\ast{}}}

\usepackage{amsmath}

\DeclareMathOperator{\mrow}{row}
\DeclareMathOperator{\mcol}{col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
%\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\nullspace}{Nul}
\DeclareMathOperator{\colspace}{Col}
\DeclareMathOperator{\rowspace}{Row}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\proj}{proj}

%-----------------------------------------------------------------------------------------
% Chemistry
%-----------------------------------------------------------------------------------------
\newcommand*\chem[1]{\ensuremath{\mathrm{#1}}}

%-----------------------------------------------------------------------------------------
% Exercise
%-----------------------------------------------------------------------------------------
\newcommand{\practice}{%
    \vspace{0.5em}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true]{block body example}%
        \scriptsize%
        \centering%
        \emoji{people-holding-hands}  Think, pair and share your solution.
    \end{beamercolorbox}%
}%

\newcommand{\poll}{%
    \emoji{ballot-box} Answer at \href{https://PollEv.com/xingshicai}{https://PollEv.com/xingshicai}
}%

\newenvironment{exerciseblock}[1]
{\begin{exampleblock}{\emoji{thinking} #1}}
{\end{exampleblock}}

\newcommand{\exercisequestion}[1]{%
    \smallskip
    \think{} #1%
}%

\newcommand{\tps}{\sweat{} Think-Pair-Share}

\newcommand{\exercise}{\think{} Exercise}

%-----------------------------------------------------------------------------------------
% Story
%-----------------------------------------------------------------------------------------
\newcommand{\story}{\section{Story Time (This will not appear in exams 😎️)}}

%-----------------------------------------------------------------------------------------
% Quote
%-----------------------------------------------------------------------------------------
\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\list{}{\leftmargin=0pt}\item\relax}

%-----------------------------------------------------------------------------------------
% Math fonts
%-----------------------------------------------------------------------------------------
% Allow unique letters in equations
\usefonttheme{professionalfonts}
% See: https://tex.stackexchange.com/a/584866/8297
\usepackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}
\setmathfont{Asana-Math.otf}
\setmathfont[range=\mathcal]{Latin Modern Math}


%-----------------------------------------------------------------------------------------
% Math symbols
%-----------------------------------------------------------------------------------------
%\usepackage{MnSymbol}
\usepackage{stmaryrd}
\usepackage{bbold} % mathbb

\newcommand{\bE}{\mathbb{E}}
\newcommand{\bN}{\mathbb{N}}

\newcommand\bfa{{\symbf{a}}}
\newcommand\bfb{{\symbf{b}}}
\newcommand\bfc{{\symbf{c}}}
\newcommand\bfd{{\symbf{d}}}
\newcommand\bfe{{\symbf{e}}}
\newcommand\bff{{\symbf{f}}}
\newcommand\bfg{{\symbf{g}}}
\newcommand\bfh{{\symbf{h}}}
\newcommand\bfi{{\symbf{i}}}
\newcommand\bfj{{\symbf{j}}}
\newcommand\bfk{{\symbf{k}}}
\newcommand\bfl{{\symbf{l}}}
\newcommand\bfm{{\symbf{m}}}
\newcommand\bfn{{\symbf{n}}}
\newcommand\bfo{{\symbf{o}}}
\newcommand\bfp{{\symbf{p}}}
\newcommand\bfq{{\symbf{q}}}
\newcommand\bfr{{\symbf{r}}}
\newcommand\bfs{{\symbf{s}}}
\newcommand\bft{{\symbf{t}}}
\newcommand\bfu{{\symbf{u}}}
\newcommand\bfv{{\symbf{v}}}
\newcommand\bfw{{\symbf{w}}}
\newcommand\bfx{{\symbf{x}}}
\newcommand\bfy{{\symbf{y}}}
\newcommand\bfz{{\symbf{z}}}

\newcommand\bfA{{\symbf{A}}}
\newcommand\bfB{{\symbf{B}}}
\newcommand\bfC{{\symbf{C}}}
\newcommand\bfD{{\symbf{D}}}
\newcommand\bfE{{\symbf{E}}}
\newcommand\bfF{{\symbf{F}}}
\newcommand\bfG{{\symbf{G}}}
\newcommand\bfH{{\symbf{H}}}
\newcommand\bfI{{\symbf{I}}}
\newcommand\bfJ{{\symbf{J}}}
\newcommand\bfK{{\symbf{K}}}
\newcommand\bfL{{\symbf{L}}}
\newcommand\bfM{{\symbf{M}}}
\newcommand\bfN{{\symbf{N}}}
\newcommand\bfO{{\symbf{O}}}
\newcommand\bfP{{\symbf{P}}}
\newcommand\bfQ{{\symbf{Q}}}
\newcommand\bfR{{\symbf{R}}}
\newcommand\bfS{{\symbf{S}}}
\newcommand\bfT{{\symbf{T}}}
\newcommand\bfU{{\symbf{U}}}
\newcommand\bfV{{\symbf{V}}}
\newcommand\bfW{{\symbf{W}}}
\newcommand\bfX{{\symbf{X}}}
\newcommand\bfY{{\symbf{Y}}}
\newcommand\bfZ{{\symbf{Z}}}

\newcommand\vbfd{{\vec{\symbf{d}}}}

\newcommand\bfzero{\symbf{0}}
\newcommand\bfone{\symbf{1}}

\newcommand\dsA{{\mathbb{A}}}
\newcommand\dsB{{\mathbb{B}}}
\newcommand\dsC{{\mathbb{C}}}
\newcommand\dsD{{\mathbb{D}}}
\newcommand\dsE{{\mathbb{E}}}
\newcommand\dsF{{\mathbb{F}}}
\newcommand\dsG{{\mathbb{G}}}
\newcommand\dsH{{\mathbb{H}}}
\newcommand\dsI{{\mathbb{I}}}
\newcommand\dsJ{{\mathbb{J}}}
\newcommand\dsK{{\mathbb{K}}}
\newcommand\dsL{{\mathbb{L}}}
\newcommand\dsM{{\mathbb{M}}}
\newcommand\dsN{{\mathbb{N}}}
\newcommand\dsO{{\mathbb{O}}}
\newcommand\dsP{{\mathbb{P}}}
\newcommand\dsQ{{\mathbb{Q}}}
\newcommand\dsR{{\mathbb{R}}}
\newcommand\dsS{{\mathbb{S}}}
\newcommand\dsT{{\mathbb{T}}}
\newcommand\dsU{{\mathbb{U}}}
\newcommand\dsV{{\mathbb{V}}}
\newcommand\dsW{{\mathbb{W}}}
\newcommand\dsX{{\mathbb{X}}}
\newcommand\dsY{{\mathbb{Y}}}
\newcommand\dsZ{{\mathbb{Z}}}

\newcommand\scA{{\mathcal{A}}}
\newcommand\scB{{\mathcal{B}}}
\newcommand\scC{{\mathcal{C}}}
\newcommand\scD{{\mathcal{D}}}
\newcommand\scE{{\mathcal{E}}}
\newcommand\scF{{\mathcal{F}}}
\newcommand\scG{{\mathcal{G}}}
\newcommand\scH{{\mathcal{H}}}
\newcommand\scI{{\mathcal{I}}}
\newcommand\scJ{{\mathcal{J}}}
\newcommand\scK{{\mathcal{K}}}
\newcommand\scL{{\mathcal{L}}}
\newcommand\scM{{\mathcal{M}}}
\newcommand\scN{{\mathcal{N}}}
\newcommand\scO{{\mathcal{O}}}
\newcommand\scP{{\mathcal{P}}}
\newcommand\scQ{{\mathcal{Q}}}
\newcommand\scR{{\mathcal{R}}}
\newcommand\scS{{\mathcal{S}}}
\newcommand\scT{{\mathcal{T}}}
\newcommand\scU{{\mathcal{U}}}
\newcommand\scV{{\mathcal{V}}}
\newcommand\scW{{\mathcal{W}}}
\newcommand\scX{{\mathcal{X}}}
\newcommand\scY{{\mathcal{Y}}}
\newcommand\scZ{{\mathcal{Z}}}

%-----------------------------------------------------------------------------------------
% Brackets
%-----------------------------------------------------------------------------------------
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

%-----------------------------------------------------------------------------------------
% Derivatives
%-----------------------------------------------------------------------------------------
\usepackage{physics}
% Fix a warning: https://tex.stackexchange.com/a/681701/8297
\AtBeginDocument{\RenewCommandCopy\qty\SI}
\ExplSyntaxOn
\msg_redirect_name:nnn { siunitx } { physics-pkg } { none }
\ExplSyntaxOff

%-----------------------------------------------------------------------------------------
% Probability
%-----------------------------------------------------------------------------------------
% Basic Probability
\newcommand{\E}[1]{{\mathbb E}\left[#1\right]}
\newcommand{\ee}[1]{{\mathbb E}[#1]}
\newcommand{\e}{{\mathbb E}}
\newcommand{\V}[1]{{\mathrm{Var}}\left(#1\right)}
\newcommand{\Vv}[1]{{\mathrm{Var}}(#1)}
%\newcommand{\var}{{\mathbb{Var}}}
\newcommand{\vv}{{\mathbb{Var}}}
\newcommand{\Cov}[1]{{\mathrm{Cov}}\left(#1\right)}
\newcommand{\bP}{{\mathbb P}}
\newcommand{\p}[1]{{\mathbb P}\left\{#1\right\}}
\newcommand{\pp}[1]{{\mathbb P}\{#1\}}
\newcommand{\psub}[2]{{\mathbb P}_{#1}\left\{#2\right\}}
\newcommand{\psup}[2]{{\mathbb P}^{#1}\left\{#2\right\}}
\newcommand{\po}[1]{{\mathbb P}_{\sigma}\left(#1\right)}
\newcommand{\pk}[1]{{\mathbb P}_{k}\left(#1\right)}
\newcommand{\I}[1]{{\mathbb 1}_{[#1]}}
\newcommand{\Cprob}[2]{{\mathbb P}\set{\left. #1 \; \right| \; #2}}
\newcommand{\probC}[2]{{\mathbb P}\set{#1 \; \left|  \; #2 \right. }}
\newcommand{\phat}[1]{\ensuremath{\hat{\mathbb P}}\left(#1\right)}
\newcommand{\Ehat}[1]{\ensuremath{\hat{\mathbb E}}\left[#1\right]}
\newcommand{\ehat}{\ensuremath{\hat{\mathbb E}}}
\newcommand{\Esup}[2]{{\mathbb E^{#1}}\left[#2\right]}
\newcommand{\esup}[1]{{\mathbb E^{#1}}}
\newcommand{\Esub}[2]{{\mathbb E_{#1}}\left[#2\right]}
\newcommand{\esub}[1]{{\mathbb E_{#1}}}

\newcommand\inprobLOW{\rightarrow_p}
\newcommand\inprobHIGH{\,{\buildrel p \over \rightarrow}\,}
\newcommand\inprob{{\inprobHIGH}}

\newcommand\inlawLOW{\rightarrow_d}
\newcommand\inlawHIGH{\,{\buildrel d \over \rightarrow}\,}
\newcommand\inlaw{{\inlawHIGH}}

\newcommand\inas{\,{\buildrel{a.s.} \over \rightarrow}\,}

\newcommand{\eql}{\,{\buildrel \scL \over =}\,}

\newcommand{\eqd}{\,{\buildrel{\mathrm{def}} \over =}\,}

\newcommand{\eqas}{\,{\buildrel{\mathrm{a.s.}} \over =}\,}

\newcommand{\convergeas}[1]{\,{\buildrel{{#1}} \over \longrightarrow}\,}

\newcommand\given[1][]{\;#1{\vert}\;}

\newcommand\cond{\;\middle|\;}

%-----------------------------------------------------------------------------------------
% Distributions
%-----------------------------------------------------------------------------------------
\DeclareMathOperator{\Bin}{Bin}
\DeclareMathOperator{\Ber}{Ber}
\DeclareMathOperator{\Poi}{Poi}
\DeclareMathOperator{\Geo}{Geo}
\DeclareMathOperator{\Unif}{Unif}
\DeclareMathOperator{\Exp}{Exp}
\DeclareMathOperator{\Be}{Beta}
\DeclareMathOperator{\Gam}{Gamma}
\DeclareMathOperator{\Gaussian}{\scN}

%-----------------------------------------------------------------------------------------
% Logic
%-----------------------------------------------------------------------------------------
\DeclareMathOperator{\Xor}{Xor}

%-----------------------------------------------------------------------------------------
% Acronyms
%-----------------------------------------------------------------------------------------
\usepackage{acro}

\acsetup{format/short = \scshape}

\DeclareAcronym{lti}{
    short = lti,
    long = linear time invariant
}

\DeclareAcronym{iid}{
    short = iid,
    long = independent and identically distributed
}

\DeclareAcronym{rv}{
    short = rv,
    long = random variable
}

\DeclareAcronym{lhs}{
    short = lhs,
    long = left hand side
}

\DeclareAcronym{rhs}{
    short = rhs,
    long = right hand side
}

\DeclareAcronym{ai}{
    short = ai,
    long = artificial intelligence
}

\DeclareAcronym{imt}{
    short = imt,
    long = invertible matrix theorem
}

\DeclareAcronym{ult}{
    short = ult,
    long = unit lower triangular
}

\DeclareAcronym{dsp}{
    short = dsp,
    long = digital signal processing
}

%-----------------------------------------------------------------------------------------
% Use Lua to extract lecture number
%-----------------------------------------------------------------------------------------
\usepackage{luacode}

\begin{luacode}
function lecturenum(jobname)
    local n = string.match(jobname, "%d+")
    tex.print(n)
end
\end{luacode}

\newcommand\lecturenum
{%
    \directlua{lecturenum("\jobname")}%
}%

%-----------------------------------------------------------------------------------------
% Silent some warnings
%-----------------------------------------------------------------------------------------
\usepackage{silence}
\AtBeginDocument{
  \WarningFilter{hyperref}{Token not allowed in a PDF string}
  \WarningFilter{siunitx}{Detected the "physics" package}
}

%-----------------------------------------------------------------------------------------
% Common Marcos for Making Slides
%-----------------------------------------------------------------------------------------
\newcommand{\blankveryshort}{\underline{\hspace{1cm}}}
\newcommand{\blankshort}{\underline{\hspace{2cm}}}
\newcommand{\blanklong}{\underline{\hspace{4cm}}}

% Now we can extract the lecture number from the file name
\title{Lecture \lecturenum{}}

\newcommand{\courseassignment}{\emoji{pencil} Required Exercises:}

\newcommand{\coursereading}{\emoji{open-book} Required Reading:}

\newcommand{\exercisepic}[2][0.9]{%
\begin{figure}[htpb]%
    \centering{}%
    \includegraphics[width=#1\textwidth]{exercise-#2.jpg}%
    \caption*{Practice makes perfect!}%
\end{figure}%
}

\newcommand{\homeworknote}{%
    \bomb{} 
    Assignments will not be collected;
    however, quiz problems will be selected from them.
}

\newcommand{\sectionhomework}[3][\small]{% #1 is optional and defaults to \normalsize
    #1% Apply the text size command directly
    \emoji{book} Section #2 --- % Adjusted parameter numbers
    \begin{itemize}
        \item[\emoji{coffee}] Read the textbook.
        \item[\emoji{cake}] All T/F questions.
        \item[\emoji{thinking}] Ex.~#3
    \end{itemize}
}

\newcommand{\lectureoutline}{\begin{frame}%
\frametitle{Summary}%
\tableofcontents{}%
\end{frame}}

%-----------------------------------------------------------------------------------------
% For this course
%-----------------------------------------------------------------------------------------
\newcommand{\coursecode}{MATH 202}
\newcommand{\coursename}{Linear Algebra}

\date{Mar-May 2024}
