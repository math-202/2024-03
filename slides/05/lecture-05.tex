\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Linear Transformation}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.8}{All odd number questions from 1 to 43.}
            \sectionhomework{1.9}{All odd number questions from 1 to 43.}
        \end{column}
    \end{columns}
\end{frame}

\section{1.8 Introduction to Linear Transformations}

\begin{frame}
    \frametitle{How to get such a funny picture?}

    \vspace{2em}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{me-2.jpg}
    \end{figure}
\end{frame}

\subsection{Transformations}

\begin{frame}
    \frametitle{Transformations}

    A \alert{transformation/function/mapping} $T:\dsR^{n}\mapsto\dsR^{m}$
    is a rule that assigns to each $\bfx \in \dsR^{n}$
    a vector $T(\bfx)$ in $\dsR^{m}$.

    $\dsR^{n}$ is called the \alert{domain}, and $\dsR^{m}$ is called the \alert{codomain}.

    $T(\bfx)$ is called the \alert{image} of $\bfx$.

    The set of all images is the \alert{range} of $T$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{linear-transformation-03.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Transformation through Matrix Multiplication}

    Given a matrix $A$ we can define a transformation $T(\bfx)= A \bfx$.

    For examples, let
    \begin{equation*}
        A =
        \begin{bmatrix}
            4 & -3 & 1 & 3 \\
            2 &  0 & 5 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfu
        =
        \begin{bmatrix} 1 \\ 1 \\ 1 \\ 1 \\ \end{bmatrix}
        ,
        \qquad
        \bfv
        =
        \begin{bmatrix}
            5 \\ 
            8
        \end{bmatrix}
    \end{equation*}
    Then $T$ maps $\bfu$ to $\bfv$ since
    \begin{equation*}
        A
        \bfu
        =
        \bfv
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Transformation by multiplication}

    Since $A$ is of size $2 \times 4$, $T(\bfx)=A \bfx$ maps
    vectors in $\dsR^{4}$ to vectors $\dsR^{2}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-02.jpg}%
    \end{figure}

    \cake{} What is the \emph{domain} and \emph{codomain} of $T$?
\end{frame}

\begin{frame}
    \frametitle{\zany{} Shear}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{shear} (verb)  --- to cut off the hair from

            \begin{flushright}
                --- Merriam Webster Dictionary
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-sheer-sheep.jpg}
                \caption{%
                    \emph{Shearing sheep} refers to the process of cutting off the woolly fleece of \emoji{sheep}
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3 --- Shear transformation}

    Let $A = \begin{bmatrix} 1 & 3 \\ 0 & 1 \end{bmatrix}$.
    The transformation $T(\bfx) = A \bfx$ is called \emph{a} \alert{shear transformation}.

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{%
                \begin{tikzpicture}[
                    scale=2.5,
                    every node/.style={black},
                    ]
                    % Draw x and y axes
                    \draw[gray!40] (0,0) grid (3,1);
                    \draw (0,0) -- (3,0) node[right]{$x_1$};
                    \draw (0,0) -- (0,1) node[above]{$x_2$};
                    \drawvector{1,0}{$\bfu$};
                    \drawvectorII[right]{0,1}{$\bfv$};
                \end{tikzpicture}
            }%
            \caption{Where does $T$ map $\bfu$ and $\bfv$ to?}
        \end{figure}
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=\linewidth]{linear-transformation-08.jpg}
            \caption{How $T$ transforms a \emoji{framed-picture}}
        \end{figure}

        \cake{} Why do we call $T$ \emph{a}, instead of \emph{the}, shear transformation?
    }%
    
\end{frame}

\subsection{Linear Transformations}%

\begin{frame}
    \frametitle{Linear Transformations}

    A transformation $T$ is \alert{linear} if
    for all $\bfu$ $\bfv$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    For example, the transformation $T(\bfx) = A \bfx$ is \emph{linear}.
\end{frame}

\begin{frame}
    \frametitle{Properties of Linear Transformations}

    If $T$ is a \emph{linear transformation}, then
    for all vectors $\bfu, \bfv$ and scalars $c, d$
    \begin{enumerate}
        \item[(iii)] $T(\bfzero) = \bfzero$,
        \item[(iv)] $T(c \bfu + d \bfv) = c T (\bfu) + d T(\bfv)$,
    \end{enumerate}

    \hint{} These follow from (i) and (ii).
\end{frame}

\begin{frame}
    \frametitle{One More Property}
    If $T$ is a \emph{linear transformation},
    then for all vectors $\bfv_1, \cdots, \bfv_{p}$
    and scalars $c_1, \cdots, c_p$.
    \begin{enumerate}
        \item[(v)] $T(c_1 \bfv_1 + \cdots c_{p} \bfv_p)
            = c_1 T(\bfv_1) + \cdots + c_{p} T(\bfv_p)$,
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 4 --- Contraction and dilation}

    Let $T(\bfx) = r \bfx$. Show that $T$ is linear.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-10.jpg}
        \caption{How $T$ transforms vectors}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- Rotation}

    Let $A = \begin{bmatrix} 0 & -1 \\ 1 & 0 \\ \end{bmatrix}$.
    Find images under $T(\bfx)=A \bfx$ for $\bfu, \bfv$ and $\bfu + \bfv$.

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=0.7,
                every node/.style={black},
                ]
                % Draw x and y axes
                \draw[gray!40] (-6,0) grid (6,6);
                \draw (-6,0) -- (6,0) node[right]{$x_1$};
                \draw (0,0) -- (0,6) node[above]{$x_2$};
                \coordinate (o) at (0,0);
                \coordinate (u) at (4,1);
                \coordinate (v) at (2,3);
                \coordinate (uv) at ($(u)+(v)$);
                \coordinate (Tu) at (-1,4);
                \coordinate (Tv) at (-3,2);
                \coordinate (Tuv) at ($(Tu)+(Tv)$);
                \draw[MintWhisper, fill=MintWhisper] (o) -- (u) -- (uv) -- (v) -- cycle;
                \draw[MintWhisper, fill=MintWhisper] (o) -- (Tu) -- (Tuv) -- (Tv) -- cycle;
                \drawvector{u}{$\bfu$};
                \drawvector[MintGreen]{v}{$\bfv$};
                \drawvector[JuliaOrange]{uv}{$\bfu+\bfv$};
                \drawvector{Tu}{$T(\bfu)$};
                \drawvector[MintGreen]{Tv}{$T(\bfv)$};
                \drawvector[JuliaOrange]{Tuv}{$T(\bfu+\bfv)$};
            \end{tikzpicture}
        }%
        \caption{A rotation transformation}
\end{figure}
\end{frame}

\section{1.9 The Matrix of a Linear Transformation}

\begin{frame}[c]{Linear Transformations and Matrix Representation}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            Given a linear transformation \( T \), can we always find a ``formula'' for it?

            \astonished{} The answer is Yes!

            Any such \( T \) can be defined as $A \bfx$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-linear-transformation.jpg}
                \caption{What is this mysterious $T$?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Standard Matrix}

\begin{frame}
    \frametitle{Example 1 --- Identity matrix}
    Let $\bfe_{1} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$
    and $\bfe_{2} = \begin{bmatrix} 0 \\ 1 \end{bmatrix}$.
    Let $T$ be a \emph{linear transformation} with
    \begin{equation*}
        T(\bfe_{1}) =
        \begin{bmatrix}
            5 \\ -7 \\ 2
        \end{bmatrix}
        ,
        \qquad
        T(\bfe_{2}) =
        \begin{bmatrix}
            -3 \\ 8 \\ 0
        \end{bmatrix}
    \end{equation*}
    How can we find a formula for $T(\bfx)$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Standard matrix}

    Let $T : \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation.

    Let $\veclist[n]{e}$ be the columns of $I_{n}$.

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            T(\bfe_1) &
            T(\bfe_2) &
            \cdots &
            T(\bfe_n)
        \end{bmatrix}
    \end{equation*}
    Then $T(\bfx) = A \bfx$ for every $\bfx \in \dsR^{n}$

    The matrix $A$ is called the \alert{standard matrix} of $T$.
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Rotation}

    Find the standard matrix $A$ for $T:\dsR^{2} \mapsto \dsR^{2}$
    which rotates a vector by degree $\phi$.

    \hint{} First check it is a linear transformation.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-16.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Reflection Over $x_{1}$ Axis}

    What is the \emph{standard matrix} for reflection through $x_{1}$ axis?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linear-transformation-17.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Reflection Over $x_{2}=-x_{1}$ Axis}

    What is the \emph{standard matrix} for reflection through $x_{1}$ axis?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linear-transformation-18.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Think-Pair-Share \think{}}

    %(a) Are these two mappings linear? Why or why not?
    %\begin{equation*}
    %    T_{1}
    %    \left(
    %        \begin{bmatrix}
    %            x_1 \\ x_2  \\ x_3
    %        \end{bmatrix}
    %    \right) =
    %    \begin{bmatrix}
    %            x_1 \\ x_2  \\ -x_3
    %    \end{bmatrix}
    %    ,
    %    \qquad
    %    T_{2}
    %    \left(
    %        \begin{bmatrix}
    %            x_1 \\ x_2
    %        \end{bmatrix}
    %    \right) =
    %    \begin{bmatrix}
    %        4 x_1 -2 x_2 \\ 3 \abs{x_2}
    %    \end{bmatrix}
    %    ,
    %\end{equation*}

    %\pause{}

    %(b) Find the standard matrix $A$ for $T(\bfx) = r \bfx$.

    %\pause{}

    %(c) 
    What is the standard matrix from the linear transformation 
    which maps the picture on the left to the one on the right?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{me.jpg}
        \hfil
        \includegraphics[width=0.4\linewidth]{me-1.jpg}
    \end{figure}
\end{frame}

\subsection{Existence and Uniqueness Questions}

\begin{frame}
    \frametitle{Onto/Surjective Mapping}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{onto} if there is \emph{at least one}
    $\bfx$ such that $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\linewidth]{linear-transformation-20-not-onto.jpg}
        \includegraphics<2>[width=0.7\linewidth]{linear-transformation-20-onto.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Onto Mapping and the Existence of Solutions}

    Let $A$ be the \emph{standard matrix} of an onto mapping $T$.

    Then the linear system $A \bfx = \bfb$ 
    is consistent for all $\bfb \in \dsR^{m}$.
\end{frame}

\begin{frame}
    \frametitle{One-to-one/Injective mapping}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{one-to-one} if there is
    \alert{at most one} $\bfx$ such that
    $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\linewidth]{linear-transformation-22-not-one-to-one.jpg}
        \includegraphics<2>[width=0.7\linewidth]{linear-transformation-22-one-to-one.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{One-to-one and the Uniqueness of Solutions}

    Let $A$ be the \emph{standard matrix} of a one-to-one mapping $T$.

    Then for every $\bfb \in \dsR^{m}$, 
    the linear system $A \bfx = \bfb$ has at most one solution.
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \sweat{} Is the following a one-to-one mapping?
%    \begin{equation*}
%        \begin{bmatrix}
%            1 & -4 & 8 & 1 \\
%            0 & 2 & -1 & 3 \\
%            0 & 0 & 0 & 5\\
%        \end{bmatrix}
%    \end{equation*}
%
%    \hint{} Use the following
%    \begin{block}{Theorem 4}
%        The followings are equivalent
%        \begin{enumerate}
%        \item[a.] For each $\bfb$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
%            \emph{consistent}.
%        \item[d.] $A$ has pivot position in every row.
%        \end{enumerate}
%    \end{block}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- When is $T$ One-to-one?}

    Let $T: \dsR^{n} \mapsto \dsR^{m}$ be a linear transformation.
    $T$ is \emph{one-to-one} if and only if $T(\bfx) = \bfzero$
    has only the trivial solution.

    \only<1>{%
        Proof of $\Rightarrow$ (only if):
    }%
    \only<2>{%
        Proof of $\Leftarrow$ (if):
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 12 --- When is $T$ Onto?}

    Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation,
    and let $A = [\bfa_{1} \, \bfa_{2} \, \dots \, \bfa_{n}]$ be its standard matrix.

    \only<1>{%
        (a) Then $T$ is onto if and only if $\Span{\bfa_{1}, \dots, \bfa_{n}} = \dsR^{m}$.

        Proof of (a) by Theorem 4 (1.4).
        \begin{block}{\hint{} Theorem 4 (1.4)}
            If $A$ is \dots, then the followings are equivalent
            \begin{enumerate}
                \item[a.] For each $\bfb \in \dsR^{m}$, the equation $A \bfx = \bfb$ is \emph{consistent}.
                \item[c.] For each $\bfb \in \dsR^{m}$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
            \end{enumerate}
        \end{block}
    }%

    \only<2>{%
        (b) Then $T$ is one-to-one if and only if $\bfa_{1}, \dots, \bfa_{n}$ are \emph{linearly
        independent}.

        Proof of (b) by Theorem 11.

        \begin{block}{Theorem 11 (1.9)}
            $T$ is \emph{one-to-one} if and only if $T(\bfx) = \bfzero$
            has only the trivial solution.
        \end{block}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Is $T$ \alt<2>{onto}{one-to-one}?

    \begin{equation*}
        T\left(
            \begin{bmatrix}
                x_1 \\ x_2
            \end{bmatrix}
        \right)
        =
        \begin{bmatrix}
            3 x_1 + x_2 \\
            5 x_1 + 7 x_2 \\
            x_1 + 3 x_2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\end{document}
