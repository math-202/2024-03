\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

% Pie chart drawing library 
\input{../tikz.tex}
\usetikzlibrary{matrix, positioning}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Systems of Linear Equations, Echelon Forms}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.1}{1, 3, 5, 7, 11, 15, 19, 21, 35, 37, 41, 43}
        \end{column}
    \end{columns}
\end{frame}

\section{Why Linear Algebra?}

\begin{frame}
    \frametitle{Because of \ac{ai}!}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{robot.jpg}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{and many other applications}

    Linear Algebra provides a \emph{concise and unified language} for many technology
    problems, such as ---

    \begin{itemize}
        \item \emoji{robot}
        Make chatbots smarter
        (\href{https://machinelearningmastery.com/examples-of-linear-algebra-in-machine-learning/}{artificial intelligence})
        \item \emoji{notes} Netflix
            (\href{https://en.wikipedia.org/wiki/Singular\_value\_decomposition}{recommendation system})
        \item \emoji{crossed-swords} maximize war effort
            (\href{https://en.wikipedia.org/wiki/Linear\_programming\#History}{optimization})
        \item \emoji{lock} build ``unbreakable'' cipher code
            (\href{https://www.math.utah.edu/~gustafso/s2016/2270/published-projects-2016/adamsTyler-moodyDavid-choiHaysun-CryptographyTheEnigmaMachine.pdf}{cryptography})
        \item \emoji{framed-picture} create funny photos
            (\href{https://observablehq.com/@yurivish/example-of-2d-linear-transforms}{image processing})
        \item \emoji{alien} control the Mars rover
            (\href{https://en.wikipedia.org/wiki/Linear\_network\_coding}{computer networking})
        \item \emoji{spider-web} process complex network
            (\href{https://en.wikipedia.org/wiki/GraphBLAS}{graph algorithms})
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example: 2D Convolution}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=\linewidth]{image-process1.jpg}
        \only<2>{%
            \begin{tikzpicture}[
                cell/.style={minimum size=9mm,rectangle,draw=black},
                node distance=5mm
                ]
                % Draw the input image matrix with ampersand replacement
                \matrix[
                    matrix of nodes,nodes={cell},ampersand
                    replacement=\&,column sep=-\pgflinewidth,row sep=-\pgflinewidth] (input) {
                    18 \& 54 \& 51 \& 239 \& 244 \\
                    55 \& 121 \& 75 \& 78 \& 95 \\
                    35 \& 24 \& 204 \& 113 \& 109 \\
                    3 \& 154 \& 104 \& 235 \& 25 \\
                    15 \& 253 \& 225 \& 159 \& 78 \\
                };

                % Draw the weight matrix with ampersand replacement
                \matrix[
                    matrix of nodes,nodes={cell},ampersand replacement=\&,
                    column sep=-\pgflinewidth,row sep=-\pgflinewidth, right=of input] (weight) {
                    1 \& 0 \& 1 \\
                    0 \& 1 \& 0 \\
                    1 \& 0 \& 1 \\
                };

                % Draw the result of the operation
                \node[right=of weight, cell] (result) {429};

                % Label the matrices
                \node[above=2mm of input] {INPUT IMAGE};
                \node[above=2mm of weight] {WEIGHT};

                % Draw border around the used values in the input image
                \draw[ultra thick, red] (input-1-1.north west) rectangle (input-3-3.south east);
            \end{tikzpicture}
        }%
        \caption{Pictures by \href{https://www.analyticsvidhya.com/blog/2019/07/10-applications-linear-algebra-data-science/}{Khyati Mahendru}}%
    \end{figure}

    See this
    \href{https://magamig.github.io/posts/real-time-video-convolution-using-webgl/}{live
        demonstration}.
\end{frame}

\section{1.1 Systems of Linear Equations}%

\subsection{Linear Systems}%

\begin{frame}[c]
    \frametitle{A Puzzle}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            A \emoji{cricket-game} and \emoji{baseball} cost \$1.10 together.

            The \emoji{cricket-game} costs \$1 more than the \emoji{baseball}.

            \cake{} Does the \emoji{baseball} cost 10 cents?
            \begin{itemize}
                \item[\emoji{hand}] No.
                \item[\emoji{expressionless}] Yes.
            \end{itemize}
            \poll{}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-baseball.jpg}
                \caption{How much does the \emoji{baseball} cost?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Daniel Kahneman}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            Daniel Kahneman is a \emoji{mending-heart} and \emoji{money-mouth-face}
            who won the Nobel \emoji{trophy} in 2002.
            He wrote:

            \small{}
            More than 50\% of students at Harvard, MIT, and Princeton gave the 
            intuitive-incorrect-answer to the \emoji{baseball} puzzle.

            A general \emph{law of least effort} applies to cognitive as well as physical
            exertion.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Kahneman.jpg}
                \caption{Daniel Kahneman (1934--)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Linear Equations}

    A \alert{linear equation} in the variables $x_{1}, \ldots, x_{n}$ is an equation like
    \begin{equation*}
        a_{1} x_{1} + a_{2} x_{2} + \dots + a_{n} x_{n} = b.
    \end{equation*}
    \only<2>{%
        For example, let $x_1$ and $x_2$ be the prices of \emoji{cricket-game} and
        \emoji{baseball},
        then the puzzle can be written as with two linear equations:
        \begin{equation*}
            x_1 + x_2 = 1.1
        \end{equation*}
        and
        \begin{equation*}
            x_1 - x_2 = 1
        \end{equation*}
    }%
    \only<3>{%
        \cake{} Which of the followings are linear equations?
        \begin{center}
            \begin{minipage}{0.4\textwidth}
                \begin{enumerate}
                    \item $x_1 - 5 x_2 + x_3 = 2$
                    \item $x_2 = 2(\sqrt{6} - x_{1})+x_3$
                    \item $x_2 = 2 (\sqrt{x_1} - 6)$
                    \item $2 x_1 + x_2 - x_3 = 2 \sqrt{6}$
                    \item $4 x_1 - 5 x_2 = x_1 x_2$
                    \item $x_1^2 + x_1 + 1 = 0$
                \end{enumerate}
            \end{minipage}
        \end{center}
    }%
\end{frame}

\begin{frame}[t]
    \frametitle{Linear Systems}

    A \alert{system of linear equations (linear system)} is group of linear equations
    using the same variables, e.g.,
    \begin{equation}
        \label{eq:sys}
        \systeme{%
            x_1 + x_2 = 1.1,
            x_1 - x_2 = 1
        }%
    \end{equation}

    \pause{}

    A \alert{solution} of a linear system is a list of numbers $(s_1, s_{2}, \dots,
    s_{n})$ such that replacing $x_i$ by $s_i$ makes \emph{all} the equations true.

    One solution of \eqref{eq:sys} is $(1.05, 0.05)$.

    \smiling{} So the \emoji{baseball} costs 5 cents!
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Wassily Leontief}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            Wassily Leontief is an \emoji{money-mouth-face} who
            won the Nobel \emoji{trophy} in 1973.

            In 1942, he came up with a linear system with $500$ variables to model
            \emoji{us} economy.

            The number of variables have to be reduced to $42$ for the best \emoji{computer} at
            the time to solve.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Leontief.jpg}
                \caption{Wassily Leontief (1905-1999)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Three Types of Solution Sets}

\begin{frame}
    \frametitle{Solution Sets}
    The set of all solutions of a linear system is the \alert{solution set}.

    \only<1>{%
        The solution set can contain one solution. 

        In this case we say the solution is \emph{unique}.

        For example
        \begin{equation*}
            \systeme{%
                x_1 + x_2 = 1.1,
                x_1 - x_2 = 1
            }%
        \end{equation*}
        has the unique solution $(1.05, 0.05)$.
    }%
    \only<2>{%
        The solution set can also contain infinitely many solution. 

        For example
        \begin{equation*}
            \systeme{%
                x_1 + x_2 = 1.1,
                2 x_1 + 2 x_2 = 2.2
            }%
        \end{equation*}
        has infinitely many solutions.
    }%
    \only<3>{%
        The solution set can also be empty.

        For example
        \begin{equation*}
            \systeme{%
                x_1 + x_2 = 1.1,
                x_1 + x_2 = -1.1
            }%
        \end{equation*}
        has no solutions.
    }%
\end{frame}

\begin{frame}[c]
    \frametitle{Three cases}

    The \emph{fundamental question} about a linear systems is that if it is
    \begin{itemize}
        \item[\inconsistent] \alert{inconsistent},  i.e., having no solutions, or
        \item[\consistent] \alert{consistent}, i.e., having
            \begin{itemize}
                \item[\emoji{unicorn}] one \alert{unique solution}, or
                \item[\emoji{infinity}] \alert{infinitely many solutions}.
            \end{itemize}
    \end{itemize}

    \astonished{} Later we will see, these are the only three possibilities.
\end{frame}

\begin{frame}
    \frametitle{Equivalence of Linear Systems}
    Two systems are \alert{equivalent} if their solution sets are the same.

    Consider
    \begin{equation*}
        \systeme{2 x_1 - x_2 + 1.5 x_3 = 8,
        x_1 - 4 x_3= -7}
    \end{equation*}
    and
    \begin{equation*}
       \systeme{4 x_1 - 2 x_2 + 3 x_3 = 16,
         - 3 x_1 + 12 x_3  = 21}
    \end{equation*}

    \cake{} Why are these two systems equivalent?
\end{frame}

\begin{frame}
    \frametitle{Unique solutions}

    The following linear system is consistent and has a unique solution.

    \begin{equation*}
        \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 3 x_2 = 3}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{3-case-1.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Non-unique solutions}

    System (a) is inconsistent and has no solutions.

    System (b) is consistent and has infinitely many solutions.

    \begin{equation*}
        (a) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 = 3}
        \quad
        (b) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 =  1}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=1.0\linewidth]{3-case-2.jpg}
    \end{figure}
\end{frame}

\subsection{Matrix Notations}%

\begin{frame}
    \frametitle{\zany{} Matrix the Film}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{matrix.jpg}
        \caption{The Matrix (1999) is a Sci-Fi thriller \emoji{film-frames}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Matrix in Linear Algebra}

    A \alert{matrix} is rectangular array of numbers like these:
    \begin{equation*}
        \begin{bmatrix}
            1 & -2 & 1 \\
            0 &  2 & -8 \\
            5 &  0 & -5 \\
        \end{bmatrix}
        \qquad
        \begin{bmatrix}
            1 & -2 & 1 \\
            0 &  2 & -8 \\
        \end{bmatrix}
    \end{equation*}
    It can contain just one row or one column ---
    \begin{equation*}
        \begin{bmatrix}
            1 & -2 & 1 \\
        \end{bmatrix}
        \qquad
        \begin{bmatrix}
            1 \\
            0 \\
            5 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Matrix notations for linear systems}

    Given a linear system
    \begin{equation*}
        \systeme{x_1 - 2 x_2 + x_3 = 0,
            2 x_2 - 8 x_3 = 8,
            5 x_1 - 5 x_3 = 10}
    \end{equation*}
    the \alert{coefficient matrix}
    and
    the \alert{augmented matrix} are
    \begin{equation*}
            \begin{bmatrix}
                1 & - 2 &    1 \\
                0 &   2 &   -8 \\
                5 &   0 &   -5
            \end{bmatrix}
        \hspace{2cm}
            \begin{bmatrix}
                1 & - 2 &    1  & 0 \\
                0 &   2 &   -8  & 8 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Solving Linear Systems}%

\begin{frame}[c]
    \frametitle{Ways to Solve Linear System}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            To solve
            \begin{equation}
                \label{eq:sys:1}
                \systeme{x_1 - 2 x_2 +   x_3 = 0,
                2 x_2 - 8 x_3 = 8,
                5 x_1 - 5 x_3 = 10}
            \end{equation}
            you can
            \begin{itemize}
                \item \emoji{cry} and give up,
                \item \emoji{phone} you mum \emoji{old-woman} for help, or
                \item \emoji{paintbrush} some pictures, or
                \item use a \emoji{robot}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{3-plane.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 1}

    Alternatively, you can show that the following are equivalent
    \begin{equation*}
        \systeme{x_1 - 2 x_2 +   x_3 = 0,
        2 x_2 - 8 x_3 = 8,
        5 x_1 - 5 x_3 = 10}
        \hspace{2cm}
        \systeme{x_1 = 1,
        x_2 = 0,
        x_3 = -1}
    \end{equation*}
    by applying three types of simplification:
    \begin{itemize}[<+->]
        \item[\emoji{plus}] \alert{Replace} one equation by the sum of itself and a
            multiple of another equation.
        \item[\emoji{up-down-arrow}] \alert{Interchange}  two equations
        \item[\emoji{multiply}] \alert{Scale} a equation by multiplying it with a \emph{non-zero} constant.
    \end{itemize}
    \pause[\thebeamerpauses]
    \hint{} Note that such operations do not change the solution set.
\end{frame}

\begin{frame}
    \frametitle{Elementary Row operations}

    The operations we have done on the equations are equivalent to
    the three types of \alert{elementary row operations} to the augmented matrix:
    \begin{itemize}
        \item[\emoji{plus}] \alert{Replace} one row by the sum of itself and a multiple of another row.
        \item[\emoji{up-down-arrow}] \alert{Interchange} two rows.
        \item[\emoji{multiply}] \alert{Scale} a row by multiplying it with a \emph{non-zero} constant.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example 1 Revisited}
    
    We solved \eqref{eq:sys:1} by applying row operations to the left (the augmented
    matrix)
    to get the right.
    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            5 &   0 &   -5  & 10
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            1 & 0 & 0  & 1 \\
            0 & 1 & 0  & 0 \\
            0 & 0 & 1  & -1
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Reversibility of Elementary Row Operations}

    Note that Elementary Row Operations are \alert{reversible}.

    For example, one 
    \emph{%
        \only<1>{replacement}\only<2>{interchange}\only<3>{scaling} 
    }%
    converts the matrix on the left
    to the one on the right.

    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            5 &   0 &   -5  & 10
        \end{bmatrix}
        \hspace{2cm}
        \only<1>{%
            \begin{bmatrix}
                1 &   2 &   -15 & 16 \\
                0 &   2 &   -8  & 8 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
        }%
        \only<2>{%
            \begin{bmatrix}
                0 &   2 &   -8  & 8 \\
                1 & - 2 &    1  & 0 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
        }%
        \only<3>{%
            \begin{bmatrix}
                2 & - 4 &    2  & 0 \\
                0 &   2 &   -8  & 8 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
        }%
    \end{equation*}
    \cake{} Do you see how to reverse it?
\end{frame}

\begin{frame}
    \frametitle{Row Equivalence}

    Two matrices are \alert{row equivalent} if we can transform one matrix
    to another via a sequence of elementary row operations.

    We use $A \sim B$ to denote that $A$ and $B$ are row equivalent.

    \pause{}

    \hint{} Obviously, two row equivalent \emph{augmented matrices} have the same solution
    set.

    \pause{}

    For example, since the following two augmented matrices are row equivalent
    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            5 &   0 &   -5  & 10
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            1 & 0 & 0  & 1 \\
            0 & 1 & 0  & 0 \\
            0 & 0 & 1  & -1
        \end{bmatrix}
    \end{equation*}
    their corresponding systems of equations are also equivalent.
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}

    Can you transfer the left to the right with three row operations: one replacing,
    one interchanging, and one scaling?
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -5 & 0 \\
            0 & 1 & -3 & -2 \\
            0 & -3 & 9 & 5 \\
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            2 & 4 & -10 & 0 \\
            0 & 0 & 0 & -1 \\
            0 & 1 & -3 & -2 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\section{Existence and Uniqueness Question}

\begin{frame}
    \frametitle{Example 2}
    Is the following system \emph{consistent}? Is the solution \emph{unique}?
    \begin{equation*}
        \systeme{
            x_1 - 2x_2 + x_3 = 0,
            2x_2 - 8x_3 = 8,
            5x_1 - 5x_3 = 10
        }
    \end{equation*}

    \pause{}
    We can use row operations to reduce the augmented matrix to:
    \[
        \begin{bmatrix}
            1 & -2 & 1 & 0 \\
            0 & 1 & -4 & 4 \\
            0 & 0 & 1 & -1
        \end{bmatrix}
    \]
    \cake{} Do you see the answer?
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    Is the following system \emph{consistent}? Is the solution \emph{unique}?
    \[
        \systeme{
            x_2 - 4x_3 = 8,
            2x_1 - 3x_2 + 2x_3 = 1,
            4x_1 - 8x_2 + 12x_3 = 1
        }
    \]
    \pause{}
    We can use row operations to reduce the augmented matrix to:
    \begin{equation*}
        \begin{bmatrix}
            2 & -3 & 2 & 1 \\
            0 & 1 & -4 & 8 \\
            0 & 0 & 0 & 15
        \end{bmatrix}
    \end{equation*}
    \cake{} Do you see the answer?
\end{frame}

\end{document}
