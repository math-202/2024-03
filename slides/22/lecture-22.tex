\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\usepackage{cancel}

\title{Lecture \lecturenum{} ---  Characteristic Equations, Diagonalization}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{5.2}{1, 9, 15, 19, 31}
            \sectionhomework{5.3}{1, 3, 5, 11, 17, 29, 31, 33, 35, 37}
        \end{column}
    \end{columns}
\end{frame}

\section{5.2 Characteristic Equations}

\begin{frame}
    \frametitle{Example 1}
    It is easy to verify that $\lambda=3$ and $\lambda=-7$ are the eigenvalues of
    \begin{equation*}
        \begin{bmatrix} 2 & 3 \\ 3 & -6 \end{bmatrix}
    \end{equation*}
    \think{} But how can we find these numbers in the first place?
\end{frame}

\subsection{Determinants}

\begin{frame}
    \frametitle{Recall Theorem 1 (3.1) --- Cofactor Expansion}

    For any $1 \le i \le n$ and $1 \le j \le n$
    \begin{equation*}
        \begin{aligned}
            \det A
            & =
            a_{i1} C_{i1} + a_{i2} C_{i2} + \cdots a_{in} C_{in} \\
            & =
            a_{1j} C_{1j} + a_{2j} C_{2j} + \cdots a_{nj} C_{nj} \\
        \end{aligned}
    \end{equation*}
    where the $(i,j)$-\alert{cofactor} of $A$ is defined by
    \begin{equation*}
        C_{ij} = (-1)^{i+j} \det A_{ij}.
    \end{equation*}
    and
    \[
        A_{ij}
        =
        \begin{bmatrix}
            a_{11}       & \cdots       & \cancel{a_{1j}} & \cdots & a_{1n}                 \\
            \vdots       & \ddots       & \vdots          & \ddots & \vdots                 \\
            \cancel{a_{i1}} & \cdots    & \cancel{a_{ij}} & \cdots & \cancel{a_{in}}        \\
            \vdots       & \ddots       & \vdots          & \ddots & \vdots                 \\
            a_{m1}       & \cdots       & \cancel{a_{mj}} & \cdots & a_{mn}
        \end{bmatrix}
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{}
    Show that $\det A = 0$ where
    \begin{equation*}
        A =
        \begin{bmatrix}
            2 & 3 & 1 \\
            4 & 0 & -1 \\
            0 & 2 & 1 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Review: Theorem 3 (3.2) --- Properties of Determinants}

    Let $A$ and $B$ be $n \times n$ matrices.
    \begin{enumerate}
        \item $A$ is invertible if and only if $\det A \ne \blankshort{}$
        \item $\det AB = \blankshort{}$
        \item $\det A^T = \blankshort{}$
        \item If $A$ is triangular, then $\det A$ is \blanklong{}.
        \item Row operation ---
            \begin{enumerate}
                \item A row replacement on A \blankshort{} the determinant.
                \item A row interchange \blankshort{} the determinant.
                \item A row scaling by a factor $c$ \blankshort{}.
            \end{enumerate}
    \end{enumerate}
\end{frame}

\subsection{The Characteristic Equation}

\begin{frame}
    \frametitle{The Characteristic Equation}

    The matrix $A - \lambda I$ is singular if and only if
    \begin{equation}
        \label{eq:char:eq}
        \det(A - \lambda I) = 0,
    \end{equation}
    which we call \alert{the characteristic equation} of $A$
    and $\det(A - \lambda I)$ \alert{the characteristic polynomial}
    of $A$.

    \only<1>{%
        \cake{} What is the characteristic polynomial of
        \begin{equation*}
            A
            =
            I_{n}
            =
            \begin{bmatrix} 
                1 & 0 &  \cdots & 0 & 0 \\
                0 & 1 &  \cdots & 0 & 0 \\
                \vdots & \vdots & \ddots & \vdots & \vdots \\
                0 & 0 &  \cdots & 1 & 0 \\
                0 & 0 &  \cdots & 0 & 1 \\
            \end{bmatrix} 
        \end{equation*}
    }%

    \only<2>{%

        The \alert{root} of a polynomial is a value of the variable which makes the
        polynomial equal to zero.

        Thus, a number $\lambda_{0}$ is an eigenvalue of $A$ if and only if it is a root of
        $\det(A -\lambda I)$.
    }%

    \only<3>{%
        \cake{} What are the roots of $\det(A - \lambda I)$ if
        \begin{equation*}
            A
            =
            I_{n}
            =
            \begin{bmatrix} 
                1 & 0 &  \cdots & 0 & 0 \\
                0 & 1 &  \cdots & 0 & 0 \\
                \vdots & \vdots & \ddots & \vdots & \vdots \\
                0 & 0 &  \cdots & 1 & 0 \\
                0 & 0 &  \cdots & 0 & 1 \\
            \end{bmatrix} 
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{The Multiplicity of Eigenvalues}
    To find eigenvalues of $A$, we can factorize its characteristic polynomial
    to get
    \begin{equation*}
        \det(A - \lambda I) 
        = 
        (\lambda - \lambda_{1})^{n_1}
        (\lambda - \lambda_{2})^{n_2}
        \dots
        (\lambda - \lambda_{p})^{n_p}
    \end{equation*}
    Then $\lambda_1, \dots, \lambda_{p}$ are the eigenvalues of $A$.

    The numbers $n_{j}$ is called the \alert{(algebraic) multiplicity} $\lambda_{j}$.

    \pause{}
    \cake{}
    We is the number $1$'s multiplicity in $\det(I_{n} - \lambda I_{n})$?
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            5 & -2 & 6 & -1 \\
            0 & 3 & -8 & 0 \\
            0 & 0 & 5 & 4 \\
            0 & 0 & 0 & 1 \\
        \end{bmatrix}
        .
    \end{equation*}
    \cake{}
    Then
    \begin{equation}
        \label{eq:eg:3}
        \det(A - \lambda I) =
        \blanklong{}
    \end{equation}

    \cake{} The multiplicities of the eigenvalues \blankshort{} are
    \begin{equation*}
        \blanklong{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    A matrix $A$ has
    \begin{equation*}
        \det(A - \lambda I)
        =
        \lambda^{6} - 4 \cdot \lambda^{5} - 12 \cdot \lambda^{4}
        .
    \end{equation*}
    \cake{} Find the eigenvalues and their multiplicities.

    \vspace{8em}

    \cake{} Is the matrix invertible?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
   
    \cake{} What are the eigenvalues of
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            2 & 2 \\
            2 & 2 \\
        \end{bmatrix}
        .
    \end{equation*}
    \hint{}
    Do you remember the eigenvalues of
    \begin{equation*}
        \begin{bmatrix}
            1 & 1 \\
            1 & 1 \\
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\subsection{Similarity}

\begin{frame}
    \frametitle{Similarity}

    $A$ is \alert{similar} to $B$ if there exists $P$ such that
    $A = PBP^{-1}$.

    \emoji{fire} Similarity provides algorithms to approximate
    eigenvalues.

    Factorizing like $A = P B P^{-1}$ is called \alert{similarity transformation}.

    \only<2-3>{%
        For example, letting
        \begin{equation*}
            A =
            \left[
                \begin{array}{cc}
                    1 & 1 \\
                    1 & 1 \\
                \end{array}
            \right]
            ,
            \qquad
            B=
            \left[
                \begin{array}{cc}
                    0 & 0 \\
                    0 & 2 \\
                \end{array}
            \right]
            ,
            \qquad
            P = 
            \left[
                \begin{array}{cc}
                    -1 & 1 \\
                    1 & 1 \\
                \end{array}
            \right]
        \end{equation*}
        Then $A$ is similar to $B$.
    }%

    \only<4>{%
        \cake{} What are the eigenvalues of $A$ and $B$?
    }%

    \only<5>{%
        \cake{} Does $A$ similar to $B$ imply that $B$ is similar to $A$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- Similar matrices}

    If $A$ and $B$ are similar, then they have the same characteristic equations
    and the same eigenvalues with the same multiplicities.
\end{frame}

\subsection{Application to Dynamic Systems}

\begin{frame}
    \frametitle{Example 5}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            0.95 & 0.03 \\
            0.05 & 0.97 \\
        \end{bmatrix}
        ,
        \qquad
        \bfx_0
        =
        \begin{bmatrix}
            0.6 \\ 0.4
        \end{bmatrix}
        ,
    \end{equation*}
    and
    \begin{equation*}
        \bfx_{k+1} = A \bfx_{k} \qquad (k \in \{0, 1, 2, \dots\})
    \end{equation*}
    How to find a closed formula of $\bfx_{k} = A^{k} \bfx_{0}$?
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- Solution}
    It is easy to verify that the eigenvalues of $A$ are $\lambda_{0} = 0.92$ and $\lambda_{1} = 1$,
    with corresponding eigenvectors
    \begin{equation*}
        \begin{bmatrix}
            \bfv_1 & \bfv_2
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 & 1 \\
            5 & -1 \\
        \end{bmatrix}
    \end{equation*}
    \only<1>{%
        \cake{} Why is this invertible?
    }%
    \only<2->{%
        Since they form a basis of $\dsR^2$, we have
        \begin{equation*}
            \bfx_{0} = c_{1} \bfv_1 + c_{2} \bfv_2
        \end{equation*}
        for some unique constants $c_{1}$ and $c_{2}$.
    }%
    \only<3>{%
        Let's try to compute $\bfx_{k}$ using this.
    }%
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{} Use $\det A^T = \det A$ to show that $A$ and $A^T$ has the same
%    characteristic equation.
%
%    \think{}  Let $A$ be $n \times n$, and suppose A has n real eigenvalues,
%    $\lambda_1, \dots, \lambda_n$. Then
%    \begin{equation*}
%        \det(A - \lambda I) = (\lambda -\lambda_1) \dots (\lambda - \lambda_n).
%    \end{equation*}
%    Explain why $\det(A) = \prod_{i=1}^n \lambda_i$.
%
%\end{frame}

\section{5.3 Diagonalization}

\begin{frame}
    \frametitle{Diagonalization}
    Given an $n \times n$ matrix $A$, diagonalization of $A$
    means factorizing it as
    \begin{equation*}
        A = P D P^{-1}
    \end{equation*}
    where $D$ is a diagonal matrix.

    \only<1>{%
        \cake{}
        What is the difference between diagonalization and similarity transformation which
        factorizes $A$ by
        \begin{equation*}
            A = P B P^{-1}
        \end{equation*}
    }%
    
    \only<2>{%
        \cake{}
        Is the following diagonalization of $A$ or similarity transformation of $A$?
        \begin{equation*}
            \begin{bmatrix}
                1 & 1 \\
                1 & 1 \\
            \end{bmatrix}
            =
            \left[
                \begin{array}{ccc}
                    -1 & 1 \\
                    1 & 1 \\
                \end{array}
            \right]
            \left[
                \begin{array}{ccc}
                    0 & 0 \\
                    0 & 2 \\
                \end{array}
            \right]
                \left[
                    \begin{array}{cc}
                        \frac{-1}{2} & \frac{1}{2} \\
                        \frac{1}{2} & \frac{1}{2} \\
                    \end{array}
                \right]
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let
    \begin{equation*}
        D
        =
        \begin{bmatrix}
            5 & 0 \\
            0 & 3 \\
        \end{bmatrix}
    \end{equation*}
    \only<1>{%
        What are $D^2, D^3$?
    }%
    \only<2>{%
        \cake{} What is $D^{k}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Let $A = P D P^{-1}$ where
    \begin{equation*}
        D
        =
        \begin{bmatrix}
            5 & 0 \\
            0 & 3 \\
        \end{bmatrix}
        ,
        \qquad
        P
        =
        \begin{bmatrix}
            1 & 1 \\
            -1 & -2 \\
        \end{bmatrix}
    \end{equation*}
    What are $A^2, A^3$ and $A^k$?
\end{frame}

\end{document}
