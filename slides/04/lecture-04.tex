\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Linear Independence}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\scriptsize]{1.5}{3, 5, 11, 15, 17, 19, 21, 23, 25, 37, 39, 41}
            \sectionhomework[\scriptsize]{1.6}{1, 3, 7, 11, 13}
            \sectionhomework[\scriptsize]{1.7}{%
                1, 5, 9, 15, 17, 19, 29, 31, 33, 37, 45. 
                \item[\smiling{}] Note: questions 39-44 are not required.
            }%
            \emoji{robot} \href{http://webwork.dukekunshan.edu.cn/webwork2}{WeBWork} Homework 1
        \end{column}
    \end{columns}
\end{frame}

\section{1.5 Solution Sets of Linear Systems}

\subsection{Homogeneous Linear Systems}

\begin{frame}
    \frametitle{A trivial example}
    \cake{} Can you think of one solution of
    \begin{equation*}
        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
            -3 x_1 -2 x_2 + 4 x_3 = 0,
            6 x_1 + x_2 - 8 x_3 = 0}
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Homogeneity}
    
    \begin{columns}[c]
        % Column 1: Etymology
        \begin{column}{0.5\textwidth}
            The term \emph{homogeneous} originates from two \emoji{greece} words:
            \begin{itemize}
                \item \textbf{Homos:} Meaning "same" or "similar"
                \item \textbf{Genos:} Meaning "kind" or "type"
            \end{itemize}
            
            Thus \emph{homogeneous} means \emph{similar kind}.
        \end{column}
        
        % Column 2: Funny Bunny Picture
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-identical.jpg}
                \caption{Homogeneous \bunny{}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Homogeneous and Nonhomogeneous Linear Systems}

    A linear system of the form $A \bfx = \bfzero$ is called \alert{homogeneous}.

    A linear system of the form $A \bfx = \bfb$  where $\bfb \ne \bfzero$ is
    called \alert{nonhomogeneous}.

    A \emph{homogeneous} system \emph{always} has the vector \blankveryshort{}
    as a solution. We call it the \alert{trivial solution}.

    Thus, a \emph{homogeneous} system is \emph{always} consistent.

    \think{}
    Can we find other (\emph{nontrivial}) solutions?
\end{frame}

\begin{frame}
    \frametitle{Nontrivial Solutions}

    \begin{block}{Review: Theorem 2 (1.2)}
        A \emph{consistent} linear system has
        \begin{itemize}
            \item[\emoji{infinity}] infinite solutions if it has \emph{at least one} free variables, or
            \item[\emoji{unicorn}] a unique solution if it has \emph{no} free variables.
        \end{itemize}
    \end{block}

    Thus, by this theorem,
    a \emph{homogeneous} linear system has nontrivial solutions
    if and only if \blanklong{}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    Does the following homogeneous system
    \begin{equation*}
        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
            -3 x_1 -2 x_2 + 4 x_3 = 0,
            6 x_1 + x_2 - 8 x_3 = 0}
    \end{equation*}
    have nontrivial solutions?

    \hint{} First step is to show that
    \begin{equation*}
        \begin{bmatrix}
            3 & 5 & -4 & 0 \\
            -3 & -2 & 4 & 0 \\
            6 & 1 & -8 & 0\\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 0 & -\frac{4}{3} & 0 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 0 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Show that the homogeneous system
    \begin{equation*}
        10 x_1 - 3 x_2 - 2 x_3 = 0
    \end{equation*}
    has a solution set
    \begin{equation*}
        \bfx =
        x_{2}
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{3}
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Parametric Vector Form}

    The solution set of Example 2 is described as
    \begin{equation*}
        \bfx = s \bfu + t \bfv,
        \qquad
        \text{where }
        s, t \in \dsR,
        \quad
        \bfu =
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    This is called a solution in \alert{parametric vector form}.

    In other words, the solution set is 
    the \blanklong{} of $\bfu$ and $\bfv$.
\end{frame}

%\begin{frame}
%    \frametitle{Why parametric vector form?}
%    
%    \cake{} If $\bfu$ and $\bfv$ are both solutions of $A \bfx = \bfzero$,
%    then why is any linear combination of the two also a solution of $A \bfx =
%    \bfzero$?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Solution sets}
%
%    The solution set of a homogeneous system is of the form
%    $\Span{\bfv_{1}, \dots, \bfv_{p}}$.
%    This is called the \emph{parametric vector form} of a solution set.
%
%    \hint{} We will give a more formal explanation later.
%\end{frame}

\subsection{Nonhomogeneous Linear Systems}

\begin{frame}
    \frametitle{Example 3}\label{example:3}
    Solve the \emph{nonhomogeneous} equation $A \bfx = \bfb$ with
    \begin{equation}
        A =
        \begin{bmatrix}
            3 & 5 & -4 \\
            -3 & -2 & 4 \\
            6 & 1 & -8 \\
        \end{bmatrix}
        ,
        \qquad
        b
        =
        \begin{bmatrix}
            7 \\ -1 \\ -4
        \end{bmatrix}
    \end{equation}
    \hint{} First step is to show that
    \begin{equation*}
        \begin{bmatrix}
            A & \bfb
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 & 5 & -4 & 7\\
            -3 & -2 & 4 & -1  \\
            6 & 1 & -8  & -4 \\
        \end{bmatrix}
        \sim
        \left[
            \begin{array}{cccc}
                1 & 0 & \frac{-4}{3} & -1 \\
                0 & 1 & 0 & 2 \\
                0 & 0 & 0 & 0 \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Parametric Vector Form}

    In other words, the solution set for Example 3 is of the form
    \begin{equation*}
        \bfx = \bfp + t \bfv,
        \qquad
        \text{where }
        \bfp =
        \begin{bmatrix}
            -1  \\ 2 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            \frac{4}{3}  \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    \hint{} Note that
    \begin{enumerate}
        \item $t \bfv$ is a solution of $A \bfx = \bfzero$;
        \item $\bfp$ is a solution of $A \bfx = \bfb$.
    \end{enumerate}
    \think{} Is the solution of a nonhomogeneous system always like this?
\end{frame}

\begin{frame}
    \frametitle{Theorem 6 -- Solution sets}
    The solution set of a \emph{nonhomogeneous} system $A \bfx = \bfb$
    is of the form $\bfp + \bfv_{h}$,
    where $\bfp$ is one particular solution,
    and $\bfv_{h}$ is any solution of $A \bfx = \bfzero$.

    \only<1>{%
        \cake{} If such $\bfp$ does not exist, what is the solution set of $A \bfx = \bfb$?
    }%

    \pause{}
    Proof (Exercise 37): Let $\bfw$ be any solution of $A \bfx = \bfb$ and define $\bfv_{h}
    = \bfw - \bfp$.
\end{frame}

\begin{frame}
    \frametitle{Geometric Perspective}

    \begin{figure}[htpb]
        \begin{tikzpicture}[
            scale=1.2,
            every node/.style={black},
            ]
            % Set background color
            \fill[white] (-2.5,-1.5) rectangle (6.5,2.5);
            % Draw x and y axes
            \draw[gray!40] (-1,-1) grid (5,2);
            \draw (-0.5,0) -- (5,0) node[right]{$x_1$};
            \draw (0,-0.5) -- (0,2) node[above]{$x_2$};
            \draw[domain=-1.5:4.5,smooth,variable=\x,BrightRed,thick] plot ({\x},{\x/2});
            \draw[domain=-1.5:4.5,smooth,variable=\x,SeaGreen,thick] plot ({\x},{\x/2-1/2});
            \node (l1) at (5.5, 1.3) {$A \bfx = \bfb$};
            \draw[thick, SeaGreen] (l1) -- +(1,0);
            \node[above=0.5 cm of l1.west,anchor=west] (l2) {$A \bfx = \bfzero$};
            \draw[thick, BrightRed] (l2) -- +(1,0);
            \only<1>{%
                \drawvector{2,0.5}{$\bfp$};
                \drawvector{0,0}{$\bfv_h$};
            }%
            \only<2>{%
                \drawvector{1.0,0.5}{$\bfv_h$};
                \begin{scope}[shift={(1,0.5)}]
                    \drawvector{2,0.5}{$\bfp$};
                \end{scope}
            }%
            \only<3>{%
                \drawvector{-1.0,-0.5}{$\bfv_h$};
                \begin{scope}[shift={(-1,-0.5)}]
                    \drawvector{2,0.5}{$\bfp$};
                \end{scope}
            }%
        \end{tikzpicture}
    \end{figure}
\end{frame}

%\section{1.6 Applications of Linear Systems}

%\begin{frame}
%    \frametitle{Example 1 --- A simple economy}
%
%    You are the \emoji{crown} of a country with only industries --- \emoji{tv}, \emoji{beer}, \emoji{carrot}.
%
%    The production and consumption of each is given below.
%
%    \begin{table}
%        \begin{tabular}{*{3}{w{c}{3em}|}c}
%            \hline
%            \multicolumn{3}{c}{Production distribution} & \\ \hline
%            \emoji{tv} & \emoji{beer} & \emoji{carrot} & Consumed by \\ \hline
%            \num{.0} & \num{.4} & \num{.6} & \emoji{tv} \\ \hline
%            \num{.6} & \num{.1} & \num{.2} & \emoji{beer} \\ \hline
%            \num{.4} & \num{.5} & \num{.2} & \emoji{carrot} \\ \hline
%        \end{tabular}
%    \end{table}
%
%    How can you set $p_{t}$, $p_{b}$ and $p_{c}$,
%    the prices of \emoji{tv}, \emoji{beer}, \emoji{carrot},
%    so that the cost and profit of each industry is the same?
%\end{frame}
%
%\begin{frame}
%    \frametitle{How to make a barbecue --- \emoji{cut-of-meat}\emoji{dango}\emoji{hot-dog}\emoji{hamburger}}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\linewidth]{Propane_Gas_Grill.jpg}
%        \caption{\href{https://commons.wikimedia.org/wiki/File:Propane\_Gas\_Grill.jpg}{Propane Gas Grill}}%
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 2 --- Balancing Chemical Equations}
%
%    When propane gas burns
%    \begin{equation*}
%        (x_1) \chem{C_3 H_8} + (x_2) \chem{O_2}
%        \to
%        (x_3) \chem{CO_2} + (x_4) \chem{H_2 O}
%    \end{equation*}
%
%    Balancing that equation means solving
%    \begin{equation*}
%        x_1
%        \begin{bmatrix}
%            3 \\ 8 \\ 0
%        \end{bmatrix}
%        +
%        x_2
%        \begin{bmatrix}
%            0 \\ 0 \\ 2
%        \end{bmatrix}
%        =
%        x_3
%        \begin{bmatrix}
%            1 \\ 0 \\2
%        \end{bmatrix}
%        +
%        x_4
%        \begin{bmatrix}
%            0 \\ 2 \\ 1
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 3 --- \emoji{car}}
%
%    The number of \emoji{car} running on the streets are given below.
%    Can we find $x_1,\ldots, x_5$?
%    
%    \begin{figure}
%        \includegraphics[width=0.65\textwidth,left]{fig-1-6-2.pdf}
%    \end{figure}
%
%    \zany{} How many equations do we need?
%\end{frame}

\section{1.7 Linear Independence}

\begin{frame}[c]
    \frametitle{Independence}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{Independence}  ---
            not looking to others for one's opinions or for guidance in conduct.

            \begin{flushright}
                --- Merriam Webster Dictionary
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-independence.jpg}
                \caption{An independent \bunny{} does not need help in exams}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Independence of Vectors?}

    In which of the two pictures,
    the two vectors look more ``independent''?

    \begin{figure}[htpb]
        \centering
        \hfil
        \begin{tikzpicture}[
            scale=0.6,
            every node/.style={black},
            ]
            % Set background colour
            \draw[white, fill=white] (-1,-1) rectangle (7,3);
            % Draw a grid
            \draw[thin,gray!40] (0,0) grid (6,2);
            % Draw x and y axes
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$v_2$};
            \drawvector{3,1}{$v_1$};
        \end{tikzpicture}
        \hfil
        \begin{tikzpicture}[
            scale=0.6,
            every node/.style={black},
            ]
            % Set background colour
            \draw[white, fill=white] (-1,-1) rectangle (7,3);
            % Draw x and y axes
            \draw[thin,gray!40] (0,0) grid (6,2);
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$v_2$};
            \drawvector{3,2}{$v_1$};
        \end{tikzpicture}
        \hfil
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Linear Independence}

    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ is \alert{linearly independent}
    if
    \begin{equation*}
        x_1 \bfv_1 + x_2 \bfv_2 + \dots + x_p \bfv_p = \bfzero
    \end{equation*}
    has only the trivial solution.

    Otherwise it is \alert{linearly dependent}.

    \pause{}

    \bomb{} An empty set of vectors is considered linearly independent by convention.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 1}

    \think{}
    Are the following vectors \emph{linearly independent}?
    \begin{equation*}
        \begin{bmatrix}
            1 \\ 2 \\ 3
        \end{bmatrix}
        ,
        \quad
        \begin{bmatrix}
            4 \\ 5 \\ 6
        \end{bmatrix}
        ,
        \quad
        \begin{bmatrix}
            2 \\ 1 \\ 0
        \end{bmatrix}
    \end{equation*}
    \only<2>{%
        In other words, does the following vector equation
        has \emph{only} the trivial solution?
        \begin{equation*}
            x_1
            \begin{bmatrix}
                1 \\ 2 \\ 3
            \end{bmatrix}
            +
            x_2
            \begin{bmatrix}
                4 \\ 5 \\ 6
            \end{bmatrix}
            +
            x_3
            \begin{bmatrix}
                2 \\ 1 \\ 0
            \end{bmatrix}
            =
            \begin{bmatrix}
                0 \\ 0 \\ 0
            \end{bmatrix}
        \end{equation*}
    }%
    \only<3>{%
        Thus, we consider the corresponding linear system
        \begin{equation*}
            \begin{bmatrix}
                1 & 4 & 2 & 0 \\
                2 & 5 & 1 & 0 \\
                3 & 6 & 0 & 0 \\
            \end{bmatrix}
            \sim
            \begin{bmatrix}
                1 & 4 & 2 & 0 \\
                0 & -3 & -3 & 0 \\
                0 & 0 & 0 & 0 \\
            \end{bmatrix}
        \end{equation*}
        \cake{}
        Does it have \emph{only} the trivial solution?
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{}
    Can you find a set containing only \emoji{one} vector in $\dsR^{3}$ which is linearly
    \emph{independent}?

    \cake{}
    Can you find another set containing only \emoji{one} vector in $\dsR^{3}$ which is
    linearly \emph{dependent}?
\end{frame}

\begin{frame}
    \frametitle{Linear Independence of Matrix columns}

    The columns of $A = \begin{bmatrix}\bfa_1 & \dots & \bfa_n\end{bmatrix}$ are \alert{linear independent} if
    $\{\bfa_{1}, \dots, \bfa_n\}$ are \alert{linearly independent}.

    In other words, if $A \bfx = \bfzero$ has \emph{only} the trivial solution.

    Otherwise its columns are \alert{linearly dependent}.

    \pause{}

    \cake{} Are the columns of the following matrix linearly independent?
    \begin{equation*}
        \begin{bmatrix}
            1 & 4 & 2 \\
            2 & 5 & 1 \\
            3 & 6 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Example 2}
%
%    \think{} Are the columns of the following matrix linearly independent?
%    \begin{equation*}
%        A =
%        \begin{bmatrix}
%            0 & 1 & 4 \\
%            1 & 2 & -1 \\
%            5 & 8 & 0 \\
%        \end{bmatrix}
%    \end{equation*}
%    \pause{}%
%    To answer the question, we consider the corresponding linear system
%    \begin{equation*}
%        \begin{bmatrix}
%            0 & 1 &  4 & 0 \\
%            1 & 2 & -1 & 0 \\
%            5 & 8 &  0 & 0 \\
%        \end{bmatrix}
%        \sim
%        \begin{bmatrix}
%            1 & 2 & -1 & 0 \\
%            0 & 1 &  4 & 0 \\
%            0 & 0 & 13 & 0 \\
%        \end{bmatrix}
%    \end{equation*}
%    \cake{}
%    Does it have \emph{only} the trivial solution?
%\end{frame}

\subsection{Sets of One or Two Vectors}

\begin{frame}
    \frametitle{Example 3 --- Two vectors}

    The following vectors are \emph{linearly dependent}
    \begin{equation*}
        \bfv_1 = \begin{bmatrix}
            3 \\ 1
        \end{bmatrix}
        ,
        \bfv_2 = \begin{bmatrix}
            6 \\ 2
        \end{bmatrix}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.7,
            every node/.style={black},
            ]
            % Set background color
            \fill[white] (-1,-1) rectangle (7,3);
            % Draw a grid
            \draw[thin,gray!40] (0,0) grid (6,2);
            % Draw x and y axes
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$\bfv_2$}%
            \drawvector{3,1}{$\bfv_1$}%
        \end{tikzpicture}
        \caption{Linearly dependent}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Two vectors}

    The following vectors are \emph{linearly independent}
    \begin{equation*}
        \bfv_1 = \begin{bmatrix}
            3 \\ 2
        \end{bmatrix}
        ,
        \bfv_2 = \begin{bmatrix}
            6 \\ 2
        \end{bmatrix}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.7,
            every node/.style={black},
            ]
            % Set background color
            \fill[white] (-1,-1) rectangle (7,3);
            % Draw x and y axes
            \draw[thin,gray!40] (0,0) grid (6,2);
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$\bfv_2$}
            \drawvector{3,2}{$\bfv_1$}
        \end{tikzpicture}
        \caption{Linearly independent}%
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{When Are Two Vectors Linearly Independent?}

    Two vectors are linearly \emph{dependent} if and only if one is
    a multiple of the other.

    \cake{} Is $\{\bfu, \bfzero\}$ linearly independent?
    Does the answer depend on $\bfu$?
\end{frame}

\subsection{Sets of Two or More Vectors}

\begin{frame}
    \frametitle{Theorem 7 --- First Part}
    The set $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ with $p \ge 2$
    is \emph{linearly dependent} if and only if
    \emph{at least one vector} in $S$ is a \emph{linear combination}
    of the others.

    \only<1>{%
        Proof of $\Rightarrow$ (only if):
    }%
    \only<2>{%
        Proof of $\Leftarrow$ (if):
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 --- Second Part}

    If $S$ is \emph{linearly dependent} and $\bfv_1 \ne \bfzero$,
    then $\bfv_j$ for some $j > 1$ is a linear combination of
    the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Consider $\bfu$ and $\bfv$ in the picture.

    When $\{\bfu, \bfv, \bfw\}$ is linearly dependent,
    is it possible that $\bfw \notin \Span{\bfu, \bfv}$?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=0.03,
                x  = {(-19.0cm,-6.0cm)},
                y  = {(10.0cm,-6.0cm)},
                z  = {(0cm,8.0cm)}]

                % Draw the axes
                \draw[axis] (0,0,0) -- (8.5,0,0) node[anchor=north east]{$x_1$};
                \draw[axis] (0,0,0) -- (0,8.5,0) node[anchor=north west]{$x_2$};
                \draw[axis] (0,0,0) -- (0,0,5.5) node[anchor=south]{$x_3$};

                % Define locations for arrows
                \coordinate (u) at (3,1,0);
                \coordinate (v) at (1,6,0);
                \coordinate (w) at (2.3,5,8);
                \coordinate (wxy) at (2.3,5,0);
                % Draw arrows
                \draw[SeaGreen, fill=SeaGreen, fill opacity=0.3] (0,0,0) -- (4,0,0) --
                    ($(u) + (v)$) -- (0,7,0) -- cycle;
                \drawvectorIII{$(u)$}{$\bfu\only<1>{ = \begin{bmatrix} 3 \\ 1 \\ 0 \end{bmatrix}}$};
                \drawvectorIII{$(v)$}{$\bfv\only<1>{ = \begin{bmatrix} 1 \\ 6 \\ 0 \end{bmatrix}}$};
                \drawvectorIII{$(w)$}{$\bfw$};
                \draw[dashed] (w) -- (wxy) node[vector point, at end] {};
            \end{tikzpicture}
        }%
        \caption{%
                Can this happen?
        }%
    \end{figure}

\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 8 --- Too many vectors!}

    Any set $\{\bfv_{1}, \dots, \bfv_{p}\} \subseteq \dsR^{n}$
    is \emph{linearly dependent} if $p > n$.

    Proof: Consider the linear system 
    \begin{equation*}
        \begin{bmatrix} \bfv_1 & \bfv_2 & \dots & \bfv_p \end{bmatrix} 
        \bfx
        = 
        \bfzero  
    \end{equation*}

    \cake{} How many variables does the system have?

    \cake{} What is the maximum number of basic variables the system can have?
\end{frame}

%\begin{frame}
%    \frametitle{Example}
%
%    Are the vectors in the picture linearly dependent?
%
%    \begin{figure}
%        \centering
%        \colorbox{white}{%
%            \begin{tikzpicture}[
%                scale=2.0,
%                every node/.style={black},
%                ]
%                \draw[step=0.5,thin,lightgray] (-1,0) grid (1,2);
%                % Draw x and y axes
%                \draw[thin] (-1.0,0) -- (1.3,0) node[anchor=west] {$x_1$};
%                \draw[thin] (0,0) -- (0,2.3) node[anchor=south] {$x_2$};
%                % Set seed for random number generator
%                \pgfmathsetseed{46}
%                % Define random angles for arrows
%                \pgfmathsetmacro{\a}{rand*360}
%                \pgfmathsetmacro{\b}{rand*360}
%                \pgfmathsetmacro{\c}{rand*360}
%                \pgfmathsetmacro{\d}{rand*360}
%                % Draw arrows
%                \drawvector{\a:1}{$v_1$};
%                \drawvector{\b:1.5}{$v_2$};
%                \drawvector{\c:2}{$v_3$};
%            \end{tikzpicture}
%        }%
%    \end{figure}
%\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 9 --- The Zero Vector}

    If $S = \{\bfv_{1},\dots, \bfv_{p}\}$ contains $\bfzero$,
    then the set is \emph{linearly dependent}.

    \cake{} Why?
\end{frame}

\begin{frame}[t]
    \frametitle{Example 6}

    \cake{} Are these columns of each matrix linearly dependent?
    \[
            \left[\begin{array}{cccc}
                    1 & 2 & 3 & 4 \\
                    7 & 0 & 1 & 1 \\
                    6 & 9 & 5 & 8 \\
            \end{array}\right]
            \qquad
            \left[\begin{array}{ccc}
                    2 & 0 & 1 \\
                    3 & 0 & 1 \\
                    5 & 0 & 8 \\
            \end{array}\right]
            \qquad
            \left[\begin{array}{cc}
                    -2 & 3 \\
                    4 & -6 \\
                    6 & -9 \\
                    10 & 15 \\
            \end{array}\right]
    \]
\end{frame}

\end{document}
