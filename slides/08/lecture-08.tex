\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Characterizations of Invertible Matrices, Partitioned Matrices}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{2.3}{All odd number questions from 1 to 47.}
            \sectionhomework[\footnotesize]{2.4}{All odd number questions from 1 to 27.}
            \emoji{robot} \href{http://webwork.dukekunshan.edu.cn/webwork2}{WeBWork} Homework 2
        \end{column}
    \end{columns}
\end{frame}

\section{2.3 Characterizations of Invertible Matrices}

\subsection{The Invertible Matrix Theorem}

\begin{frame}
    \frametitle{Theorem 8 --- The \acf{imt}}

    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
        \only<1>{%
            \item[a.] $A$ is an invertible matrix.
            \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
            \item[c.] $A$ has $n$ pivot positions.
            \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
            \item[e.] The columns of $A$ form a linearly independent set.
            \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
        }%
        \only<2>{%
            \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
            \item[h.] The columns of $A$ spans $\dsR^n$.
            \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
            \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
            \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
            \item[l.] $A^T$ is an invertible matrix.
        }%
    \end{enumerate}

    \hint{} There is no need to mesmerize this. 
    But if asked, you should be able to prove it.
\end{frame}

\begin{frame}[c]
    \frametitle{A road map of proof}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{tikzpicture}
                    \begin{scope}
                        % Define nodes in a circle
                        \node (a) at (90:1.5cm) {(a)};
                        \node (b) at (162:1.5cm) {(b)};
                        \node (c) at (234:1.5cm) {(c)};
                        \node (d) at (306:1.5cm) {(d)};
                        \node (j) at (18:1.5cm) {(j)};

                        % Draw arrows between nodes
                        \draw[arrownode] (a) -- (j) ;
                        \draw[arrownode] (j) -- (d) ;
                        \draw[arrownode] (d) -- (c) ;
                        \draw[arrownode] (c) -- (b) ;
                        \draw[arrownode] (b) -- (a) ;
                    \end{scope}
                    \begin{scope}[yshift=-3.5cm]
                        % Define nodes in a circle
                        \node (a) at (210:1cm) {(a)};
                        \node (k) at (90:1cm) {(k)};
                        \node (g) at (-30:1cm) {(g)};

                        % Draw arrows between nodes
                        \draw[arrownode] (a) -- (k);
                        \draw[arrownode] (k) -- (g);
                        \draw[arrownode] (g) -- (a);
                    \end{scope}
                \end{tikzpicture}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \begin{tikzpicture}[yscale=0.8]
                    % Define nodes in a circle
                    \node (g) at (-2, 4) {(g)};
                    \node (h) at (0,  4) {(h)};
                    \node (i) at (2,  4) {(i)};

                    % Draw arrows between nodes
                    \draw[doublearrow] (g) -- (h);
                    \draw[doublearrow] (h) -- (i);
                    % Define nodes in a circle
                    \node (d) at (-2, 2) {(d)};
                    \node (e) at (0, 2) {(e)};
                    \node (f) at (2, 2) {(f)};

                    % Draw arrows between nodes
                    \draw[doublearrow] (d) -- (e);
                    \draw[doublearrow] (e) -- (f);
                    % Define nodes in a circle
                    \node (a) at (-1, 0) {(a)};
                    \node (l) at (1, 0) {(l)};

                    % Draw arrows between nodes
                    \draw[doublearrow] (a) -- (l);
                \end{tikzpicture}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- Proof of (j) \rightarrow{} (d)}

    \begin{enumerate}
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
    \end{enumerate}

    \think{} How to prove (j) \rightarrow{} (d)?

    \hint{} Proof by contrapositive, i.e., $\neg \text{(d)} \imp \neg \text{(j)}$.
\end{frame}

\begin{frame}
    \label{frame:ajdcb}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[c.] $A$ has $n$ pivot positions.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
    \end{enumerate}

    \think{} How to prove
    \begin{itemize}
        \item (a) \rightarrow{} (j) \cake{}
        \item (j) \rightarrow{} (d) --- See the previous slide.
        \item (d) \rightarrow{} (c) \cake{}
        \item (c) \rightarrow{} (b) --- $A$ is of size $n \times n$.
        \item (b) \rightarrow{} (a) --- Theorem 7 (2.2).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}
    \begin{enumerate}
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
    \end{enumerate}
    
    \think{} How to prove (k) \rightarrow{} (g)?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[c.] $A$ has $n$ pivot positions.
    \end{enumerate}

    \think{} How to prove
    \begin{itemize}
        \item (a) \rightarrow{} (k) \cake{}
        \item (k) \rightarrow{} (g) --- See the previous slide.
        \item (g) \rightarrow{} (c) --- By Theorem 4 (1.4).
        \item (c) \rightarrow{} (a) --- See page~\pageref{frame:ajdcb}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
    \end{enumerate}

    \think{} How to prove
    \begin{itemize}
        \item (g) $\leftrightarrow$ (h) --- By Theorem 4 (1.4).
        \item (g) $\leftrightarrow$ (i) \cake{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
    \end{enumerate}

    \think{} How to prove
    \begin{itemize}
        \item (d) $\leftrightarrow$ (e) \cake{}
        \item (e) $\leftrightarrow$ (f) --- By Theorem 12 (1.9)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}

    \think{} How to prove
    \begin{itemize}
        \item (a) \rightarrow{} (l)
        \item (l) \rightarrow{} (a)
    \end{itemize}

    Both follows from Theorem 6 (2.2)
    \begin{block}{Theorem 6}
        \begin{enumerate}
            \item[c.] If $A$ is invertible, then so is $A^{T}$, and $(A^{T})^{-1} = (A^{-1})^{T}$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{Invertible Linear Transformations}%

\begin{frame}
    \frametitle{Invertible Transformations}

    A linear transform
    $T: \dsR^{n} \mapsto \dsR^{n}$
    is \alert{invertible} if there exists
    $S: \dsR^{n} \mapsto \dsR^{n}$
    such that
    \begin{equation*}
        \begin{aligned}
            & S(T(\bfx)) = \bfx, \qquad \text{for all } \bfx \in \dsR^{n} \\
            & T(S(\bfx)) = \bfx, \qquad \text{for all } \bfx \in \dsR^{n} \\
        \end{aligned}
    \end{equation*}

    The transformation $S$ is called the \alert{inverse} of $T$.

    %\cake{} Prove that if $T$ is invertible, 
    %then it must be a \emph{onto} mapping.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            % Vertically sheared square for x
            \begin{scope}[shift={(-4,-1.5)},xslant=0.5]
                \draw[codomain] (0,0) rectangle ++(2,2) node[black, midway] (x) {$\bfx$};
            \end{scope}
            % Vertically sheared square for T(x)
            \begin{scope}[shift={(2,-1.5)},xslant=0.5]
                \draw[codomain] (0,0) rectangle ++(2,2) node[black, midway] (y) {$\alt<2>{S}{T}(\bfx)$};
            \end{scope}
            % Draw an arrow from x to y, curving upward
            \draw[->, thick] (x) to[bend left] node[midway, above] {\alt<2>{$S$}{$T$}} (y);
            % Draw an arrow from y to x, curving upward
            \draw[->, thick] (y) to[bend left] node[midway, below] {\alt<2>{$T$}{$S$}} (x);
        \end{tikzpicture}
        \caption{%
            \only<1>{$S(T(\bfx)) = \bfx, \qquad \text{for all } \bfx \in \dsR^{n}$}
            \only<2>{$T(S(\bfx)) = \bfx, \qquad \text{for all } \bfx \in \dsR^{n}$}
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 9}

    Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
    let $A$ be its standard matrix.

    Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Prove that if a linear mapping $T:\dsR^{n} \mapsto \dsR^{n}$ is one-to-one,
    then $T$ is invertible.

    Proof:
    \only<1>{%
        \begin{block}{Step 1: Theorem 12 (1.9)}
            Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation,
            and let $A$ be its standard matrix.

            Then $T$ is one-to-one if and only if the columns of $A$ are \emph{linearly
            independent}.
        \end{block}
    }%
    \only<2>{%
        \begin{block}{Step 2: Use \ac{imt}}
            \begin{itemize}
                \item[a.] $A$ is an invertible matrix.
                \item[e.] The columns of $A$ form a linearly independent set.
            \end{itemize}
        \end{block}
    }%
    \only<3>{%
        
        \begin{block}{Step 3: Theorem 9}
            Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
            let $A$ be its standard matrix.
            Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
        \end{block}
    }%
\end{frame}

\section{2.4 Partitioned Matrices}%

\begin{frame}
    \frametitle{Example 1 --- Partition a matrix into blocks}

    How to write this matrix
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{cccccc}
                a_{11} & a_{12} & a_{13} & a_{14} & a_{15} & a_{16} \\ 
                a_{21} & a_{22} & a_{23} & a_{24} & a_{25} & a_{26} \\
                a_{31} & a_{32} & a_{33} & a_{34} & a_{35} & a_{36} \\ 
            \end{array}
        \right]
    \end{equation*}
    as a $2 \times 3$ \alert{partitioned/block matrix}?

    \pause{}

    \emoji{fire}
    Monition:
    \begin{itemize}
        \item Useful when the matrices are too large for the memory of computers.
        \item Speed up computations by using GPU to parallelize the computation.
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{partition-example-01.jpg}
%    \end{figure}
%\end{frame}

\subsection{Multiplication of Partitioned Matrices}

\begin{frame}
    \frametitle{Example 3 --- Multiplication}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc|cc}
                4 & 1 & 8 & 3 & -2 \\
                0 & -5 & 2 & 7 & 9 \\ \hline
                -1 & 6 & -3 & 5 & 2 \\
            \end{array}
        \right]
        ,
        \qquad
        B
        =
        \left[
            \begin{array}{cc}
                -8 & 9 \\
                2 & -5 \\
                7 & 4 \\ \hline
                0 & -6 \\
                -9 & 1 \\
            \end{array}
        \right]
    \end{equation*}
    \think{}
    How to compute $A B$ using matrix partitions?
\end{frame}

%\begin{frame}
%    \frametitle{Multiplication}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{partition-example-02.jpg}
%        \includegraphics[width=\linewidth]{partition-example-03.jpg}
%    \end{figure}
%
%\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    \think{}
    How can we compute
    \begin{equation*}
        \left[ \begin{array}{c|c|c}
                -3 & 1 & 2 \\
                1 & -4 & 5 \\
        \end{array} \right]
        \left[ \begin{array}{cc}
                a & b \\ \hline
                c & d \\ \hline
                e & f \\
        \end{array} \right]
    \end{equation*}
    using matrix partitions?
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Column-Row Expansion}

    If $A$ is $m \times n$ and $B$ is $n \times p$, then
    \begin{equation*}
        \begin{aligned}
            A B
            &
            =
            \begin{bmatrix}
                \mcol_1(A) & \mcol_2(A) & \cdots & \mcol_n(A)
            \end{bmatrix}
            \begin{bmatrix}
                \mrow_1(B) \\ \mrow_2(B) \\ \cdots \\ \mrow_n(B)
            \end{bmatrix}
            \\
            &
            =
            \mcol_1(A)
            \mrow_1(B)
            +
            \mcol_2(A)
            \mrow_2(B)
            +
            \cdots
            +
            \mcol_n(A)
            \mrow_n(B)
            .
        \end{aligned}
    \end{equation*}

    The proof is simply to check with the row-column rule.
\end{frame}

\subsection{Inverses of Partitioned Matrices}

\begin{frame}
    \frametitle{Example 5 --- Inverses of Partitioned Matrices}

    \only<1>{%
        The matrix
        \begin{equation*}
            A =
            \begin{bmatrix}
                A_{11} & A_{12} \\
                0 & A_{22} \\
            \end{bmatrix}
        \end{equation*}
        is said to be \alert{block upper triangular}.

        Assume that $A_{11}$ is of size $p \times p$ and $A_{22}$ is of size $q \times q$,
        and $A$ is invertible. 
        \think{}
        How can we compute $A^{-1}$?

    }%

    \only<2->{%
        Denote $A^{-1}$ by $B$ and partition it so that
        \begin{equation*}
            \begin{bmatrix}
                A_{11} & A_{12} \\
                0 & A_{22} \\
            \end{bmatrix}
            \begin{bmatrix}
                B_{11} & B_{12} \\
                B_{21} & B_{22} \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                I_{p} & 0 \\
                0 & I_{q} \\
            \end{bmatrix}
        \end{equation*}
    }%
    \only<2>{%
        \think{} Can we write down a system of equations to find $B$?
    }%

    \only<3>{%
        Solve the system of equations, we get
        \begin{equation*}
            B
            =
            \begin{bmatrix}
                B_{11} & B_{12} \\
                B_{21} & B_{22} \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                A_{11}^{-1} & -A_{11}^{-1} A_{12} A_{22}^{-1} \\
                0 & A_{22}^{-1} \\
            \end{bmatrix}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Block Diagonal Matrix}

    A \alert{block diagonal matrix} is a \emph{partitioned matrix} with zero blocks off the main
    diagonal, i.e., a matrix of the following form:
    \begin{equation*}
        \begin{bmatrix}
            A_{11} & 0 \\
            0 & A_{22} \\
        \end{bmatrix}
    \end{equation*}

    Such a matrix is invertible if and only if each block on the diagonal
    is invertible, i.e., $A_{11}$ and $A_{22}$ are invertible.

    \emoji{eyes} See Exercises 15 and 16.

    \pause{}
    \cake{}
    Is the following matrix invertible?
    \begin{equation*}
        \left[
            \begin{array}{cc|c}
                1 & 3 & 0 \\
                2 & 6 & 0 \\ \hline
                0 & 0 & 4 \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\end{document}
