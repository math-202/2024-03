\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Difference Equations}

\input{../tikz.tex}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.8}{1, 3, 7, 13, 17, 19, 21, 23, 27, 31, 33, 35}
        \end{column}
    \end{columns}
\end{frame}

\section{4.8 Applications to Linear Difference Equations}

\subsection{Linear Independence in the Space \texorpdfstring{$\dsS$}{S} of Signals}

\begin{frame}
    \frametitle{Linear Independence in $\dsS$}

    Three signals $\{u_{k}\}$, $\{v_{k}\}$, $\{w_{k}\}$
    are \alert{linearly independent} if
    \begin{equation*}
        c_{1} \{u_{k}\}
        +
        c_{2} \{v_{k}\}
        +
        c_{3} \{w_{k}\}
        =
        \{0\}
    \end{equation*}
    \only<1>{%
        or equivalently
        \begin{equation*}
            c_{1} u_{k}
            +
            c_{2} v_{k}
            +
            c_{3} w_{k}
            =
            0
            ,
            \qquad
            \text{for all }
            k \in \dsZ
        \end{equation*}
    }%
    implies $c_{1} = c_{2} = c_{3} = 0$.
    \only<2->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=6,
                    ymin=-1.5, ymax=1.5,
                    xtick={-5,...,5},
                    ytick={-1,0,1},
                    xlabel=$k$,
                    domain=-5:5,
                    samples=11,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.9\textwidth,
                    height=0.3\textwidth,
                    axis background/.style={fill=white},
                    legend style={at={(0.1,1.1)}, anchor=north west, font=\footnotesize,
                    line width=0.5pt},
                    line width=2pt,
                    axis line style={thin},
                    ]

                    \only<2>{%
                        \addplot[
                            ycomb,
                            draw={JuliaOrange},
                            mark=square*,
                            mark options={JuliaOrange, fill=JuliaOrange, scale=1.2},
                            ] {ifthenelse(x == 2, 1, 0)};
                        \addlegendentry{$S^{2}(\delta)$};
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                            ] {ifthenelse(x == 1, 1, 0)};
                        \addlegendentry{$S(\delta)$};
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=triangle*,
                            mark options={BrightRed, fill=BrightRed, scale=0.8},
                            ] {ifthenelse(x == 0, 1, 0)};
                        \addlegendentry{$\delta$};
                    }%
                    \only<3>{%
                        \addplot[
                            ycomb,
                            draw={SeaGreen},
                            mark=square*,
                            mark options={SeaGreen, fill=SeaGreen, scale=1.2},
                            ] {ifthenelse(x == 0, -1, 0)};
                        \addlegendentry{$-\delta$};
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                            ] {ifthenelse(x == 0, 1, 0)};
                        \addlegendentry{$\delta$};
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=triangle*,
                            mark options={BrightRed, fill=BrightRed, scale=0.8},
                            ] {0};
                        \addlegendentry{$\{0\}$};
                    }%
                \end{axis}
            \end{tikzpicture}
            \caption{%
                \cake{}
                Are these three signals linearly independent?
            }%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Casorati matrix}
    \only<1->{%
        The equation
        \begin{equation}
            \label{eq:1}
            c_{1} \{u_{k}\}
            +
            c_{2} \{v_{k}\}
            +
            c_{3} \{w_{k}\}
            =
            \{0\}
            \tag{1}
        \end{equation}
        is equivalent to
        \begin{equation*}
            \begin{bmatrix}
                u_{k} & v_{k} & w_{k} \\
                u_{k+1} & v_{k+1} & w_{k+1} \\
                u_{k+2} & v_{k+2} & w_{k+2} \\
            \end{bmatrix}
            \begin{bmatrix}
                c_{1} \\ c_{2} \\ c_{3}
            \end{bmatrix}
            =
            \begin{bmatrix}
                0 \\ 0 \\ 0
            \end{bmatrix}
            ,
            \qquad
            \text{for all }
            k \in \dsZ
        \end{equation*}
        The matrix is call \alert{Casorati matrix} of signals, denoted by $C(k)$.
    }%


    \only<2>{%
        \cake{}
        If the $C(k)$ is \blankshort{} for some $k$, then
        \eqref{eq:1} has a unique solution
        \begin{equation*}
            c_1 = c_2 = c_3 = 0
        \end{equation*}
        In this case, $\{u_k\}$, $\{v_k\}$ and $\{w_k\}$ are linearly independent.
    }%
\end{frame}

\begin{frame}
    \frametitle{Two Quick Examples}

    Let's check the matrix $C(0)$ for the following signals.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=-6, xmax=6,
                ymin=-1.5, ymax=1.5,
                xtick={-5,...,5},
                ytick={-1,0,1},
                xlabel=$k$,
                domain=-5:5,
                samples=11,
                clip=false,
                scale only axis=true,
                % set `width' and `height' to the desired values
                width=0.9\textwidth,
                height=0.3\textwidth,
                axis background/.style={fill=white},
                legend style={at={(0.1,1.1)}, anchor=north west, font=\footnotesize,
                line width=0.5pt},
                line width=2pt,
                axis line style={thin},
                ]

                \only<1>{%
                    \addplot[
                        ycomb,
                        draw={JuliaOrange},
                        mark=square*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=1.2},
                        ] {ifthenelse(x == 2, 1, 0)};
                    \addlegendentry{$\{w_k\}=S^{2}(\delta)$};
                    \addplot[
                        ycomb,
                        draw={DaylightBlue},
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                        ] {ifthenelse(x == 1, 1, 0)};
                    \addlegendentry{$\{v_k\}=S(\delta)$};
                    \addplot[
                        ycomb,
                        draw={BrightRed},
                        mark=triangle*,
                        mark options={BrightRed, fill=BrightRed, scale=0.8},
                        ] {ifthenelse(x == 0, 1, 0)};
                    \addlegendentry{$\{u_k\}=\delta$};
                }%
                \only<2>{%
                    \addplot[
                        ycomb,
                        draw={SeaGreen},
                        mark=square*,
                        mark options={SeaGreen, fill=SeaGreen, scale=1.2},
                        ] {ifthenelse(x == 0, -1, 0)};
                    \addlegendentry{$\{u_k\}=-\delta$};
                    \addplot[
                        ycomb,
                        draw={DaylightBlue},
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                        ] {ifthenelse(x == 0, 1, 0)};
                    \addlegendentry{$\{v_k\}=\delta$};
                    \addplot[
                        ycomb,
                        draw={BrightRed},
                        mark=triangle*,
                        mark options={BrightRed, fill=BrightRed, scale=0.8},
                        ] {0};
                    \addlegendentry{$\{w_k\}=\{0\}$};
                }%
            \end{axis}
        \end{tikzpicture}
        \caption{%
            \cake{}
            Are these three signals linearly independent?
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Are $1^{k}$, $(-2)^{k}$ and $3^{k}$ linearly independent?

    Let's check the Casorati matrix $C(0)$.
\end{frame}

\begin{frame}
    \frametitle{\astonished{} A Warning}

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\textwidth]{bunny-sleep-in-rain.jpg}
            \caption{It's \emoji{cloud-with-rain}! I will go to class when it's \emoji{sun}!}
        \end{figure}
    }%
    \only<2->{%
        Suppose it is true that:
        \begin{itemize}
            \item If it is \emoji{cloud-with-rain}, I will skip Linear Algebra class.
        \end{itemize}
        Does this mean that
        \begin{itemize}
            \item If it is \emoji{sun} tomorrow, I will \emph{not} skip Linear Algebra class.
        \end{itemize}
    }%
    \only<3>{%
        We know that:
        \begin{itemize}
            \item If $C(k)$ is \emph{invertible} for some $k$, then the signals are linearly
                \emph{independent}.
        \end{itemize}
        Does this mean that
        \begin{itemize}
            \item If $C(k)$ is \emph{singular} for some $k$, then the signals are linearly
                \emph{dependent}.
        \end{itemize}
    }%
\end{frame}

\begin{frame}
    \frametitle{Casorati Matrix and Linear Difference Equations}

    If $C(k)$ is \emph{singular} for some $k$, then the signals may or may not be linearly \emph{dependent}.

    \pause{}

    However, if the signals are solutions of the \emph{same} \alert{homogeneous linear
    difference equation}, then
    \begin{itemize}
        \item either all Casorati matrices are singular and the signals are linearly \emph{dependent},
        \item or all Casorati matrices are invertible and signals are linearly \emph{independent}.
    \end{itemize}

    \dizzy{} What is a homogeneous linear difference equation?
\end{frame}

\subsection{Linear Difference Equations}

\begin{frame}[c]
    \frametitle{Fibonacci's Puzzle}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            \only<1>{%
                No \bunny{} in month $0$ and before.

                At month $1$, one pair of baby \bunny{}.

                A pair of baby \bunny{} matures after one month.

                Every month after, a mature pair produces a baby pair.  

                The \bunny{} never \emoji{skull}.
            }%
            \only<2>{%
                Let $f_k$ denote the pairs of \bunny{} you have at month $k$.

                The sequence
                \begin{equation*}
                    \{f_k\}
                    =
                    \dots, 0, 0, 0, 1, 1, 2, 3, 5, \dots
                \end{equation*}
                is called the \alert{Fibonacci sequence}.

                \cake{}
                Can you guess the next few numbers?
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-never-dies.jpg}
                \caption{When \bunny{} never die}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Linear Difference Equations}
    \only<1-2>{%
        It is easy to guess that $\{f_k\}$ satisfies
        \begin{equation}
            \label{eq:linear-difference-equation}
            f_{k+2} - f_{k+1} - f_{k} = 0, \qquad \text{for all } k \in \dsZ
        \end{equation}
        This is called a \alert{linear difference equations}.
    }%

    \only<1>{%
        \wink{} Why is this true? Come to COMPSCI 203 to learn more.
    }%

    \only<2->{%
        Let $a_{0}, \dots, a_{n}$ be scalars with $a_{0}$ and $a_{n}$ being nonzero,
        and let $\{y_{k}\}, \{z_{k}\}$ be signals.
        The equation
        \begin{equation*}
            a_{0} y_{k+n}
            +
            a_{1} y_{k+n-1}
            +
            \dots
            a_{n} y_{k}
            =
            z_{k}
            ,
            \qquad
            \text{for all }
            k \in \dsZ.
        \end{equation*}
        is called a \alert{linear difference equation} of \alert{order $n$}.
    }%
    \only<2>{%
        What are $n, a_{0}, \dots a_{n}$ and $\{z_n\}$ 
        in \eqref{eq:linear-difference-equation}?
    }%

    \only<3>{%
        If $z_{k} =0$ for all $k$, then it is call \alert{homogeneous}.

        Otherwise it is \alert{nonhomogeneous}.
    }%
\end{frame}

\begin{frame}
    \frametitle{The shift \acf{lti}}

    \only<1>{%
        Recall that $S^{n}$ is the \ac{lti}
        \begin{equation*}
            S^{n}(\{y_{k}\}) = \{y_{k-n}\}
        \end{equation*}
    }%

    \only<2>{%
        Then $c S^{n}$ is the \ac{lti}
        \begin{equation*}
            c S^{n}(\{y_{k}\}) = \{c y_{k-n}\}
        \end{equation*}
    }%

    \only<3>{%
        \cake{} Can you describe $2 S^{-2}(\{y_{k}\})$?
    }%
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=-6, xmax=6,
                ymin=0.0, ymax=2.5,
                xtick={-5,...,5},
                ytick={0,0.5,...,2},
                xlabel=$k$,
                domain=-5:5,
                samples=11,
                clip=false,
                scale only axis=true,
                % set `width' and `height' to the desired values
                width=0.9\textwidth,
                height=0.4\textwidth,
                axis background/.style={fill=white},
                legend style={at={(0.1,0.9)}, anchor=north west, font=\footnotesize,
                line width=0.5pt},
                line width=2pt,
                axis line style={thin},
                ]

                \only<1>{%
                    \addplot[
                        ycomb,
                        draw={DaylightBlue},
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                        ] {ifthenelse(x == 2, 1, 0)};
                    \addlegendentry{$S^{2}(\{y_{k}\})$};
                }%
                \only<2>{%
                    \addplot[
                        ycomb,
                        draw={DaylightBlue},
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                        ] {ifthenelse(x == 3, 1.5, 0)};
                    \addlegendentry{$\frac{3}{2} S^{2}(\{y_{k}\})$};
                }%
                \only<1->{%
                    \addplot[
                        ycomb,
                        draw={BrightRed},
                        mark=square*,
                        mark options={BrightRed, fill=BrightRed, scale=0.8},
                        ] {ifthenelse(x == 0, 1, 0)};
                    \addlegendentry{$\{y_{k}\}$};
                }%
            \end{axis}
        \end{tikzpicture}
    \end{figure}
    
\end{frame}

\begin{frame}
    \frametitle{Linear Difference Equations and \acf{lti}}

    Let
    \begin{equation*}
        T 
        = 
        a_{0} S^{-n}
        +
        a_{1} S^{-n+1}
        +
        \dots
        +
        a_{n} S^{0}
    \end{equation*}
    Then
    \begin{equation*}
        T(\{y_{k}\})
        =
        \{
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        \}
    \end{equation*}

    \pause{}

    Thus the equation
    \begin{equation*}
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    is equivalent to
    \begin{equation*}
        T(\{y_{k}\}) = \{z_{k}\}.
    \end{equation*}
    An \ac{lti} such as $T$ is called a \alert{filter}.
\end{frame}

\pgfmathdeclarefunction{cosfunc}{2}{%
    \pgfmathparse{cos(deg((#2*pi*#1)/4))}%
}
\pgfmathdeclarefunction{combinedFunc}{2}{%
    \pgfmathparse{%
        (sqrt(2)/4)*cosfunc(#1+2, #2) + 
        (1/2)*cosfunc(#1+1, #2) + 
        (sqrt(2)/4)*cosfunc(#1, #2)
    }%
}
\begin{frame}
    \frametitle{Example 3}
    Consider the filter
    \begin{equation*}
        T = 
        \frac{\sqrt{2}}{4} 
        S^{-2}
        +
        \frac{1}{2} S^{-1}
        +
        \frac{\sqrt{2}}{4} S^{0}
    \end{equation*}
    \only<2>{%
        Then
        \begin{equation*}
            T(\{y_{k}\})
            =
            \big\{
                \blanklong{}
            \big\}
        \end{equation*}

        \think{} What happens when we apply the filter?
    }%
    \only<3->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=0, xmax=14,
                    ymin=-1.3, ymax=1.3,
                    xtick={0,1,2},
                    ytick={-1,0,1},
                    xlabel=$k$,
                    domain=0:12,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.8\textwidth,
                    height=0.4\textwidth,
                    axis background/.style={fill=white},
                    legend style={at={(0.8,0.9)}, anchor=north west, font=\footnotesize}
                    ]

                    \only<3>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                            samples=13,
                            ] {cosfunc(x, 1)};
                        \addlegendentry{$\left\{y_{k}\right\}=\left\{\cos\left(\frac{\pi k}{4}\right)\right\}$};
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=*,
                            mark options={BrightRed, fill=BrightRed, scale=1.0},
                            samples=13,
                            ] {combinedFunc(x, 1)};
                        \addlegendentry{$T(\{y_{k}\})$};
                        \addplot[
                            smooth,
                            draw={DaylightBlue},
                            samples=50,
                            ] {cosfunc(x, 1)};
                        \addplot[
                            smooth,
                            draw={BrightRed},
                            samples=50,
                            ] {combinedFunc(x, 1)};
                    }%
                    \only<4>{%
                        \addplot[
                            ycomb,
                            draw={DaylightBlue},
                            mark=*,
                            mark options={DaylightBlue, fill=DaylightBlue, scale=1.0},
                            samples=13,
                            ] {cosfunc(x, 3)};
                        \addlegendentry{$\left\{y_{k}\right\}=\left\{\cos\left(\frac{3 \pi k}{4}\right)\right\}$};
                        \addplot[
                            ycomb,
                            draw={BrightRed},
                            mark=*,
                            mark options={BrightRed, fill=BrightRed, scale=1.0},
                            samples=13,
                            ] {combinedFunc(x, 3)};
                        \addlegendentry{$T(\{y_{k}\})$};
                        \addplot[
                            smooth,
                            draw={DaylightBlue},
                            samples=50,
                            ] {cosfunc(x, 3)};
                        \addplot[
                            smooth,
                            draw={BrightRed},
                            samples=50,
                            ] {combinedFunc(x, 3)};
                    }%
                \end{axis}
            \end{tikzpicture}
        \end{figure}
    }%
    \only<4>{%
        \hint{} Filters filter out signals of a particular frequency!
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Given the homogeneous linear difference equation
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    how can we find a $\{y_k\}$ that satisfies it?

    \only<1>{%
        In other words, we want to find the signals that will be filtered out.

        This is called \alert{solving}  the equation or \alert{finding a solution}.
    }%

    \only<2->{%
        \think{}
        Maybe we can try if we can find a number $r$ such that $\{r^{k}\}$ is a solution?
    }%

    \only<3>{%
        Answer: $r = 3, 1, -2$ will work.
    }%
\end{frame}

\begin{frame}
    \frametitle{\zany{} Why $\{r^{k}\}$?}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-scratch-paper.jpg}
        \caption{Because people have tried it before!}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Solutions of Homogeneous Linear Difference Equations}

    In general, $r^{k}$ is a solution of the equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    if $r$ is a \alert{root (solution)} of
    the \alert{auxiliary equation} 
    \begin{equation*}
        x^{n}
        +
        a_{1} x^{n-1}
        +
        \dots
        +
        a_{n}
        =
        0
        ,
    \end{equation*}
    where $x$ is unknown variable.

    \think{} Does this method give all the solutions?
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{} Find \emph{all} solutions of
%    \begin{equation*}
%        f_{k+2} - f_{k+1} - f_{k} = 0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ,
%    \end{equation*}
%\end{frame}

\subsection{Solution Sets of Linear Difference Equations}

\begin{frame}
    \frametitle{A Vector Space}

    Let
    \begin{equation*}
        T 
        = 
        a_{0} S^{-n}
        +
        a_{1} S^{-n+1}
        +
        \dots
        +
        a_{n} S^{0}
    \end{equation*}
    Then the solution set of the homogeneous linear difference equation
    \begin{equation*}
        T(\{y_{k}\})
        =
        \{0\}
    \end{equation*}
    is the \blankshort{} of the \ac{lti} $T$.

    \pause{}

    \astonished{} So the solution set is a \blankshort{} of $\dsS$.

    \think{} What is its dimension? How to find a basis?
\end{frame}

\begin{frame}
    \frametitle{Theorem 19}

    If $a_n \ne 0$, 
    and $\{z_{k}\}$ is given, 
    the equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    has a unique solution $\{y_{k}\}$,
    whenever $y_{0}, \dots, y_{n-1}$ are specified.
\end{frame}

\begin{frame}
    \frametitle{Theorem 20}

    \only<1>{%
        The set $H$ of all solutions of the $n$-th order homogeneous linear difference equation
        \begin{equation*}
            y_{k+n}
            +
            a_{1} y_{k+n-1}
            +
            \dots
            +
            a_{n} y_{k}
            =
            0
            ,
            \qquad
            \text{for all }
            k \in \dsZ,
        \end{equation*}
        is an $n$-dimensional vector space.
    }%

    \only<2->{%
        Proof: 
    }%
    \only<2-3>{%
        For $\{y_{k}\} \in H$, let
        \begin{equation*}
            F(\{y_{k}\})
            =
            (y_{0}, \dots, y_{n-1})
        \end{equation*}
    }%
    \only<3-4>{%
        Then $F: H \mapsto \dsR^n$ is
        \begin{itemize}
            \item a linear transformation,
            \item one-to-one by Theorem 19,
            \item onto by Theorem 19.
        \end{itemize}
    }%
    \only<4>{%
        Thus, $F: H \mapsto \dsR^n$ is an isomorphism.

        And we have $\dim H = \dim R^{n} = n$ by the following:

        \begin{block}{Exercise 52 (4.5)}
             Isomorphic finite-dimensional vector spaces have the same dimension.
        \end{block}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find a basis of the solution set of
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}

    Answer: $\{3^{k}, 1^k, (-2)^k\}$.
\end{frame}


\subsection{Nonhomogeneous Equations}

\begin{frame}
    \frametitle{Review: Theorem 6 (1.5)}
    The solution set of a \emph{nonhomogeneous} linear system $A \bfx = \bfb$
    is of the form $\bfp + \bfv_{h}$,
    where $\bfp$ is one particular solution,
    and $\bfv_{h}$ is any solution of $A \bfx = \bfzero$.

    \begin{figure}[htpb]
        \begin{tikzpicture}[
            scale=1.2,
            every node/.style={black},
            ]
            % Set background color
            \fill[white] (-2.5,-1.5) rectangle (6.5,2.5);
            % Draw x and y axes
            \draw[gray!40] (-1,-1) grid (5,2);
            \draw (-0.5,0) -- (5,0) node[right]{$x_1$};
            \draw (0,-0.5) -- (0,2) node[above]{$x_2$};
            \draw[domain=-1.5:4.5,smooth,variable=\x,BrightRed,thick] plot ({\x},{\x/2});
            \draw[domain=-1.5:4.5,smooth,variable=\x,SeaGreen,thick] plot ({\x},{\x/2-1/2});
            \node (l1) at (5.5, 1.3) {$A \bfx = \bfb$};
            \draw[thick, SeaGreen] (l1) -- +(1,0);
            \node[above=0.5 cm of l1.west,anchor=west] (l2) {$A \bfx = \bfzero$};
            \draw[thick, BrightRed] (l2) -- +(1,0);
            \only<1>{%
                \drawvector{2,0.5}{$\bfp$};
                \drawvector{0,0}{$\bfv_h$};
            }%
            \only<2>{%
                \drawvector{1.0,0.5}{$\bfv_h$};
                \begin{scope}[shift={(1,0.5)}]
                    \drawvector{2,0.5}{$\bfp$};
                \end{scope}
            }%
            \only<3>{%
                \drawvector{-1.0,-0.5}{$\bfv_h$};
                \begin{scope}[shift={(-1,-0.5)}]
                    \drawvector{2,0.5}{$\bfp$};
                \end{scope}
            }%
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Nonhomogeneous Equations}

    The general solution of
    \begin{equation}
        \label{eq:nonhomogeneous}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}
    can be written as a particular solution of \eqref{eq:nonhomogeneous}
    plus any solution of
    \begin{equation}
        \label{eq:homogeneous}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        .
    \end{equation}

    The proof is the same as that of Theorem 6 (1.5).
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    \only<1-2>{%
        Verify that $k^2$ is a solution
        \begin{equation}
            \label{eq:nonhomogeneous:1}
            y_{k+2}
            -
            4 y_{k+1}
            +
            3 y_{k}
            =
            -4 k
            ,
            \qquad
            \text{for all }
            k \in \dsZ
            ,
        \end{equation}
        \pause{}
        Then find all solutions of to \eqref{eq:nonhomogeneous:1}.
    }%
    \only<3>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\textwidth]{4.8-fig-2.jpg}
            \caption{A geometric viusalization}
        \end{figure}
    }%
\end{frame}

\subsection{Reduction to Systems of First-Order Equations}

\begin{frame}
    \frametitle{Example 6}

    \think{} How can we write the following equation
    \begin{equation}
        y_{k+3}
        -
        2 y_{k+2}
        -
        5 y_{k+1}
        +
        6 y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}
    as
    \begin{equation*}
        \begin{bmatrix} y_{k+1} \\ y_{k+2} \\ y_{k+3} \end{bmatrix}
        = 
        A
        \begin{bmatrix} y_k \\ y_{k+1} \\ y_{k+2} \end{bmatrix}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
    \end{equation*}
\end{frame}

\begin{frame}[label=current]
    \frametitle{First-Order Equations}

    In general
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation*}
    can be written as
    \begin{equation*}
        \bfx_{k+1} = A \bfx_{k}
    \end{equation*}
    where
    \begin{equation}
        \label{eq:first-order:1}
        \bfx_{k} =
        \begin{bmatrix}
            y_{k}     \\
            y_{k+1}   \\
            \vdots    \\
            y_{k+n-1} \\
        \end{bmatrix}
        ,
        \qquad
        A
        =
        \begin{bmatrix}
            0      & 1        & 0        & \dots  &     0   \\
            0      & 0        & 1        &        &     0   \\
            \vdots &          &          & \ddots & \vdots  \\
            0      & 0        & 0        &        & 1       \\
            -a_{n} & -a_{n-1} & -a_{n-2} & \dots  & -a_{1}  \\
        \end{bmatrix}
    \end{equation}
    The equation \eqref{eq:linear-difference-equation} is called a \alert{first-order difference equation} .
\end{frame}

\end{document}
