\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Change of Basis}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.6}{1, 3, 5, 7, 15, 17}
        \end{column}
    \end{columns}
\end{frame}

\section{4.5 The Dimension of a Vector Space}

\subsection{Subspaces of a Finite-Dimensional Space}

\begin{frame}[standout]
    We find a basis of a subspace by adding more and more linearly independent vectors.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{bunny-subspace-basis.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 12 --- Dimensions of Subspaces}

    Let $H$ be a subspace of a finite-dimensional vector space $V$.

    \only<1->{%
        Any linearly independent set in $H$ can be expanded,
        if \emph{necessary}, to a basis for $H$.
        Moreover,
        \begin{equation*}
            \dim H \le \dim V
        \end{equation*}
    }%
    \only<2->{%
        Proof: 
    }%
    \only<2>{%
        Let $S = \{\bfu_{1}, \dots, \bfu_{p}\}$ be linearly independent.

        If $\Span{S} = H$, then $S$ is a \blankshort{} of $H$.

        Otherwise, there exists $\bfu_{k+1}$ which is not in $\Span{S}$.
        Then
        \begin{equation*}
            \{\bfu_1, \dots, \bfu_k, \bfu_{k+1}\}
        \end{equation*}
        is larger linearly independent set.
    }%
    \only<3>{%
        But since $\dim V < \infty$, we can add at most $\dim V$ vectors to $S$.

        When we stop, we must have a basis for $H$.

        Since this basis has at most $\dim V$ elements, $\dim H \le \dim V$.
    }%
\end{frame}

\begin{frame}[standout]
    When we have the correct number of \alt<2>{spanning vectors}{linearly independent vectors},
    we always have a basis.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{bunny-subspace-smiling.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- The Basis Theorem}

    Let $V$ be a $p$-dimensional vector space with $p \ge 1$.

    \only<1-2>{%
        Any linearly independent set of $p$ vectors is a basis of $V$.

    }%
    \only<2>{%
        Proof: By Theorem 12, if
        \begin{equation*}
            S = \{\bfv_{1}, \dots, \bfv_{p}\}
        \end{equation*}
        is linearly independent, we can extend it to a basis for $V$.

        But by Theorem 10, $V$ is $p$-dimensional means adding more vectors to $S$ will
        make it \blanklong{}.

        So $S$ must already be a basis for $V$.
    }%

    \only<3->{%
        Any set of $p$ vectors spanning $V$ is a basis of $V$.
    }%

    \only<4->{%
        Proof: %
    }%
    \only<4-5>{%
        We can use
        \begin{block}{Theorem 5 (4.3): The Spanning Set Theorem}
            Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$
            and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
            \begin{enumerate}
                \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
            \end{enumerate}
        \end{block}
    }%
    \only<5-6>{%
        By this theorem,
        \begin{equation*}
            \Span{\bfv_{1}, \dots, \bfv_{p}} = V
        \end{equation*}
        then some subset $S'$ of $S$ is a basis of $V$.

    }%
    \only<6->{%
        Since $V$ is $p$-dimensional, $\abs{S'} = \blankshort{}$.

        Hence $S = S'$ and $S$ is a basis of $V$.
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let $H$ be a subspace of $\dsR^{n}$ with $\dim H = n$.

    In other words, $H$ has a basis $\scB = \{\bfb_1, \dots, \bfb_n\}$.
    Thus
    \begin{itemize}
        \item $\scB$ is linearly independent,
        \item and $\Span{\scB} = H$.
    \end{itemize}

    \cake{} Is it possible that $H \ne \dsR^{n}$?
\end{frame}

\subsection{The Dimensions of Null Space, Column Space, and Row Space}

\begin{frame}
    \frametitle{The Rank of a Matrix}

    The \alert{rank} of an $m \times n$ matrix $A$ is the dimension of
    $\colspace A$, which is the number of pivot columns by the following theorem.

    \only<1>{%
        \begin{block}{Theorem 6 (4.3): Bases of column spaces}
            The pivot columns of a matrix $A$ form a basis for $\colspace A$.
        \end{block}
    }%

    \only<2>{%
        \begin{block}{Example 9 (4.3)}
            Since
            \begin{equation*}
                A =
                \begin{bmatrix}
                    1 & 4 & 0 &  2 & -1\\
                    3 & 12 & 1 & 5 & 5 \\
                    2 & 8 & 1 &  3 & 2 \\
                    5 & 20 & 2 &  8 & 8 \\
                \end{bmatrix}
                \sim
                \begin{bmatrix}
                    1 & 4 & 0 &  2 & 0\\
                    0 & 0 & 1 & -1 & 0 \\
                    0 & 0 & 0 &  0 & 1 \\
                    0 & 0 & 0 &  0 & 0 \\
                \end{bmatrix}
                =
                B
                ,
            \end{equation*}
            and the $\{\bfb_1, \bfb_3, \bfb_5\}$ is a basis of $\colspace B$,
            $\{\bfa_1, \bfa_3, \bfa_5\}$ is a basis of $\colspace A$.

            Thus $\rank A = \dim \colspace A = \dim \colspace B = 3$.
        \end{block}
    }%
\end{frame}

\begin{frame}
    \frametitle{The nullity of a matrix}
    The \alert{nullity} of an $m \times n$ matrix $A$ is the dimension of
    $\nullspace A$, which is the number of free variables in $A \bfx =
    \bfzero$ (see 4.2).

    \pause{}

    \begin{block}{Example 3 (4.2)}
        \small{}
        Let
        \begin{equation*}
            A =
            \left[
                \begin{array}{ccccc}
                    -3 & 6 & -1 & 1 & -7 \\
                    1 & -2 & 2 & 3 & -1 \\
                    2 & -4 & 5 & 8 & -4 \\
                \end{array}
            \right]
            .
        \end{equation*}
        The solution of $A \bfx = \bfzero$ is
        \begin{equation*}
            \bfx
            =
            x_{2}
            \begin{bmatrix}
                2 \\ 1 \\ 0 \\ 0 \\ 0
            \end{bmatrix}
            +
            x_{4}
            \begin{bmatrix}
                1 \\ 0 \\ -2 \\ 1 \\ 0
            \end{bmatrix}
            +
            x_{5}
            \begin{bmatrix}
                -3 \\ 0 \\ 2 \\ 0 \\ 1
            \end{bmatrix}
            =
            x_{2} \bfu + x_{4} \bfv + x_{5} \bfw.
        \end{equation*}
        Thus $\{\bfu, \bfv, \bfw\}$ is a basis of $\nullspace A$ and $\dim \nullspace A = 3$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 14 --- The Rank Theorem}

    Let $A$ be a $m \times n$ matrix. We have
    \begin{equation*}
        \rank A + \nullity A = m
    \end{equation*}

    \pause{}

    Proof:
    Let
    \begin{equation*}
        A
        =
        \left[\begin{array}{cccccccccc}
                0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                0 & 0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                0 & 0 & 0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \bbs{} & \bast{} \\
        \end{array}\right]
    \end{equation*}

    \cake{} How many basic variables and free variables do we have in $A$?

    \cake{} What about the rank and nullity of $A$?
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    \cake{} Consider
    \begin{equation*}
        A =
        \begin{bmatrix}
            -3 & 6 & -1 & 1 & -7 \\
            1 & -2 & 2 & 3 & -1 \\
            2 & -4 & 5 & 8 & -4 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & -2 & 2 & 3 & -3 \\
            0 & 0 & 1 & 2 & -2 \\
            0 & 0 & 0 & 0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    \cake{} What are the nullity and rank of $A$?
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    \only<1>{%
        \cake{} If A is a $7 \times 9$ matrix with a two-dimensional null space, what is the rank of A?
    }%

    \only<2>{%
        \cake{} Could a $6 \times 9$ matrix have a two-dimensional null space?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 7 --- The Big Picture of Linear Algebra}

    \cake{} What are $\colspace A$, $\nullspace A$, $\colspace A^{T}$ and $\nullspace A^T$ where
    \begin{equation*}
        A = 
        \begin{bmatrix} 
            3 & 0 & -1 \\
            3 & 0 & -1 \\
            4 & 0 & 5
        \end{bmatrix}
        ,
        \qquad
        A^{T}=
        \begin{bmatrix} 
            3 & 3 & 4 \\
            0 & 0 & 0 \\
            -1 & -1 & 5
        \end{bmatrix} 
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{4.5-fig-2.jpg}
    \end{figure}

    \astonished{} In Chatper 6, we will see that $\nullspace A$ and $\rowspace A$ are perpendicular!
\end{frame}

\subsection{Applications to Systems of Equations}

\begin{frame}
    \frametitle{Example 8}
    Let $A$ be a $40 \times 42$ matrix.

    Suppose that $\nullspace A$ has a basis of $2$ vectors.

    \think{} Does
    \begin{equation*}
        A \bfx = \bfb
    \end{equation*}
    have a solution for all $\bfb \in \dsR^{40}$?

    \cake{} What is the condition for this to be true?
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Show that
%    \begin{equation*}
%        \scB = \{1, 1-t, 2 -4 t + t^2, 6-18t+9t^2-t^3\}
%    \end{equation*}
%    forms a basis of $\dsP_{3}$.
%
%    \begin{block}{Proof}
%        \small{}
%        First we can to show that these vectors are \blankshort{}
%        by showing that their coordinate vectors are \blankshort{}.
%
%        Then by Theorem \blankshort{} in this section, 
%        they form a basis of $\dsP_{3}$ because \blankshort{}.
%    \end{block}
%\end{frame}

%\subsection*{\zany{} None-integer ``dimensions''}
%
%\begin{frame}
%    \frametitle{Infinite:dimensional vector space}
%    
%    \cake{} Can you guess what is the dimension of the vector space
%    $\dsS$ be the set of doubly infinite sequences like
%    \begin{equation*}
%        \{y_{k}\} = (\cdots, y_{-3}, y_{-2}, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
%    \end{equation*}
%
%    \pause{}
%
%    \cake{} What about
%    Let $C([0,1])$ be to the set of continuous function on the interval $[0,1]$?
%
%    \dizzy{} Do they have the same dimension?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Fractal dimensions}
%    The word \emph{dimension} in mathematics in general means \emph{complexity}.
%
%    The following \alert{fractal} has a \alert{fractal dimension} of
%    $1.585$.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.4\linewidth]{Sierpinski_triangle.jpg}
%        \caption{Sierpinski Triangle}
%    \end{figure}
%
%    \emoji{clapper} See this \href{https://youtu.be/gB9n2gHsHN4}{video} by  
%    3Blue1Brown.
%\end{frame}

\subsection{Rank and the Invertible Matrix Theorem}

\begin{frame}
    \frametitle{Theorem 8 (2.3) --- The Invertible Matrix Theorem}

    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
        \footnotesize{}
        \item[a.] $A$ is an invertible matrix.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
        \item[c.] $A$ has $n$ pivot positions.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{The Invertible Matrix Theorem (cont.)}

    \dizzy{} Let's add a few more.

    \begin{enumerate}
        \item[m.] The columns of $A$ is a basis of $\dsR^{n}$
        \item[n.] $\colspace A = \dsR^{n}$
        \item[o.] $\dim \colspace A = n$
        \item[p.] $\rank A = n$
        \item[q.] $\nullspace A = \{\bfzero\}$
        \item[r.] $\dim \nullspace A = 0$
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Practice Problem 1}
%    
%    Let $V$ be  a nonzero finite-dimensional vector space.
%    Are the following two statements true?
%
%    \begin{enumerate}[<+->]
%        \item If $\dim V = p$ and if $S$ is a linearly dependent subset of $V$, then $S$ contains more than $p$ vectors.
%        \item If $S$ spans $V$ and if $T$ is a subset of $V$ that contains more vectors than $S$, then $T$ is linearly dependent.
%    \end{enumerate}
%\end{frame}

\section{4.6 Change of Basis}

\begin{frame}
    \frametitle{Two Bases}

    Consider two bases 
    $\scB=\{\bfb_{1}, \bfb_{2}\}$ 
    and
    $\scC=\{\bfc_{1}, \bfc_{2}\}$.

    \cake{} What the coordinates $[\bfx]_{\scB}$
    and $[\bfx]_{\scC}$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{change-of-basis-01.jpg}
        \caption{Two bases and the same vector}%
    \end{figure}

    \think{} Given $[\bfx]_{\scB}$, how do we compute $[\bfx]_{\scC}$
    and vice versa.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider two bases
    $\scB = \{\bfb_1, \bfb_2\}$
    and
    $\scC = \{\bfc_1, \bfc_2\}$, such that
    \begin{equation*}
        \bfb_1 = 4 \bfc_1 + \bfc_2,
        \qquad
        \bfb_2 = -6 \bfc_1 + \bfc_2
        .
    \end{equation*}
    Suppose that we are given
    \begin{equation*}
        \bfx = 3 \bfb_1 + \bfb_2.
    \end{equation*}
    \cake{} What is $[\bfx]_\scB$?

    \think{} What is $[\bfx]_\scC$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 15 --- Change of bases}

    Let
    $\scB = \{\bfb_1, \bfb_2, \dots, \bfb_n\}$
    and
    $\scC = \{\bfc_1, \bfc_2, \dots, \bfc_n\}$
    be two bases of $V$.
    Then
    \begin{equation*}
        [\bfx]_{\scC}
        =
        \underset{\scC\leftarrow\scB}{P}
        \cdot
        [\bfx]_{\scB}
        ,
    \end{equation*}
    \pause{}
    where
    \begin{equation*}
        \underset{\scC\leftarrow\scB}{P}
        =
        \begin{bmatrix}
            [\bfb_1]_{\scC} &
            [\bfb_2]_{\scC} &
            \cdots &
            [\bfb_n]_{\scC}
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{change-of-basis-02.jpg}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{A digression}
%
%    \cake{} Show that for any basis $\scB$ of $V$,
%    \begin{equation*}
%        [\bfzero]_{\scB}
%        =
%        \begin{bmatrix}
%            0 \\
%            0 \\
%            \vdots \\
%            0
%        \end{bmatrix}
%    \end{equation*}
%
%    \pause{}
%
%    \think{}  Show that a
%    $\{\bfu_{1}, \dots, \bfu_{p}\}$
%    in $V$ is linearly independent if and only if the set of coordinate vectors
%    $\{[\bfu_{1}]_{\scB}, \dots, [\bfu_{p}]_{\scB}\}$
%    is linearly independent in $\dsR^n$.
%\end{frame}

\begin{frame}
    \frametitle{The Other Way}

    The matrix
    \begin{equation*}
        \underset{\scC\leftarrow\scB}{P}
        =
        \begin{bmatrix}
            [\bfb_1]_{\scC} &
            [\bfb_2]_{\scC} &
            \cdots &
            [\bfb_n]_{\scC}
        \end{bmatrix}
        .
    \end{equation*}
    is invertible because its columns are linearly independent.

    \only<1>{%
        \puzzle{} Prove that they are linearly independent using that $[ \quad ]_\scC: V
        \mapsto \dsR^{n}$ is a linear transformation.
    }%

    \pause{}

    Thus
    \begin{equation*}
        [\bfx]_{\scC}
        =
        \underset{\scC\leftarrow\scB}{P}
        \cdot
        [\bfx]_{\scB}
        ,
    \end{equation*}
    implies
    \begin{equation*}
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        \cdot
        [\bfx]_{\scC}
        =
        [\bfx]_{\scB}
        .
    \end{equation*}
    In other words,
    \begin{equation*}
        \underset{\scB\leftarrow \scC}{P}
        =
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        .
    \end{equation*}
\end{frame}

\subsection{Change of Basis in \texorpdfstring{$\dsR^{n}$}{R^n}}

\begin{frame}
    \frametitle{Change of Basis in $\dsR^{2}$}

    Let $\scE$ be the standard basis of $\dsR^{2}$.
    Consider another two bases:
    \begin{equation*}
        \scB =
        \left\{
            \bfb_1,
            \bfb_2
        \right\}
        ,
        \qquad
        \scC =
        \left\{
            \bfc_1,
            \bfc_2
        \right\}
    \end{equation*}

    \only<1>{%
        \cake{} What are $[\bfb_1]_\scE$, $[\bfb_2]_\scE$, $[\bfc_1]_\scE$, and $[\bfc_2]_\scE$?
    }%

    \only<2>{%
        Then
        \begin{equation*}
            \begin{aligned}
                \underset{\scE \leftarrow \scB}{P}
                &
                =
                \begin{bmatrix}
                    [\bfb_1]_{\scE},[\bfb_2]_{\scE}
                \end{bmatrix}
                =
                \begin{bmatrix}
                    \underline{\hspace{2cm}}
                \end{bmatrix}
                =
                P_{\scB}
                ,
                \\
                \underset{\scE \leftarrow \scC}{P}
                &
                =
                \begin{bmatrix}
                    [\bfc_1]_{\scE},[\bfc_2]_{\scE}
                \end{bmatrix}
                =
                \begin{bmatrix}
                    \underline{\hspace{2cm}}
                \end{bmatrix}
                =
                P_{\scC}
                .
            \end{aligned}
        \end{equation*}

        \cake{} Given 
        $\underset{\scE \leftarrow \scC}{P}$,
        what is
        $\underset{\scC \leftarrow \scE}{P}$?

    }%

    \only<3>{%
        Thus
        \begin{equation*}
            \underset{\scC \leftarrow \scB}{P}
            =
            \underset{\scC \leftarrow \scE}{P}
            \cdot
            \underset{\scE \leftarrow \scB}{P}
            =
            \left(\underset{\scE \leftarrow \scC}{P}\right)^{-1}
            \cdot
            \underset{\scE \leftarrow \scB}{P}
            =
            P_\scC^{-1} 
            \cdot
            P_\scB
            .
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Consider two bases of $\dsR^{2}$ ---
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                -9 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -5 \\ -1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \scC =
        \left\{
            \begin{bmatrix}
                1 \\ -4
            \end{bmatrix}
            ,
            \begin{bmatrix}
                3 \\ -5
            \end{bmatrix}
        \right\}
        .
    \end{equation*}

    \cake{} What are
    \begin{equation*}
        \underset{\scE \leftarrow \scB}{P}
        \qquad
        \underset{\scE \leftarrow \scC}{P}
        \qquad
        \underset{\scC \leftarrow \scE}{P}
        \qquad
        \underset{\scC \leftarrow \scB}{P}
    \end{equation*}

    \pause{}

    %\think{} Compute
    %$
    %\underset{\scC \leftarrow \scB}{P}
    %$
    %using
    %\begin{equation*}
    %    \begin{bmatrix}
    %        \underset{\scE \leftarrow \scC}{P}
    %        &
    %        \underset{\scE \leftarrow \scB}{P}
    %    \end{bmatrix}
    %    \sim
    %    \begin{bmatrix}
    %        I
    %        &
    %        \underset{\scC \leftarrow \scB}{P}
    %    \end{bmatrix}
    %\end{equation*}

    To summarize, to compute
    $\underset{\scC \leftarrow \scB}{P}$
    we can use the fact that
    \begin{equation*}
        \begin{bmatrix}
            P_\scC
            &
            P_\scB
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            I
            &
            \underset{\scC \leftarrow \scB}{P}
        \end{bmatrix}
            .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        \bfb_{1} = \begin{bmatrix} 1 \\ -3 \end{bmatrix}
        \qquad
        \bfb_{2} = \begin{bmatrix} -2 \\ 4 \end{bmatrix}
        \qquad
        \bfc_{1} = \begin{bmatrix} -7 \\ 9 \end{bmatrix}
        \qquad
        \bfc_{2} = \begin{bmatrix} -5 \\ 7 \end{bmatrix}
    \end{equation*}
    and consider the two bases of $\dsR^{2}$
    $\scB = \{\bfb_1, \bfb_2\}$
    and
    $\scC = \{\bfc_1, \bfc_2\}$.

    \only<1>{%
        \sweat{}
        Compute $\underset{\scB \leftarrow \scC}{P}$.
    }%

    \only<2>{%
        \cake{}
        Compute $\underset{\scC \leftarrow \scB}{P}$ given that
        \begin{equation*}
            \underset{\scB \leftarrow \scC}{P}
            =
            \begin{bmatrix}
                \cake{}
                5 & 3 \\
                6 & 4
            \end{bmatrix}
        \end{equation*}
    }%
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Let $\scF = \{f_1, f_2\}$ and $\scG = \{g_1, g_2\}$ be bases for a vector space $V$, and let $P$ be a matrix
%    whose columns are $[f_1]_G$ and $[f_2]_G$. Which of the following equations is satisfied
%    by $P$ for all $v \in V$?
%    \begin{enumerate}
%        \item $[v]_\scF = P [v]_\scG$
%        \item $[v]_\scG = P [v]_\scF$
%    \end{enumerate}
%\end{frame}

\end{document}
