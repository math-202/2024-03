\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{3d}

\title{Lecture \lecturenum{} --- Null Spaces, Column Spaces, Row Spaces and Linear Transformations}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{4.2}{All odd number questions from 1 to 47.}
        \end{column}
    \end{columns}
\end{frame}

\section{4.2 Null Spaces, Column Spaces, Row Spaces and Linear Transformations}

\subsection{The Null Space of a Matrix}

\begin{frame}[c]
    \frametitle{\zany{} Null}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{null} (noun)  --- The number $0$.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-null-space.jpg}
                \caption{%
                    Explicating the null space!
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Null Spaces}

    Let $A$ be a $m \times n$ matrix.

    The \alert{null space} of $A$ is the solution
    set of $A \bfx = \bfzero$, denoted by $\nullspace A$, i.e.,
    \begin{equation*}
        \nullspace A = \{\bfx: \bfx \in \dsR^{n}, A \bfx = \bfzero\}
    \end{equation*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{null-space.jpg}
    \end{figure}

    \cake{} Why is $\nullspace A$ drawn as a line in the picture?
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc}
                1 & -3 & -2 \\
                -5 & 9 & 1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu =
        \left[
            \begin{array}{c}
                5 \\
                3 \\
                -2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    \cake{} Is $\bfu$ in $\nullspace A$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 2: A Null Space is a Vector Space}

    The null space of $A$ is a subspace of $\dsR^{n}$.

    \think{} Why does this imply that it is a vector space?

    \vspace{-1em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{Properties of Subspaces}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \vspace{-0.5em}
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$.
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \think{} Show that the following $H$ is a subspace of $\dsR^{4}$.
    \begin{equation*}
        H
        =
        \left\{
            \left[
                \begin{array}{c}
                    a \\
                    b \\
                    c \\
                    d \\
                \end{array}
            \right]
            :
            a - 2b + 5c = d,
            c -a = b
        \right\}
        .
    \end{equation*}
\end{frame}

\subsection{An Explicit Description of \texorpdfstring{$\nullspace A$}{Nul A}}

\begin{frame}
    \frametitle{Example 3}

    Finding a spanning set for the null space of the matrix
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
        \sim
        \left[
            \begin{array}{cccccc}
                1 & -2 & 0 & -1 & 3 \\
                0 & 0 & 1 & 2 & -2 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
        =
        U
    \end{equation*}

    \pause{}
    Answer:
    \[
        \begin{bmatrix}
            x_1 \\
            x_2 \\
            x_3 \\
            x_4 \\
            x_5
        \end{bmatrix}
        =
        x_2
        \begin{bmatrix}
            2 \\
            1 \\
            0 \\
            0 \\
            0
        \end{bmatrix}
        +
        x_4
        \begin{bmatrix}
            1 \\
            0 \\
            -2 \\
            1 \\
            0
        \end{bmatrix}
        +
        x_5
        \begin{bmatrix}
            -3 \\
            0 \\
            2 \\
            0 \\
            1
        \end{bmatrix}
        =
        x_1 \bfu + x_2 \bfv + x_3 \bfw
    \]
    Thus the spanning set of $\nullspace A$ is $\{ \bfu, \bfv, \bfw \}$.
\end{frame}

\begin{frame}
    \frametitle{Example 3: Two Facts}

    For any matrix $A$, two things are true for $\nullspace A$:

    \begin{block}{Fact 1}
        The vectors which spans $\nullspace A$ found using the method in Example 3, such as
        \only<2>{$\bfu, \bfv, \bfw$}
        \only<1>{%
            \begin{equation*}
                \bfu
                =
                \begin{bmatrix}
                    2 \\
                    1 \\
                    0 \\
                    0 \\
                    0
                \end{bmatrix}
                ,
                \qquad
                \bfv
                =
                \begin{bmatrix}
                    1 \\
                    0 \\
                    -2 \\
                    1 \\
                    0
                \end{bmatrix}
                ,
                \qquad
                \bfw
                =
                \begin{bmatrix}
                    -3 \\
                    0 \\
                    2 \\
                    0 \\
                    1
                \end{bmatrix}
            \end{equation*}
        }%
        are always linearly independent.
    \end{block}

    \only<2->{%
        \begin{block}{Fact 2}
            When $\nullspace A$ contains nonzero vectors, 
            the number of vectors in the spanning set for $\nullspace A$
            equals the number of free variables in the equation $A \bfx = \bfzero$.
        \end{block}
    }%
\end{frame}

\subsection{The Column Space of a Matrix}

\begin{frame}
    \frametitle{Column spaces}

    Let $A$ be a $m \times n$ matrix. 
    The \alert{column space} of $A$ is the span
    of its columns, denoted by $\colspace A$.

    If $A = [\bfa_{1}\, \dots \bfa_{n}]$, then
    \begin{equation*}
        \colspace A = \Span{\bfa_{1}\, \dots \bfa_{n}}
    \end{equation*}

    In other words, letting $T(\bfx) = A \bfx$, then
    \begin{equation*}
        \colspace A = \{\bfb: \bfb = T (\bfx) \text{ for some } \bfx \in \dsR^{n}\}.
    \end{equation*}
    \cake{} So we can call $\colspace A$ the \blankshort{} of $T$.
\end{frame}


\begin{frame}
    \frametitle{Theorem 3: Column Spaces Are Subspaces}

    The column space of an $m \times n$ matrix $A$ is a subspace of $\dsR^{m}$.

    \pause{}
    This follows directly from:

    \begin{block}{Theorem 1: The span of vectors}
        If $\bfv_1, \dots, \bfv_p$ are in a vector space $V$, then $\Span{\bfv_1, \dots,
        \bfv_p}$ is a subspace of $V$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find $A$ such that
    \begin{equation*}
        \colspace A =
        \left\{
            \begin{bmatrix}
                6a - b \\
                a + b \\
                -7 a \\
            \end{bmatrix}
            :
            a, b \in \dsR
        \right\}
        .
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                yscale=0.4,
                xscale=0.9,
                x  = {(-0.4cm,-0.2cm)},
                y  = {(1.8cm,-0.8cm)},
                z  = {(0cm,0.25cm)}]
                % Define style for the axes and cubes
                \tikzstyle{axis} = [->,black,thick];


                % Original vectors
                \coordinate (v1) at (6, 1, -7);
                \coordinate (v2) at (-1, 1, 0);

                % Linear combinations
                \coordinate (v1_plus_v2) at ($(v1)+(v2)$);
                \coordinate (v1_minus_v2) at ($(v1)-(v2)$);
                \coordinate (minus_v1_plus_v2) at ($-1*(v1)+(v2)$);
                \coordinate (minus_v1_minus_v2) at ($-1*(v1)-(v2)$);

                \draw[white,fill=MintCream, opacity=0.9] 
                    (v1_plus_v2) -- 
                    (minus_v1_plus_v2) -- 
                    (minus_v1_minus_v2) -- 
                    (v1_minus_v2) -- 
                    cycle;

                \node[vector point] at (0,0,0) {};

                % Draw the axes
                \draw[axis] (0,0,0) -- (0,0,7) node[anchor=south]{$x_3$};
                \draw[axis] (0,0,0) -- (7,0,0) node[anchor=north east]{$x_1$};
                \draw[axis] (0,0,0) -- (0,2,0) node[anchor=north west]{$x_2$};

                \node (h) at (-2,2,-8) {$\colspace A$};
                \draw[thin,dotted] (h) -- ($0.8*(v1_plus_v2)$);
                \drawvectorIII{v1}{left:\tiny $\begin{bmatrix} 6 \\ 1 \\ -7 \end{bmatrix} $};
                \drawvectorIII{v2}{above left:\tiny $\begin{bmatrix} -1 \\ 1 \\ 0 \end{bmatrix} $};
            \end{tikzpicture}
        \end{center}
        \caption{The column space of $A$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A Fact about Column Spaces}

    The column space of an $m \times n$ matrix $A$ is all of $\mathbb{R}^m$
    if and only if the equation $A \bfx = \bfb$ has a (at least one) solution for each $\bfb$ in
    $\mathbb{R}^m$.

    \pause{}

    This follows from the following theorem:
    \begin{block}{Theorem 4 (1.4)}

        If $A = \begin{bmatrix} \bfa_1 & \dots & \bfa_n \end{bmatrix}$ 
        is a $m \times n$ matrix
        and $\bfx \in \dsR^n$, 
        then the followings are equivalent
        \begin{enumerate}
            \item[a.] For each $\bfb \in \dsR^{m}$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
                \emph{consistent}.
            \item[c.] For each $\bfb \in \dsR^{m}$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{Row Space}

\begin{frame}
    \frametitle{Row space}

    The \alert{row space} of $A$, denote by $\rowspace A$,  is $\colspace A^{T}$.

    \pause{}

    \begin{exampleblock}{Example 5}
        Let
        \begin{equation*}
            A
            =
            \begin{bmatrix}
                -2 & -5 & 8 & 0 & -17 \\
                1 & 3 & -5 & 1 & 5 \\
                3 & 11 & -19 & 7 & 1 \\
                1 & 7 & -13 & 5 & -3 \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                \bfr_1 \\ \bfr_2 \\ \bfr_3 \\ \bfr_4
            \end{bmatrix}
        \end{equation*}
        Then
        \begin{equation*}
            \rowspace A = \colspace A^{T}
            = \Span{\bfr_{1}^{T}, \dots, \bfr_{4}^{T}}.
        \end{equation*}

        \cake{} What $\rowspace A^{T}$?
    \end{exampleblock}
\end{frame}

\subsection{The Contrast Between \texorpdfstring{$\nullspace A$}{Nul A} and \texorpdfstring{$\colspace A$}{Col A}}

\begin{frame}[c]
    \frametitle{Example 6--8}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            Let
            \begin{equation*}
                A =
                \left[
                    \begin{array}{cccc}
                        2 & 4 & -2 & 1 \\
                        -2 & -5 & 7 & 3 \\
                        3 & 7 & -8 & 6 \\
                    \end{array}
                \right]
            \end{equation*}
            The solution of $A \bfx = \bfzero$ is
            \begin{equation*}
                \begin{aligned}
                    x{_1} & = -9 \cdot x{_3} \\
                    x{_2} & = 5 \cdot x{_3} \\
                    x{_3} & = x{_3} \\
                    x{_4} & = 0 \\
                \end{aligned}
            \end{equation*}
        \end{column}
        \begin{column}{0.6\textwidth}
            \only<1>{%
                $\colspace A$ is  subspace of $\dsR^{\txtq}$
                and $\nullspace A$ is  subspace of  $\dsR^{\txtq}$
            }%
            \only<2>{%
                Find a non-zero vector in $\colspace $A.
            }%
            \only<3>{%
                Find a non-zero vector in $\nullspace $A.
            }%
            \only<4>{%
                Is $\bfu = \begin{bmatrix}3 \\ -2 \\ -1 \\ 0 \end{bmatrix}
                    \in \nullspace A$? $\colspace A$?
            }%
            \only<5>{%
                Is $ \bfv = \begin{bmatrix} 3 \\ -1 \\3 \end{bmatrix} \in \colspace A$?
                    $\nullspace A$?
            }%
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $A = \begin{bmatrix} -6 & 12 \\ 3 & 6 \end{bmatrix}$ 
    and $\mathbf{w} = \begin{bmatrix} 2 \\ 1 \end{bmatrix}$.

    Is $\mathbf{w}$ in $\mathrm{Nul}(A)$?
    Is $\mathbf{w}$ is in $\mathrm{Col}(A)$?
\end{frame}

\subsection{Kernel and Range of a Linear Transformation}

\begin{frame}[c]
    \frametitle{\zany{} Kernel}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{kernel} (noun)  ---
            \begin{enumerate}
                \item a softer, usually edible part of a nut, seed, or fruit stone contained within its shell. 
                \item the central or most important part of something. 
            \end{enumerate}
            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-kernel.jpg}
                \caption{%
                    Can you find the kernel of a linear transformation?
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Linear Transformation of Vector Spaces}

    Recall that a transformation $T:\dsR^{m} \mapsto \dsR^{n}$ is \alert{linear} if
    for all vectors $\bfu, \bfv \in \dsR^{m}$ and all scalars $c \in \dsR$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    \pause{}

    Let $V$ and $W$ be vector spaces.
    A transformation $T:V \mapsto W$ is \alert{linear} if
    for all vectors $\bfu, \bfv \in V$ and all scalars $c \in \dsR$
    \begin{enumerate}[(i)]
        \item  \underline{\hspace{5cm}}
        \item  \underline{\hspace{5cm}}
    \end{enumerate}
    \cake{} How should we finish this definition?
\end{frame}

\begin{frame}
    \frametitle{Kernels and Ranges}

    Let $T: V \mapsto W$ be a linear transformations.

    \only<1-3>{%
        The \alert{kernel} of $T$ is the solution
        \begin{equation*}
            \{\bfv \in V: T(\bfv) = \bfzero\}.
        \end{equation*}
    }%
    \only<2-3>{%
        The \alert{range} of $T$ is the subset of $W$
        \begin{equation*}
            \{\bfw \in W: T(\bfv) = \bfw \text{ for some } \bfv \in V\}.
        \end{equation*}
    }%
    \only<3>{%
        \cake{} If $T(\bfx) = A \bfx$ where $A$ is a matrix,
        what is another name for kernel of $T$?
        What above range of $T$?
    }%
    \only<4>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\linewidth]{kernel-range.jpg}
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Kernels and Ranges Are Subspaces}

    Let $T: V \mapsto W$ be a \emph{linear transformation} from a vector space $V$ to
    another vector space $W$.

    \only<1>{Then the \emph{kernel} of $T$ is a subspace of $V$.}
    \only<2>{Then the \emph{range} of $T$ is a subspace of $W$.}

    \vspace{-1em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{Properties of subspaces}
                \footnotesize{}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \vspace{-0.5em}
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$.
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{block}{Properties of linear transformation}
                \footnotesize{}
                A mapping $T:V \mapsto W$ is \alert{linear} if
                \vspace{-0.5em}
                for all $\bfu, \bfv \in V$
                \begin{enumerate}[(i)]
                    \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
                    \item  $T(c \bfu) = c T(\bfu)$.
                \end{enumerate}
            \end{block}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    For a function $f$ which is differentiable on $[a,b]$, 
    let $D(f) = f'$.

    \only<1>{
        \think{} Is $D$ a linear transformation?
        \vspace{-1.5em}
        \begin{columns}[totalwidth=\linewidth]
            \begin{column}{0.49\textwidth}
                \begin{block}{Properties of linear transformation}
                    \footnotesize{}
                    A mapping $T:V \mapsto W$ is \alert{linear} if
                    for all $\bfu, \bfv \in V$
                    \vspace{-0.5em}
                    \begin{enumerate}[(i)]
                        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
                        \item  $T(c \bfu) = c T(\bfu)$.
                    \end{enumerate}
                \end{block}
            \end{column}
            \begin{column}{0.49\textwidth}
            \end{column}
        \end{columns}
    }
    \only<2>{
        \sweat{} What are the \emph{kernel} and \emph{range} of $D$?
    }
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    Consider the equation
    \begin{equation*}
        y''(t) + \omega^{2} y(t) = 0
    \end{equation*}
    where $\omega$ is a constant.

    \think{} Find a linear transformation's \emph{kernel} is precisely the solution set this
    equation.
\end{frame}
\begin{frame}
    \frametitle{\tps{}}

    Let $A$ be an $m \times n$ matrix.

    Which of the following are true?
    \begin{enumerate}
        \item The null space of $A$ is a vector space.
        \item The null space of an $m \times n$ matrix is in $\mathbb{R}^m$.
        \item The column space of $A$ is the range of the linear transformation $x \mapsto Ax$.
        \item If the equation $Ax = b$ is consistent for some $b \in \dsR^{m}$, 
            then $\operatorname{Col} A = \mathbb{R}^m$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Exercise 48}

    Let $V$ and $W$ be vector spaces, and let $T: V \mapsto W$
    be a linear transformation.
    Given a subspace $Z$ of $W$,
    let
    \begin{equation*}
        U = \{ \bfx: T(\bfx) \in Z \}
    \end{equation*}
    \think{} Show that $U$ is a subspace of $W$.

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{Properties of Subspaces}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \vspace{-0.5em}
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$.
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
