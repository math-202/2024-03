\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Coordinate Systems}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.4}{3, 7, 9, 11, 13, 23, 25, 27, 29, 31, 41}
            \smiling{} Starting from this week, there will \emph{not} be WeBWork assignments.
        \end{column}
    \end{columns}
\end{frame}

\section{4.4 Coordinate Systems}

\subsection{Coordinates}

\begin{frame}
    \frametitle{What Is the \emoji{carrot}?}

    A \bunny{} is hungry and looking for food.

    We want help, but we can only give it some numbers.

    \cake{}
    What numbers should we give it?
    
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.6, every node/.style={black}]
                % Draw white background
                \fill[white] (-1,-1) rectangle (6,6);
                % Draw grid
                \draw[step=1cm, gray, very thin] (0, 0) grid (5,5);
                % Set seed for random number generator
                \pgfmathsetseed{48}
                % Draw x and y axes
                \draw[thick] (0,0) -- (5,0) node[anchor=west] {$x_1$};
                \draw[thick] (0,0) -- (0,5) node[anchor=south] {$x_2$};
                % Draw vector to (4,3)
                \drawvector{4,3}{\emoji{carrot}};
                \node at (0,0) {\emoji{rabbit-face}};
            \end{tikzpicture}
            \caption{How to help the \emoji{rabbit-face} finding \emoji{carrot}?}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- The Unique Representation Theorem}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a \emph{basis} of $V$.
    Then for each $\bfx \in V$, there exists a \emph{unique} list of scalars
    $c_1, \dots, c_n$
    such that
    \begin{equation*}
        \bfx = c_1 \bfb_1 + \dots c_n \bfb_n
    \end{equation*}

    \pause{}

    Proof by Contradiction \emoji{crossed-swords}:
    Suppose that there is a different list of scalars $d_1, \dots, d_n$
    such that
    \begin{equation*}
        \bfx = d_1 \bfb_1 + \dots d_n \bfb_n
    \end{equation*}
    then
\end{frame}

\begin{frame}
    \frametitle{The Definition of Coordinates}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.

    The \alert{coordinates of $\bfx$ relative to $\scB$}
    are the unique list of scalars $c_1, \dots, c_n$ which satisfy
    \begin{equation*}
        \bfx = c_1 \bfb_1 + \dots c_n \bfb_n.
    \end{equation*}

    \pause{}

    We denote this by
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            c_1 \\ \vdots \\ c_{n}
        \end{bmatrix}
        .
    \end{equation*}

    The mapping $\bfx \mapsto [\bfx]_{\scB}$ is the \alert{coordinate mapping}.

    \cake{} What is the domain and codomain of this mapping?
\end{frame}

\begin{frame}
    \frametitle{The coordinates of the \emoji{carrot}}

    Let $\scE = \{\bfe_1, \bfe_2\}$ be the \emph{standard basis} of $\dsR^{2}$.

    What is the coordinates of the vector \emoji{carrot} relative to $\scE$?
    
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.6, every node/.style={black}]
                % Draw white background
                \fill[white] (-1,-1) rectangle (6,6);
                % Draw grid
                \draw[step=1cm, gray, very thin] (0, 0) grid (5,5);
                % Set seed for random number generator
                \pgfmathsetseed{48}
                % Draw x and y axes
                \draw[thick] (0,0) -- (5,0) node[anchor=west] {$x_1$};
                \draw[thick] (0,0) -- (0,5) node[anchor=south] {$x_2$};
                % Draw vector to (4,3)
                \drawvector{4,3}{\emoji{carrot}};
                \drawvector[SeaGreen]{1,0}{below:$\bfe_{1}$};
                \drawvector[SeaGreen]{0,1}{left:$\bfe_{2}$};
            \end{tikzpicture}
            \caption{How to help the \emoji{rabbit-face} finding \emoji{carrot}?}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider a basis for $\dsR^{2}$
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                1 \\ 0
            \end{bmatrix}
            ,
            \begin{bmatrix}
                1 \\ 2
            \end{bmatrix}
        \right\}
        .
    \end{equation*}
    What is $\bfx$ if
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            -2 \\ 3
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} What is the coordinate of $\bfx = \begin{bmatrix} 1 \\ 6 \end{bmatrix}$
    relative to the \emph{standard basis} $\scE = \{\bfe_1, \bfe_2\}$?
\end{frame}

\begin{frame}
    \frametitle{Different Bases}

    The same vector has different coordinates for different bases.

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.7, transform shape]
                \filldraw[white] (-3.5, -0.5) rectangle (6.5, 6.5);
                \only<1-2>{%
                    \draw[step=1, lightgray, very thin] (0, 0) grid (3, 6);
                }%
                \only<3>{%
                    \begin{scope}[yscale=2, xslant=1]
                        \draw[step=1, lightgray, very thin] (-3, 0) grid (3, 3);
                    \end{scope}
                }%
                \drawvector{2,6}{$\bfx$}; 
                \only<1>{%
                    \drawvector[SeaGreen]{1,0}{below:$\bfe_{1}$};
                    \drawvector[SeaGreen]{0,1}{left:$\bfe_{2}$};
                }%
                \only<2-3>{%
                    \drawvector[JuliaOrange]{1,0}{below:$\bfb_{1}$};
                    \drawvector[JuliaOrange]{1,2}{right:$\bfb_{2}$};
                }%
            \end{tikzpicture}
        \end{center}
        \caption{%
            \only<1>{%
                \cake{} What is $[\bfx]_{\scE}$?
            }%
            \only<2->{%
                \cake{} What is $[\bfx]_{\scB}$?
            }%
        }%
    \end{figure}
\end{frame}

\subsection{Coordinates in \texorpdfstring{$\mathbb{R}^{n}$}{R^n}}

\begin{frame}
    \frametitle{Example 4}

    Let
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                2 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -1 \\ 1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \bfx =
        \begin{bmatrix}
            4 \\ 5
        \end{bmatrix}
        .
    \end{equation*}

    Then what is $[\bfx]_{\scB}$?

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[scale=0.5]
                \filldraw[white] (-3.5, -0.5) rectangle (6.5, 6.5);
                \coordinate (b1) at (2,1); % Base vector 1
                \coordinate (b2) at (-1,1); % Base vector 2

                % Assuming you want to draw a grid within a 2x3 rectangle spanned by b1 and b2
                \foreach \i in {1,2,3}{% Loop over x direction
                    \foreach \j in {1,2}{% Loop over y direction
                        % Draw horizontal lines
                        \draw[lightgray, very thin] ($\i*(b1) + \j*(b2)$) -- ($\i*(b1) + \j*(b2) - (b1)$);
                        % Draw vertical lines
                        \draw[lightgray, very thin] ($\i*(b1) + \j*(b2)$) -- ($\i*(b1) + \j*(b2) - (b2)$);
                    }
                }
                \draw[lightgray, very thin] (0,0) -- ($3*(b1)$);
                \draw[lightgray, very thin] (0,0) -- ($2*(b2)$);
                \drawvector[SeaGreen]{b1}{below:$\bfb_1$}; 
                \drawvector[SeaGreen]{b2}{below:$\bfb_2$}; 
                \drawvector{4,5}{$\bfx$}; 
            \end{tikzpicture}
        \end{figure}
    }%
    \only<2>{%
        To answer the question, we need solve
        \begin{equation*}
            \bfx = c_1 \bfb_1 + c_2 \bfb_2
        \end{equation*}
        i.e.,
        \begin{equation*}
            [\bfb_1 \quad \bfb_2] 
            \begin{bmatrix}
                c_1 \\ c_2
            \end{bmatrix}
            = \bfx
        \end{equation*}
        The matrix $[\bfb_1 \quad \bfb_2]$ is called the \alert{change of coordinates matrix}.
    }%
\end{frame}

\begin{frame}
    \frametitle{Change of Coordinates Matrix}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $\dsR^{n}$.
    Let
    \begin{equation*}
        P_{\scB} =
        \begin{bmatrix}
            \bfb_{1} & \dots & \bfb_{n}
        \end{bmatrix}
        .
    \end{equation*}
    We call $P_{\scB}$ the \alert{change of coordinates matrix} from $\scB$ to
    $\scE$,
    \begin{equation*}
        P_{\scB} 
        \begin{bmatrix}
            c_1 \\ \vdots \\ c_n
        \end{bmatrix}
        =
        \begin{bmatrix}
            \bfb_{1} & \dots & \bfb_{n}
        \end{bmatrix}
        \begin{bmatrix}
            c_1 \\ \vdots \\ c_n
        \end{bmatrix}
        =
        \begin{bmatrix}
            x_1 \\ \vdots \\ x_n
        \end{bmatrix}
    \end{equation*}
    maps coordinates in $\scB$ to coordinates in $\scE$.

    \cake{} What is $P_{\scB}$ when $\scB = \scE$ is the standard basis of $\dsR^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Properties of the change of coordinates matrix}

    For $\bfx \in \dsR^{n}$
    \begin{equation*}
        \bfx
        = 
        \begin{bmatrix} x_1 \\ \vdots \\ x_n \end{bmatrix} 
        =
        [\bfx]_{\scE}
        \pause{}
        = 
        P_{\scB} [\bfx]_{\scB}
        =
        \begin{bmatrix} \bfb_1 & \dots & \bfb_n \end{bmatrix} [\bfx]_{\scB}
    \end{equation*}
    \pause{}
    So we have
    \begin{equation*}
        [\bfx]_{\scB} 
        = 
        P_{\scB}^{-1}  \bfx
        =
        \begin{bmatrix} \bfb_1 & \dots & \bfb_n \end{bmatrix}^{-1} \bfx
    \end{equation*}

    \cake{} Why is $P_{\scB}$ invertible?
    
    \cake{} Why is $\bfx \mapsto [\bfx]_{\scB}$ a linear transformation?
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Is the mapping $\bfx \mapsto [\bfx]_{\scB} = (P_{\scB})^{-1} \bfx$ always one-to-one?
%
%    Is it always onto?
%
%    What is your justification?
%\end{frame}

\subsection{The Coordinate Mapping}

\begin{frame}
    \frametitle{Coordinate Mapping}

    Let $\scB = \{\bfb_1, \dots, \bfb_{n}\}$ be a basis of a vector space $V$.

    Consider the \alert{coordinate mapping} $\bfx \mapsto [\bfx]_{\scB}$. 

    \cake{} What is the domain and codomain of this mapping?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{coordinate-mapping.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- Coordinate mapping is linear}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.
    The mapping $\bfx \mapsto [\bfx]_{\scB}$ is a one-to-one \emph{linear
    mapping} from
    $V$ onto $\dsR^{n}$.


    \only<2-3>{%
        Proof of Linearity:
    }%

    \only<2>{%

        First we need to prove that
        \begin{equation*}
            [\bfu+\bfv]_{\scB} = \blanklong{} \qquad \text{for all $\bfu, \bfv \in V$}
        \end{equation*}
    }%
    \only<3>{%
        Then we need to prove that
        \begin{equation*}
            [c \bfu]_{\scB} = \blankshort{} \qquad \text{for all $\bfu \in V$ and all $c \in \dsR$}
        \end{equation*}
    }%
    \only<4>{%
        \puzzle{} See Exercise 27 and 28 for the mapping being one-to-one and onto.

        \begin{block}{Isomorphism}
            A one-to-one and onto linear mapping 
            from a vector space $V$ onto a vector space $W$
            is called an \alert{isomorphism}.

            \astonished{}
            In other words, Theorem 8 says $V$ and $\dsR^{n}$ are indistinguishable as vector spaces. 
        \end{block}
    }%
\end{frame}

\begin{frame}
    \frametitle{Coordinates of linear combinations}
    \cake{}
    Since $\bfx \mapsto [\bfx]_{\scB}$ is a linear mapping,
    we have
    \begin{flalign*}
        [
        c_{1} \bfu_{1}
        +
        c_{2} \bfu_{2}
        +
        \cdots
        c_{p} \bfu_{p}
        ] _{\scB}
        =
        &&
    \end{flalign*}

    \pause{}

    \hint{}
    In words, the $\scB$-coordinate vector of a linear combination of
    $\veclist[p]{u}$ is the same linear combination of their coordinate vectors.
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Let $\scB = \{1, t, t^2, t^3\}$ be the standard basis of $\dsP_{3}$.
    Then the polynomial (vector)
    \begin{equation*}
        \bfp(t) = a_0 + a_1 t + a_2 t^2 + a_3 t^3
    \end{equation*}
    has the coordinates
    \begin{equation*}
        [\bfp]_{\scB}
        =
        \begin{bmatrix}
            \phantom{a_0} \\ \phantom{a_1} \\ \phantom{a_2} \\ \phantom{a_3}
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Use coordinates vectors to show that
    \begin{equation*}
        \bfp_1(t) = 1 + 2 t^2,
        \qquad
        \bfp_2(t) =
        4 + t + 5t^2,
        \qquad
        \bfp_3(t) =
        3 + 2t
    \end{equation*}
    are linearly \emph{dependent}.

    \only<1>{%
        \cake{} What are the coordinates for these polynomials (vectors)?
    }%

    \only<2>{%
        Thus, we only need to show that
        \[
            A
            =
            \begin{bmatrix}
                1 & 4 & 3 & 0 \\
                0 & 1 & 2 & 0 \\
                2 & 5 & 0 & 0
            \end{bmatrix}
            \sim
            \begin{bmatrix}
                1 & 4 & 3 & 0 \\
                0 & 1 & 2 & 0 \\
                0 & 0 & 0 & 0
            \end{bmatrix}
        \]
        \cake{}
        Why are the columns of $A$ linearly \emph{dependent}?
    }%

    \only<3>{%
        So far we have shown that the following are linearly \emph{dependent}:
        \begin{equation*}
            [\bfp_1(t)]_{\scB},
            \qquad
            [\bfp_2(t)]_{\scB},
            \qquad
            [\bfp_3(t)]_{\scB}
        \end{equation*}

        \cake{} Why does this imply that $\bfp_1(t), \bfp_2(t), \bfp_3(t)$ are linearly \emph{dependent}?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 7}

    Let
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            3 \\ 6 \\ 2
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            -1 \\ 0 \\ 1
        \end{bmatrix}
        ,
        \qquad
        \bfx =
        \begin{bmatrix}
            3 \\ 12 \\ 7
        \end{bmatrix}
        .
    \end{equation*}

    \only<1>{%
        \cake{} Are $\bfv_1, \bfv_2$ linearly independent?
    }%

    \only<2>{%
        \cake{} Is $\bfx \in \Span{\bfv_1, \bfv_2}$?
        If so what is its coordinate $[\bfx]_{\{\bfv_{1}, \bfv_{2}\}}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 7 in picture}
    The coordinate mapping $\bfx \mapsto [\bfx]_{\{\bfv_{1}, \bfv_{2}\}}$ is a
    \emph{one-to-one} linear mapping from $\Span{\bfv_{1}, \bfv_{2}}$ \emph{onto} $\mathbb{R}^{2}$.

    In other words, the two vector spaces are isomorphic.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{coordinate-r3.jpg}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{}
%    Use coordinate vectors to show that the following is a basis of $\dsP_{2}$.
%    \begin{equation*}
%        \scB =
%        \{1+t^2, t-3t^2, 1+t-3t^2\}.
%    \end{equation*}
%
%    \think{}
%    Find $\bfq \in \dsP_{2}$ such that
%    \begin{equation*}
%        [\bfq]_{\scB} =
%        \begin{bmatrix}
%            -1 \\ 1 \\2
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Exercise}
%
%    \sweat{}
%    Let $S$ be a finite set in a vector space $V$ with the property
%    that every $x \in V$ has a unique representation as a linear
%    combination of elements of $S$. Show that $S$ is a basis of $V$.
%\end{frame}

\end{document}
