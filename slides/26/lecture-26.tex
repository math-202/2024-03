\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{angles, quotes}

\title{Lecture \lecturenum{} --- Orthogonal Sets,  Orthogonal Projections}

\begin{document}

\maketitle{}

\begin{frame}[c]
    \frametitle{%
        Assignments%
        \footnote{\homeworknote{}}%
        \footnote{%
            \smiling{}
            Skip ``Angels in $\dsR^{2}$ and $\dsR^{3}$`` in 6.1.
        }%
    }%

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.2}{1, 5, 7, 11, 13, 17, 21, 33, 35, 37, 39, 41}
        \end{column}
    \end{columns}
\end{frame}

\lectureoutline{}

\section{6.2 Orthogonal Sets}

\begin{frame}
    \frametitle{Example 1: Orthogonal Sets}

    Let
    \begin{equation*}
        \bfu_{1} =
        \begin{bmatrix}
            3 \\ 1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{2} =
        \begin{bmatrix}
            -1 \\ 2 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{3} =
        \begin{bmatrix}
            \frac{-1}{2} \\ -2 \\ \frac{7}{2}
        \end{bmatrix}
    \end{equation*}
    \cake{} Verify that the three are pairwise orthogonal.

    \pause{}

    A set of vectors $\{\bfu_1, \dots, \bfu_p\}$ in $\dsR^{n}$
    is an \alert{orthogonal set} if $\bfu_i \cdot \bfu_j = 0$
    for all $i \ne j$.

    \cake{} Is $\{\bfu_{1}\}$ an orthogonal set?
\end{frame}

\begin{frame}
    \frametitle{Theorem 4: Bases}

    If $S = \{\bfu_1, \dots, \bfu_p\}$ is an orthogonal set of \emph{non-zero}
    vectors in $\dsR^{n}$,
    then the vectors in $S$ are linearly independent and hence forms a basis for
    the subspace spanned by $S$.

    \pause{}
    Proof: Let's try to show that if
    \begin{equation*}
        \bfzero = c_{1} \bfu_{1} + \dots + c_p \bfu_{p}
    \end{equation*}
    then $c_1 = 0, \dots, c_p = 0$.
\end{frame}

\begin{frame}
    \frametitle{Orthogonal basis}

    An \alert{orthogonal basis} for a subspace $W$ of $\dsR^n$ is a basis
    that is also an \emph{orthogonal set}.

    \only<2>{%
        \cake{} In the following picture, which sets of vectors form \emph{orthogonal bases} for
        $\dsR^{2}$?

        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=0.5, 
                every label/.style={black, inner sep=1pt},
                every node/.style={black, inner sep=1pt}
                ]
                \fill[white] (-6, -1) rectangle (6, 6);

                % Grid
                \draw[grid style] (-5, 0) grid (5, 5);

                % Axes
                \draw[axis style] (-5, 0) -- (5, 0) node[right] {$x_1$};
                \draw[axis style] (0, 0) -- (0, 5) node[above] {$x_2$};

                % Vector u
                \draw[vector arrow] (0, 0) -- (3, 4) node[right] {$\mathbf{u}$};

                % Vector v
                \draw[vector arrow] (0, 0) -- (-2, 1.5) node[left] {$\mathbf{v}$};

                % Vector u'
                \draw[vector arrow, red] (0, 0) -- (0, 4) node[above right] {$\mathbf{u}'$};

                % Vector v
                \draw[vector arrow, red] (0, 0) -- (-2, 0) node[below left] {$\mathbf{v}'$};
            \end{tikzpicture}
        \end{figure}

        \cake{} Which sets of vectors form bases of $\dsR^{2}$?
    }%
    \only<3>{%
        \cake{} Can you think of an orthogonal basis of $\Span{\bfu}$?
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=0.8, 
                every label/.style={black, fill=white, inner sep=1pt},
                every node/.style={black, fill=white, inner sep=1pt}
                ]
                \fill[white] (-6, -1) rectangle (6, 6);

                % Grid
                \draw[grid style] (-5, 0) grid (5, 5);

                % Axes
                \draw[axis style] (-5, 0) -- (5, 0) node[right] {$x_1$};
                \draw[axis style] (0, 0) -- (0, 5) node[above] {$x_2$};

                % Vector u
                \draw[red, thin] (-1/2, -4/6) -- (3, 4) node[right] {$\Span{\mathbf{u}}$};
                \draw[vector arrow] (0, 0) -- (3/2, 2) node[right] {$\mathbf{u}$};
            \end{tikzpicture}
        \end{figure}
    }%
\end{frame}

\begin{frame}{Theorem 5: Compute the Coordinates}
    Let $\scB = \{\bfu_1, \dots, \bfu_p\}$ be an \emph{orthogonal basis} of $W$.

    For every $\bfy \in W$, we have
    \begin{equation*}
        \bfy = c_{1} \bfu_1 + \dots + c_p \bfu_p
    \end{equation*}
    where
    \begin{equation*}
        c_{j} = \frac{\bfy \cdot \bfu_{j}}{\bfu_{j}\cdot \bfu_j},
        \qquad
        (j \in \{1, \dots, p\})
    \end{equation*}
    \only<1>{%
        In other words, we have
        \begin{equation*}
            [\bfy]_{\scB} = \begin{bmatrix} c_1 \\ \vdots \\ c_p \end{bmatrix}
        \end{equation*}
        where $c_j$ for $j \in \{1, \dots, p\}$ are given above.
    }%

    \only<2>{%
        Proof: Let's try to compute $\bfy \cdot \bfu_1$.
    }%
    \only<3>{%
        \cake{} Why do we have $\bfu_j \cdot \bfu_j \ne 0$ for all $j \in \{1, \dots, p\}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Express
    \begin{equation*}
        \bfy =
        \begin{bmatrix}
            6 \\ 1 \\ -8
        \end{bmatrix}
    \end{equation*}
    as a linear combination of the orthogonal basis
    \begin{equation*}
        \bfu_{1} =
        \begin{bmatrix}
            3 \\ 1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{2} =
        \begin{bmatrix}
            -1 \\ 2 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{3} =
        \begin{bmatrix}
            \frac{-1}{2} \\ -2 \\ \frac{7}{2}
        \end{bmatrix}
    \end{equation*}

    \pause{}

    Answer:
    \begin{equation}
        \bfy = \bfu_{1} -2 \bfu_{2} -2 \bfu_{3}
    \end{equation}
\end{frame}

\subsection{An Orthogonal Projection}

\begin{frame}[c]
    \frametitle{\zany{} Projection}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{projection} (noun)  --- an image projected on a surface.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

            Projection of data from high-dimension to low-dimension
            helps to
            \begin{itemize}
                \item understand the data,
                \item visualize the data, and
                \item reduce computational cost.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-shadow.jpg}
                \caption{%
                    A projection of \emoji{victory-hand-medium-skin-tone} to \bunny{}
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}[c]
    \frametitle{Projections to a Vector}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            If
            \begin{equation*}
                \bfy = \bfz + \hat{\bfy}
            \end{equation*}
            such that
            \begin{itemize}
                \item $\bfz$ is orthogonal to $\bfu$, 
                \item and $\hat{\bfy} = \alpha \bfu$ for some $\alpha \in \dsR$,
            \end{itemize}
            then
            \begin{itemize}
                \item 
                    $\hat{\bfy}$ is called the \alert{orthogonal
                    projection/projection} of $\bfy$ onto $\bfu$
                \item
                    $\bfz$ is called the \alert{component} of $\bfy$ orthogonal to $\bfu$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{%
                \begin{tikzpicture}[
                    scale=0.8, 
                    every label/.style={black, inner sep=1pt},
                    every node/.style={black, inner sep=1pt}
                    ]
                    \begin{scope}[rotate=10]
                        \draw[red, thin] (-1,0) -- (4,0);
                        \begin{scope}[xshift=3cm,yshift=0cm]
                            \drawvector[LavenderDream]{0, 4}{above right:$\mathbf{z}$};
                        \end{scope}
                        \drawvector[JuliaOrange]{3, 0}{below right:$\mathbf{\hat{y}}$};
                        \drawvector{2, 0}{above left:$\mathbf{u}$};
                        \drawvector[SeaGreen]{3, 4}{above left:$\mathbf{y}$};
                    \end{scope}
                \end{tikzpicture}
            }%
            \caption{The projection of $\bfy$ to $\bfu$ is $\hat{\bfy}$.}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Compute the Projection}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}

            The \emph{projection} of $\bfy$ to $\bfu$ can be computed as
            \begin{equation*}
                \hat{\bfy}
                =
                \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
                \bfu
                .
            \end{equation*}

            Proof --- Let $\bfz = \bfy - \alpha \bfu$.

            \cake{} What does it mean to say $\bfz$ is orthogonal to $\bfu$?

            \vspace{3.5cm}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \colorbox{white}{%
                    \begin{tikzpicture}[
                        scale=0.8, 
                        every label/.style={black, inner sep=1pt},
                        every node/.style={black, inner sep=1pt}
                        ]
                        \begin{scope}[rotate=10]
                            \draw[red, thin] (-1,0) -- (4,0);
                            \begin{scope}[xshift=3cm,yshift=0cm]
                                \drawvector[LavenderDream]{0, 4}{above right:$\mathbf{z}$};
                            \end{scope}
                            \drawvector[JuliaOrange]{3, 0}{below right:$\mathbf{\hat{y}}$};
                            \drawvector{2, 0}{above left:$\mathbf{u}$};
                            \drawvector[SeaGreen]{3, 4}{above left:$\mathbf{y}$};
                        \end{scope}
                    \end{tikzpicture}
                }%
                \caption{How to compute $\hat{\bfy}$?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Projections to a Subspace}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \only<1>{%
                We know that the projection of $\bfy$ to $\bfu$ is
                \begin{equation*}
                    \hat{\bfy} = \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu} \bfu
                \end{equation*}
                \cake{}
                What about $c \bfu$?

                \vspace{3cm}
            }%
            \only<2>{%
                Since the projection of $\bfy$ to every vector in the subspace $L =
                \Span{\bfu}$ is \emph{the same}, we may define \alert{the projection}  of $\bfy$ to $L$ by
                \begin{equation*}
                    \proj_{L} \bfy =
                    \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
                    \bfu
                    .
                \end{equation*}
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \colorbox{white}{%
                    \begin{tikzpicture}[
                        scale=0.8, 
                        every label/.style={black, inner sep=1pt},
                        every node/.style={black, inner sep=1pt}
                        ]
                        \begin{scope}[rotate=10]
                            \draw[red, thin] (-2,0) -- (4,0);
                            \begin{scope}[xshift=3cm,yshift=0cm]
                                \drawvector[LavenderDream]{0, 4}{above right:$\mathbf{z}$};
                            \end{scope}
                            \drawvector[JuliaOrange]{3, 0}{below right:$\mathbf{\hat{y}}$};
                            \drawvector{2, 0}{above left:$\mathbf{u}$};
                            \only<1>{%
                                \drawvector{-1, 0}{above left:$-\frac{1}{2} \mathbf{u}$};
                            }%
                            \drawvector[SeaGreen]{3, 4}{above left:$\mathbf{y}$};
                            \only<2>{%
                                \node at (-1,0.5) {\small $L = \Span{\bfu}$};
                            }%
                        \end{scope}
                    \end{tikzpicture}
                }%
                \caption{%
                    \only<1>{%
                        What is the projection of $\bfy$ to $-\frac{1}{2} \bfu $?
                    }%
                    \only<2>{%
                        What is the projection of $\bfy$ to $L = \Span{\bfu}$?
                    }%
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Projection}
%
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.8\linewidth]{projection.jpg}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        \bfy =
        \begin{bmatrix}
            7 \\ 6
        \end{bmatrix}
        ,
        \qquad
        \bfu =
        \begin{bmatrix}
            4 \\ 2
        \end{bmatrix}
    \end{equation*}
    \only<1>{%
        Find $\hat{\bfy}$, the projection of $\bfy$ to $\bfu$.
    }%
    \only<2>{%
        Find $\bfz$ such that $\bfz$ is the component of $\bfy$ orthogonal to $\bfu$.
    }%
    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=0.5,
                every node/.style={black},
                declare function={
                    xmin=0.0; xmax=9.0;
                    ymin=0.0; ymax=7.0;
                },
                ]
                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                % Coordinates
                \coordinate (o) at (0, 0);
                \coordinate (u) at (4, 2);
                \coordinate (y) at (7, 6);
                \coordinate (z) at (-1, 2);
                \coordinate (yhat) at (8, 4);

                % Vectors
                \draw[red, thin] ($-0.1*(u)$) -- ($2.2*(u)$) node[pos=1.0, above right] {$L=\Span{\bfu}$};
                \draw[thin, dashed] (y) -- (yhat);
                \pic [draw,red,thick,angle radius=0.3cm] 
                        {right angle = y--yhat--o};
                \only<2>{%
                    \drawvector[LavenderDream]{z}{above right:$\mathbf{z}$};
                }%
                \drawvector[JuliaOrange]{yhat}{below right:$\mathbf{\hat{y}}$};
                \drawvector{u}{above left:$\mathbf{u}$};
                \drawvector[SeaGreen]{y}{above left:$\mathbf{y}$};
            \end{tikzpicture}
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \cake{} What is the distance from $\bfy$ to $L$?

    \begin{figure}
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=0.5,
                every node/.style={black},
                declare function={
                    xmin=0.0; xmax=9.0;
                    ymin=0.0; ymax=7.0;
                },
                ]
                % Grid
                \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                % Axes
                \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                % Coordinates
                \coordinate (o) at (0, 0);
                \coordinate (u) at (4, 2);
                \coordinate (y) at (7, 6);
                \coordinate (z) at (-1, 2);
                \coordinate (yhat) at (8, 4);

                % Vectors
                \draw[red, thin] ($-0.1*(u)$) -- ($2.2*(u)$) node[pos=1.0, above right] {$L=\Span{\bfu}$};
                \draw[thin, dashed] (y) -- (yhat);
                \pic [draw,red,thick,angle radius=0.3cm] 
                {right angle = y--yhat--o};
            \drawvector[LavenderDream]{z}{above right:$\mathbf{z}$};
            \drawvector[JuliaOrange]{yhat}{below right:$\mathbf{\hat{y}}$};
            \drawvector{u}{above left:$\mathbf{u}$};
            \drawvector[SeaGreen]{y}{above left:$\mathbf{y}$};
                \end{tikzpicture}
        }%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Theorem 5: A geometric interpretation}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Let $\scB = \{\bfu_1, \dots, \bfu_p\}$ be an \emph{orthogonal basis} of $W$.

            For every $\bfy \in W$, we have
            \begin{equation*}
                \bfy = c_{1} \bfu_1 + \dots + c_p \bfu_p
            \end{equation*}
            where
            \begin{equation*}
                c_{j} = \frac{\bfy \cdot \bfu_{j}}{\bfu_{j}\cdot \bfu_j},
                \qquad
                (j \in \{1, \dots, p\})
            \end{equation*}

            \hint{} This is a decomposition of $\bfy$ into projections onto
            one-dimensional subspaces.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \colorbox{white}{%
                    \begin{tikzpicture}[
                        scale=0.5,
                        every node/.style={black},
                        declare function={
                            xmin=0.0; xmax=7.0;
                            ymin=0.0; ymax=6.0;
                        },
                        ]

                        \begin{scope}[rotate=15]
                            % Axes
                            \draw[vector arrow, SeaGreen] (xmin, 0) -- (xmax, 0) node[right] {$\bfu_1$};
                            \draw[vector arrow, SeaGreen] (0, ymin) -- (0, ymax) node[above] {$\bfu_2$};

                            % Coordinates
                            \coordinate (o) at (0, 0);
                            \coordinate (y) at (5, 4);
                            \coordinate (yhatI) at (5, 0);
                            \coordinate (yhatII) at (0, 4);

                            % Dashed lines
                            \draw[dashed, thin] (y) -- (yhatI);
                            \draw[dashed, thin] (y) -- (yhatII);

                            % Vectors
                            \drawvector{y}{above left:$\mathbf{y}$};
                            \drawvector[JuliaOrange]{yhatI}{below right:$\mathbf{\hat{y}}_{1}$};
                            \drawvector[JuliaOrange]{yhatII}{below left:$\mathbf{\hat{y}}_{2}$};
                            \pic [draw,red,thick,angle radius=0.3cm] 
                            {right angle = yhatI--o--yhatII};
                    \end{scope}
                \end{tikzpicture}
            }%
            \caption{A decomposition of $\bfy$ onto one-dimensional subspaces.}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Orthonormal Sets}

\begin{frame}
    \frametitle{Orthonormal Sets}

    A set $\{\bfu_{1},\dots,\bfu_{p}\}$ is an \alert{orthonormal set} if it is an \alert{orthogonal set of unit vectors}.

    \only<1>{%
        \begin{block}{\hint{} How to remember the word}
            \begin{center}
                Orthonormal = Orthogonal + Normal
            \end{center}
        \end{block}
    }%

    \only<2>{%
        \cake{}
        Given an orthogonal set $\{\bfu_{1},\dots,\bfu_{p}\}$, how can we find an orthonormal set
        $\{\bfv_{1}, \dots, \bfv_{p}\}$ such that $\bfv_{i}$ and $\bfu_{i}$ have the same
        direction for $i \in \{1, \dots, p\}$?

        \begin{figure}
            \colorbox{white}{%
                \begin{tikzpicture}[
                    scale=1.0,
                    every node/.style={black},
                    declare function={
                        xmin=-2.0; xmax=5.0;
                        ymin=0.0; ymax=3.0;
                    },
                    ]
                    % Grid
                    \draw[lightgray, very thin] (xmin, ymin) grid (xmax, ymax);

                    % Axes
                    \draw[axis style] (xmin, 0) -- (xmax, 0) node[right] {$x_1$};
                    \draw[axis style] (0, ymin) -- (0, ymax) node[above] {$x_2$};

                    % Coordinates
                    \coordinate (o) at (0, 0);
                    \coordinate (u1) at (4, 2);
                    \coordinate (u2) at (-1, 2);

                    % Vectors
                    \drawvector{u1}{above right:$\mathbf{u}_1$};
                    \drawvector{u2}{above left:$\mathbf{u}_2$};
                    \drawvector[SeaGreen]{$1/sqrt(20)*(u1)$}{below right:$\mathbf{v}_1$};
                    \drawvector[SeaGreen]{$1/sqrt(5)*(u2)$}{below left:$\mathbf{v}_2$};
                \end{tikzpicture}
        }%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    
    \think{} How to show the following vectors form an \alert{orthonormal set} ---
    \begin{equation*}
        \bfv_{1}
        =
        \frac{1}{\sqrt{11}}
        \begin{bmatrix}
            3 \\
            1 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \frac{1}{\sqrt{6}}
        \begin{bmatrix}
            -1 \\
            2 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3}
        =
        \frac{1}{\sqrt{66}}
        \begin{bmatrix}
            -1 \\
            -4 \\
            7 \\
        \end{bmatrix}
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Let
    \begin{equation*}
        U
        =
        \left[
            \begin{array}{cc}
                \frac{\sqrt{2}}{2} & \frac{2}{3} \\
                \frac{\sqrt{2}}{2} & \frac{-2}{3} \\
                0 & \frac{1}{3} \\
            \end{array}
        \right]
        ,
        \qquad
        \bfx
        =
        \left[
            \begin{array}{c}
                \sqrt{2} \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Note that $U$ has \alert{orthonormal columns}, i.e., its columns form a
    \emph{orthonormal set}.

    \only<1>{%
        \cake{} What are $U^T U$? 
    }%

    \only<2>{%
        \cake{} What are $\norm{U \bfx}$ and $\norm{\bfx}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 6 --- Orthonormal columns}

    An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U =
    \blankshort{}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 --- Orthonormal columns}

    Let $U$ be an $m \times n$ matrix with orthonormal columns,
    and let $x$ and $y$ be in $\dsR^n$.
    Then
    \begin{enumerate}[a.]
        \item $\norm{U \bfx} = \blankshort{}$.
        \item $(U \bfx) \cdot (U \bfy) = \bfx \cdot \bfy$.
        \item $(U \bfx) \cdot (U \bfy) = 0$ if and only if $\bfx \cdot \bfy = 0$.
    \end{enumerate}
    \cake{} Why does (b) imply (a)?
\end{frame}

\begin{frame}
    \frametitle{Orthogonal Matrix}

    An \alert{orthogonal matrix} is a \emph{square} matrix $U$ with $U^{-1} = U^{T}$.

    \only<1>{
        For example, the following is an \emph{orthogonal matrix} ---
        \begin{equation*}
            \begin{bmatrix}
                \cos(\theta) & \sin(\theta) \\
                -\sin(\theta) & \cos(\theta)
            \end{bmatrix}
        \end{equation*}
    }
    \only<2->{
        \hint{} Note that an \emph{orthogonal matrix} has \emph{orthonormal columns}.
    }
    \only<2>{%
        \begin{block}{Theorem 6}
            An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U = I$.
        \end{block}

        \astonished{} Such matrix must also have orthonormal rows! (Exercise 35, 36.)
    }%
    \only<3>{

        \cake{} Is every matrix with \emph{orthonormal columns}
        an \emph{orthogonal matrix}?
    }
\end{frame}

\end{document}
