\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Matrix Factorizations}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{2.5}{All odd number questions from 1 to 29.}
            \sectionhomework[\footnotesize]{2.6}{All odd number questions from 1 to 11.}
        \end{column}
    \end{columns}
\end{frame}

\section{2.5 Matrix Factorizations}

\subsection{The LU Factorization}

\begin{frame}
    \frametitle{Factorization}
    A \alert{factorization} of an integer $i$ is an equation that expresses 
    $i$ as a product of two or more integers. For example:
    \begin{equation*}
        12 = 4 \times 3 = 6 \times 2 = 3 \times 2 \times 2
    \end{equation*}

    \pause{}

    A \alert{factorization}  of a matrix $A$ is an equation that expresses 
    $A$ as a product of two or more matrices. For example:
    \begin{equation*}
        \left[
            \begin{array}{ccc}
                8 & 10 & 6 \\
                14 & 19 & 9 \\
                9 & 20 & 3 \\
            \end{array}
        \right]
        =
        \only<2>{%
            \left[
                \begin{array}{ccc}
                    1 & 2 & 3 \\
                    1 & 2 & 6 \\
                    3 & 1 & 4 \\
                \end{array}
            \right]
            \left[
                \begin{array}{ccc}
                    0 & 3 & -1 \\
                    1 & -1 & 2 \\
                    2 & 3 & 1 \\
                \end{array}
            \right]
        }%
        \only<3>{%
            \left[
                \begin{array}{ccc}
                    0 & 1 & 0 \\
                    3 & 1 & 0 \\
                    2 & 0 & 1 \\
                \end{array}
            \right]
            \left[
                \begin{array}{ccc}
                    2 & 3 & 1 \\
                    8 & 10 & 6 \\
                    5 & 14 & 1 \\
                \end{array}
            \right]
        }%
        \only<4>{%
            \left[
                \begin{array}{ccc}
                    0 & 1 & 0 \\
                    3 & 1 & 0 \\
                    2 & 0 & 1 \\
                \end{array}
            \right]
            \left[
                \begin{array}{ccc}
                    0 & 0 & 1 \\
                    1 & 2 & 3 \\
                    3 & 1 & 2 \\
                \end{array}
            \right]
            \left[
                \begin{array}{ccc}
                    0 & 3 & -1 \\
                    1 & -1 & 2 \\
                    2 & 3 & 1 \\
                \end{array}
            \right]
        }%
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{The LU Factorization}
    For example, the following is called an \alert{LU factorization} of $A$:
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            1 & 0 & 0 & 0 \\
            \ast & 1 & 0 & 0 \\
            \ast & \ast & 1 & 0 \\
            \ast & \ast & \ast & 1
        \end{bmatrix}
        \begin{bmatrix}
            \blacksquare & \ast & \ast & \ast & \ast & \ast \\
            0 & \blacksquare & \ast & \ast & \ast & \ast \\
            0 & 0 & 0 & \blacksquare & \ast & \ast \\
            0 & 0 & 0 & 0 & 0 & 0
        \end{bmatrix}
        =
        L U
    \end{equation*}
    where the matrix $L$ is said to be a \alert{\acf{ult} matrix},
    and $U$ is a matrix in \emph{echelon form}.

    \pause{}

    \cake{} If $A$ is of size $m \times n$, what are the sizes of $L$ and $U$?

    \cake{} The matrix $L$ is invertible. Why? (Use \ac{imt})
\end{frame}

\begin{frame}
    \frametitle{\emoji{fire} Motivations}
    
    In industrial applications, it is common to solve a sequence of linear systems:
    \begin{equation*}
        A \bfx = \bfb_1, \, A \bfx = \bfb_2, \, \dots, \, A \bfx = \bfb_p
    \end{equation*}
    where $A$ is fixed but $\bfb_{1}, \dots, \bfb_{p}$ are different.

    \pause{}

    Given the LU factorization $A = LU$, we can speedup solving $A \bfx = \bfb$ by
    solving
    \begin{equation*}
        L \bfy = \bfb, \qquad U \bfx = \bfy
    \end{equation*}
    due to the special structures of $L$ and $U$.

    \pause{}

    \astonished{} Moreover, solving $A \bfx = \bfb_1$ can lead to an LU
    factorization of $A$, which can be used to solve $A \bfx = \bfb_2$ and so on.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    \only<1>{%
        We are given
        \begin{equation*}
            A
            =
            \begin{bmatrix}
                3 & -7 & -2 & 2\\
                -3 & 5 & 1 & 0\\
                6 & -4 & 0 & -5\\
                -9 & 5 & -5 & 12\\
            \end{bmatrix}
            =
            \begin{bmatrix}
                1 & 0 & 0 & 0\\
                -1 & 1 & 0 & 0\\
                2 & -5 & 1 & 0\\
                -3 & 8 & 3 & 1\\
            \end{bmatrix}
            \begin{bmatrix}
                3 & -7 & -2 & 2\\
                0 & -2 & -1 & 2\\
                0 & 0 & -1 & 1\\
                0 & 0 & 0 & -1\\
            \end{bmatrix}
            =
            L U
        \end{equation*}

        \think{}
        How does this simplify solving $A \bfx = \bfb$ where 
        $\bfb = \begin{bmatrix} -9 \\ 5 \\ 7 \\ 11 \end{bmatrix}$?
    }%
    \only<2>{%
        The first step is to solve $L \bfy = \bfb$:
        \begin{equation*}
            \begin{bmatrix} L & \bfb \end{bmatrix} 
            =
            \begin{bmatrix}
                1 &  0 & 0 & 0 & -9 \\
                -1 &  1 & 0 & 0 &  5 \\
                2 & -5 & 1 & 0 &  7 \\
                -3 &  8 & 3 & 1 & 11 \\
            \end{bmatrix}
            =
            \left[\begin{array}{ccccc}
                    1 & 0 & 0 & 0 & -9 \\
                    0 & 1 & 0 & 0 & -4 \\
                    0 & 0 & 1 & 0 & 5 \\
                    0 & 0 & 0 & 1 & 1 \\
            \end{array}\right]
            = 
            \begin{bmatrix} I & \bfy \end{bmatrix} 
        \end{equation*}

        \cake{} How many row operations do we need to reduce $\begin{bmatrix} L & \bfb \end{bmatrix}$ to
        $\begin{bmatrix} I & \bfy \end{bmatrix}$?
    }
    \only<3>{%
        The second step is to solve $U \bfx = \bfy$:
        \begin{equation*}
            [U \quad \bfy] = 
            \begin{bmatrix}
                3 & -7 & -2 & 2 & -9 \\
                0 & -2 & -1 & 2 & -4 \\
                0 &  0 & -1 & 1 &  5 \\
                0 &  0 &  0 & -1 & 1 \\
            \end{bmatrix}
            \sim
            \begin{bmatrix}
                1 & 0 & 0 & 0 & 3 \\
                0 & 1 & 0 & 0 & 4 \\
                0 & 0 & 1 & 0 & -6 \\
                0 & 0 & 0 & 1 & -1 \\
            \end{bmatrix}
            =
            [I \quad \bfy]
        \end{equation*}
        Since $U$ is already in echelon form, we only need to carry out the \emph{backward
        phase}:
        \begin{itemize}
            \item scale each pivot element to 1;
            \item use replacement to turn every number above a pivot postilion into 0.
        \end{itemize}
    }%
\end{frame}


\subsection{An LU Factorization Algorithm}

\begin{frame}
    \frametitle{\ac{ult} Elementary Matrices}

    \cake{} Consider the following elementary matrices
    \begin{equation*}
        \begin{aligned}
            &
            \left[
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            \right]
            ,
            \quad
            \left[
                \begin{array}{ccc}
                    1 & 0 & 3 \\
                    0 & 0 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            \right]
            ,
            \\
            &
            \left[
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    0 & -5 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            \right]
            ,
            \quad
            \left[
                \begin{array}{ccc}
                    0 & 1 & 0 \\
                    1 & 0 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            \right]
            .
        \end{aligned}
    \end{equation*}
    \cake{} Which of them are \ac{ult}?

    \cake{} What makes an \emph{\ac{ult} elementary matrix}?
\end{frame}

\begin{frame}
    \frametitle{Properties of \ac{ult} Elementary Matrices}
    Let
    \begin{equation*}
        E_{1}
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                -2 & 1 & 0 \\
                0 & 0 & 1 \\
            \end{array}
        \right]
        \only<2>{
        ,
        \quad
        E_{2} =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 5 & 1 \\
            \end{array}
        \right]
        }
    \end{equation*}
    \cake{} Then what is
    \begin{equation*}
        \only<1>{
        E_{1}^{-1}
        =
        \left[
            \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            }
        \right]
        }
        \only<2>{
        E_2 E_1 
        =
        \left[
            \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            }
        \right]
        }
    \end{equation*}
    \only<1>{\cake{} What does this say about the inverse of a \emph{\ac{ult} elementary matrix}?}
    \only<2>{\cake{} What does this say about the product of \emph{\ac{ult} elementary matrices}?}
\end{frame}

\begin{frame}
    \frametitle{The Idea of the Algorithm}

    Assume that there exist \emph{\ac{ult} elementary} matrices
    $E_1, \dots, E_p$ such that
    \begin{equation*}
        E_p \dots E_1 A = U
    \end{equation*}
    where $U$ is in echelon form.

    \pause{}

    Since elementary matrices are invertible, we have
    \begin{equation*}
        A 
        = (E_p \dots E_1)^{-1} U
        = (E_1^{-1} \dots E_p^{-1}) U
    \end{equation*}
    \cake{} Why is $E_1^{-1} \dots E_p^{-1}$ the \ac{ult} matrix $L$ we need?

    \pause{}

    Note that
    \begin{equation*}
        E_p \dots E_1 L 
        = 
        E_p \dots E_1 (E_1^{-1} \dots E_p^{-1})
        =
        I
    \end{equation*}
    \cake{} What does this tell us about the matrix $L$?
\end{frame}

\begin{frame}
    \frametitle{The algorithm}

    To find a LU factorization of $A$ ---
    \begin{enumerate}
        \item First reduce $A$ to an echelon form $U$ using \emph{only} row
            \emph{replacements} which add the multiple of row to a row \emph{below} it.
        \item To find $L$ is to find the matrix which is going to be reduced to $I$ by the same operations.
    \end{enumerate}

    \bomb{} Step 1 is not always possible. Which means LU factorization is not always possible.
\end{frame}

\begin{frame}[c]
    \frametitle{Example 2}

    \only<1>{
        \begin{equation*}
            A
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                -4 & -5 & 3 & -8 & 1 \\
                2 & -5 & -4 & 1 & 8 \\
                -6 & 0 & 7 & -3 & 1 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                \blankveryshort{} & 1 &   &      \\
                \blankveryshort{} &   & 1 &      \\
                \blankveryshort{} &   &   & 1    \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<2>{
        \begin{equation*}
            A_{1}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & -9 & -3 & -4 & 10 \\
                0 & 12 & 4 & 12 & -5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2&  1 &   &   \\
                1 & \blankveryshort{} & 1 &   \\
                -3& \blankveryshort{} &   & 1 \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<3>{
        \begin{equation*}
            A_{2}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 4 & 7 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2& 1 &   &   \\
                1 &-3 & 1 &   \\
                -3& 4 & \blankveryshort{} & 1 \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<4>{
        \begin{equation*}
            U
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 0 & 5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2& 1 &   &   \\
                1 &-3 & 1 &   \\
                -3& 4 & 2 & 1 \\
            \end{bmatrix}
        \end{equation*}
    }
\end{frame}


\begin{frame}[c]
    \frametitle{Example 2 Recap}

    A simple trick to carry out the algorithm ---
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{LU-02.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Fine the LU factorization of
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{cc}
                1 & 2 \\
                4 & 7 \\
            \end{array}
        \right]
        =
        \left[
            \phantom{
                \begin{array}{cc}
                    1 & 0 \\
                    4 & 1 \\
                \end{array}
            }
        \right]
        \left[
            \phantom{
                \begin{array}{cc}
                    1 &  2 \\
                    0 & -1 \\
                \end{array}
            }
        \right]
        =
        L U
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{When LU Factorization Is not Possible}
    
    Not every matrix can be LU factorized. 

    However, every matrix has a factorization like this:
    \begin{equation*}
        A = 
        \left[
            \begin{array}{ccc}
                -1 & 3 & 3 \\
                0 & -1 & 2 \\
                -2 & -1 & 0 \\
            \end{array}
        \right]
        =
        \left[
            \begin{array}{ccc}
                \frac{1}{2} & 1 & 0 \\
                0 & \frac{-2}{7} & 1 \\
                1 & 0 & 0 \\
            \end{array}
        \right]
        \left[
            \begin{array}{ccc}
                -2 & -1 & 0 \\
                0 & \frac{7}{2} & 3 \\
                0 & 0 & \frac{20}{7} \\
            \end{array}
        \right]
        =
        L' U
    \end{equation*}
    where $L'$ is a \ac{ult} array with some of its rows interchanged.

    \emoji{smile} This still helps solving linear systems.
\end{frame}

%\begin{frame}
%    \frametitle{\sweat{} Think-Pair-Share}
%
%    Let $A$ be a lower triangular $n \times n$ matrix with nonzero entries
%    on the diagonal.
%    Show that $A$ is invertible and $A^{-1}$ is also lower triangular.
%
%    \begin{enumerate}
%        \item Why $A$ can be reduced to $I$ using only row replacements and scaling?
%        \item Why applying these operations to $I$ gives a lower triangular array?
%        \item Do you remember the algorithm to find $A^{-1}$?
%    \end{enumerate}
%\end{frame}

\section{2.6 The Leontief Input-Output Model}%

\begin{frame}[c]
    \frametitle{\zany{} Wassily Leontief}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            Wassily Leontief is an \emoji{money-mouth-face} who
            won the Nobel \emoji{trophy} in 1973.

            In 1942, he came up with a linear system with $500$ variables to model
            \emoji{us} economy.

            The number of variables have to be reduced to $42$ for the best \emoji{computer} at
            the time to solve.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Leontief.jpg}
                \caption{Wassily Leontief (1905-1999)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{How Much Do We Need?}

    The \bunny{} kingdom has three industries,
    \emoji{tv}, \emoji{beer} and \emoji{carrot}.
    \begin{table}[]
        \begin{tabular}{@{}c*{3}{w{c}{5em}}@{}}
            \toprule
            \multirow{2}{*}{Purchased from} & \multicolumn{3}{c}{Inputs Consumed per Unit of Output}                                                 \\ \cmidrule(l){2-4}
                               & \multicolumn{1}{c}{\emoji{tv}}  & \multicolumn{1}{c}{\emoji{beer}} & \multicolumn{1}{c}{\emoji{carrot}} \\ \midrule
            \emoji{tv}     & 0.5   & 0.4   & 0.2   \\
            \emoji{beer}  & 0.2   & 0.3   & 0.1   \\
            \emoji{carrot}& 0.1   & 0.1   & 0.3   \\ \bottomrule
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}[c]
    \frametitle{The Question}

    Let
    $
    \bfx
        =
        \begin{bmatrix}
            x_t \\ x_b \\ x_c
        \end{bmatrix}
    $
    be the \alert{production vector} that lists the output of each sector next
    year.

    \pause{}

    Let
    $
    \bfd
        =
        \begin{bmatrix}
            d_t \\ d_b \\ d_c
        \end{bmatrix}
    $
    be the \alert{final demand vector} that lists the demand of products from
    \bunny{} next year.

    \pause{}

    Let \alert{intermediate demand} be the need of each sector to produce $\bfd$.

    \pause{}

    Leontief asked if we can find $\bfx$ such that
    \begin{equation*}
        \bfx = \text{intermediate demand} + \bfd.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Consumption Vectors}

    Given the following table,
    how much \emoji{tv}, \emoji{beer}, and \emph{carrot} 
    are needed to produce $x_t$ \emoji{tv}?

    \begin{table}[]
        \begin{tabular}{@{}c*{3}{w{c}{5em}}@{}}
            \toprule
            \multirow{2}{*}{Purchased from} & \multicolumn{3}{c}{Inputs Consumed per Unit of Output}                                                 \\ \cmidrule(l){2-4}
                               & \multicolumn{1}{c}{\emoji{tv}}  & \multicolumn{1}{c}{\emoji{beer}} & \multicolumn{1}{c}{\emoji{carrot}} \\ \midrule
            \emoji{tv}     & 0.5   & 0.4   & 0.2   \\
            \emoji{beer}  & 0.2   & 0.3   & 0.1   \\
            \emoji{carrot}& 0.1   & 0.1   & 0.3   \\ \bottomrule
                               & $\bfc_t$  & $\bfc_b$  & $\bfc_c$  \\
        \end{tabular}
    \end{table}

    \cake{} What about $x_b$ \emoji{beer}, and $x_c$ \emoji{carrot}?
\end{frame}

\begin{frame}
    \frametitle{The Intermediate Demand}
    Then
    \begin{equation*}
        \begin{aligned}
        \text{intermediate demand}
        &
        = x_t \bfc_t + x_b \bfc_b + x_c \bfc_c
        \\
        &
        =
        \begin{bmatrix}
            \bfc_t & \bfc_b & \bfc_c
        \end{bmatrix}
        \begin{bmatrix}
            x_t \\ x_b \\ x_c
        \end{bmatrix}
        = C \bfx,
        \end{aligned}
    \end{equation*}
    where $C$ is called the \alert{consumption} matrix.

\end{frame}

\begin{frame}
    \frametitle{The Solution}
    
    Thus, the equation
    \begin{equation*}
        \bfx = \text{intermediate demand} + \bfd.
    \end{equation*}
    is equivalent to
    \begin{equation*}
        \bfx = C \bfx + \bfd
        .
    \end{equation*}
    \think{} Why the solution is
    \begin{flalign*}
        &
        \bfx = (I- C)^{-1} \bfd.
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Back to the Example}

    In the example
    \begin{equation*}
        C =
        \begin{bmatrix}
            0.5   & 0.4   & 0.2   \\
            0.2   & 0.3   & 0.1   \\
            0.1   & 0.1   & 0.3   \\
        \end{bmatrix}
        ,
        \qquad
        \bfd
        =
        \begin{bmatrix}
            50 \\ 30 \\ 20
        \end{bmatrix}
        .
    \end{equation*}
    So the solution is
    \begin{equation*}
        \bfx
        =
        (I-C)^{-1} \bfd
        \approx
        \left[
            \begin{array}{c}
                225.93 \\
                118.52 \\
                77.78 \\
            \end{array}
        \right]
    \end{equation*}

    \emoji{wink} Now you should ask the professor one natural question. What is
    that?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- The Existence of the Solution}

    If the sum of each column in $C$ is \emph{strictly} less than $1$,
    then $(I-C)^{-1}$ exists.

    \only<1>{%
        Recall that
        \begin{equation*}
            1 + x + x^{2} + x^{3} \dots
            =
            \sum_{i=0}^{\infty} x^{i}
            =
            \frac{1}{1-x}
        \end{equation*}
        if $\abs{x} < 1$.
    }%

    \only<2>{%
        The idea for proving Theorem 11 is similar.
        We can show that
        \begin{equation*}
            \begin{aligned}
                \bfx & = \bfd + C\bfd + C^{2} \bfd + \dots \\
                     & = (I + C + C^{2} + \dots ) \bfd \\
                     & = (I - C)^{-1} \bfd
            \end{aligned}
        \end{equation*}
        if $C$ satisfies the condition in Theorem 11.
    }
\end{frame}

\begin{frame}
    \frametitle{Approximate Solution}

    So we can take a large $m$ and approximate by
    \begin{equation*}
        (I - C)^{-1}
        \approx
        I + C + C^{2} + \dots C^{m}
        .
    \end{equation*}

    Let
    \begin{equation*}
        \dot{\bfx}
        =
        (I + C + C^{2} + \dots C^{32})\bfd
    \end{equation*}
    Then 
    \begin{equation*}
        \dot{\bfx} - \bfx
        =
        \left[
            \begin{array}{c}
                -0.033 \\
                -0.016 \\
                -0.011 \\
            \end{array}
        \right]
    \end{equation*}
\end{frame}

\end{document}
