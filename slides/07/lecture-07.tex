\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- The Inverse of a Matrix}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{2.2}{All odd number questions from 1 to 47.}
        \end{column}
    \end{columns}
\end{frame}

\section{2.2 The Inverse of a Matrix}

\subsection{Invertible Matrices}

\begin{frame}
    \frametitle{The Inverse of a Number}

    The \alert{inverse} of the number $5$, which we denote by $1/5$ or $5^{-1}$, is the
    number which satisfies
    \begin{equation*}
        5 \cdot 5^{-1} = 1,
        \qquad
        5^{-1} \cdot 5 = 1.
    \end{equation*}

    \cake{} What is the analogue of $1$ for matrices?
\end{frame}

\begin{frame}
    \frametitle{\zany{} Singular}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{singular} (adjective)  --- strange or eccentric in some respect. 

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-singular.jpg}
                \caption{%
                    A singular \bunny{}
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Invertible Matrices}

    An $n \times n$ \emph{square} matrix is \alert{invertible/non-singular} if there is
    matrix $C$ which satisfies
    \begin{equation*}
        AC = I_n, \qquad CA = I_n.
    \end{equation*}
    We call the matrix $C$ \emph{the} \alert{inverse} of $A$.

    A matrix which is \emph{not} invertible is called \alert{singular}.

    \pause{}

    \cake{} If $A = I_n$, then what is the inverse of $A$?

    %\cake{} In general, it is not true that $A B = A C$ implies $B = C$.  However if $A$
    %is \emph{invertible}, then this is true. Why?
\end{frame}

\begin{frame}
    \frametitle{\emoji{unicorn} The Uniqueness of the Inverse Matrix}
    
    Any matrices $A$ has \emph{at most} one inverse, which we denote by $A^{-1}$.

    Proof --- Let $B$ and $C$ be the inverse of $A$.
    Then
    \begin{equation*}
        B = B I = \hspace{0.8\linewidth} = C
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \cake{} When is $A = [a]$ invertible (non-singular)?
%\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            2 & 5 \\
            -3 & -7 \\
        \end{bmatrix}
        ,
        \qquad
        C
        =
        \begin{bmatrix}
            -7 & -5 \\
            3 & 2  \\
        \end{bmatrix}
        \qquad
    \end{equation*}
    Show that $A^{-1} = C$.

    \hint{} If $A C = I$ then $C A = I$. (Exercise 10.)
\end{frame}

\begin{frame}
    \frametitle{\zany{} Determinant}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{determinant} (noun)  ---
            a factor which decisively affects the nature or outcome of something.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-determinant.jpg}
                \caption{%
                    The path chosen is the determinant of your fate
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- $2 \times 2$ matrices}

    Let $A = \begin{bmatrix}a & b\\c & d\end{bmatrix}$.
    We call $ad -bc$ the \alert{determinant} of $A$ and denote it by $\det A$

    If $\det A \ne 0$, then $A$ is invertible,
    and
    \begin{equation*}
        A^{-1} = \frac{1}{\det A}
        \begin{bmatrix}
            d & -b \\ -c & a
        \end{bmatrix}
    \end{equation*}
    Otherwise, $A$ is singular.

\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} Is the following matrix \emph{singular}?
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            3 & 5 \\
            4 & 6 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5 --- The unique solution}

    If $A$ is invertible, then for each $\bfb \in \dsR^{n}$,
    $A \bfx = \bfb$ has a unique solution $A^{-1} \bfb$.
\end{frame}

%\begin{frame}
%    \frametitle{Example 3 --- Deflection of an elastic beam}
%
%    Let $\bff \in \dsR^{3}$ be the forces applied a beam at \#1, \#2, and \#3.
%
%    Let and $\bfy \in \dsR^{3}$ be the amount of movement.
%
%    It can be shown (by physics) that
%    $\bfy = D \bff$
%    for some $D$ which called the \emph{flexibility matrix}.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{elastic-beam.jpg}
%    \end{figure}
%
%    \think{} How to interpret the columns of of $D$ and $D^{-1}$?
%\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    Use the inverse of the matrix $A$ in Example 2 to solve the system
    \begin{equation*}
        \systeme{
            3 x_1 + 4 x_2 = 3,
            5 x_1 + 6 x_2 = 7
        }
    \end{equation*}
    \hint{} Usually we do not solve a linear system of equations by finding the inverse of
    a matrix because it is \emoji{snail}.
\end{frame}

\begin{frame}
    \frametitle{Theorem 6 --- Inverses and transposes}

    \begin{enumerate}[<+->]
        \item[a.] If $A$ is invertible, then $(A^{-1})^{-1} = A$.
        \item[b.] If $A$ and $B$ are both invertible, then $ (AB)^{-1} = B^{-1}
            A^{-1}$.
        \item[c.] If $A$ is invertible, then so is $A^{T}$, and $(A^{T})^{-1} = (A^{-1})^{T}$.
    \end{enumerate}
\end{frame}

\subsection{Elementary Matrices}%

\begin{frame}
    \frametitle{Elementary matrices}

    An \alert{elementary matrix} is obtained by performing
    \emph{one} row operation on an identity matrix.

    Recall that
    \begin{equation*}
        I_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
    \end{equation*}
    \cake{} How to get the following from $I_3$ using one \emph{row operation}?
    \begin{equation*}
        \only<1>{E_{1}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}}
        \only<2>{E_{2}
        =
        \begin{bmatrix}
            0 & 1 & 0 \\
            1 & 0 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}}
        \only<3>{E_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 5 \\
        \end{bmatrix}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    Let
    \begin{equation*}
        E_{1}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{2}
        =
        \begin{bmatrix}
            0 & 1 & 0 \\
            1 & 0 & 0 \\
            0 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        E_{3}
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 5 \\
        \end{bmatrix}
        ,
    \end{equation*}
    \begin{equation*}
        A =
        \begin{bmatrix}
            a & b & c\\
            d & e & f\\
            g & h & j
        \end{bmatrix}.
    \end{equation*}
    Then
    \begin{flalign*}
        &
        \only<1>{
            E_{1} A =
            %\begin{bmatrix}
            %    a & b & c\\
            %    d & e & f\\
            %    g - 4a & h -4b & j - 4c
            %\end{bmatrix}
        }
        \only<2>{
            E_{2} A =
            %\begin{bmatrix}
            %    d & e & f\\
            %    a & b & c\\
            %    g & h & j
            %\end{bmatrix}
        }
        \only<3>{
            E_{3} A =
            %\begin{bmatrix}
            %    a & b & c\\
            %    d & e & f\\
            %    5 g & 5 h & 5 j
            %\end{bmatrix}
        }
        &
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Properties of Elementary Matrices (1)}

    Performing an elementary row operation (replacement, interchange, scaling) on $A$
    can be written as $E A$,
    where $E$ is an \emph{elementary matrix} created by performing the same
    operation on $I_{n}$.

    \pause{}

    \cake{} Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            a & b & c\\
            d & e & f\\
            g & h & j
        \end{bmatrix}
        \qquad
        B =
        \begin{bmatrix}
            a & b & c\\
            d - 2a & e -2b & f-2c\\
            g & h & j
        \end{bmatrix}.
    \end{equation*}
    Can you find an elementary matrix $E$ such that $E A = B$?
\end{frame}

\begin{frame}
    \frametitle{Properties of Elementary Matrices (2)}

    Each \emph{elementary matrix} $E$ is invertible since row operations are reversible.
\end{frame}

\begin{frame}
    \frametitle{Example 6}

    What is $E^{-1}$ for
    \begin{equation*}
        E
        =
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            -4 & 0 & 1 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 --- Invertible matrices}

    $A$ is invertible if and only if $A \sim I_{n}$.

    In this case, the same row operations reducing $A$ to $I_{n}$ also reduce
    $I_{n}$ to $A^{-1}$.
\end{frame}

\begin{frame}
    \frametitle{An Algorithm}

    Let $A$ be an $n \times n$ matrix.

    Row reduce $[A \quad I_n]$ to reduced echelon form.

    If $A$ is \emph{non-singular}, then $[A \quad I_n] \sim [I_n \quad A^{-1}]$.

    Otherwise $A$ is \emph{singular}.
\end{frame}

\begin{frame}
    \frametitle{Example 7}
    To find the inverse of
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            0 & 1 & 2\\
            1 & 0 & 3\\
            4 & -3 & 8\\
        \end{bmatrix}
    \end{equation*}
    reduce
    \begin{equation*}
        \begin{bmatrix}
            A & I_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 & 1 & 2 & 1 & 0 & 0\\
            1 & 0 & 3 & 0 & 1 & 0\\
            4 & -3 & 8 & 0 & 0 & 1\\
        \end{bmatrix}
    \end{equation*}
    to its reduced echelon form
    \begin{equation*}
        \begin{bmatrix}
            1 & 0 & 0 & -4 & 7 & -2 \\
            0 & 1 & 0 & -2 & 4 & -1 \\
            0 & 0 & 1 & 2 & -2 & 0 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            I_3 & A^{-1}
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Prove that if $A$ is invertible, then $5 A$ is also invertible.
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    What are the inverses of
%    \begin{equation*}
%        \begin{bmatrix}
%            1 & 0 \\
%            1 & 1 \\
%        \end{bmatrix}
%        ,
%        \qquad
%        \begin{bmatrix}
%            1 & 0 & 0 \\
%            1 & 1 & 0 \\
%            1 & 1 & 1 \\
%        \end{bmatrix}
%        ,
%    \end{equation*}
%    Can you \emph{guess} the inverse of
%    the corresponding $n \times n$ matrix?
%\end{frame}

\end{document}
