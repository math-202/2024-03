\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{3d}

\title{Lecture \lecturenum{} --- Linearly Independent Sets; Bases}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{4.3}{All odd number questions from 1 to 45.}
        \end{column}
    \end{columns}
\end{frame}

\section{4.3 Linearly Independent Sets; Bases}

\begin{frame}
    \frametitle{Review: Linear independence in $\dsR^{n}$}
    \cake{}
    Which of the followings sets of vectors is linearly independent?
    \begin{figure}[htpb]
        \centering
        \hfil
        \begin{tikzpicture}[
            scale=0.6,
            every node/.style={black},
            ]
            % Set background colour
            \draw[white, fill=white] (-1,-1) rectangle (7,3);
            % Draw a grid
            \draw[thin,gray!40] (0,0) grid (6,2);
            % Draw x and y axes
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$v_2$};
            \drawvector{3,1}{$v_1$};
        \end{tikzpicture}
        \hfil
        \begin{tikzpicture}[
            scale=0.6,
            every node/.style={black},
            ]
            % Set background colour
            \draw[white, fill=white] (-1,-1) rectangle (7,3);
            % Draw x and y axes
            \draw[thin,gray!40] (0,0) grid (6,2);
            \draw (-0,0) -- (6,0) node[right]{$x_1$};
            \draw (0,-0) -- (0,2) node[above]{$x_2$};
            \drawvector{6,2}{$v_2$};
            \drawvector{3,2}{$v_1$};
        \end{tikzpicture}
        \hfil
    \end{figure}

    \pause{}

    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in $\dsR^{n}$
    is \alert{linearly independent}
    if
    \begin{equation*}
        c_1 \bfv_1 + c_2 \bfv_2 + \dots + c_p \bfv_v = \bfzero
    \end{equation*}
    has only the trivial solution $c_1 = c_2 = \dots = c_p = 0$.

    Otherwise it is \alert{linearly dependent}.
\end{frame}

\begin{frame}
    \frametitle{Linear independence in vector spaces}
    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in a \emph{vector space} $V$
    is \alert{linearly independent} if
    \begin{equation*}
        \underline{\hspace{5cm} = \hspace{1cm}}
    \end{equation*}
    has only the trivial solution $c_1 = c_2 = \dots = c_p = 0$.

    Otherwise it is \alert{linearly dependent}.

    \pause{}

    \cake{} Is $\{\bfzero\}$ linearly independent?

    \cake{} If $\bfv \ne \bfzero$, is $\{\bfv\}$ linearly independent?
\end{frame}


\begin{frame}
    \frametitle{Theorem 7 (1.7): Linear Independence of Two or More Vectors}
    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
    be a set of vectors in a \emph{$\dsR^{m}$}
    with $p \ge 2$.

    \only<1-2>{%
        Then $S$ is \emph{linearly dependent} if and only if
        \emph{at least one vector} in $S$ is a \emph{linear combination}
        of the others.
    }%

    \only<2>{%
        \cake{} How to use this theorem to show that the following vectors are linearly dependent?
        \begin{equation*}
            \begin{bmatrix}
                1 \\ 1 \\ 1 \\ 1
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                1 \\ 1 \\ 1 \\ 2
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                3 \\ 3 \\ 3 \\ 4
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                1 \\ 3 \\ 5 \\ 7
            \end{bmatrix}
        \end{equation*}
    }%

    \only<3-4>{%
        If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
        then some $\bfv_j$ with ($j > 1$) is a linear combination of
        the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
    }%

    \only<4>{%
        \cake{} What is the $j$ for the following vectors?
        \begin{equation*}
            \begin{bmatrix}
                1 \\ 1 \\ 1
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                -1 \\ -1 \\ -1
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                3 \\ 3 \\ 4
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                1 \\ 3 \\ 5
            \end{bmatrix}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 4: Linear Independence in Vector Spaces}

    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
    be a set of vectors in a \emph{vector space $V$}
    with $p \ge 2$.

    \only<1>{%
        Then $S$ is \emph{linearly dependent} if and only if
        \emph{at least one vector} in $S$ is a \emph{linear combination}
        of the others.
    }%

    \only<2->{%
        If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
        then some $\bfv_j$ with ($j > 1$) is a linear combination of
        the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
    }%

    \only<3>{%
        \emoji{eyes} The proof is nearly identical to the proof of Theorem 7 (1.7).

        \cake{} Given two vectors $\bfv_1$ and $\bfv_2$ in $V$, when are they linearly dependent?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider three vectors
    $\bfp_{1}(t) = 1$, $\bfp_{2}(t) = t$
    and $\bfp_{3}(t) = 4 - t$
    in the vector space $\dsP_{1}$.

    \cake{} Are they linearly independent?
\end{frame}

%\begin{frame}
%    \frametitle{Example 2}
%
%    Consider $\{\sin t, \cos t\}$ in $C[0,1]$.
%    Are they linearly independent?
%    What about $\{\sin 2t, \sin t \cos t\}$?
%\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    Let $T:V \mapsto W$ be a \emph{one-to-one} linear transformation.
    Show that if
    $\{T(\bfv_{1}), \ldots, T(\bfv_{p})\}$
    is linearly dependent,
    then so is
    $\{\bfv_{1}, \ldots, \bfv_{p}\}$.

    \begin{block}{Proof}
        \footnotesize{}
        Since $\{T(\bfv_{1}), \ldots, T(\bfv_{p})\}$ are linearly dependent,
        there exist $c_{1}, \dots, c_{p}$, which are not all $0$,  such that
        \begin{equation*}
            c_1 T(\bfv_{1}) + \dots c_{p} T(\bfv_{p})
            =
            \bfzero
        \end{equation*}
        Thus, by the linearity of $T$,
        \begin{equation*}
            T(\underline{\hspace{3cm}})
            =
            \bfzero
        \end{equation*}
        Again, by the linearity of $T$, $T(\underline{\hspace{1cm}}) = \bfzero$.
        Since $T$ is one-to-one, we have
        \begin{equation*}
            \underline{\hspace{3cm}}
            =
            \bfzero
        \end{equation*}
        This implies that $\{\bfv_1, \dots, \bfv_p\}$ is linearly dependent.
    \end{block}
\end{frame}

\subsection{Bases}

\begin{frame}
    \frametitle{\zany{} Bases}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{basis, bases (plural)} (noun)  --- the underlying support or foundation for an
            idea, argument, or process.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-3d.jpg}
                \caption{%
                    What is a \alert{basis} of the 3-dimensional space we are in?
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Bases (plural)}

    Let $H$ be a subspace of a vector space $V$.

    A set of vectors $\scB = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
    is \emph{a} \alert{basis} (singular) of $H$ if
    \begin{enumerate}[(i)]
        \item $\scB$ is \emph{linearly independent},
        \item and $\Span{\scB} = H$.
    \end{enumerate}

    \smiling{} This is to say that a basis is the \emph{essence} of a subspace.

    \pause{}

    \cake{} Why do we use \emph{a} instead of \emph{the}?

    \cake{} What is a basis of the zero subspace $\{\bfzero\}$?
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let $A = [\bfa_{1} \, \ldots \, \bfa_{n}]$ be an invertible $n \times n$ matrix.

    \think{}
    Show that the columns of $A$ a basis for $\dsR^{n}$.

    \pause{}

    \begin{block}{The Definition of a Basis}
        The set $\scB$ is \emph{a} \alert{basis} of $H$ if
        \vspace{-0.5em}
        \begin{enumerate}[(i)]
            \item $\scB$ is \emph{linearly independent},
            \item and $\Span{\scB} = H$.
        \end{enumerate}
    \end{block}

    \pause{}

    \begin{block}{The Invertible Matrix Theorem (2.3)}
        Let $A$ be $n \times n$.
        The following are equivalent.
        \vspace{-0.5em}
        \begin{enumerate}
            \item[a.] $A$ is an invertible matrix.
            \item[e.] The columns of $A$ form a linearly independent set.
            \item[h.] The columns of $A$ spans $\dsR^n$.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            The set $\{\bfe_{1}, \dots, \bfe_{n}\}$
            is called the \alert{standard basis} of $\dsR^{n}$

            \cake{}
            How two write $\begin{bmatrix} 5 \\ 1 \\ 3 \end{bmatrix}$
            as a linear combination of the standard basis of
            $\dsR^{3}$?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{tikzpicture}[
                    scale=1.2,
                    x  = {(-0.7cm,-0.7cm)},
                    y  = {(1.0cm,-0.0cm)},
                    z  = {(0cm,1.0cm)}]
                    % Define style for the axes and cubes
                    \tikzstyle{axis} = [->,black,thick];

                    % Original vectors
                    \coordinate (e1) at (1, 0, 0);
                    \coordinate (e2) at (0, 1, 0);
                    \coordinate (e3) at (0, 0, 1);

                    % Draw the axes
                    \draw[axis] (0,0,0) -- (0,0,1.7) node[anchor=south]{$x_3$};
                    \draw[axis] (0,0,0) -- (1.7,0,0) node[anchor=north east]{$x_1$};
                    \draw[axis] (0,0,0) -- (0,1.7,0) node[anchor=north west]{$x_2$};

                    \drawvectorIII{e1}{above left:$\bfe_1$};
                    \drawvectorIII{e2}{above right:$\bfe_2$};
                    \drawvectorIII{e3}{above left:$\bfe_3$};
                \end{tikzpicture}
                \caption{The standard basis of $\dsR^{3}$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    \think{} How to check if the following vectors form a basis of $\dsR^{3}$?
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            3 \\ 0 \\ -6
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            -4 \\ 1 \\ 7
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3} =
        \begin{bmatrix}
            -2 \\ 1 \\ 5
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    The set $S = \{1, t, t^{2}, \dots, t^{n}\}$
        is called the \alert{standard basis} of $\dsP_{n}$.

    \cake{}
    Given a vector $\bfp = 3 + t^2$,
    how can we write it as a linear combination of the standard basis of
    $\dsP_{3}$?
\end{frame}

\subsection{The Spanning Set Theorem}

\begin{frame}
    \frametitle{Example 7}

    Let
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            0 \\ 2 \\ -1
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            2 \\ 2 \\ 0
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3} =
        \begin{bmatrix}
            6 \\ 16 \\ -5
        \end{bmatrix}
        ,
    \end{equation*}
    and $H = \Span{\bfv_1, \bfv_2, \bfv_3}$.
    Show that $\{\bfv_{1}, \bfv_{2}\}$ is a basis of $H$.

    \only<1>{%
        \begin{figure}[htpb]
            \begin{center}
                \begin{tikzpicture}[yscale=0.4, xslant=0.5, rotate=30]
                    % Original vectors
                    \coordinate (o) at (0, 0);
                    \coordinate (v1) at (0, 1);
                    \coordinate (v2) at (1, 0);
                    \coordinate (v3) at ($5*(v1) + 3*(v2)$);
                    \coordinate (v4) at ($5*(v1) + 3*(v2) + (1,1)$);

                    \draw[white, fill=white] (o) rectangle (v4);
                    \draw[step=1cm,gray,very thin] (o) grid (v4);

                    \drawvector{v1}{left:$v_1$};
                    \drawvector{v2}{right:$v_2$};
                    \drawvector{v3}{$v_3$};
                    \node at ($(v4)+(0.5,-0.5)$) {$H$};
                \end{tikzpicture}
            \end{center}
            \caption{\hint{} $\bfv_{3}  = 5 \bfv_{1} + 3 \bfv_{2}$}
        \end{figure}
    }%
    \only<2>{%
        \begin{block}{The Definition of a Basis}
            The set $\scB$ is \emph{a} \alert{basis} of $H$ if
            \vspace{-0.5em}
            \begin{enumerate}[(i)]
                \item $\scB$ is \emph{linearly independent},
                \item and $\Span{\scB} = H$.
            \end{enumerate}
        \end{block}
    }%
    \only<3>{%
        Proof that $\bfv_1$ and $\bfv_2$ are linearly independent:
    }%
    \only<4->{%
        Proof that $\Span{\bfv_{1}, \bfv_{2}} = H$:
    }%
    \only<4>{%
        First we show that $\Span{\bfv_{1}, \bfv_{2}} \subseteq H$.
    }%

    \only<5>{%
        Then we show that $H \subseteq \Span{\bfv_{1}, \bfv_{2}}$.
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 5: The Spanning Set Theorem}

    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$
    and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
    \begin{enumerate}[<+->]
        \item[a.] If a vector $\bfv_{k}$ in $S$ is the linear combination of other vectors
            in $S$, then $\bfv_{k}$ can be removed so that the remaining vectors still span $H$.
        \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
    \end{enumerate}
\end{frame}

\subsection{Bases for Null Space of $A$ and Column Space of $A$}

\begin{frame}
    \frametitle{Review: Example 3 (4.2)}

    Recall that for
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
    \end{equation*}
    the equation $A \bfx = \bfzero$ has a solution set
    \begin{equation*}
        \bfx
        =
        x_{2}
        \begin{bmatrix}
            2 \\ 1 \\ 0 \\ 0 \\ 0
        \end{bmatrix}
        +
        x_{4}
        \begin{bmatrix}
            1 \\ 0 \\ -2 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{5}
        \begin{bmatrix}
            -3 \\ 0 \\ 2 \\ 0 \\ 1
        \end{bmatrix}
        =
        x_{2} \bfu + x_{4} \bfv + x_{5} \bfw.
    \end{equation*}
    Thus, $\nullspace A = \Span{\bfu, \bfv, \bfw}$. Moreover, $\bfu, \bfv, \bfw$ are
    linearly independent.

    \cake{} What do we call the set $\{\bfu, \bfv, \bfw\}$?
\end{frame}

\begin{frame}
    \frametitle{Example 8}

    \think{} Which columns form a basis of $\colspace B$?
    \begin{equation*}
        B =
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    \only<1>{%
        \begin{block}{Theorem 5: The Spanning Set Theorem}
            Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
            \begin{enumerate}
                \item[a.] If a vector $\bfv_{k}$ in $S$ is the linear combination of other vectors
                    in $S$, then $\bfv_{k}$ can be removed so that the remaining vectors still span $H$.
                \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
            \end{enumerate}
        \end{block}
    }%
    \only<2->{%
        \cake{} How can we apply the theorem here?
    }%

    \only<2>{%
        \hint{} Note that $\bfb_2 = 4 \bfb_1$ and $\bfb_4 = 2 \bfb_1 - \bfb_3$.
    }%
    \only<3>{%
        \cake{} Do you see why $\bfb_1, \bfb_3, \bfb_5$ are linearly independent?
    }%
\end{frame}

\begin{frame}
    \frametitle{What a Matrix Is Not in Reduced Echelon Form}

    If $A \sim B$, then $A \bfx = \bfzero$ and $B \bfx = \bfzero$ have the same solution set.

    Thus,
    \begin{equation*}
        x_1 \bfa_1 + x_2 \bfa_2 + \dots + x_n \bfa_n = 0
    \end{equation*}
    if and only if
    \begin{equation*}
        x_1 \bfb_1 + x_2 \bfb_2 + \dots + x_n \bfb_n = 0
    \end{equation*}

    In other words, the columns of $A$ have \emph{exactly the same linear dependence
    relationship} as the columns of $B$.
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    Since
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & 4 & 0 &  2 & -1\\
            3 & 12 & 1 & 5 & 5 \\
            2 & 8 & 1 &  3 & 2 \\
            5 & 20 & 2 &  8 & 8 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
        =
        B
        ,
    \end{equation*}
    and 
    \begin{equation*}
        \bfb_2 = 4 \bfb_1, \qquad \bfb_4 = 2 \bfb_1 - \bfb_3,
    \end{equation*}
    we have
    \begin{equation*}
        \bfa_2 = 4 \bfa_1, \qquad \bfa_4 = 2 \bfa_1 - \bfa_3.
    \end{equation*}

    \cake{} Can you think of a basis of $\colspace A$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 6: Bases of Column Spaces}

    The pivot columns of a matrix $A$ form a basis for $\colspace A$.

    \emoji{eyes} The proof is essential in Example 9.
\end{frame}

\begin{frame}
    \frametitle{\emoji{zap} A Warning}

    Since
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & 4 & 0 &  2 & -1\\
            3 & 12 & 1 & 5 & 5 \\
            2 & 8 & 1 &  3 & 2 \\
            5 & 20 & 2 &  8 & 8 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
        =
        B
        ,
    \end{equation*}
    and $\{\bfb_1, \bfb_3, \bfb_5\}$ is a basis for $\colspace B$,
    can we say that
    \begin{equation*}
        \text{$\{\bfb_1, \bfb_3, \bfb_5\}$ is a basis for $\colspace A$}
    \end{equation*}

    \cake{} Why?

    \bomb{} Row reduction can change the column space!
\end{frame}

\begin{frame}
    \frametitle{Theorem 7: The Bases of Row Spaces}

    \only<1-2>{%
        If two matrices $A$ and $B$ are row equivalent, then their row spaces are the same.
    }%

    \only<3->{%
        If $B$ is in echelon form, the nonzero rows of $B$ form a basis for the row space of $A$
        as well as for that of $B$ .
    }%

    \only<1>{%
        \astonished{} Row reduction \emph{does not} change the row space!
    }%

    \only<2->{%
        Proof:
    }%
    \only<2>{%
        We show that $\rowspace A = \rowspace B$.
    }%
    \only<3>{%
        If $B$ is in echelon form, the following implies that the rows of $B$ are linearly independent.
        \begin{block}{Theorem 4}
            Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ be a set of vectors with $p \ge 2$.

            If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
            then some $\bfv_j$ with ($j > 1$) is a linear combination of
            the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
        \end{block}
    }%
    \only<4>{%
        This is because no row of $B$ is a linear combination of the rows above it.
        \vspace{-2em}
        \begin{figure}[htpb]
            \begin{equation*}
                \left[\begin{array}{cccccccccc}
                        0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                        0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                        0 & 0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                        0 & 0 & 0 & 0 & 0 & \bbs{} & \bast{} & \bast{} & \bast{} & \bast{} \\
                        0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \bbs{} & \bast{} \\
                \end{array}\right]
            \end{equation*}
            \caption{A matrix in echelon form}
        \end{figure}
        Thus the rows of $B$ form a basis for $\rowspace B = \rowspace B$.
    }%
\end{frame}

\subsection{Two Views of a Basis}

\begin{frame}
    \frametitle{Two views of a basis}

    If $S$ is a basis of $H$,  then it is

    \begin{itemize}[<+->]
        \item the \emph{smallest} spanning set
            --- deleting any vectors in $S$ will make it no longer \emph{span $H$};
        \item the \emph{largest} independent set
            --- adding any vector of $H$ to $S$
            will make it no longer \emph{linearly independent}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example 11}

    \cake{} Which of the following are bases of $\dsR^{3}$?
    \begin{equation*}
        \only<1>{%
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
        }%
        \only<2>{%
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                4  \\
                5  \\
                6 
            \end{bmatrix}
        }%
        \only<3>{%
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                4  \\
                5  \\
                6 
            \end{bmatrix}
            ,
            \quad
            \begin{bmatrix}
                7  \\
                8  \\
                9 
            \end{bmatrix}
        }%
    \end{equation*}
\end{frame}

\end{document}
