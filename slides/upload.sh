#!/usr/bin/env bash

find . -name "lecture-*.pdf" -exec cp {} ~/now/math202/slides \;
