\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Least-squares Problems, Machine Learning and Linear Models}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{%
        Assignments%
        \footnote{\homeworknote{}}%
        \footnote{%
            \smiling{}
            Skip ``Alternative Calculations of Least-Squares Solutions`` in 6.5 and
            ``Multiple Regression'' in 6.6.
        }%
    }%

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic[0.8]{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{6.5}{1, 5, 7, 9, 13, 15, 27, 29, 31}
            \sectionhomework[\footnotesize]{6.6}{1, 5, 7, 9, 13, 21}
        \end{column}
    \end{columns}
\end{frame}

\section{6.5 Least-squares Problems}

\begin{frame}
    \frametitle{Can we solve this?}
    
    The equation
    \begin{equation*}
    \begin{bmatrix} 2 & -2 \\ -3 & 3 \end{bmatrix}\bfx =  \begin{bmatrix} 7 \\ 25 \end{bmatrix}
    \end{equation*}
    has no solution.

    \think{} But can we find the \emph{best} approximate solution?
\end{frame}

\subsection{Solution of the General Least-Squares Problem}

\begin{frame}
    \frametitle{Least-squares Solutions}

    A \alert{least-squares solution} of $A \bfx = \bfb$ is a vector $\hat{\bfx}$ such
    that
    \begin{equation*}
        \norm{\bfb - A \hat{\bfx}}
        \le
        \norm{\bfb - A \bfx}
        ,
        \qquad
        \text{for all } \bfx.
    \end{equation*}
    \only<1>{\cake{} What is $\hat{\bfx}$ when $A$ is invertible?}%
    \only<2->{This impels that $\hat{\bfx}$ is the \emph{least-squares} solution
        if and only if
        \begin{equation*}
            A \hat{\bfx} =  \proj_{\colspace A} \bfb
        \end{equation*}
    }%
    \only<2>{
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{6.5-fig-1.jpg}
    \end{figure}
    }
    \only<3>{\cake{} We know $\proj_{\colspace A} \bfb$ is unique.
    But is $\hat{\bfx}$ necessarily unique?}
\end{frame}

\begin{frame}
    \frametitle{Normal Equations}

    Suppose that $\hat{\bfx}$ satisfies $A \hat{\bfx} = \hat{\bfb} = \proj_{\colspace A} \bfb$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{6.5-fig-2.jpg}
    \end{figure}
    \only<1>{%
        \cake{} What is $A^{T} (\bfb - A \hat{\bfx})$?
    }%
    \only<2>{%
        This computation $\hat{\bfx}$ is also a \emph{solution} of
        \begin{equation*}
            A^{T} A \bfx = A^{T} \bfb
        \end{equation*}
        This equation is call the \alert{normal equation} of $A \bfx = \bfb$.
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- Find least-squares solutions}

    The set of least-squares solutions of $A \bfx = \bfb$
    coincides with the \emph{nonempty} set of solutions of
    $A^T A \bfx =A^T \bfb$.

    \only<2>{%
        One direction: We have shown that if $\hat{\bfx}$ is a \emph{least-squares}, then it is
        also a solution to the normal equation.
    }%

    \only<3->{%
        The other direction: If $A^{T} A \hat{\bfx} = A^{T} \bfb$, then 
        \begin{equation*}
            A^{T} (A \hat{\bfx} - \bfb) = \bfzero
        \end{equation*}
        So $(A \hat{\bfx} - \bfb) \in (\colspace A)^{\perp}$.
    }%
    \only<4->{%
        Thus
        \begin{equation*}
            \bfb = A \hat{\bfx} - (A \hat{\bfx} - \bfb)  
        \end{equation*}
        is an orthogonal decomposition of $\bfb$.
    }%

    \only<5>{%
        Since such decomposition is unique, $\hat{\bfx} = \proj_{\colspace A} \bfb$.
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Find the least-squares solutions for
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            4 & 0 \\
            0 & 2 \\
            1 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            2 \\ 0 \\ 11
        \end{bmatrix}
    \end{equation*}

    Solution: The normal equation $A^{T} A \bfx = A^{T} \bfb$ is
    \begin{equation*}
        \begin{bmatrix} 
            17 & 1\\
            1 & 5\\
        \end{bmatrix} 
        \begin{bmatrix}
            x_1 \\ x_2
        \end{bmatrix}
        =
        \begin{bmatrix}
            19 \\ 11
        \end{bmatrix}
    \end{equation*}
    Since $A^{T} A$ is invertible,
    the unique solution to this equation is
    \begin{equation*}
        \hat{\bfx}
        =
        (A^{T} A)^{-1} A^{T} \bfb
        =
        \begin{bmatrix} 1 \\ 2 \end{bmatrix} 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \only<1>{%
        Find the least-squares solutions for
        \begin{equation*}
            A
            =
            \begin{bmatrix}
                1 & 1 & 0 & 0 \\
                1 & 1 & 0 & 0 \\
                1 & 0 & 1 & 0 \\
                1 & 0 & 1 & 0 \\
                1 & 0 & 0 & 1 \\
                1 & 0 & 0 & 1 \\
            \end{bmatrix}
            ,
            \qquad
            \bfb
            =
            \begin{bmatrix}
                -3 \\ -1 \\ 0 \\ 2 \\ 5 \\ 1
            \end{bmatrix}
        \end{equation*}
    }%
    \only<2>{%
        Solution: The augmented matrix for $A A^T \bfx = A^T \bfb$ has the reduced echelon form
        \begin{equation*}
            \begin{bmatrix}
                1 & 0 & 0 & 1 & 3 \\
                0 & 1 & 0 & -1 & -5 \\
                0 & 0 & 1 & -1 & -2 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{bmatrix}
        \end{equation*}
        \cake{} What is the solution set?
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- Unique least-squares solutions}

    Let $A$ be a $m \times n$ matrix. The following statements are equivalent:
    \begin{enumerate}[a.]
        \item The equation $A \bfx = \bfb$ has a unique least-squares solution for each
            $\bfb \in \dsR^m$.
        \item The columns of $A$ are linearly independent.
        \item The matrix $A^T A$ is invertible.
    \end{enumerate}
    When these are true, the least-squares solution for $A \bfx = \bfb$ is
    \begin{equation*}
        \hat{\bfx} = (A^{T} A)^{-1} A^{T} \bfb.
    \end{equation*}

    Proof: Exercise 27-29.
\end{frame}

\begin{frame}
    \frametitle{Least-squares errors}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The distance from $\bfb$ to $A \hat{\bfx}$ is called the
            \alert{least-squares error}.

            In Example 1, the least-squares error is
            \begin{equation*}
                \norm{\bfb - A \hat{\bfx}}
                =
                \sqrt{84}
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.5-fig-3.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\subsection{Alternative Calculations of Least-Squares Solutions}
%
%\begin{frame}
%    \frametitle{Example 4}
%
%    What are the least-squares solutions for
%    \begin{equation*}
%        A =
%        \begin{bmatrix}
%            1 & -6 \\
%            1 & -2 \\
%            1 & 1 \\
%            1 & 7 \\
%        \end{bmatrix}
%        ,
%        \qquad
%        \bfb =
%        \begin{bmatrix}
%            -1 \\ 2 \\ 1 \\ 6
%        \end{bmatrix}
%    \end{equation*}
%    \hint{} The columns of $A$ are orthogonal.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Theorem 15 --- Using QR factorization}
%
%    Let $A$ be a $m \times n$ matrix with linearly independent columns and let $A = QR$ be a
%    QR factorization as in Theorem 12.
%
%    Then for each $\bfb \in \dsR^{m}$ there is a unique least-squares solution
%    \begin{equation*}
%        \hat{\bfx} = R^{-1} Q^T \bfb
%        .
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Theorem 15 --- What the proof needs}
%    \begin{block}{Theorem 12 --- QR Factorization}
%
%        If $A$ has linearly independent columns,
%        then $A$ can be factored as $A = QR$,
%        where $Q$ is a matrix whose columns form an
%        orthonormal basis for $\colspace A$.
%    \end{block}
%    \begin{block}{Theorem 10 --- Orthonormal bases}
%
%        If the columns of $Q$ is an \emph{orthonormal basis} of $W$, then
%        \begin{equation*}
%            \proj_{W} \bfy
%            =
%            Q Q^{T} \bfy
%            .
%        \end{equation*}
%    \end{block}
%\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%    {\small
%    \begin{block}{Theorem 13}
%    Let $A$ be $m \times n$. The following statements are equivalent:
%    \begin{enumerate}
%        \item[b.] The columns of $A$ are linearly independent.
%        \item[c.] The matrix $A^T A$ is invertible.
%    \end{enumerate}
%    \end{block}
%    }
%
%    \begin{block}{Proof of $\text{c} \imp \text{b}$ }
%        Assume that the columns of $A$ are not linearly independent.
%        Then there exist $\bfx \ne \bfzero$ such that
%        \begin{equation*}
%            A \bfx
%            =
%            \blankshort{}
%        \end{equation*}
%        This implies that $A^{T}A \bfx = \blankveryshort{}$.
%        This a contradiction to c because c implies that \blankshort{}.
%    \end{block}
%\end{frame}

\section{6.6 Machine Learning and Linear Models}

%\begin{frame}
%    \frametitle{Because of Machine Learning}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.5\linewidth]{machine_learning_2x.jpg}
%        \caption{\url{https://xkcd.com/1838/}}%
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Fitting a Line to Data}

    Give experimental data $(x_{1}, y_{1}), \dots (x_{n}, y_{n})$,
    how to find a line which \emph{best}
    fit the data?

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=0, xmax=30,
                    ymin=0, ymax=120,
                    xtick={0,5,...,30},
                    ytick={0,20,...,100},
                    xlabel={$x$},
                    ylabel={$y$},
                    samples=40, % increase samples because of potential duplicate x values
                    clip=false,
                    scale only axis=true,
                    width=0.7\textwidth,
                    height=0.4\textwidth,
                    legend style={%
                        font=\footnotesize,
                        line width=0.4pt
                    },
                    ]
                    % Simulate the data with random noise around the line y = 95 - 2.7x
                    \addplot[
                        only marks,
                        scatter, % enable scatter to simulate the 'randomness' of data points
                        draw=BrightRed,
                        mark=*,
                        mark options={BrightRed, fill=BrightRed, scale=0.6},
                        ]
                        coordinates {
                            (0,95 + rand*10) (1,92.3 + rand*10) (2,89.6 + rand*10) (2,89.6 + rand*10) % Duplicate x at 2
                            (3,86.9 + rand*10) (4,84.2 + rand*10) (5,81.5 + rand*10) (6,78.8 + rand*10)
                            (7,76.1 + rand*10) (8,73.4 + rand*10) (9,70.7 + rand*10) (10,68 + rand*10)
                            (11,65.3 + rand*10) (12,62.6 + rand*10) (13,59.9 + rand*10) (14,57.2 + rand*10)
                            (15,54.5 + rand*10) (16,51.8 + rand*10) (17,49.1 + rand*10) (18,46.4 + rand*10)
                            (19,43.7 + rand*10) (20,41 + rand*10) (21,38.3 + rand*10) (22,35.6 + rand*10)
                            (23,32.9 + rand*10) (24,30.2 + rand*10) (25,27.5 + rand*10) (26,24.8 + rand*10)
                            (27,22.1 + rand*10) (28,19.4 + rand*10) (28,19.4 + rand*10) % Duplicate x at 28
                        };
                    \addlegendentry{Data};

                    % Add the fitted line without noise
                    \addplot[
                        smooth,
                        thick,
                        draw=DaylightBlue,
                        domain=-1:29,
                        ] {95-2.7*x};
                    \addlegendentry{Model};
                \end{axis}
            \end{tikzpicture}
        }%

        \caption{%
            The best line?
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Least-squares Lines}

    The \alert{least-squares line} is the line $y = \beta_0 + \beta_1 x$ which
    minimizes \alert{the sum of the squares of the residuals}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{6.6-fig-1.jpg}
        \caption{What is \alert{residual}?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Finding the least-squares line}

    Let
    \begin{equation*}
        X =
        \begin{bmatrix}
            1 & x_{1} \\
            1 & x_{2} \\
            \vdots & \vdots \\
            1 & x_{n} \\
        \end{bmatrix}
        ,
        \quad
        \symbf{\beta}
        =
        \begin{bmatrix}
            \beta_0 \\ \beta_1
        \end{bmatrix}
        ,
        \quad
        \bfy =
        \begin{bmatrix}
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix}
    \end{equation*}
    Then
    \begin{equation*}
        X \symbf{\beta} - \bfy
        =
            \begin{bmatrix}
            \beta_0 + \beta_1 x_{1} - y_{1} \\
            \beta_0 + \beta_1 x_{2} - y_{2} \\
            \vdots \\
            \beta_0 + \beta_1 x_{n} - y_{n} \\
        \end{bmatrix}
    \end{equation*}

    Thus \alert{the sum of the squares of the residuals} is $\norm{X
    \symbf{\beta} - \bfy}^{2}$.

    So finding the \emph{least-squares line} means
    finding the \emph{least-squares solutions} of $X \symbf{\beta} = \bfy$
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Finding the least-square line for $(2,1), (5,2), (7,3), (8,3)$.

    \only<1>{%
        Let
        \begin{equation*}
            X =
            \begin{bmatrix}
                1 & 2 \\
                1 & 5 \\
                1 & 7 \\
                1 & 8 \\
            \end{bmatrix}
            ,
            \quad
            \symbf{\beta}
            =
            \begin{bmatrix}
                \beta_0 \\ \beta_1
            \end{bmatrix}
            ,
            \quad
            \bfy =
            \begin{bmatrix}
                1 \\ 2 \\ 3 \\ 3
            \end{bmatrix}
        \end{equation*}
        Then, we need to find the least-squares solution of
        \begin{equation*}
            X
            \begin{bmatrix} \beta_0 \\ \beta_1 \end{bmatrix} 
            =
            \bfy
        \end{equation*}
        In other words, we need to solve
        \begin{equation*}
            X^{T} X
            \begin{bmatrix} \beta_0 \\ \beta_1 \end{bmatrix} 
            =
            X^{T} \bfy
        \end{equation*}
    }%

    \only<2>{%
    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=0, xmax=9,
                    ymin=0, ymax=4,
                    xtick={0,1,...,10},
                    ytick={0,1,...,4},
                    xlabel={$x$},
                    ylabel={$y$},
                    samples=40, % increase samples because of potential duplicate x values
                    clip=false,
                    scale only axis=true,
                    width=0.7\textwidth,
                    height=0.4\textwidth,
                    legend style={%
                        at={(1.1,0.3)},
                        anchor=south east,
                        font=\footnotesize,
                        line width=0.4pt
                    },
                    ]
                    \addplot[
                        only marks,
                        scatter, % enable scatter to simulate the 'randomness' of data points
                        draw=BrightRed,
                        mark=*,
                        mark options={BrightRed, fill=BrightRed, scale=1.0},
                        ]
                        coordinates {
                            (2,1) (5,2) (7,3) (8,3)
                        };
                    \addlegendentry{Data};

                    % Add the fitted line without noise
                    \addplot[
                        smooth,
                        thick,
                        draw=DaylightBlue,
                        domain=-1:10,
                        ] {0.3571*x + 0.2857};
                    \addlegendentry{Model};
                \end{axis}
            \end{tikzpicture}
        }%

        \caption{%
            The least-squares line for $(2,1), (5,2), (7,3), (8,3)$
        }%
    \end{figure}
    }%
\end{frame}

\subsection{The General Linear Model}

\begin{frame}
    \frametitle{The General Linear Model}

    In general, a linear model is of the form $X \symbf{\beta} = \bfy$,
    where
    \begin{equation*}
        X
        =
        \begin{bmatrix} 
            f_{0}(x_{1}) & f_{1}(x_{1}) & \dots & f_{k}(x_{1}) \\
            f_{0}(x_{2}) & f_{1}(x_{2}) & \dots & f_{k}(x_{2}) \\
            \vdots & \vdots & \ddots & \vdots \\
            f_{0}(x_{n}) & f_{1}(x_{n}) & \dots & f_{k}(x_{n}) \\
        \end{bmatrix} 
    \end{equation*}

    \only<2>{%
        The aim is to find $\symbf{\beta}$ which minimizes the \emph{norm/length} of $\symbf{\epsilon}$  in       
        \begin{equation*}
            X \symbf{\beta} = \bfy + \symbf{\epsilon}.
        \end{equation*}

        This amounts to find the \emph{least-squares solutions} for $X \symbf{\beta} = \bfy$ by
        finding $\hat{\symbf{\beta}}$, the exact solution of
        \begin{equation*}
            X^{T} X \symbf{\beta} = X^{T} \bfy.
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
    \emph{least-squares fit} of the data by
    \begin{equation*}
        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2}
    \end{equation*}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.6-fig-4.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            In this case
            \begin{equation*}
                X
                =
                \begin{bmatrix} 
                    1 & x_{1} &  x_{1}^{2} \\
                    1 & x_{2} &  x_{2}^{2} \\
                    \vdots & \vdots & \vdots \\
                    1 & x_{n} &  x_{n}^{2} \\
                \end{bmatrix} 
            \end{equation*}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
    \emph{least-squares fit} of the data by
    \begin{equation*}
        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2} + \beta_{3} x^{3}
    \end{equation*}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.6-fig-5.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            In this case
            \begin{equation*}
                X
                =
                \begin{bmatrix} 
                    1 & x_{1} &  x_{1}^{2}   & x_{1}^{3} \\
                    1 & x_{2} &  x_{2}^{2}   & x_{2}^{3} \\
                    \vdots & \vdots & \vdots & \vdots  \\
                    1 & x_{n} &  x_{n}^{2}   & x_{n}^{3} \\
                \end{bmatrix} 
            \end{equation*}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Multiple Regression}
%
%    Given data $(u_{1}, v_{1}, y_{1}), \dots, (u_{n}, v_{n}, y_{n})$, how to find the
%    \emph{least-squares fit} of the data by
%    \begin{equation*}
%        y = \beta_{0} + \beta_{1} u + \beta_{2} v
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-fig-6.jpg}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%
%        \end{column}
%    \end{columns}
%\end{frame}

%\begin{frame}
%    \frametitle{One Last Exercise}
%
%    \think{} Given the data $(1, 7.9), (2, 5.4), (3, -0.9)$,
%    write down the system of linear equations whose solution will give the
%    least-squares fit of the data in the form of
%    \begin{equation*}
%        y = \beta_{1} \cos(x) + \beta_{2} \sin(x)
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-exercise.jpg}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}[standout]
    \Huge{}
    The End
    \begin{figure}[htpb]
        \includegraphics[width=0.6\textwidth]{bunny-fun.jpg}
    \end{figure}
\end{frame}

\end{document}
