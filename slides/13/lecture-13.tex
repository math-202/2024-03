\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{3d}

\title{Lecture \lecturenum{} --- Vector Space and Subspace}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{4.1}{All odd number questions from 1 to 41.}
        \end{column}
    \end{columns}
\end{frame}

\section{4 Vector Spaces}

\begin{frame}
    \frametitle{\acf{dsp}}
    
    \alert{\ac{dsp}} refers to the manipulation, analysis, and
    modification of digital signals such as sound, images, and data.

    \ac{dsp} and \ac{ai} allows us to control smart speakers by speaking.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{Amazon-Alexa.jpg}
        \caption{Amazon's smart speaker Alexa}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discrete-time signal}
    
    For \ac{dsp} to work with voice, analog signals must be convert to discrete-time
    signal.

    A discrete-time signal $\{y_{n}\}_{n \in \dsZ}$ can be seen as \alert{vector} in a
    \alert{vector space}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{Sampled.signal.pdf}
        \caption{Discrete Time Signal}
    \end{figure}
\end{frame}

\section{4.1 Vector Space and Subspace}

\subsection{Vector Spaces}

\begin{frame}[c]
    \frametitle{\zany{} Axiom}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{axiom} (noun)  --- 

            \begin{enumerate}
                \item a statement or proposition which is regarded as being established, accepted, or self-evidently true. 
                \item a statement or proposition on which an abstractly defined structure is based.
            \end{enumerate}

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-axioms.jpg}
                \caption{%
                    We will use axioms to ``cook up'' vector spaces.
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Review of ${\color{orange}\dsR^{n}}$}

    The set ${\color{orange}\dsR^{n}}$
    together with vector addition and scalar multiplication have these 
    \emph{axioms} (properties):

    For all $\bfv, \bfw, \bfu \in {\color{orange}\dsR^{n}}$ and all $r, s \in \dsR$
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \item  $\bfv + \bfw   \in {\color{orange}\dsR^{n}}$
                \item  $\bfv + \bfw  = \bfw  +  \bfv$
                \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                \item  there exists $\bfzero \in {\color{orange}\dsR^{n}}$ such that  $\bfv +  \bfzero =  \bfv$
                \item  there exists $-\bfv  \in {\color{orange}\dsR^{n}}$ such that $-\bfv  +  \bfv =  \bfzero$
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \pause{}
            \begin{enumerate}
                \setcounter{enumi}{5}
                \item  $r \cdot  \bfv \in {\color{orange}\dsR^{n}}$
                \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                \item  $1 \cdot  \bfv =  \bfv$
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Vector spaces}

    A \alert{vector space}  consists of a set ${\color{red}V}$ along with two
    operations `$+$' and `$\cdot$' such that the following
    \emph{axioms} (properties) hold:

    For all $\bfv, \bfw, \bfu \in {\color{red}V}$
    (\emph{vectors}) and all $r, s \in \dsR$ (\emph{scalars})
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \item  $\bfv + \bfw   \in {\color{red}V}$
                \item  $\bfv + \bfw  = \bfw  +  \bfv$
                \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                \item  there exists $\bfzero \in {\color{red}V}$ such that  $\bfv +  \bfzero =  \bfv$
                \item  there exists $-\bfv  \in {\color{red}V}$ such that $-\bfv  +  \bfv =  \bfzero$
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \setcounter{enumi}{5}
                \item  $r \cdot  \bfv \in {\color{red}V}$
                \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                \item  $1 \cdot  \bfv =  \bfv$
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Three Properties}

    It follows from the axioms of vector spaces that
    \begin{itemize}
        \item the zero vector $\bfzero$ is unique
        \item for each $\bfu \in V$, $-\bfu$ is unique
        \item $0 \bfu = \bfzero$ for any $\bfu \in V$
        \item $r \bfzero = \bfzero$ for any $r \in \dsR$
        \item $-1 \cdot \bfu = -\bfu$
    \end{itemize}

    \hint{} Since we can derive them from the axioms of vector spaces, we do not need to
    to include them in the axioms.
\end{frame}

\begin{frame}
    \frametitle{Proof of $0 \bfu = \bfzero$}
    \vspace{-2em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{ Axioms of Vector Spaces }
                \footnotesize{}
                For all $\bfv, \bfw \in V$ and $r, s \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{block}{Proof of $0 \bfu = \bfzero$}
                \footnotesize{}
                By axiom \blankveryshort{},
                \begin{equation}
                    \label{eq:WMLD}
                    0 \bfu = (0+0) \bfu = 0 \bfu + 0 \bfu
                \end{equation}
                By axiom \blankveryshort{}, there exists $-(0 \bfu)$ such that
                \begin{equation*}
                    -(0 \bfu) + 0 \bfu = \bfzero
                \end{equation*}
                Add $-(0\bfu)$ to both sides of \eqref{eq:WMLD}, we get:
                \vspace{2.8cm}
            \end{block}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \vspace{-2em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{Axioms of Vector Spaces }
                \footnotesize{}
                For all $\bfv, \bfw \in V$ and $r, s \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{block}{Proof of $r \bfzero = \bfzero$}
                \footnotesize{}
                By \blankveryshort{},
                \begin{equation*}
                    r \mathbf{0} + r \mathbf{0}= r(\mathbf{0} + \mathbf{0})
                \end{equation*}
                Thus, by \blankveryshort{},
                \begin{equation}
                    \label{eq:1}
                    r \mathbf{0} + r \mathbf{0}= r\mathbf{0}
                \end{equation}
                By \blankveryshort{}, there exists $-(r \bfzero)$ such
                that
                \begin{equation*}
                    -(r \bfzero) + r \bfzero = \bfzero
                \end{equation*}
                Adding $-(r \bfzero)$ to both sides of \eqref{eq:1}
                \begin{equation}
                    \label{eq:2}
                    (r \mathbf{0} + r \mathbf{0}) + (- r\mathbf{0})
                    =\bfzero
                \end{equation}
                By \blankveryshort{},
                the LHS of \eqref{eq:2} equals
                \begin{equation*}
                    r \mathbf{0} + (r \mathbf{0} + (- r\mathbf{0}))
                    =
                    r \mathbf{0}
                \end{equation*}
            \end{block}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 1: $\dsR^{n}$}

    Let $\dsR^{n}$ be the set of all matrices of size $n \times 1$.

    \only<1>{%
        In other words, $\dsR^{n}$ contains all matrices of the form
        \begin{equation*}
            \begin{bmatrix}
                x_1 \\ x_2 \\ \vdots \\ x_n
            \end{bmatrix}
        \end{equation*}
    }%

    \only<2->{%
        Then $\dsR^{n}$ together with the following operations form a vector space:
    }%

    \only<2>{%
        Addition:
        \begin{equation*}
            \begin{bmatrix}
                x_1 \\ x_2 \\ \vdots \\ x_n
            \end{bmatrix}
            +
            \begin{bmatrix}
                y_1 \\ y_2 \\ \vdots \\ y_n
            \end{bmatrix}
            =
            \begin{bmatrix}
                x_1 + y_1 \\ x_2 + y_2 \\ \vdots \\ x_n + y_n
            \end{bmatrix}
        \end{equation*}
    }%

    \only<3>{%
        Scalar multiplication:
        \begin{equation*}
            r
            \begin{bmatrix}
                x_1 \\ x_2 \\ \vdots \\ x_n
            \end{bmatrix}
            =
            \begin{bmatrix}
                r x_1 \\ r x_2 \\ \vdots \\ r x_n
            \end{bmatrix}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2: Arrows on a Plane}

    Let $V$ be the set of all arrows in a plane.

    \only<1>{%
        Regard two arrows as equal if they have the same direction and length.
    }%

    \only<2->{%
        Then $V$ together with the following operations form a vector space:
    }%

    \only<2>{%
        Define the addition of two arrows using the parallelogram rule.
    }%

    \only<3>{%
        For a scalar $r \in \dsR$, define the product of $r$ and an arrow as the arrow
        with the same direction and length as the original arrow multiplied by $r$.
    }%
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                scale=0.8, 
                transform shape]
                \tikzset{vector/.style={DaylightBlue, thick, -Latex}};
                \coordinate (o) at (0,0);
                \coordinate (u) at (1,2);
                \coordinate (v) at (3,1);
                \coordinate (w) at ($(u)+(v)$);
                \coordinate (tr) at (2, 0.5);
                \coordinate (u1) at ($(u)+(v)$);
                \coordinate (v1) at ($(v)+(u)$);
                \only<1>{%
                    \drawvectorShifted{tr}{$(u)+(tr)$}{$\bfu$};
                }%
                \only<2>{%
                    \draw[fill=MintCream] (o) -- (u) -- (w) -- (v) -- cycle;
                    \drawvector{v}{$\bfv$};
                    \drawvectorShifted[JuliaOrange]{v}{u1}{};
                    \drawvectorShifted[JuliaOrange]{u}{v1}{};
                    \drawvector[MintCream]{w}{above right:$\bfu+\bfv$};
                }%
                \only<3>{%
                    \drawvector[JuliaOrange]{$1.7*(u)$}{$r \bfu$};
                }%
                \drawvector{u}{$\bfu$};
            \end{tikzpicture}
        \end{center}
        \caption{%
            \only<1>{%
                Two arrows that are equal.
            }%
            \only<2>{%
                The sum of two arrows.
            }%
            \only<3>{%
                An arrow multiplied by a scalar.
            }%
        }%
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Example 3: Signals}

    Let $\dsS$ be the set of doubly infinite sequences like
    \begin{equation*}
        \{y_{k}\} = (\cdots, y_{-3}, y_{-2}, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
    \end{equation*}

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=26,
                    ymin=-1.1, ymax=1.1,
                    xtick={-5,0,5,10}, 
                    ytick=\empty,
                    clip=false,
                    scale only axis=true,
                    % set `width' and `height' to the desired values
                    width=0.9\textwidth,
                    height=0.3\textwidth,
                    y axis line style={opacity=0},
                    ]

                    \addplot[
                        ycomb,
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=0.5},
                        domain=-5:25,
                        samples=31,
                        ] {ifthenelse(x < 0, 0, sin(deg(x)*0.3+0.7))};
                \end{axis}
            \end{tikzpicture}
            \caption{%
                Elements of $\dsS$ can represent a signal measured at discrete times
            }%
        \end{figure}
    }%
    \only<2-3>{%
        \think{} How can we define the following operations to make $\dsS$ a vector space?
    }%

    \only<2>{
        Addition:
        \begin{equation*}
            \begin{aligned}
                \{y_{k}\} + \{z_{k}\} 
                &
                =
                (\cdots, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
                +
                (\cdots, z_{-1}, z_{0}, z_{1}, z_{2}, \cdots)
                \\
                &
                = \blanklong{}
            \end{aligned}
        \end{equation*}
    }
    \only<3>{
        Scalar multiplication:
        \begin{equation*}
            \begin{aligned}
                r \{y_{k}\} 
                &
                = 
                r
                (\cdots, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
                \\
                &
                =
                \blanklong{}
            \end{aligned}
        \end{equation*}
    }
    \only<4>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=26,
                    ymin=-1.1, ymax=1.1,
                    xtick={-5,0,5,10}, 
                    ytick=\empty,
                    clip=false,
                    scale only axis=true,
                    width=0.9\textwidth,
                    height=0.3\textwidth,
                    y axis line style={opacity=0},
                    domain=-5:25,
                    samples=31,
                    ycomb,
                    legend style={at={(0.1,0.1)}, anchor=north west, font=\small}
                    ]

                    \addplot[
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=0.5},
                        ] {ifthenelse(x < 0, 0, sin(deg(x)*0.3+0.7))};
                    \addlegendentry{$\{y_{k}\}$};

                    \addplot[
                        mark=*,
                        mark options={BrightRed, fill=BrightRed, scale=0.5},
                        ] {ifthenelse(x < -2, 0, 0.5*cos(deg(x)*0.2+0.5))};
                    \addlegendentry{$\{z_{k}\}$};

                    \addplot[
                        mark=*,
                        mark options={JuliaOrange, fill=JuliaOrange, scale=0.5},
                        ] {ifthenelse(x < 0, 0, sin(deg(x)*0.3+0.7))+ifthenelse(x < -2, 0, 0.5*cos(deg(x)*0.2+0.5))};
                    \addlegendentry{$\{y_{k}\}+\{z_{k}\}$};

                \end{axis}
            \end{tikzpicture}
            \caption{%
                Adding two signals
            }%
        \end{figure}
    }%
    \only<5>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=middle,
                    xmin=-6, xmax=26,
                    ymin=-1.1, ymax=1.1,
                    xtick={-5,0,5,10}, 
                    ytick=\empty,
                    clip=false,
                    scale only axis=true,
                    width=0.9\textwidth,
                    height=0.2\textwidth,
                    y axis line style={opacity=0},
                    domain=-5:25,
                    samples=31,
                    ycomb,
                    legend style={at={(0.1,0.1)}, anchor=north west, font=\small}
                    ]

                    \addplot[
                        mark=*,
                        mark options={DaylightBlue, fill=DaylightBlue, scale=0.5},
                        ] {ifthenelse(x < 0, 0, sin(deg(x)*0.3+0.7))};
                    \addlegendentry{$\{y_{k}\}$};

                    \addplot[
                        mark=*,
                        mark options={BrightRed, fill=BrightRed, scale=0.5},
                        ] {2*ifthenelse(x < 0, 0, sin(deg(x)*0.3+0.7))};
                    \addlegendentry{$r \{y_{k}\}$};

                \end{axis}
            \end{tikzpicture}
            \caption{%
                Multiply a signal by a scalar
            }%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 4: Polynomials}

    Let $\dsP_n$ be the set of polynomials of degree at most $n$, i.e.,
    \begin{equation*}
        \bfp(t) = a_{0} + a_{1} t + a_{2} t^{2} + \cdots + a_{n} t^{n}
    \end{equation*}
    where $a_{0}, a_{1}, \ldots, a_{n}$ are real numbers.

    \only<1>{%
        For example, some elements in $P_{3}$ are
        \begin{equation*}
            1 + t + t^2 + t^3, 
            \qquad
            1 + 2 t + 3 t^2 + 4 t^3, 
            \qquad
            1 + 2 t^2 + t^3
        \end{equation*}
    }

    \only<2>{%
        \bomb{} The variable $t$ does not matter. For example 
        \begin{equation*}
            x + x^2
            ,
            \qquad 
            t + t^{2}
            ,
            \qquad 
            \temoji{rabbit-face} + \temoji{rabbit-face}^{2}
        \end{equation*}
        are the same element of $\dsP_2$.

        \cake{} Which of the following are in $\dsP_3$?
        \begin{equation*}
            1 +2t, \qquad 1 + 2 t + 3 t^2, \quad 1 + 2t + t^2 + t^3, \quad 2 + t^{4}
        \end{equation*}
    }%

    \only<3->{%
        \think{} How can we define the following operations to make $\dsP_n$ a vector space?
    }%

    \only<3>{
        Addition:
        \begin{equation*}
            \begin{aligned}
                \bfp(t) 
                +
                \bfq(t) 
                &
                =
                (a_{0} + a_{1} t +  \cdots + a_{n} t^{n})
                +
                (b_{0} + b_{1} t +  \cdots + b_{n} t^{n})
                \\
                &
                = \blanklong{}
            \end{aligned}
        \end{equation*}
    }
    \only<4>{
        Scalar multiplication:
        \begin{equation*}
            \begin{aligned}
                r \bfp(t) 
                &
                =
                r (a_{0} + a_{1} t +  \cdots + a_{n} t^{n})
                \\
                &
                = \blanklong{}
            \end{aligned}
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{Example 5: Real-Valued Functions}

    Let $V$ be the set of real-valued functions on the domain of $\dsD$ (typically $\dsD = \dsR$).

    \only<1>{%
        For example, some elements in $V$ are
        \begin{equation*}
            \sin(t), 2 t + 1, \exp(t), \dots.
        \end{equation*}
        \bomb{} Again, which variables, e.g., $t, x, z, \temoji{rabbit-face}, \dots$,  we use does not matter.
    }%

    \only<2->{%
        \think{} How can we define the following operations to make $V$ a vector space?
    }%

    \only<2>{
        Addition:
        \begin{equation*}
            (\bff+\bfg)(t) = \blanklong{}
        \end{equation*}
    }
    \only<3>{
        Scalar multiplication:
        \begin{equation*}
            (r \bff)(t) = \blanklong{}
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{More Example}
    
    Let $C^n([0,1])$ be to the set of functions that are at least three times
    continuously differentiable on the interval $[0,1]$.

    Then $C^{n}([0,1])$ with the usual addition and scalar multiplication
    is a vector space.

    \pause{}
    
    Let $M_{n}(\dsR)$ (or $\dsR^{n \times n}$) be 
    the set of $n \times n$ matrices with real entries.

    Then $M_{n}(\dsR)$ with the usual addition and scalar multiplication
    is a vector space.

    \hint{} These notations are used in the WeBWork assignment.
\end{frame}

\begin{frame}[standout]
    Vectors are \alert{elements of a vector space}, not just column matrices like
    \begin{equation*}
        \begin{bmatrix}
            1 \\ 2 \\ 3
        \end{bmatrix}
    \end{equation*}
    There are many other vector spaces.
\end{frame}

\subsection{Subspaces}

\begin{frame}
    \frametitle{Subspaces}

    Let $V$ be a vector space.
    If $H \subseteq V$ satisfies the following
    \begin{enumerate}
        \item[a.] $\bfzero \in H$,
        \item[b.] for each $\bfu, \bfv \in H$, $\bfu + \bfv \in H$,
        \item[c.] each $\bfu \in H$ and each scalar $c$, $c \bfu \in H$,
    \end{enumerate}
    we say $H$ is a \alert{subspace} of $V$.

    \only<2>{%
        \dizzy{} Is $\dsR^{2}$ a subspace of $\dsR^{3}$?
    }%

    \only<3>{%
        \cake{} Why is every vector space $V$ itself a subspace?
    }%
\end{frame}

\begin{frame}
    \frametitle{Subspaces are Vector Spaces}
    \vspace{-2em}
    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{block}{Axioms of Vector Spaces}
                \footnotesize{}
                For all $\bfv, \bfw \in V$ and $r, s \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{block}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{block}{Properties of subspaces}
                \footnotesize{}
                For all $\bfv, \bfu \in H$ and $r \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$.
                \end{enumerate}
                \cake{} Which axioms does (a), (b), and (c) correspond to?

                \cake{} Which axioms does $H$ have because $H \subseteq V$?

                \vspace{2cm}
            \end{block}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    
    Let $V$ be a vector space and $H = \{\bfzero\}$.

    \cake{} Show that $H$ is a subspace of $V$, i.e.,
    \begin{enumerate}
        \item[a.] $\bfzero \in H$,
        \item[b.] for each $\bfu, \bfv \in H$, $\bfu + \bfv \in H$,
        \item[c.] each $\bfu \in H$ and each scalar $c$, $c \bfu \in H$.
    \end{enumerate}

    The subspace $H$ is called the \alert{zero subspace} of $V$.
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    Let $\dsP$ be the set of \emph{all} polynomials.
    
    Let $V$ be the vector space of all real functions with domain $\dsR$.

    \cake{} Show that $\dsP$ is a subspace of $V$, i.e., $\dsP \subseteq V$ and
    \begin{enumerate}
        \item[a.] $\bfzero \in \dsP$,
        \item[b.] for each $\bfu, \bfv \in \dsP$, $\bfu + \bfv \in \dsP$,
        \item[c.] each $\bfu \in \dsP$ and each scalar $c$, $c \bfu \in \dsP$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    \only<1>{%
        Consider the following subset of $\dsR^{3}$
        \begin{equation*}
            H =
            \left\{
                \begin{bmatrix}
                    s \\ t \\ 0
                \end{bmatrix}
                :
                s, t \in \dsR
            \right\}
        \end{equation*}
        \cake{} Show that $H$ is a subspace of $\dsR^{3}$, i.e.,
        \begin{enumerate}
            \item[a.] $\bfzero \in H$,
            \item[b.] for each $\bfu, \bfv \in H$, $\bfu + \bfv \in H$,
            \item[c.] each $\bfu \in H$ and each scalar $c$, $c \bfu \in H$.
        \end{enumerate}
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \begin{center}
                \begin{tikzpicture}[
                    scale=1.2,
                    x  = {(-1cm,-1cm)},
                    y  = {(1cm,-0.3cm)},
                    z  = {(0cm,1cm)}]
                    % Define style for the axes and cubes
                    \tikzstyle{axis} = [->,black,thick];

                    \begin{scope}[canvas is xy plane at z=0.0]
                        \draw[white, fill=MintCream, opacity=0.8] (-0.8,-1.2) rectangle (0.8,1.2);
                    \end{scope}

                    % Draw the axes
                    \draw[axis] (0,0,0) -- (1.1,0,0) node[anchor=north east]{$x_1$};
                    \draw[axis] (0,0,0) -- (0,1.5,0) node[anchor=north west]{$x_2$};
                    \draw[axis] (0,0,0.0) -- (0,0,1.9) node[anchor=south]{$x_3$};

                    \node[vector point] at (0,0,0.0) {};

                    \node (h) at (-1,2,0.5) {$H$};
                    \coordinate (h1) at (-0.7,1.0,0.0);
                    \draw[thin,dotted] (h) -- (h1);
                \end{tikzpicture}
            \end{center}
            \caption{$H$ is a plane in $\dsR^{3}$ through the origin}%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 10}

    Consider the following subset of $\dsR^{3}$.
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                scale=1.2,
                x  = {(-1cm,-1cm)},
                y  = {(1cm,-0.3cm)},
                z  = {(0cm,1cm)}]
                % Define style for the axes and cubes
                \tikzstyle{axis} = [->,black,thick];

                % Draw the axes
                \draw[black,thick] (0,0,0) -- (0,0,0.5);
                \draw[axis] (0,0,0) -- (1.1,0,0) node[anchor=north east]{$x_1$};
                \draw[axis] (0,0,0) -- (0,1.5,0) node[anchor=north west]{$x_2$};

                \begin{scope}[canvas is xy plane at z=0.5]
                    \draw[white,fill=MintCream, opacity=0.8] (-0.8,-1.2) rectangle (0.8,1.2);
                \end{scope}

                \draw[axis] (0,0,0.5) -- (0,0,1.9) node[anchor=south]{$x_3$};

                \node[vector point] at (0,0,0.5) {};

                \node (h) at (-1,2,0.5) {$H$};
                \coordinate (h1) at (-0.7,1.0,0.5);
                \draw[thin,dotted] (h) -- (h1);
            \end{tikzpicture}
        \end{center}
        \caption{$H$ is a plane in $\dsR^{3}$ not through the origin}%
    \end{figure}
    
    \cake{} Is $H$ a subspace of $\dsR^{3}$? Why?
\end{frame}

\subsection{A Subspace Spanned by a Set}

\begin{frame}
    \frametitle{Example 11}

    \only<1>{%
        Given $\bfv_{1}, \bfv_{2} \in V$, let $H = \Span{\bfv_{1}, \bfv_{2}}$.

        \cake{} Show that $H$ is a subspace of $V$, i.e.,
        \begin{enumerate}
            \item[a.] $\bfzero \in H$,
            \item[b.] for each $\bfu, \bfv \in H$, $\bfu + \bfv \in H$,
            \item[c.] each $\bfu \in H$ and each scalar $c$, $c \bfu \in H$.
        \end{enumerate}
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \begin{center}
                \begin{tikzpicture}[
                    scale=16,
                    x  = {(-0.5cm,-0.4cm)},
                    y  = {(0.8cm,-0.15cm)},
                    z  = {(0cm,0.5cm)}]
                    % Define style for the axes and cubes
                    \tikzstyle{axis} = [->,black,thick];

                    % Draw the axes
                    \draw[axis] (0,0,0) -- (0.3,0,0) node[anchor=north east]{$x_1$};
                    \draw[axis] (0,0,0) -- (0,0.3,0) node[anchor=north west]{$x_2$};

                    % Original vectors
                    \coordinate (v1) at (0.2, 0.3, 0.1);
                    \coordinate (v2) at (0.3, 0.1, 0.1);

                    % Linear combinations
                    \coordinate (v1_plus_v2) at (0.5, 0.4, 0.2);
                    \coordinate (v1_minus_v2) at (-0.1, 0.2, 0);
                    \coordinate (minus_v1_plus_v2) at (0.1, -0.2, 0);
                    \coordinate (minus_v1_minus_v2) at (-0.5, -0.4, -0.2);

                    \draw[white,fill=MintCream, opacity=0.9] 
                        (v1_plus_v2) -- 
                        (minus_v1_plus_v2) -- 
                        (minus_v1_minus_v2) -- 
                        (v1_minus_v2) -- 
                        cycle;

                    \draw[axis] (0,0,0) -- (0,0,0.3) node[anchor=south]{$x_3$};

                    \node[vector point] at (0,0,0) {};

                    \node (h) at (-0.15,0.15,0.05) {$H$};
                    \coordinate (h1) at ($0.5*(v1)-0.7*(v2)$);
                    \draw[thin,dotted] (h) -- (h1);
                    \drawvectorIII{v1}{below right:$\bfv_1$};
                    \drawvectorIII{v2}{below left:$\bfv_2$};
                \end{tikzpicture}
            \end{center}
            \caption{The span of $\bfv_{1}, \bfv_{2}$ is a plane through the origin}%
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 1: The span of vectors}

    If $\bfv_1, \dots, \bfv_p$ are in a vector space $V$, then $\Span{\bfv_1, \dots,
    \bfv_p}$ is a subspace of $V$.

    We call $\Span{\bfv_1, \dots, \bfv_p}$ the subspace \alert{spanned/generated} by
    $\bfv_1, \dots, \bfv_p$.

    \only<1>{%
        The proof is essentially the same as Example 11.
    }%

    \only<2>{%
        \begin{block}{Example 12}
            \cake{} Can you show the following $H$ is a subspace of $\dsR^{4}$?
            \begin{equation*}
                H
                =
                \left\{
                    \begin{bmatrix}
                        a - 3b \\ b - a \\ a \\ b
                    \end{bmatrix}
                    :
                    a, b \in \dsR
                \right\}
            \end{equation*}
        \end{block}
    }%
\end{frame}


\begin{frame}
    \frametitle{\tps{}}
    Which of the following two sets of vectors are vector spaces?
    \begin{equation*}
        S
        =
        \left\{
            \begin{bmatrix}
                x \\ y
            \end{bmatrix}
            :
            x \ge 0, y \ge 0
        \right\}
    \end{equation*}
    and
    \begin{equation*}
        H
        =
        \left\{
            \begin{bmatrix}
                2 t \\ 0 \\ -t + 1
            \end{bmatrix}
            :
            t \in \dsR
        \right\}
    \end{equation*}
\end{frame}

\end{document}
