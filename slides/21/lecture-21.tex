\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Eigenvectors and Eigenvalues}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{5.1}{1, 3, 13, 19, 31, 33, 35, 37, 39, 41, 43}
        \end{column}
    \end{columns}
\end{frame}

\section{5 Eigenvectors and Eigenvalues}

\begin{frame}
    \frametitle{The Life of \emoji{owl}}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{owl-life-stages.jpg}
        \caption{%
            An \emoji{owl}'s life has 3 stages 
            --- 
            juvenile \emoji{baby}, subadult \emoji{child} and adult \emoji{person}.
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Fate of \emoji{owl}}

    Let $j_k, s_{k}, a_{k}$ denote the number of \emoji{owl} in a forest of each
    of these three stages in the year $k$.

    \only<2>{%
        The data suggests
        \begin{equation*}
            \begin{aligned}
                j_{k+1} & = 0.33 a_k, \\
                s_{k+1} & = 0.18 j_k, \\
                a_{k+1} & = 0.71 s_k + 0.94 a_k
            \end{aligned}
        \end{equation*}
    }%
    \only<3->{%
        In other words,
        \begin{equation}
            \label{eq:difference}
            \begin{bmatrix}
                j_{k+1} \\
                s_{k+1} \\
                a_{k+1} \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                0 & 0 & .33 \\
                .18 & 0 & 0 \\
                0 & .71 & .94 \\
            \end{bmatrix}
            \begin{bmatrix}
                j_{k} \\
                s_{k} \\
                a_{k} \\
            \end{bmatrix}
            \qquad
            \text{for all }
            k \in \{0, 1, \dots\}
        \end{equation}
    }%
    \only<4>{%
        \think{} Will the \emoji{owl} become extinct?

        \alert{Eigenvectors} and \alert{eigenvalues} of this matrix will tell us the
        answer.
    }%
\end{frame}

\section{5.1 Eigenvectors and Eigenvalues}

\begin{frame}
    \frametitle{Example 1}

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            3 & -2 \\
            1 & 0
        \end{bmatrix}
        ,
        \quad
        \bfu =
        \begin{bmatrix}
            -1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            2 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    \only<1>{%
        \cake{} How does the linear transformation $\bfx:\mapsto A \bfx$ transform the area?
    }%
    \only<2>{%
        \cake{} Can you draw $A \bfv$ and $A \bfu$?
    }%
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                show background rectangle,
                background rectangle/.style={fill=white},
                scale=0.8,
                every node/.style={black},
                ]
                % Draw x and y axes
                \draw[gray!40] (-5.5,0) grid (5.5,3);
                \coordinate (e1) at (0,1);
                \coordinate (e2) at (1,0);
                \coordinate (u) at (-1,1);
                \coordinate (v) at (2,1);
                \coordinate (Au) at (-5,1);
                \coordinate (Av) at (4,2);
                \draw (0,0) -- (5,0) node[right]{$x_1$};
                \draw (0,0) -- (0,3) node[above]{$x_2$};
                \only<1>{%
                    \fill[SeaGreen, opacity=0.4] (e1) -- (e2) -- (1,1) -- cycle;
                    \drawvector[SeaGreen]{e1}{$e_1$};
                    \drawvector[SeaGreen]{e2}{$e_2$};
                }%
                \only<3->{%
                    \drawvector{Au}{$A \bfu$};
                    \drawvector{Av}{$A \bfv$};
                }%
                \only<2->{%
                    \drawvector{u}{$\bfu$};
                    \drawvector{v}{$\bfv$};
                }%
            \end{tikzpicture}
        \end{center}
    \end{figure}
    \only<3>{%
        \cake{} What is special about $\bfv$ for the transformation $\bfx \mapsto A \bfx$?
    }%
    \only<4>{%
        \cake{} Can you guess where is $A \left(\frac{1}{2} \bfv\right)$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Eigenvectors}

    An \alert{eigenvector} of an $n \times n$ matrix $A$ 
    is a \emph{nonzero} vector $\bfx$ such that
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    for some scalar (real number) $\lambda$.

    \only<2>{%
        \cake{} Is $\bfzero$ an eigenvector of $A$?

        \vspace{4em}

        \cake{} If $\bfx$ is an eigenvector of $A$, 
        is $2 \bfx$ also an eigenvector of $A$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Eigenvalues}
    A scalar $\lambda$ is an \alert{eigenvalue} of $A$ if
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    has a non-trivial solution.

    Such an $\bfx$ is an \emph{eigenvector} corresponding to $\lambda$.

    \only<2>{%
        \cake{} Does $\lambda$ being an eigenvalue of $A$
        imply that $2 \lambda$ is an eigenvalue of $A$?
    }%

    \only<3>{%
        \cake{} Is it possible that both $\lambda$ and $2 \lambda$ are eigenvalues of $A$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    \only<1>{%
        Let
        \begin{equation*}
            A =
            \left[
                \begin{array}{cc}
                    1 & 6 \\
                    5 & 2 \\
                \end{array}
            \right]
            ,
            \qquad
            \bfu =
            \begin{bmatrix}
                6 \\ -5
            \end{bmatrix}
            ,
            \qquad
            \bfv =
            \begin{bmatrix}
                3 \\ -2
            \end{bmatrix}
        \end{equation*}

        \cake{}
        Are $\bfu$ and $\bfv$ eigenvectors of $A$?
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \begin{center}
                \begin{tikzpicture}[
                    show background rectangle,
                    background rectangle/.style={fill=white},
                    scale=0.24,
                    every node/.style={black},
                    ]
                    % Draw x and y axes
                    \draw[gray!40] (-25.5,-5.5) grid (6.5,20.5);
                    \coordinate (u) at (6,-5);
                    \coordinate (v) at (3,-2);
                    \coordinate (Au) at (-24,20);
                    \coordinate (Av) at (-9,11);
                    \draw (-25.5,0) -- (6.5,0) node[right]{$x_1$};
                    \draw (0,-5.5) -- (0,20.5) node[above]{$x_2$};
                    \only<2->{%
                        \drawvector[thick]{Au}{$A \bfu$};
                        \drawvector[thick]{Av}{$A \bfv$};
                    }%
                    \drawvector[thick]{u}{$\bfu$};
                    \drawvector[thick]{v}{$\bfv$};
                \end{tikzpicture}
            \end{center}
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{cc}
                1 & 6 \\
                5 & 2 \\
            \end{array}
        \right]
    \end{equation*}
    \think{}
    Is $7$ an eigenvalue?

    \only<2>{%
        This is the same as asking if
        \begin{equation*}
            A \bfx =  7 \bfx
        \end{equation*}
        has a non-trivial solution.
    }%
    \only<3>{%
        In other words, we are asking if
        \begin{equation*}
            (A - 7 I) \bfx =  \bfzero
        \end{equation*}
        has a non-trivial solution?
    }%
\end{frame}

\begin{frame}
    \frametitle{Eigenspace}

    As Example 3 shows, 
    $\lambda$ is an eigenvalue of $A$ if and only if
    \begin{equation}
        \label{eq:eigen}
        (A - \lambda I) \bfx = \bfzero
    \end{equation}
    has a non-trivial (non-zero) solution.

    \pause{}

    The solution set $H$ of \eqref{eq:eigen} is the \blankshort{} space of the
    matrix $A -\lambda I$.

    We call $H$ the \alert{eigenspace} of $A$ corresponding to $\lambda$.

    \pause{}

    It contains the \blankshort{} vector (\bomb{}) and \emph{all} eigenvectors of $A$
    corresponding to $\lambda$.
\end{frame}

\begin{frame}
    \frametitle{Example of Eigenspaces}

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                show background rectangle,
                background rectangle/.style={fill=white},
                scale=0.35,
                every node/.style={black},
                ]
                % Draw x and y axes
                %\draw[gray!40] (-8.5,-6.5) grid (9.5,9.5);
                \coordinate (u) at (1,1);
                \coordinate (v) at (1.5,-1.25);
                \coordinate (Au) at (7,7);
                \coordinate (Av) at (6,-5);
                \draw (-8.5,0) -- (9.5,0) node[right]{$x_1$};
                \draw (0,-6.5) -- (0,9.5) node[above]{$x_2$};
                \drawvector{Au}{$A \bfu = 7 \bfu$};
                \drawvector{Av}{below:$A \bfv = -4 \bfv$};
                \drawvector{u}{above left:$\bfu = \begin{bmatrix} 1 \\ 1 \end{bmatrix} $};
                \drawvector{v}{below:$\bfv = \begin{bmatrix} \frac{3}{2} \\ -\frac{5}{4} \end{bmatrix} $};
            \end{tikzpicture}
        \end{center}
        \caption{\cake{} What are the eigenspaces for $\lambda=7$ and $\lambda=-4$?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \only<1-3>{%
        The number $2$ is an eigenvalue of
        \begin{equation*}
            A =
            \begin{bmatrix}
                4 & -1 & 6 \\
                2 &  1 & 6 \\
                2 & -1 & 8 \\
            \end{bmatrix}
            .
        \end{equation*}
    }%
    \only<1>{%
        \cake{} How can we verify this?
    }%
    \only<2>{%
        \think{} How to find a basis of its eigenspace?

        We can reduce the augmented matrix of $(A-2I) \bfx = \bfzero$ to echelon form
        \[
            \begin{bmatrix}
                2 & -1 & 6 & 0 \\
                2 & -1 & 6 & 0 \\
                2 & -1 & 6 & 0 
            \end{bmatrix}
            \sim
            \begin{bmatrix}
                2 & -1 & 6 & 0 \\
                0 & 0 & 0 & 0 \\
                0 & 0 & 0 & 0 
            \end{bmatrix}
        \]
        \cake{} What is the dimension of this eigenspace?
    }%
    \only<3>{%
        This is equivalent to
        \begin{equation*}
            2 x_1 - x_2 + 6 x_3 = 0
        \end{equation*}
        \cake{} What is the solution set?
    }%
    \only<4>{%
        Thus, the eigenspace contains vectors of the form
        \[
            \begin{bmatrix}
                x_1 \\
                x_2 \\
                x_3
            \end{bmatrix}
            = x_2
            \begin{bmatrix}
                \frac{1}{2} \\
                1 \\
                0
            \end{bmatrix}
            + x_3
            \begin{bmatrix}
                -3 \\
                0 \\
                1
            \end{bmatrix},
            \quad x_2 \text{ and } x_3 \text{ free}
        \]
        \cake{} Can you find a basis for this eigenspace?
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Consider the following matrix $A$:
    $$
    A = \begin{bmatrix}
        1 & 1 \\
        1 & 1 \\
    \end{bmatrix}
    $$

    The eigenvalues of $A$ are the $\lambda$'s which makes
    $$
    A - \lambda I
    $$
    singular.

    \cake{} What are the eigenvalues of $A$?
\end{frame}

\begin{frame}
    \frametitle{When $0$ is an eigenvalue}

    The umber $0$ is an eigenvalue of $A$ if and only if
    \begin{equation*}
        A \bfx = 0 \bfx = \bfzero
    \end{equation*}
    has a nontrivial solution.

    In other words, this happens if and only if $A$ is \blankshort{}.

    \pause{}

    \begin{block}{The Invertible Matrix Theorem continued (5.2)}
        Let $A$ be an $n \times n$ matrix.
        Then $A$ is invertible if and only if:
        \begin{enumerate}
            \item[r.] The number $0$ is not an eigenvalue of $A$.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 1 --- Eigenvalues of Triangular Matrices}

    The eigenvalues of a triangular matrix are on its main diagonal.

    \only<1>{%
        \hint{}
        Recall that an \alert{upper triangular matrix} looks like
        \begin{equation*}
            \begin{bmatrix} 
                \ast & \ast & \ast & \cdots & \ast \\
                0 & \ast & \ast & \cdots & \ast \\
                0 & 0 & \ast & \ddots & \vdots \\
                \vdots & \vdots & \ddots & \ddots & \ast \\
                0 & 0 & \cdots & 0 & \ast
            \end{bmatrix} 
        \end{equation*}
        and a \alert{lower triangular matrix} looks like
        \begin{equation*}
            \begin{bmatrix} 
                \ast & 0 & \cdots & 0 & 0 \\
                \ast & \ast & \ddots & \vdots & \vdots \\
                \vdots & \ddots & \ddots & 0 & \vdots \\
                \vdots & \cdots & \ast & \ast & 0 \\
                \ast & \cdots & \ast & \ast & \ast
            \end{bmatrix} 
        \end{equation*}
    }%

    \only<2>{%
        Proof for $3 \times 3$ matrix:
        If $A$ is upper triangular, then
        \begin{equation*}
            \begin{aligned}
                A - \lambda I 
                &
                = 
                \begin{bmatrix}
                    a_{11} & a_{12} & a_{13} \\
                    0 & a_{22} & a_{23} \\
                    0 & 0 & a_{33}
                \end{bmatrix}
                -
                \begin{bmatrix}
                    \lambda & 0 & 0 \\
                    0 & \lambda & 0 \\
                    0 & 0 & \lambda
                \end{bmatrix}
                \\
                &
                =
                \begin{bmatrix}
                    a_{11} - \lambda & a_{12} & a_{13} \\
                    0 & a_{22} - \lambda & a_{23} \\
                    0 & 0 & a_{33} - \lambda
                \end{bmatrix}
            \end{aligned}
        \end{equation*}
        \cake{} When is $A - \lambda I$ singular, i.e., $\lambda$ is an eigenvalue of $A$?
    }%
\end{frame}

\begin{frame}
    \frametitle{A Little Bit of Logic}
    To prove that
    \begin{itemize}
        \item If it \emoji{cloud-with-rain}, then I will skip class.
    \end{itemize}
    we can instead prove:
    \begin{itemize}
        \item If I am in the class, then it is not \blankshort{}.
    \end{itemize}

    \pause{}

    \begin{block}{Contrapositive}
        The \alert{contrapositive} of a statement $P \imp Q$ is $\neg Q \imp \neg P$.

        \astonished{}
        These two statements are equivalent.
    \end{block}

    \pause{}

    \cake{}
    What is the \emph{contrapositive} of
    \begin{itemize}
        \item If \panda{} can take derivatives, then \cat{} knows linear algebra.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 2 --- Linear Independence}

    If $\bfv_1, \dots, \bfv_r$ are eigenvectors corresponding to
    \emph{distinct} eigenvalues $\lambda_1,\dots, \lambda_r$,
    then they are linearly independent.

    \only<1>{%
        \begin{figure}[htpb]
            \begin{center}
                \begin{tikzpicture}[
                    show background rectangle,
                    background rectangle/.style={fill=white},
                    scale=0.25,
                    every node/.style={black},
                    ]
                    % Draw x and y axes
                    %\draw[gray!40] (-8.5,-6.5) grid (9.5,9.5);
                    \coordinate (u) at (1,1);
                    \coordinate (v) at (1.5,-1.25);
                    \coordinate (Au) at (7,7);
                    \coordinate (Av) at (6,-5);
                    \draw (-8.5,0) -- (9.5,0) node[right]{$x_1$};
                    \draw (0,-6.5) -- (0,9.5) node[above]{$x_2$};
                    \drawvector{Au}{$A \bfu = 7 \bfu$};
                    \drawvector{Av}{below:$A \bfv = -4 \bfv$};
                    \drawvector{u}{above left:$\bfu = \begin{bmatrix} 1 \\ 1 \end{bmatrix} $};
                    \drawvector{v}{below:$\bfv = \begin{bmatrix} \frac{3}{2} \\ -\frac{5}{4} \end{bmatrix} $};
                \end{tikzpicture}
            \end{center}
            \caption{\cake{} What are the eigenspaces for $\lambda=7$ and $\lambda=-4$?}
        \end{figure}
    }%
    \only<2>{%
        Proof by contradiction: 
        Assume that $\{\bfv_{1}, \dots, \bfv_{r}\}$ are linearly dependent.

        \cake{}
        Why can we apply the following?

        \begin{block}{Theorem 7 (1.7)}
            If $S$ is \emph{linearly dependent} and $\bfv_1 \ne \bfzero$,
            then $\bfv_j$ for some $j > 1$ is a linear combination of
            the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
        \end{block}
    }%
\end{frame}

\subsection{Difference Equations and Eigenvalues}

\begin{frame}
    \frametitle{Difference Equations and Eigenvalues}
    
    Consider the following \alert{first-order difference equation}
    \begin{equation}
        \label{eq:difference:2}
        \bfx_{x+1} = A \bfx_{k}, \qquad (k = 0, 1, \dots).
    \end{equation}

    Let $\lambda$ be an eigenvalue of $A$.
    Let $\bfx_{0}$ be an eigenvector corresponding to $\lambda$ and
    \begin{equation*}
        \bfx_{k} = \lambda^{k} \bfx_{0}, \qquad \text{for all } k \ge 1.
    \end{equation*}
    \cake{} Is this a solution to \eqref{eq:difference:2}?

    \pause{}

    \hint{} Any linear combinations of such solutions are also solutions.
\end{frame}

\begin{frame}
    \frametitle{Will \emoji{owl} Become Extinct?}
    
    Recall that in the \emoji{owl} population model
    \begin{equation}
        \label{eq:difference:1}
        \begin{bmatrix}
            j_{k+1} \\
            s_{k+1} \\
            a_{k+1} \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 & 0 & .33 \\
            .18 & 0 & 0 \\
            0 & .71 & .94 \\
        \end{bmatrix}
        \begin{bmatrix}
            j_{k} \\
            s_{k} \\
            a_{k} \\
        \end{bmatrix}
    \end{equation}
    One of the eigenvectors of the matrix in \eqref{eq:difference:1} is
    \begin{equation*}
        \bfx_{0}
        =
        \begin{bmatrix}
            j_{0} \\
            s_{0} \\
            a_{0} \\
        \end{bmatrix}
        =
        \left[
            \begin{array}{c}
                0.31754\\
                0.05811\\
                0.94646\\
            \end{array}
        \right]
    \end{equation*}
    corresponding to the eigenvalues $\lambda = 0.98359$

    \emoji{cake} What would happen if the initial population is $1000 \bfx_{0}$?
    What about $10^{10} \bfx_{0}$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    If $A^2$ is the zero matrix, what are the eigenvalues of $A$?

    \hint{} Recall that $\lambda$ is an eigenvalue of $A$ if
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    for some $\bfx \ne \bfzero$.
\end{frame}

\begin{frame}[label=current]
    \frametitle{\zany{} Can \ac{ai} do this?}
    Let
    \begin{equation*}
        A = \begin{bmatrix} 3 & -1 & 3 \\ -1 & 3 & 3 \\ 6 & 6 & 2 \end{bmatrix}
    \end{equation*}
    and \(\lambda = -4\).

    \puzzle{}
    Can you ask ChatGPT to find a basis of the eigenspace of $\lambda$?
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Learn from YouTube}

    \begin{figure}[htpb]
        \centering
        \href{https://youtu.be/PFDu9oVAE-g}{\includegraphics[width=\textwidth]{YouTube-eigenvectors.jpg}}
    \end{figure}

    \emoji{eyes} Check this video at: \url{https://youtu.be/PFDu9oVAE-g}
\end{frame}

\end{document}
