\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Determinants}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Assignments\footnote{\homeworknote{}}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework[\footnotesize]{3.1}{All odd number questions from 1 to 45.}
            \sectionhomework[\footnotesize]{3.2}{All odd number questions from 1 to 51.}
        \end{column}
    \end{columns}
\end{frame}

\section{3 Determinants}

\begin{frame}
    \frametitle{Weighing Diamonds with a Two-Pan Balance}
    
    How can we determine the weights of three \emoji{gem}, 
    using a \emoji{balance-scale} \emph{as accurately as possible}?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{balance_scales.jpg}
        \caption{Two-Pan Balance}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Design Matrices}

    We can use a design matrix $D$ such as
    \begin{equation*}
        D
        =
        \begin{bmatrix}
            1 & 1 & -1 \\
            1 & -1 & 1 \\
            -1 & 1 & 1
        \end{bmatrix}
    \end{equation*}
    in which $(D)_{ij} = 1/-1$ if $\temoji{gem}_j$ goes the left/right pan in the 
    $i$-th weighing.

    \cake{} If $x_1, x_2, x_3$ are the weights of the three \emoji{gem},
    what do $b_1, b_2, b_3$ represent in
    \begin{equation*}
        D \begin{bmatrix}x_1 \\ x_2 \\ x_3\end{bmatrix}
        = 
        \begin{bmatrix}b_1 \\ b_2 \\ b_3\end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Maximize the determinant}
    Then we can compute $x_{1}, x_{2}, x_{3}$ by solving
    \begin{equation*}
        D \begin{bmatrix}x_1 \\ x_2 \\ x_3\end{bmatrix}
        = 
        \begin{bmatrix}b_1 \\ b_2 \\ b_3\end{bmatrix}
    \end{equation*}
    \emoji{disappointed} The measurements will \emph{always} have some random errors.

    \emoji{smile} But we can improve accuracy by choosing a $D$ such that
    the \alert{determinant} of $D D^{T}$, is maximized.

    This is called a \alert{D-Optimal designs}.\footnote{%
        Why this works is a statistic question.
        \emoji{eyes} See
        \href{https://www.itl.nist.gov/div898/handbook/pri/section5/pri521.htm}{here}
        for details.
    }%
\end{frame}

\section{3.1 Introduction to Determinants}

\begin{frame}
    \frametitle{\zany{} Singular}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{singular} (adjective)  --- strange or eccentric in some respect. 

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

            \cake{} What does it mean to say an $n \times n$ matrix $A$ is \emph{singular}?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-singular.jpg}
                \caption{%
                    A singular \bunny{}
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\zany{} Determinant}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{determinant} (noun)  ---
            a factor which decisively affects the nature or outcome of something.

            \begin{flushright}
                --- Oxford Languages
            \end{flushright}

            \cake{} What is the \emph{determinant} of
            \begin{equation*}
                A =
                \begin{bmatrix} 
                    a & b \\
                    c & d
                \end{bmatrix} 
            \end{equation*}

            \cake{} When does $\det A = 0$?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-determinant.jpg}
                \caption{%
                    The path chosen is the determinant of your fate
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Determinants of $2 \times 2$ Matrices}
    Recall that for
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            a_{11} & a_{12} \\
            a_{21} & a_{22} \\
        \end{bmatrix}
    \end{equation*}
    we define its \emph{determinant} to be
    \begin{equation*}
        \det(A) = a_{11} a_{22} - a_{12} a_{21}
    \end{equation*}

    \only<2>{%
        A trick to remember the definition:
        \begin{equation*}
            \begin{tikzpicture}[ampersand replacement=\&]
                \matrix (m) [matrix of math nodes,left delimiter={[},right delimiter={]}] {
                    a_{11} \& a_{12} \\
                    a_{21} \& a_{22} \\
                };
                \tikzset{
                    every path/.style={thick, ->, opacity=0.8},
                };
                \draw[BrightRed] (m-1-1.center) -- (m-2-2.center);
                \draw[DaylightBlue] (m-2-1.center) -- (m-1-2.center);
            \end{tikzpicture}
        \end{equation*}
    }%

    \only<3>{%
        And by Theorem 4 (2.2),
        \begin{equation*}
            \text{$A$ is \alert{singular}}
            \Leftrightarrow
            \det(A) = 0
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Determinants of $3 \times 3$ Matrices}
    For a $3 \times 3$ matrices
    $
    A
    =
    \begin{bmatrix}
        a_{11} & a_{12} & a_{13} \\
        a_{21} & a_{22} & a_{23} \\
        a_{31} & a_{32} & a_{33} \\
    \end{bmatrix}
    $,
    we define
    \begin{equation*}
        \begin{aligned}
            \det(A)
            =
        &
        a_{11} a_{22} a_{33} + a_{12} a_{23} a_{31} + a_{13}a_{21}a_{32}
        \\
        &
        -a_{11}a_{23}a_{32}
        -a_{12}a_{21}a_{33}
        -a_{13}a_{22}a_{31}
        \end{aligned}
    \end{equation*}

    \only<2>{%
        A trick to remember the definition:
        \begin{equation*}
            \begin{tikzpicture}[ampersand replacement=\&]
                \matrix (m) [matrix of math nodes,left delimiter={[},right delimiter={]}] {
                    a_{11} \& a_{12} \& a_{13} \& a_{11} \& a_{12} \\
                    a_{21} \& a_{22} \& a_{23} \& a_{21} \& a_{22} \\
                    a_{31} \& a_{32} \& a_{33} \& a_{31} \& a_{32} \\
                };
                \tikzset{
                    every path/.style={thick, ->, opacity=0.8},
                };
                \draw[BrightRed] (m-1-1.center) -- (m-3-3.center);
                \draw[BrightRed] (m-1-2.center) -- (m-3-4.center);
                \draw[BrightRed] (m-1-3.center) -- (m-3-5.center);
                \draw[DaylightBlue] (m-3-1.center) -- (m-1-3.center);
                \draw[DaylightBlue] (m-3-2.center) -- (m-1-4.center);
                \draw[DaylightBlue] (m-3-3.center) -- (m-1-5.center);
            \end{tikzpicture}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Determinants of Singular Matrices}

    When $a_{11} \ne 0$, we have
    \begin{equation*}
        A
        \sim
        \begin{bmatrix}
            a_{11} & a_{12} & a_{13} \\
            0 & a_{11} a_{22}-a_{12}a_{21} & a_{11} a_{23} - a_{13} a_{21} \\
            0 & 0 & a_{11} \det(A) \\
        \end{bmatrix}
    \end{equation*}

    \cake{} Why does $\det(A) = 0$ implies that $A$ is \emph{singular} ?
\end{frame}

\begin{frame}
    \frametitle{Determinants of $n \times n$ Matrices}

    Let $A = [a_{ij}]$ be a $n \times n$ matrix.
    We define the \alert{determinant} of $A$ by
    \begin{equation*}
        \det(A)
        =
        \begin{cases}
            a_{11} & \text{if } n = 1, \\
            \sum_{j=1}^{n} (-1)^{1+j} a_{1j} \det A_{1j} & \text{if } n \ge 2.
        \end{cases}
    \end{equation*}
    where $A_{ij}$ is $A$ with the $i$-th row and $j$-th column removed.

    \pause{}

    \think{} Can you highlight \alt<2>{$A_{2j}$}{$A_{1j}$} in the following matrix?
    \begin{equation*}
        \begin{bmatrix}
            a_{11}       & \cdots & a_{1(j-1)} & \only<2>{\cellcolor{DaylightBlue}} a_{1j} & a_{1(j+1)} & \cdots & a_{1n}       \\
            a_{21}       & \cdots & a_{2(j-1)} & \only<3>{\cellcolor{DaylightBlue}} a_{2j}       & a_{2(j+1)} & \cdots & a_{2n}       \\
            \vdots       & \ddots & \vdots     & \vdots       & \vdots     & \ddots & \vdots       \\
            a_{m1}       & \cdots & a_{m(j-1)} & a_{mj}       & a_{m(j+1)} & \cdots & a_{mn}
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Determinants of $1 \times 1$ matrices}

    For $n=1$,
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            a_{11}
        \end{bmatrix}
        .
    \end{equation*}
    Thus
    \begin{equation*}
        \det(A)
        =
        a_{11}
    \end{equation*}
    \cake{} When is $A$ \emph{singular} in this case?
\end{frame}

\begin{frame}
    \frametitle{Determinants of $2 \times 2$ matrices}

    For $n=2$,
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            a_{11} & a_{12} \\
            a_{21} & a_{22} \\
        \end{bmatrix}
        .
    \end{equation*}
    Thus
    \begin{flalign*}
        \det(A) =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Determinants of $3 \times 3$ matrices}

    For $n=3$,
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            a_{11} & a_{12} & a_{13} \\
            a_{21} & a_{22} & a_{23} \\
            a_{31} & a_{32} & a_{33} \\
        \end{bmatrix}
        ,
    \end{equation*}
    and
    \begin{flalign*}
        \det(A) =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    \think{} What is the determinant of
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & 5 & 0 \\
            2 & 4 & -1 \\
            0 & -2 & 0 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Cofactors}

    The $(i,j)$-\alert{cofactor} of $A$ is defined by
    \begin{equation*}
        C_{ij} = (-1)^{i+j} \det A_{ij}.
    \end{equation*}

    The patterns of the signs of the cofactors are shown in the following matrix:
    \[
        \begin{bmatrix}
            \textcolor{BrightRed}{+} & \textcolor{DaylightBlue}{-} & \textcolor{BrightRed}{+} & \dots \\
            \textcolor{DaylightBlue}{-} & \textcolor{BrightRed}{+} & \textcolor{DaylightBlue}{-} & \\
            \textcolor{BrightRed}{+} & \textcolor{DaylightBlue}{-} & \textcolor{BrightRed}{+} &  \\
            \vdots &  &  & \ddots \\
        \end{bmatrix}
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 1 Revisited}
    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            a_{11} & a_{12} & a_{13} \\
            a_{21} & a_{22} & a_{23} \\
            a_{31} & a_{32} & a_{33} \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            1 & 5 & 0 \\
            2 & 4 & -1 \\
            0 & -2 & 0 \\
        \end{bmatrix}
    \end{equation*}
    \only<1>{%
        Then
        \begin{flalign*}
            a_{31} C_{31} + a_{32} C_{32} + a_{33} C_{33}
            =
        &&
        \end{flalign*}

        \vspace{5em}
        is called a \alert{cofactor expansion} along row $3$. 
    }%
    \only<2>{%
        \cake{} What about
        \begin{flalign*}
            a_{12} C_{12} + a_{22} C_{22} + a_{32} C_{32}
            =
        &&
        \end{flalign*}

        \vspace{5em}
        is called a \alert{cofactor expansion} along row $3$. 
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 1 --- Cofactor Expansion}

    For any $1 \le i \le n$ and $1 \le j \le n$
    \begin{equation*}
        \begin{aligned}
            \det A
            & =
            a_{i1} C_{i1} + a_{i2} C_{i2} + \cdots a_{in} C_{in} \\
            & =
            a_{1j} C_{1j} + a_{2j} C_{2j} + \cdots a_{nj} C_{nj} \\
        \end{aligned}
    \end{equation*}

    \hint{} To compute $\det A$, we can choose any row or any column and do a
    cofactor expansion.

    \zany{} For a proof see
    \href{https://math.libretexts.org/@go/page/70203}{here}.
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    \cake{} Along which row/column should we expand?
    \begin{equation*}
        \left[
            \begin{array}{ccccc}
                3 & -7 & 8 & 9 & -6 \\
                0 & 2 & -5 & 7 & 3 \\
                0 & 0 & 1 & 5 & 0 \\
                0 & 0 & 0 & 4 & -1 \\
                0 & 0 & 0 & 0 & -2 \\
            \end{array}
        \right]
    \end{equation*}

    \pause{}
    \begin{block}{Theorem 2 --- Triangular matrices}

        If a $n \times n$ matrix $A$ is triangular, 
        then $\det A$ is the product of the entries on the main diagonal of $A$.
    \end{block}
\end{frame}


\section{3.2 Properties of Determinants}

\begin{frame}
    \frametitle{Let's Play a Game}

    Please give me a random $2 \times 2$ matrix (containing small numbers)
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccc}
                \blankveryshort{} & \blankveryshort{}  \\
                \blankveryshort{} & \blankveryshort{} \\
            \end{array}
        \right]
    \end{equation*}
    \cake{} What is $\det A$?

    \pause{}

    Now please
    \begin{enumerate}
        \item do a row replacement of your choice;
        \item interchange two rows;
        \item scale a row of your choice by $2$.
    \end{enumerate}
    \cake{} What is the determinant of the result?
\end{frame}

\begin{frame}
    \frametitle{Theorem 3 --- Row operations}

    Let $A$ be a square matrix.
    \begin{enumerate}[<+->]
        \item Replacement --- If row replacement in $A$ produces $B$, 
            then $\det B = \det A$.
        \item Interchange --- If interchanging two rows of $A$ produces $B$,
            then $\det B = -\det A$.
        \item Scale --- If scaling one row of $A$ by $k$ to produce $B$,
            then $\det B = k \det A$.
    \end{enumerate}

    \pauseafteritem{}

    \zany{} For a proof, see \href{https://math.libretexts.org/@go/page/14511}{here}.
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    What is the determinant of
    \begin{equation*}
        \begin{bmatrix}
             3 & -1 & 2  & -5 \\
             0 &  5 & -3 & -6 \\
            -6 &  7 & -7 &  4 \\
            -5 & -8 & 0  &  9
        \end{bmatrix}
    \end{equation*}

    \cake{} Is A invertible?
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Compute $\det(A)$, where 
    \begin{equation*}
        A = \begin{bmatrix}
            0 & 1 & 2 & -1 \\
            2 & 5 & -7 & 3 \\
            0 & 3 & 6 & 2 \\
            -2 & -5 & 4 & -2
        \end{bmatrix}
    \end{equation*}

    \cake{} Is A invertible?
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- Invertible matrices}

    An $n \times n$ matrix $A$ is invertible if and only if
    $\det(A) \ne 0$.

    \pause{}

    \bigskip{}

    \begin{columns}[totalwidth=\textwidth, t]
        \begin{column}{0.7\textwidth}
            Proof ---
            Suppose we reduced $A$ to an echelon form $U$ using \emph{only}
            \begin{itemize}
                \item row replacement
                    \item and row exchange.
            \end{itemize}
            Then
            \begin{equation*}
                \abs{\det(A)} = \abs{\det(U)}.
            \end{equation*}
            \hint{} To reduce to echelon form, we do not need to scale any row.
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{footnotesize}
                \pause{}
                \cake{} Is $\det(U) = 0$?
                \begin{equation*}
                    \begin{aligned}
                        U = 
                        \left[\begin{array}{cccc}
                                \bbs{} & \bast{} & \bast{} & \bast{} \\
                                0 & \bbs{} & \bast{} & \bast{} \\
                                0 & 0 & \bbs{} & \bast{} \\
                                0 & 0 & 0 & \bbs{}
                        \end{array}\right]
                    \end{aligned}
                \end{equation*}
                \pause{}
                \cake{} What about this?
                \begin{equation*}
                    \begin{aligned}
                        U = 
                        \left[\begin{array}{cccc}
                                \bbs{} & \bast{} & \bast{} & \bast{} \\
                                0 & \bbs{} & \bast{} & \bast{} \\
                                0 & 0 & 0 & \bast{} \\
                                0 & 0 & 0 & \bbs{}
                        \end{array}\right]
                    \end{aligned}
                \end{equation*}
            \end{footnotesize}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Column Operations}

\begin{frame}
    \frametitle{Theorem 5 --- Transpose}
    If $A$ is a $n \times n$ matrix, then $\det A^{T} = \det A$

    Proof by induction.

\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{cccc}
                -1 & 5 & -1 & 5 \\
                -2 & -2 & -4 & -1 \\
                -1 & -5 & -4 & -3 \\
                0 & 0 & 0 & 4 \\
            \end{array}
        \right]
        \qquad
        B
        =
        \left[
            \begin{array}{cccc}
                -1 & -2 & -1 & 5 \\
                5 & -2 & -5 & -1 \\
                -1 & -4 & -4 & -3 \\
                0 &  0 &  0 & 4 \\
            \end{array}
        \right]
        .
    \end{equation*}
    If $\det A = -64$, what is $\det B$?
\end{frame}

\subsection{Determinants and Matrix Products}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Let
%    \begin{equation*}
%        A = \begin{bmatrix}
%            \cos{\theta_1} & -\sin{\theta_1} \\
%            \sin{\theta_1} & \cos{\theta_1}
%        \end{bmatrix}
%        \quad \text{and} \quad
%        B = \begin{bmatrix}
%            \cos{\theta_2} & -\sin{\theta_2} \\
%            \sin{\theta_2} & \cos{\theta_2}
%        \end{bmatrix}
%    \end{equation*}
%
%    \cake{} What is $C = BA$?
%
%    \cake{} What are $\det A, \det B$ and $\det C$?
%\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 6 --- Multiplication}

    If $A$ and $B$ are $n \times n$ matrices, then $\det AB = \det A \det B$.

    \zany{} For a proof, see textbook or \href{https://math.libretexts.org/@go/page/14511}{here}.

    \pause{}

    \begin{block}{Example 6}
        Let
        \[
            A = \begin{bmatrix}
                6 & 1 \\
                3 & 2
            \end{bmatrix}
            \quad \text{and} \quad
            B = \begin{bmatrix}
                4 & 3 \\
                1 & 2
            \end{bmatrix}.
        \]
        Verify that $\det(AB) = \det(A) \det(B)$.
    \end{block}

    %\pause{}
    %\cake{} Let
    %\begin{equation*}
    %    A =
    %    \left[
    %        \begin{array}{cccc}
    %            -1 & -1 & 3 & -3 \\
    %            5 & 1 & -2 & 0 \\
    %            -1 & -4 & -1 & -1 \\
    %            -4 & -1 & 0 & -2 \\
    %        \end{array}
    %    \right]
    %    ,
    %    \quad
    %    B =
    %    \left[
    %        \begin{array}{cccc}
    %            0 & -1 & 1 & 1 \\
    %            5 & -2 & 4 & -2 \\
    %            4 & -5 & 0 & 1 \\
    %            4 & 5 & 2 & -5 \\
    %        \end{array}
    %    \right]
    %    .
    %\end{equation*}
    %If $\det A = 188$ and $\det B = -84$, what is $\det AB$?
\end{frame}

%\begin{frame}
%    \frametitle{\sweat{} Think-Pair-Share}
%
%    Fix a $n \times n$ matrix and a $j$ with $1 \le j \le n$.
%    \begin{equation*}
%        A = \begin{bmatrix} \bfa_1 & \bfa_2 & \dots & \bfa_n \end{bmatrix}.
%    \end{equation*}
%    The function
%    \begin{equation*}
%        T(\bfx) = \det
%        \begin{bmatrix}
%            \bfa_1 & \bfa_2 & \dots \bfa_{j-1} & \bfx & \bfa_{j+1} & \dots & \bfa_n
%        \end{bmatrix}
%    \end{equation*}
%    is a \emph{linear transformation}.
%
%    \think{} Can you prove this?
%
%    \hint{} How did we compute determinants?
%\end{frame}

\end{document}
