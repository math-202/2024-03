\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\usetikzlibrary{angles}

\title{Lecture \lecturenum{} --- Orthogonal Projections, The Gram-Schmidt Process}

\begin{document}

\maketitle{}

\lectureoutline{}

\begin{frame}[c]
    \frametitle{%
        Assignments%
        \footnote{\homeworknote{}}%
        \footnote{%
            \smiling{}
            Skip ``Angels in $\dsR^{2}$ and $\dsR^{3}$`` in 6.1.
        }%
    }%

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.3}{1, 3, 7, 11, 13, 17, 19, 31}
            \sectionhomework{6.4}{1, 5, 9, 13, 15, 23, 25}
        \end{column}
    \end{columns}
\end{frame}

\section{6.3 Orthogonal Projections}

\begin{frame}[c]
    \frametitle{Projections to a Subspace}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            If
            \begin{equation*}
                \bfy = \bfz + \hat{\bfy}
            \end{equation*}
            such that $\bfz \in W^{\perp}$ and $\hat{\bfy} \in W$,
            then
            \begin{itemize}
                \item 
                    $\hat{\bfy}$ is called the \alert{orthogonal projection} of $\bfy$ onto $W$,
                \item
                    $\bfz$ is called the \alert{component} of $\bfy$ orthogonal to $W$
            \end{itemize}
            We denote such a $\hat{\bfy}$ by $\proj_W \bfy$.
        \end{column}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{%
                \begin{tikzpicture}[
                    xscale=1.0,
                    yscale=1.4,
                    x  = {(-0.7cm,-0.7cm)},
                    y  = {(1.0cm,-0.2cm)},
                    z  = {(0cm,1.0cm)}]


                    % Adding the surface
                    \coordinate (v1) at ( 1.0,  3.5, 0.0);
                    \coordinate (v2) at (-1.0,  3.5, 0.0);
                    \coordinate (v3) at (-1.0, -0.8, 0.0);
                    \coordinate (v4) at ( 1.0, -0.8, 0.0);
                    \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) node[above right,
                        xshift=0.3cm] {$W$} -- cycle;

                    % Draw the vectors
                    \coordinate (o) at (0, 0, 0);
                    \coordinate (y) at (0, 1.5, 1.5);
                    \coordinate (yhat) at (0, 1.5, 0);
                    \coordinate (z) at (0, 0, 1.5);
                    \drawvectorIII{z}{above right:$\bfz = \bfy - \mathbf{\hat{y}}$};
                    \drawvectorIII{y}{above right:$\bfy$};
                    \drawvectorIII{yhat}{below right:$\mathbf{\hat{y}} = \proj_{W} \bfy$};
                    \node[below, vector point, label=below:$\bfzero$] at (o) {};
                    \draw[dashed, thin] (y) -- (yhat);
                    \draw[dashed, thin] (z) -- (y);
                    % Draw the right angle
                    \pic [draw,red,thick,angle radius=0.3cm] 
                    {right angle = o--yhat--y};
                \end{tikzpicture}
            }%
            \caption{The projection of $\bfy$ to $W$}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let $\{\bfu_{1}, \dots, \bfu_{5}\}$ be an \emph{orthogonal basis} for $\dsR^{5}$.
    Let
    \begin{equation*}
        \bfy =
        c_{1} \bfu_1
        +
        c_{2} \bfu_2
        +
        c_{3} \bfu_3
        +
        c_{4} \bfu_4
        +
        c_{5} \bfu_5
        .
    \end{equation*}
    Let $W = \Span{\bfu_1, \bfu_2}$.

    \cake{} What $\hat{\bfy} = \proj_{W} \bfy$ and $\bfz = \bfy - \hat{\bfy}$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- The Orthogonal Decomposition Theorem}

    Let \( W \) be a subspace of \( \mathbb{R}^n \).
    Each \( \bfy \) in \( \mathbb{R}^n \) can be written uniquely as
    \[
        \bfy = \hat{\bfy} + \bfz
    \]
    where \( \hat{\bfy} \in W\) and \( \bfz \in W^\perp \). 

    \pause{}

    In fact, if \( \{\bfu_1, \ldots, \bfu_p\} \) is an orthogonal basis of \( W \), then
    \[
        \hat{\bfy} = \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1 + \cdots +
        \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
        \quad
        \text{and} 
        \quad 
        \bfz = \bfy - \hat{\bfy}
    \]

    \only<1>{%
        \cake{} Why is $\hat{\bfy} \in W$?
    }%
    \only<2>{%
        Step 1: Show that $\bfz = \bfy - \hat{\bfy} \in W^\perp$.
    }%
    \only<3>{%
        Step 2: Show that if $\bfy = \hat{\bfy}_1  + \bfz_1$ with $\bfz_1 \in W^\perp$ and
        $\hat{\bfy}_1 \in W$, then
        \begin{equation*}
            \bfz_1 = \bfz
            \quad
            \text{and}
            \quad
            \hat{\bfy}_1 = \hat{\bfy}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Let $W = \Span{\bfu_1, \bfu_2}$ where
    \begin{equation*}
        \bfu_1
        =
        \left[
            \begin{array}{c}
                2 \\
                5 \\
                -1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu_2
        =
        \left[
            \begin{array}{c}
                -2 \\
                1 \\
                1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfy
        =
        \left[
            \begin{array}{c}
                1 \\
                2 \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    \cake{}
    What are $\hat{\bfy}$ and $\bfz$?
\end{frame}

\subsection{A Geometric Interpretation of the Orthogonal Projection}

\begin{frame}
    \frametitle{A Geometric Interpretation}

    The $\hat{\bfy} = \proj_W \bfy$ is the
    sum of its projections onto one-dimensional
    subspaces that are mutually orthogonal.

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{%
            \begin{tikzpicture}[
                scale=1.5,
                x  = {(-0.7cm,-0.7cm)},
                y  = {(1.0cm,-0.2cm)},
                z  = {(0cm,1.0cm)}]


                % Adding the surface
                \coordinate (v1) at ( 1.5,  3.5, 0.0);
                \coordinate (v2) at (-1.5,  3.5, 0.0);
                \coordinate (v3) at (-1.5, -1.0, 0.0);
                \coordinate (v4) at ( 1.5, -1.0, 0.0);
                \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) node[above right,
                    xshift=0.3cm] {$W$} -- cycle;

                % Draw the vectors
                \coordinate (o) at (0, 0, 0);
                \coordinate (y) at      (0.8, 1.8, 1.3);
                \coordinate (yhat) at   (0.8, 1.8, 0.0);
                \coordinate (yhat1) at  (0.8, 0.0, 0.0);
                \coordinate (yhat2) at  (0.0, 1.8, 0.0);
                \coordinate (z) at      (0.0, 0.0, 1.3);
                \coordinate (u1) at (1.2, 0, 0);
                \coordinate (u2) at (0, 3.2, 0);
                \drawvectorIII{z}{above right:$\bfz = \bfy - \mathbf{\hat{y}}$};
                \drawvectorIII{y}{above right:$\bfy$};
                \drawvectorIII{u1}{below left:$\bfu_1$};
                \drawvectorIII{u2}{above right:$\bfu_2$};
                \node[below, vector point, label=below:$\bfzero$] at (o) {};
                \draw[dashed, thin] (y) -- (yhat);
                \draw[dashed, thin] (y) -- (yhat1);
                \draw[dashed, thin] (y) -- (yhat2);
                \draw[dashed, thin] (yhat) -- (yhat1);
                \draw[dashed, thin] (yhat) -- (yhat2);
                \tikzset{
                    vector line/.style={SeaGreen, ultra thick},
                }
                \drawvectorIII{yhat}{below right:$\mathbf{\hat{y}} = \proj_{W} \bfy =
                    \hat{\bfy}_1 + \hat{\bfy}_2$};
                \drawvectorIII{yhat1}{below right:$\mathbf{\hat{y}}_1$};
                \drawvectorIII{yhat2}{below right:$\mathbf{\hat{y}}_2$};
                % Draw the right angle
                \pic [draw,red,thick,angle radius=0.3cm] 
                {right angle = o--yhat--y};
        \end{tikzpicture}
    }%
    \caption{The projection of $\bfy$ to $W$}
\end{figure}
\end{frame}

\subsection{Properties of Orthogonal Projections}

\begin{frame}{Review}
    \begin{block}{Theorem 5 (6.2)}
        Let $\{\bfu_1, \dots, \bfu_p\}$ be an \emph{orthogonal basis} of $W$,
        then for all $\bfy \in W$,
        \begin{equation*}
            \bfy 
            = 
            \frac{\bfy \cdot \bfu_{1}}{\bfu_{1}\cdot \bfu_1} \bfu_1 
            +
            \dots 
            +
            \frac{\bfy \cdot \bfu_{p}}{\bfu_{p}\cdot \bfu_p} \bfu_p
            .
        \end{equation*}
    \end{block}
    \begin{block}{Theorem 8 (6.3)}
    If $\{\bfu_{1}, \dots, \bfu_{p}\}$ is an \emph{orthogonal basis} of $W$,
    then
    \begin{equation*}
        \hat{\bfy} =
        \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1
        +
        \cdots
        +
        \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
        \quad
        \text{and}
        \quad
        \bfz = \bfy - \hat{\bfy}
    \end{equation*}
    where $\hat{\bfy} \in W$ and $z \in W^{\perp}$.

    \cake{} What is $\hat{\bfy}$ and $\bfz$ if $\bfy \in W$?
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Theorem 9 --- The Best Approximation Theorem}

    Let $\hat{\bfy} = \proj_W \bfy$.
    Then for all $\bfv \in W$ with $\bfv \ne \hat{\bfy}$,
    \begin{equation*}
        \norm{\bfy - \bfv}
        \alert{>}
        \norm{\bfy - \hat{\bfy}}
        .
    \end{equation*}

    \only<1>{%
        \hint{} It also follows from this theorem that if $\bfy \in W$, then $\hat{\bfy}
        = \bfy$.
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            \colorbox{white}{%
                \begin{tikzpicture}[
                    xscale=1.7,
                    yscale=1.5,
                    x  = {(-0.7cm,-0.7cm)},
                    y  = {(1.0cm,-0.2cm)},
                    z  = {(0cm,1.0cm)}]


                    % Adding the surface
                    \coordinate (v1) at ( 1.5,  3.5, 0.0);
                    \coordinate (v2) at (-1.5,  3.5, 0.0);
                    \coordinate (v3) at (-1.5, -0.3, 0.0);
                    \coordinate (v4) at ( 1.5, -0.3, 0.0);
                    \filldraw[surface yellow] (v1) -- (v2) -- (v3) -- (v4) node[above right,
                        xshift=0.3cm] {$W$} -- cycle;

                    % Draw the vectors
                    \coordinate (o) at (0, 0, 0);
                    \coordinate (y) at      (1.2, 2.2, 1.6);
                    \coordinate (yhat) at   (1.2, 2.2, 0.0);
                    \coordinate (yhat1) at  (1.2, 0.0, 0.0);
                    \coordinate (yhat2) at  (0.0, 2.2, 0.0);
                    \coordinate (z) at      (0.0, 0.0, 1.6);
                    \coordinate (v) at (0.0, 2.9, 0);
                    \draw[thick] (y) -- (yhat)
                        node[midway, left] {$\norm{\bfy-\hat{\bfy}}$};
                    \draw[thick] (y) -- (v) 
                        node[midway, above right] {$\norm{\bfy-\bfv}$};
                    \draw[thick] (yhat) -- (v) node[midway, below right]
                        {$\norm{\hat{\bfy}-\bfv}$};
                    \drawvectorIII{z}{below left:$\bfz = \bfy - \mathbf{\hat{y}}$};
                    \drawvectorIII{y}{above right:$\bfy$};
                    \node[below, vector point, label=below:$\bfzero$] at (o) {};
                    \drawvectorIII{yhat}{below right:$\mathbf{\hat{y}}$};
                    \drawvectorIII{v}{below right:$\mathbf{v}$};
                    % Draw the right angle
                    \pic [draw,red,thick,angle radius=0.3cm] 
                    {right angle = y--yhat--v};
            \end{tikzpicture}
        }%
        \caption{The orthogonal projection of $\bfy$ onto $W$ is the closest point in $W$ to $\bfy$.}
        \end{figure}
    }%
    \only<3>{%
        Proof: By Pythagorean Theorem, we have
        \begin{equation*}
            \norm{\bfy-\bfv}^2 = \norm{\bfy-\hat{\bfy}}^2 + \norm{\bfv-\hat{\bfy}}^2
        \end{equation*}
        Thus
        \begin{equation*}
            \norm{\bfy-\bfv}^2 > \norm{\bfy-\hat{\bfy}}^2
        \end{equation*}
        unless
        \begin{equation*}
            \norm{\bfv-\hat{\bfy}} = 0
        \end{equation*}
        \cake{} When do we have $\norm{\bfv-\hat{\bfy}} = 0$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 10: Orthonormal Bases}

    If $\{\bfu_{1},\dots, \bfu_{p}\}$ is an \emph{orthonormal basis} of $W$, then
    \begin{equation*}
        \proj_{W} \bfy
        =
        (\bfy \cdot \bfu_{1})
        \bfu_{1}
        +
        (\bfy \cdot \bfu_{2})
        \bfu_{2}
        +
        \cdots
        (\bfy \cdot \bfu_{p})
        \bfu_{p}
        .
    \end{equation*}

    \only<1>{%
        This follows from the following:
        \begin{block}{Theorem 8 (6.3)}
        If $\{\bfu_{1}, \dots, \bfu_{p}\}$ is an \emph{orthogonal basis} of $W$,
        then
        \begin{equation*}
            \hat{\bfy} =
            \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1
            +
            \cdots
            +
            \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
            \quad
            \text{and}
            \quad
            \bfz = \bfy - \hat{\bfy}
        \end{equation*}
        where $\hat{\bfy} \in W$ and $z \in W^{\perp}$.
        \end{block}
    }%
    \only<2->{%
        Thus letting $U = \begin{bmatrix}\bfu_{1} & \cdots & \bfu_{p}
        \end{bmatrix}$, we have
        \begin{equation*}
            \proj_{W} \bfy
            =
            U U^{T} \bfy
            .
        \end{equation*}
    }%
    \only<3>{%
        \cake{} What is $U^{T} U$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Pattern Recognition}
    \only<1>{%
        \think{} How can we check if there is a $\perp$ sign in the $3 \times 3$ grid
        without checking the grid directly?

        Step 1: Turn the pattern into a vector $\bfw \in \dsR^{n}$.
        Let $W = \Span{\bfw}$.

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\textwidth]{6.3-fig-5.jpg}
            \caption{%
            }%
        \end{figure}

        \cake{} There are only two vectors in $W$ consists of only $0$'s and $1$'s. Which
        two?
    }%
    \only<2>{%
        Step 2:
        Find a basis $\{\bfv_1, \dots, \bfv_{n-1}\}$ of $W^{\perp}$.

        If $\bfx \in W^{\perp}$
        then
        \begin{equation*}
            \bfw^{T}
            \bfx
            =
            [0]
        \end{equation*}

        \cake{} So $W^{\perp}$ is the \blankshort{} space of the matrix $\bfw^{T}$.
        
        \cake{} The dimension of $W^{\perp}$ is $n-1$ because there are $n-1$ variables
        \blankshort{} in the system.
    }%
    \only<3>{%
        Step 3: Let $B = 
            \begin{bmatrix}
                \bfv_1^{T} \\ \vdots \\ \bfv_{n-1}^{T}
            \end{bmatrix}$

        \cake{} Why do we have $B \bfu = \bfzero$ if and only if $\bfu \in W =
        \Span{\bfw}$?

        First, if $\bfu = c \bfw$, then
        \begin{equation*}
            B
            \bfu
            =
            \begin{bmatrix}
                \bfv_1^{T} \bfu \\ \vdots \\ \bfv_{n-1}^{T} \bfu
            \end{bmatrix} 
            =
            \begin{bmatrix}
                \bfv_1 \cdot \bfu \\ \vdots \\ \bfv_{n-1} \cdot \bfu
            \end{bmatrix} 
            =
            \begin{bmatrix}
                0 \\ \vdots \\ 0
            \end{bmatrix} 
        \end{equation*}

        If
        \begin{equation*}
            B
            \bfu
            =
            \begin{bmatrix}
                \bfv_1^{T} \bfu \\ \vdots \\ \bfv_{n-1}^{T} \bfu
            \end{bmatrix} 
            =
            \begin{bmatrix}
                \bfv_1 \cdot \bfu \\ \vdots \\ \bfv_{n-1} \cdot \bfu
            \end{bmatrix} 
            =
            \begin{bmatrix}
                0 \\ \vdots \\ 0
            \end{bmatrix} 
        \end{equation*}
        then $\bfu \in (W^{\perp})^{\perp} = W$.
    }%
    \only<4>{%
        Step 4:
        Let $M = B^{T} B$. 
        Let $\bfu$ be a vector consists of only $0$'s and $1$'s.

        Then
        \begin{equation*}
            \bfu^{T} M \bfu 
            =
            \bfu^{T} B^{T} B \bfu 
            =
            (B \bfu)^{T} B \bfu 
            =
            (B \bfu) \cdot (B \bfu)
        \end{equation*}
        Thus
        \begin{equation*}
            \bfu^{T} M \bfu  = 0
            \iff
            B \bfu = \bfzero
            \iff
            \bfu \in \Span{\bfw}
        \end{equation*}

        In this case, $\bfu$ must be $\bfzero$ or $\bfw$.

        Thus, $\bfu = \bfw$ if and only if $\bfu^{T} M \bfu = 0$ and $\bfu \cdot \bfu \ne
        0$.

        \wink{} This is how we check if $\bfu = \bfw$ without inspecting it directly.
    }%
\end{frame}

\section{6.4 The Gram-Schmidt Process}

\begin{frame}[c]
    \frametitle{Example 1}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Let $W = \Span{\bfx_1, \bfx_2}$ where
            \begin{equation*}
                \bfx_1
                =
                \begin{bmatrix}
                    3 \\ 6 \\ 0
                \end{bmatrix}
                ,
                \quad
                \bfx_2
                =
                \begin{bmatrix}
                    1 \\ 2 \\ 2
                \end{bmatrix}
            \end{equation*}

            Find an orthogonal basis of $W$.

            \vspace{3cm}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \colorbox{white}{%
                    \begin{tikzpicture}[
                        scale=0.6,
                        x  = {(-0.6cm,-0.5cm)},
                        y  = {(1.0cm,-0.1cm)},
                        z  = {(0cm,1.3cm)}]

                        % Coordinates
                        \coordinate (o) at (0, 0, 0);
                        \coordinate (x1) at (3, 6, 0);
                        \coordinate (v1) at (x1);
                        \coordinate (x2) at (1.0, 2.0, 2.0);
                        \coordinate (v2) at (0.0, 0.0, 2.0);
                        \coordinate (p) at (1.0, 2.0, 0.0);

                        % Axes
                        \draw[axis style] (o) -- (2,0,0) node[below] {$x_1$};
                        \draw[axis style] (o) -- (0,5,0) node[below] {$x_2$};
                        \draw[axis style] (o) -- (0,0,4.5) node[left] {$x_3$};

                        % Adding the surface
                        \filldraw[surface yellow] (o) -- (v1) -- (3, 6, 4) -- ($2*(v2)$) 
                            node[above right, xshift=0.3cm] {$W$} -- cycle;

                        % Draw the vectors
                        \draw[dashed] (v2) -- (x2);
                        \draw[dashed] (p) -- (x2);
                        \drawvectorIII{v2}{above left:$\bfv_2$};
                        \drawvectorIII{x1}{below left:$\bfv_1 = \bfx_1$};
                        \drawvectorIII{x2}{above right:$\bfx_2$};
                        \node[below, vector point, label=below:$\bfzero$] at (o) {};
                        \drawvectorIII{p}{below left:$\bfp$};
                \end{tikzpicture}
            }%
            \caption{An orthogonal basis of $W$}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Example 2}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \only<1>{%
                Let $W = \Span{\bfx_1, \bfx_2, \bfx_3}$ where
                \begin{equation*}
                    \bfx_1
                    =
                    \begin{bmatrix}
                        1 \\ 1 \\ 1 \\ 1
                    \end{bmatrix}
                    ,
                    \quad
                    \bfx_2
                    =
                    \begin{bmatrix}
                        0 \\ 1 \\ 1 \\ 1
                    \end{bmatrix}
                    ,
                    \quad
                    \bfx_3
                    =
                    \begin{bmatrix}
                        0 \\ 0 \\ 1 \\ 1
                    \end{bmatrix}
                    .
                \end{equation*}
                \think{}
                How to find an orthogonal basis of $W$.
            }%

            \only<2>{%
                Step 1: Let 
                \begin{equation*}
                    \bfv_{1} = \bfx_1
                \end{equation*}
                and
                \begin{equation*}
                    \bfv_{2} = \bfx_{2} - \proj_{\Span{\bfv_{1}}}{\bfx_{2}}
                \end{equation*}

                Then $\{\bfv_1, \bfv_2\}$ is an orthogonal basis of $\Span{\bfx_1, \bfx_2}$.
            }%
            \only<3>{%
                Step 2 (Optional): 
                Scale $\bfv_{2}$ to make computation simpler.

                Let's call it $\bfv_2'$.

                Let $W_{2} = \Span{\bfv_1, \bfv_2'}$.
            }%
            \only<4>{%
                Step 3: Let 
                \begin{equation*}
                    \bfv_{3} = \bfx_{3} - \proj_{W_{2}}{\bfx_{3}}
                \end{equation*}

                Then $\{\bfv_1, \bfv_2', \bfv_{3}\}$ is an orthogonal basis of
                $W$.
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.4-fig-2.jpg}
                \caption{An orthogonal basis of $W$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- The Gram-Schmidt Process}
    Given a basis $\{\bfx_1, \dots, \bfx_p\}$ for a non-zero vector
    subspace $W$ of $\mathbb{R}^n$, define $\bfv_1, \dots, \bfv_p$ by
    \begin{align*}
        \bfv_1 & = \bfx_1, \\
        \bfv_2 & = \bfx_2 - \frac{\bfx_2 \cdot \bfv_1}{\bfv_1 \cdot \bfv_1} \bfv_1, \\
               & \vdots \\
        \bfv_p & = \bfx_p - \sum_{i=1}^{p-1} \frac{\bfx_p \cdot \bfv_i}{\bfv_i \cdot \bfv_i} \bfv_i.
    \end{align*}
    The set of vectors $\{\bfv_1, \dots, \bfv_p\}$ is an orthogonal basis for $W$. 

    In addition,
    \begin{equation*}
        \Span{\bfx_1, \dots, \bfx_k} = \Span{\bfv_1, \dots,
        \bfv_k}, \qquad  (1 \leq k \leq p).
    \end{equation*}
\end{frame}

\subsection{Orthonormal bases}

\begin{frame}
    \frametitle{Example 3 -- Orthonormal bases}

    Construct an orthonormal basis form the orthogonal basis
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix}
            3 \\ 6 \\ 0
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{QR Factorization}

\begin{frame}
    \frametitle{Example 4}
    Let
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                1 & 1 & 0 \\
                1 & 1 & 1 \\
                1 & 1 & 1 \\
            \end{array}
        \right]
    \end{equation*}

    \only<1>{%
        \think{}
        How to find $Q$ and $R$ such that $A = QR$ with
        \begin{itemize}
            \item $Q$ is $4 \times 3$ matrix with \alert{orthonormal columns} , 
            \item $R$ is $3 \times 3$ upper triangular matrix with non-zero columns
                on the diagonal.
        \end{itemize}
    }%
    \only<2>{%
        Answer: The follow matrices satisfy the above conditions:
        \begin{equation*}
            Q = \begin{bmatrix}
                \frac{1}{2} & -3\frac{\sqrt{12}}{2} & 0 \\
                \frac{1}{2} & \frac{\sqrt{12}}{2} & -2\frac{\sqrt{6}}{2} \\
                \frac{1}{2} & \frac{\sqrt{12}}{2} & \frac{\sqrt{6}}{2} \\
                \frac{1}{2} & \frac{\sqrt{12}}{2} & \frac{\sqrt{6}}{2}
            \end{bmatrix}
            ,
            \qquad
            R
            =
            \begin{bmatrix}
                2 & \frac{3}{2} & 1 \\
                0 & \frac{3\sqrt{12}}{2} & \frac{2\sqrt{12}}{2} \\
                0 & 0 & \frac{2\sqrt{6}}{2}
            \end{bmatrix}
        \end{equation*}
        This is called \alert{QR factorization} of $A$. 
    }%
\end{frame}

\begin{frame}
    \frametitle{Theorem 12 --- QR Factorization}

    If $A$ is an $m \times n$ matrix with linearly independent columns,
    then $A$ can be factored as $A = QR$,
    \begin{itemize}
        \item where $Q$ is an $m \times n$ matrix whose columns form an orthonormal basis for $\colspace A$
        \item and $R$ is an $n \times n$ upper triangular invertible matrix with positive entries on its diagonal.
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%    Suppose $A = QR$, where $Q$ is $m \times n$ and $R$ is $n \times n$.
%    Show that if the columns of $A$ are linearly independent,
%    then $R$ must be invertible.
%
%    \bomb{} This is not necessary a QR factorization.
%
%    \hint{} Why does equation $R \bfx = 0$ have only the trivial solution?
%\end{frame}

\end{document}
