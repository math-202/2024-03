---
title: Remote Exam Procedure
subtitle: MATH 202 Linear Algebra
colorlinks: true
urlcolor: Maroon
marings: 1.5in
...

If you have not been officially granted the permission to attend the course 
remotely by your instructor, 
you will not be allowed to take the exams remotely.

Currently only the following students who have been granted permissions to attend the
course remotely ---

* Lexue Song
* Zirui Shen

To take the exams remotely, follow these steps ---

1. 5 minutes before the exam, go to GradeScope, download and print the exam papers.
2. Join the course zoom meeting when the exam starts, with your *camera on*.
3. Start answering the questions.
4. When the exam time ends, stop writing.
5. You have 15 minutes to 
    1. scan your answers (e.g., with Apps such as [Microsoft Lens](https://apps.apple.com/us/app/microsoft-lens-pdf-scanner/id975925059))
    2. convert them into a *single* PDF file in *correct* order
    3. upload the PDF file to GradeScope.
